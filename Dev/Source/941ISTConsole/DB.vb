﻿Imports System.Data.OleDb
Imports System.Transactions
Imports System.Text
Imports System.Configuration
Imports System.Data.OracleClient

''' <summary>
''' Database object
''' </summary>
''' <remarks></remarks>
Public Class DB
    Inherits DBBase

    Public Enum T7Required
        Y
        N
    End Enum

#Region "Helpers"

    Public Shared Function Connection(ByVal connString As String) As OleDbConnection
        Return New OleDbConnection(connString)
    End Function

    Public Shared Function GetData(ByVal connString As String, ByVal sql As String) As DataSet
        Dim ds As New DataSet()
        Try
            Using conn As OleDbConnection = Connection(connString)
                Using da As New OleDbDataAdapter(sql, conn)
                    da.Fill(ds)
                End Using
            End Using
        Catch ex As Exception
            Throw ex
        End Try

        Return ds
    End Function

    Public Shared Function GetDataTable(ByVal connString As String, ByVal sql As String) As DataTable
        Dim dt As DataTable = New DataTable()

        Try
            Using conn As IDbConnection = Connection(connString)
                conn.Open()
                Using cmd As New OracleCommand(sql, conn)
                    Using da As New OracleDataAdapter(cmd)
                        da.Fill(dt)
                    End Using
                End Using
            End Using
        Catch ex As Exception
            Throw ex
        End Try

        Return dt

    End Function

    Public Shared Function GetDataRow(ByVal connString As String, ByVal sql As String) As DataRow
        Dim dt As DataTable
        Dim dr As DataRow = Nothing

        Try
            dt = GetDataTable(connString, sql)

            If dt.Rows.Count > 0 Then
                dr = dt.Rows(0)
            End If
        Catch ex As Exception
            Throw ex
        End Try

        Return dr

    End Function

#End Region

    ''' <summary>
    ''' object to encapsulate all DB interactions for the ASNRecords process
    ''' </summary>
    ''' <remarks></remarks>
    Public Class ASNRecords

        Public Shared Function InvoiceISTOrders() As ResponseBack(Of Object)

            Dim respBack As New ResponseBack(Of Object)()
            Dim msg As String = String.Empty
            Dim ordersInvoiced As String = String.Empty
            Dim sb As New StringBuilder()
            Dim trans As OleDbTransaction

            Try

                'Using transcope As New TransactionScope()

                Using conn As IDbConnection = GetDBConnection()

                    Try
                        conn.Open()
                        trans = conn.BeginTransaction()

                        Try
                            Using cmd As IDbCommand = GetDataCommand()

                                cmd.Connection = conn
                                cmd.Transaction = trans
                                cmd.CommandType = CommandType.Text



                                Dim dtOrdersToInvoice As DataTable = GetISTOrdersToInvoice(cmd)

                                If Not dtOrdersToInvoice Is Nothing AndAlso dtOrdersToInvoice.Rows.Count > 0 Then

                                    sb.AppendFormat("OrdersToInvoice: {0}, Attended: => ", dtOrdersToInvoice.Rows.Count)

                                    'here make a loop
                                    For Each rOrder As DataRow In dtOrdersToInvoice.Rows
                                        InvoiceIST(rOrder, cmd)
                                        sb.AppendFormat("{0},", rOrder("CO_ODNO"))
                                    Next

                                    'notifying the success
                                    respBack.RespCode = ResponseCode.Success

                                    If sb.ToString().Length > 0 Then
                                        ordersInvoiced = Strings.Left(sb.ToString(), sb.Length - 1)
                                    End If

                                Else

                                    'notifying the success
                                    respBack.RespCode = ResponseCode.Success
                                    ordersInvoiced = "No data for IST Orders to Invoice"

                                End If


                                'commiting the transaction
                                'transcope.Complete()

                                trans.Commit()


                            End Using
                        Catch ex As Exception
                            trans.Rollback()
                            Throw ex
                        End Try

                    Catch ex As Exception
                        Throw ex
                    End Try

                End Using

                'End Using


            Catch ex As TransactionAbortedException
                msg = "TransactionAborted Exception DAL_ROC.InvoiceISTOrders()-> " & vbNewLine & "ERROR: " & ex.ToString()
                respBack.RespCode = ResponseCode.SQLError

            Catch ex As Exception
                msg = "App Exception DAL_ROC.InvoiceISTOrders()-> " & vbNewLine & "ERROR: " & ex.ToString()
                respBack.RespCode = ResponseCode.Failure

            Finally

                Dim finalMsg As String = IIf(String.IsNullOrEmpty(msg), ordersInvoiced, String.Format("{0} || {1}", ordersInvoiced, msg))
                respBack.Message = finalMsg

            End Try

            Return respBack

        End Function

        Public Shared Function GetISTOrdersToInvoice(ByRef cmd As IDbCommand) As DataTable
            Dim returnTable As DataTable = Nothing

            Dim sQry As String = "SELECT 'IST' " & _
                                ", T1.CO_ODNO " & _
                                ", T1.REQUEST_AUTH_USER AS EM_NO " & _
                                ", T3.SHIP_TO " & _
                                ", NVL(T2.OPERATOR_ID,'') AS OPERID " & _
                                ", NVL(T1.IST_INV_DT,T3.TIMESTAMP) AS INV_DT " & _
                                "FROM LCBO_IST_HDR T1 " & _
                                "INNER JOIN COHDR T3 ON T1.CO_ODNO = T3.CO_ODNO " & _
                                "LEFT JOIN  DBO.ON_EMP_MAS T2 ON UPPER(TRIM(T1.REQUEST_AUTH_USER)) = UPPER(TRIM(T2.EM_NO)) " & _
                                "WHERE STATUS = 'SUBMITTED' " & _
                                "AND CO_STATUS = 'C' " & _
                                "AND ORDER_CANCELED_Y_OR_N = 'Y' " & _
                                "AND END_OF_ORDER_FL = 'Y' " & _
                                "AND ORDER_TYPE = 'IST'"


            Try

                cmd.CommandText = sQry
                Dim ds As New DataSet()

                Dim adptr As IDataAdapter = GetDataAdapter(cmd)
                adptr.Fill(ds)

                If Not IsNothing(ds) Then

                    If ds.Tables.Count > 0 Then

                        returnTable = ds.Tables(0)

                    End If

                End If

            Catch ex As Exception
                Throw New Exception("DAL_ROC.GetISTOrdersToInvoice()->" & vbNewLine & _
                                   "qry:" & sQry & vbNewLine & _
                                   "ERROR:" & ex.ToString())

            End Try

            Return returnTable

        End Function

        Public Shared Sub InvoiceIST(ByVal drHeader As DataRow, ByRef cmd As IDbCommand)

            Dim sQry As String = String.Empty
            Dim tmpdate As Date
            'write to asn table
            Try
                CreateASNRecord(drHeader, cmd, T7Required.Y)
                tmpdate = DateTime.Parse(drHeader("INV_DT"))

                'update ist flags - inv_dt and status

                sQry = "UPDATE DBO.LCBO_IST_HDR " & _
                       " SET IST_INV_DT = TO_DATE('" & tmpdate.ToString("yyyyMMddHHmmss") & "','YYYYMMDDHH24MISS') " & vbNewLine & _
                       " ,STATUS = 'INVOICED' " & vbNewLine & _
                       " WHERE CO_ODNO = '" & drHeader("CO_ODNO") & "' "

                cmd.CommandText = sQry

                If cmd.ExecuteNonQuery() <= 0 Then
                    Throw New Exception("Error Updating IST STATUS for order: '" & drHeader("CO_ODNO") & "' - No records updated!")
                End If

                'Need to update LCBO_IST_DTL TABLES PICKED QTY, SHIPPED CASES, SHIPPED UNITS
                sQry = " UPDATE DBO.LCBO_IST_DTL IST                                       " & vbNewLine & _
                        " SET (IST.PICKED_QTY, IST.SHIPPED_CASES, IST.SHIPPED_UNITS) =      " & vbNewLine & _
                        " (SELECT DTL.PICKED_QTY, DTL.PICKED_CASES, DTL.PICKED_UNITS        " & vbNewLine & _
                        "  FROM DBO.CODTL DTL                                               " & vbNewLine & _
                        "  WHERE IST.CO_ODNO = DTL.CO_ODNO AND IST.COD_LINE = DTL.COD_LINE  " & vbNewLine & _
                        "  AND IST.DELETED_FL = 'N'                                         " & vbNewLine & _
                        "  AND DTL.PICKED_QTY > 0                                           " & vbNewLine & _
                        "  UNION                                                            " & vbNewLine & _
                        "  SELECT 0, 0, 0                                                   " & vbNewLine & _
                        "  FROM DBO.CODTL DTL                                               " & vbNewLine & _
                        "  WHERE IST.CO_ODNO = DTL.CO_ODNO AND IST.COD_LINE = DTL.COD_LINE  " & vbNewLine & _
                        "  AND DTL.PICKED_QTY <= 0                                          " & vbNewLine & _
                        "  )                                                                " & vbNewLine & _
                        " WHERE IST.CO_ODNO = '" & drHeader("CO_ODNO") & "' "

                cmd.CommandText = sQry

                If cmd.ExecuteNonQuery() <= 0 Then
                    Throw New Exception("Error Updating Picked Qty for order: '" & drHeader("CO_ODNO") & "' - No records updated!")
                End If

                'Need to calculate total full cases and total part cases
                Dim full_cases As Integer = 0
                Dim part_cases As Integer = 0
                Dim divisor As Integer = 12
                Dim accumulator As Integer = 0
                Dim dr As DataRow = Nothing
                Dim prodCount As Integer = 0
                Dim unitCount As Integer = 0

                For Each itm As DataRow In GetISTItemsInfo(drHeader("CO_ODNO"), cmd).Rows
                    full_cases = full_cases + itm("PICKED_CASES")
                    'calculate the part cases
                    accumulator = accumulator + (itm("PICKED_UNITS") * itm("PACK_FACTOR"))
                Next

                part_cases = Math.Floor(accumulator / divisor)

                If (accumulator Mod divisor > 0) Then
                    part_cases = part_cases + 1
                End If

                sQry = "UPDATE DBO.LCBO_IST_HDR " & vbNewLine & _
                       " SET TTL_FULL_CASES = " & full_cases & vbNewLine & _
                       " , TTL_PART_CASES = " & part_cases & vbNewLine & _
                       " WHERE CO_ODNO = '" & drHeader("CO_ODNO") & "' "

                cmd.CommandText = sQry

                If cmd.ExecuteNonQuery() <= 0 Then
                    Throw New Exception("Error Updating FULL CASES AND PART CASES for order: '" & drHeader("CO_ODNO") & "' - No records updated!")
                End If

            Catch ex As Exception
                Throw New ISTInvoiceException("DAL_ROC.InvoiceIST(" & drHeader("CO_ODNO") & ")" & vbNewLine & _
                                              "QRY: " & sQry & vbNewLine & _
                                              "MSG: " & ex.ToString())
            End Try

        End Sub

        Public Shared Function CreateASNRecord(ByVal drHeaderInfo As DataRow, ByRef cmd As IDbCommand, Optional ByVal T7_required As T7Required = T7Required.N) As Boolean
            Dim bRetval As Boolean = True
            Dim sDest As String = Right("0000" & drHeaderInfo("SHIP_TO").ToString.Trim, 4)
            Dim sOrderNum As String = Right("00000000" & drHeaderInfo("CO_ODNO"), 8)
            Dim dtmInvDt As DateTime = Convert.ToDateTime(drHeaderInfo("INV_DT"))


            Dim sQry As String = " INSERT INTO DBO.LCBO_ASN_TRAN (  " & vbNewLine & _
                                    " 	CO_ODNO           " & vbNewLine & _
                                    " 	,INV_DT            " & vbNewLine & _
                                    " 	,SEND_TO           " & vbNewLine & _
                                    " 	,ASN_NAME          " & vbNewLine & _
                                    " 	,ASN_CREATE_DT     " & vbNewLine & _
                                    " 	,T7_REQUIRED       " & vbNewLine & _
                                    " 	,T7_CREATE_DT      " & vbNewLine & _
                                    " 	,LINK_CO_ODNO      " & vbNewLine & _
                                    " 	,ASN_STATUS)       " & vbNewLine & _
                                    " VALUES ('" & drHeaderInfo("CO_ODNO") & "' " & vbNewLine & _
                                    " 	, '" & dtmInvDt.ToString("yyyy-MM-dd") & "' " & vbNewLine & _
                                    " 	, '" & sDest & "' " & vbNewLine & _
                                    " 	, ' ' " & vbNewLine & _
                                    " 	, NULL " & vbNewLine & _
                                    " 	, '" & T7_required.ToString() & "' " & vbNewLine & _
                                    " 	, NULL " & vbNewLine & _
                                    " 	, '" & drHeaderInfo("CO_ODNO") & "' " & vbNewLine & _
                                    " 	,0)       "

            Try

                cmd.CommandText = sQry
                cmd.Parameters.Clear()

                If cmd.ExecuteNonQuery() < 0 Then
                    Throw New Exception("Failed to C8 Record.")
                End If
            Catch ex As Exception
                Throw New Exception("DAL_ROC.CreateASNRecord(" & drHeaderInfo("CO_ODNO") & ")->" & vbNewLine & _
                                                "qry:" & sQry & vbNewLine & _
                                                "ERROR:" & ex.ToString())
            End Try
            Return bRetval
        End Function


        Shared Function GetISTItemsInfo(ByVal sOrderNum As String, ByRef cmd As IDbCommand) As DataTable
            Dim returnTable As DataTable = Nothing

            Dim sQry As String = " Select DTL.CO_ODNO   " & vbNewLine & _
                                " ,  DTL.COD_LINE      " & vbNewLine & _
                                " ,  DTL.ITEM          " & vbNewLine & _
                                " ,  DTL.PICKED_QTY    " & vbNewLine & _
                                " ,  DTL.PICKED_CASES  " & vbNewLine & _
                                " ,  DTL.PICKED_UNITS  " & vbNewLine & _
                                " ,  IM.CASE_SZ        " & vbNewLine & _
                                " ,  IM.PACK_FACTOR    " & vbNewLine & _
                                " from DBO.CODTL DTL INNER JOIN DBO.IMMAS IM " & vbNewLine & _
                                " ON IM.ITEM = DTL.ITEM                      " & vbNewLine & _
                                " WHERE CO_ODNO = '" & sOrderNum.Trim.PadRight(16) & "' "


            Try
                cmd.CommandText = sQry
                Dim adptr As IDataAdapter = GetDataAdapter(cmd)

                Dim ds As New DataSet()
                adptr.Fill(ds)

                If Not IsNothing(ds) Then

                    If ds.Tables.Count > 0 Then

                        returnTable = ds.Tables(0)

                    End If

                End If


            Catch ex As Exception
                Throw New Exception("DAL_ROC.GetISTItemsInfo(" & sOrderNum & ")" & vbNewLine & _
                                    "qry:" & sQry & vbNewLine & _
                                    "ERROR:" & ex.ToString())
            End Try

            Return returnTable

        End Function

    End Class

    ''' <summary>
    ''' Object to encapsulate all database interactions for the ASNFiles_out_of_941 process
    ''' </summary>
    ''' <remarks></remarks>
    Public Class ASNFILES_OUT_OF_941

        Public Shared Function getASNList(ByVal co_odno As String) As DataTable
            Dim dt As New DataTable
            Dim sql As StringBuilder = New StringBuilder()

            Try
                sql.Append(" SELECT T1.CO_ODNO ")
                sql.Append(" , TO_CHAR(TO_DATE(T1.INV_DT,'YYYY-MM-DD'),'YYMMDD') AS INV_DT ")
                sql.Append(" , T1.SEND_TO ")
                sql.Append(" , T1.ASN_NAME ")
                sql.Append(" , T1.ASN_CREATE_DT ")
                sql.Append(" , T1.T7_REQUIRED ")
                sql.Append(" , T1.T7_CREATE_DT ")
                sql.Append(" , T1.LINK_CO_ODNO ")
                sql.Append(" , T1.ASN_STATUS ")
                sql.Append(" FROM DBO.LCBO_ASN_TRAN T1 ")
                If String.IsNullOrEmpty(co_odno) Then
                    sql.Append(" WHERE ASN_CREATE_DT IS NULL")
                Else
                    sql.Append(" WHERE TRIM(T1.CO_ODNO) = '" & co_odno.Trim & "' ")
                End If

                dt = GetDataTable(DataLayerConfig.OracleOleDbConnString, sql.ToString())

            Catch ex As Exception
                Throw New Exception("DB.getASNList()" & vbNewLine & _
                                    "qry:" & sql.ToString() & vbNewLine & _
                                    "ERROR:" & ex.Message)
            End Try

            Return dt

        End Function

        Public Shared Function GetASNItems(ByVal co_odno As String) As DataTable
            Dim dt As New DataTable
            Dim sql As StringBuilder = New StringBuilder()

            Try
                sql.Append("SELECT ITEM,")
                sql.Append(" SUM(PICKED_QTY) AS PICKED_QTY ")
                sql.Append(" FROM DBO.CODTL ")
                sql.Append(" WHERE TRIM(CO_ODNO) = '" & co_odno.Trim & "'")
                sql.Append(" GROUP BY ITEM ")
                sql.Append(" HAVING SUM(PICKED_QTY) > 0 ")
                sql.Append(" ORDER BY ITEM ASC ")

                dt = GetDataTable(DataLayerConfig.OracleOleDbConnString, sql.ToString())

            Catch ex As Exception
                Throw New Exception("DB.GetASNItems()" & vbNewLine & _
                                    "qry:" & sql.ToString() & vbNewLine & _
                                    "ERROR:" & ex.Message)
            End Try

            Return dt

        End Function

        Public Shared Function UpdateASNCreationDate(ByVal co_odno As Int64, _
                                                     ByVal ASNName As String) As Boolean
            Dim trans As OracleTransaction
            Dim result As Boolean = False
            Dim sql As StringBuilder = New StringBuilder()

            Using conn As IDbConnection = Connection(DataLayerConfig.OracleOleDbConnString)
                Try
                    conn.Open()
                    trans = conn.BeginTransaction()
                    Try
                        Using cmd As New OracleCommand("SELECT SYSDATE FROM DUAL", conn, trans)
                            sql.Append("UPDATE DBO.LCBO_ASN_TRAN ")
                            sql.Append(" SET ASN_CREATE_DT = SYSDATE ")
                            sql.Append(",ASN_Name = '" & ASNName & "'")
                            sql.Append(" WHERE TRIM(CO_ODNO) = '" & co_odno.ToString() & "'")

                            cmd.CommandText = sql.ToString()

                            If cmd.ExecuteNonQuery() <> 1 Then
                                Throw New ISTException(3001, "There was a problem updating the ASN transaction table - CO_ODNO = '" & co_odno & "'")
                            End If

                            sql.Clear()
                            sql.Append("UPDATE DBO.LCBO_IST_HDR ")
                            sql.Append(" SET ASN_SENT = SYSDATE ")
                            sql.Append(" WHERE TRIM(CO_ODNO) = '" & co_odno.ToString() & "'")

                            cmd.CommandText = sql.ToString()

                            If cmd.ExecuteNonQuery() <> 1 Then
                                Throw New ISTException(3002, "There was a problem updating the ASN header table - CO_ODNO = '" & co_odno & "'")
                            End If

                        End Using

                        trans.Commit()
                        result = True

                    Catch ex As Exception
                        trans.Rollback()
                        Throw ex
                    End Try
                Catch ex As Exception
                    Throw ex
                End Try
            End Using

            Return result

        End Function

    End Class

    ''' <summary>
    ''' Object to encapsulate all database interactions for the T1FILES_INTO_941 process
    ''' </summary>
    ''' <remarks></remarks>
    Public Class T1FILES_INTO_941

        Public Shared Function UpdateIST(ByVal co_odno As String, ByVal status As String) As Boolean
            Dim result As Boolean = False
            Dim sql As StringBuilder = New StringBuilder()
            Dim trans As OleDbTransaction

            Try

                Using conn As IDbConnection = GetDBConnection()

                    conn.Open()

                    trans = conn.BeginTransaction()

                    Try
                        sql.Append("UPDATE DBO.LCBO_IST_HDR ")
                        sql.Append(" SET T1_STATUS_DT = SYSDATE ")
                        sql.Append(",T1_STATUS = '" & status & "'")
                        sql.Append(" WHERE TRIM(CO_ODNO) = '" & System.Convert.ToInt64(co_odno).ToString() & "'")

                        Using cmd As IDbCommand = GetDataCommand(sql.ToString(), conn, trans)

                            If cmd.ExecuteNonQuery() <> 1 Then
                                Throw New ISTException(3001, "There was a problem updating the IST header table - CO_ODNO = '" & co_odno & "'")
                            End If

                        End Using

                        'transcope.Complete()
                        trans.Commit()

                        result = True
                    Catch ex As Exception
                        trans.Rollback()
                        Throw ex
                    End Try

                End Using

            Catch ex As Exception
                Throw New ISTException(3003, "There was a problem updating the IST header table - CO_ODNO = '" & co_odno & "'", ex)

            End Try

            Return result

        End Function

    End Class

    Public Class T1FILES_OUT_OF_941

        Public Shared Function GetCompletedOrders() As DataTable
            Dim dt As DataTable
            Dim sql As StringBuilder = New StringBuilder()

            Try

                'sql.Append("SELECT ")
                'sql.Append("TRIM(H.URL_NO) URL_NO, ")
                'sql.Append("TRIM(H.VD_NO) VD_NO, ")
                'sql.Append("TRIM(H.PO_STATUS) PO_STATUS ")
                'sql.Append("FROM ")
                'sql.Append("DBO.URL_HDR H INNER JOIN ")
                'sql.Append("DBO.LOGTRAN L ON L.TR_URL_NO = H.URL_NO INNER JOIN ")
                'sql.Append("DBO.ON_CUST_MAS C ON H.VD_NO = C.CU_NO ")
                'sql.Append("WHERE ")
                'sql.Append("H.PO_STATUS = 'C' AND ")
                'sql.Append("L.TRAN_TYPE = 'RCPO' AND ")
                'sql.Append("TO_CHAR(L.TR_DATE_TIME, 'YYYYMMDD') = '" & Now.ToString("yyyyMMdd") & "' AND ")
                'sql.Append("C.CU_TYPE = 'IST' ")
                'sql.Append("GROUP BY H.URL_NO, H.VD_NO, H.PO_STATUS")

                sql.Append("SELECT ")
                sql.Append("TRIM(UH.URL_NO) AS URL_NO, ")
                sql.Append("TRIM(VD_NO) AS VD_NO, ")
                sql.Append("PO_STATUS ")
                sql.Append("from ")
                sql.Append("URL_HDR UH INNER JOIN  ")
                sql.Append("URL_DTL UD ON UD.URL_NO = UH.URL_NO  ")
                sql.Append("WHERE ")
                sql.Append("TO_CHAR(TIMESTAMP, 'YYYYMMDD') = '" & Now.ToString("yyyyMMdd") & "' AND  ")
                sql.Append("PO_STATUS = 'C' AND ")
                sql.Append("VD_NO LIKE 'S%' ")
                sql.Append("GROUP BY ")
                sql.Append("UH.URL_NO, ")
                sql.Append("VD_NO, ")
                sql.Append("PO_STATUS ")

                dt = GetDataTable(DataLayerConfig.OracleOleDbConnString, sql.ToString())

            Catch ex As Exception
                Throw New ISTException(3001, "DB.GetCompletedOrders(). There was a problem getting the completed IST Orders. SQL: " & _
                                        Environment.NewLine & sql.ToString() & _
                                        Environment.NewLine & ex.Message)
            End Try

            Return dt

        End Function

    End Class

End Class
