﻿Imports System.Collections.Generic
Imports System.Linq
Imports System.Text

Public Class ResponseBack(Of T)
    Private _RespCode As ResponseCode = ResponseCode.NotSet
    Private _Message As String = String.Empty
    Private _ReturnedObject As T = Nothing

    Public Sub New()

    End Sub

    Public Sub New(respCode As ResponseCode)
        Me._RespCode = respCode
    End Sub

    Public Sub New(respCode As ResponseCode, message As String)
        Me._RespCode = respCode
        Me._Message = message
    End Sub

    Public Sub New(respCode As ResponseCode, message As String, returnedObject As T)
        Me._RespCode = respCode
        Me._Message = message
        Me._ReturnedObject = returnedObject
    End Sub

    Public Property RespCode As ResponseCode
        Get
            Return Me._RespCode
        End Get
        Set(value As ResponseCode)
            Me._RespCode = value
        End Set
    End Property

    Public Property Message As String
        Get
            Return Me._Message
        End Get
        Set(value As String)
            Me._Message = value
        End Set
    End Property

    Public Property ReturnedObject As T
        Get
            Return Me._ReturnedObject
        End Get
        Set(value As T)
            Me._ReturnedObject = value
        End Set
    End Property


    Public Function GetResponseCodeMeaning() As String
        Dim returnValue As String = String.Empty

        Select Case Me.RespCode

            Case ResponseCode.Success
                returnValue = "Operation Successful"

            Case ResponseCode.Failure
                returnValue = "Operation Failure"

            Case ResponseCode.SQLError
                returnValue = "SQL Error"

            Case Else
                returnValue = "ResponseCode Not Set"

        End Select

        Return returnValue

    End Function


    Public Overrides Function ToString() As String

        Dim custMsg As String = IIf(String.IsNullOrEmpty(Me.Message), Me.GetResponseCodeMeaning(), Me.Message)

        Dim returnValue As String = String.Format("{0} : {1}", Me.RespCode.ToString(), custMsg)

        Return returnValue

    End Function

End Class

Public Enum ResponseCode

    Success = 0
    Failure = 1
    SQLError = 2
    NotSet = 99999

End Enum