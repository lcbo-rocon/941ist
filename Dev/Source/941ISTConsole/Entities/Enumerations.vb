﻿

Public Enum LogBuilderType

    NotSet
    ASNRecord
    ASNFile
    T1FilesInto941
    ASNFilesInto941
    T1FilesOutOf941

End Enum

Public Enum RoutineType

    NotSet
    ASNRecord
    ASNFile
    T1FilesInto941
    ASNFilesInto941
    T1FilesOutOf941

End Enum

Public Enum LogFileType

    NotSet
    ErrFile
    LogFile
    ASNFilesOutOf941
    T1FilesInto941
    ASNFilesInto941
    T1FilesOutOf941

End Enum

Public Enum DateTimeSufixType

    JustTime
    JustDate
    AllDateTime

End Enum

Public Enum ManagedKey

    NotSet

End Enum
