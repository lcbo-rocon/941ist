﻿Imports System.Configuration
Imports System.Text
Imports System.IO
Imports System.Linq

Public Class T1FILES_INTO_941

    Private Shared prefix As String = "T1FILES_INTO_941"
    Private setup As SetupEnv
    Private logMsg As StringBuilder

    Public Sub New()

        setup = New SetupEnv()
        logMsg = New StringBuilder()

    End Sub

    ''' <summary>
    ''' Log writer
    ''' </summary>
    ''' <param name="msg">The message to log</param>
    ''' <param name="includeTime">optional parameter to append timestamp to the message</param>
    ''' <remarks></remarks>
    Protected Friend Sub log(ByVal msg As String, _
                             Optional ByVal includeTime As Boolean = False)

        logMsg.AppendFormat("{0}{1}{2}", msg, IIf(includeTime, " - " & Now, ""), Environment.NewLine)
        LogService.WriteLog(msg, Nothing, setup.LogFileName)

    End Sub

    Public Function ProcessT1Files() As String
        Dim files As Dictionary(Of String, FileInfo)
        Dim line As String
        Dim successFiles As Int32 = 0
        Dim co_odno As String
        Dim status As String
        Dim store As String

        LogService.WriteLog("Starting 941ISTConsole." & prefix, LogFileType.LogFile)
        log("*******************************************", False)
        log("Starting 941ISTConsole." & prefix, True)
        log("Env: " & setup.Environment)
        log("Work directory: " & setup.InDir)

        Try

            files = New DirectoryInfo(setup.InDir).GetFiles(setup.FileMask).ToDictionary(Function(k) k.Name, Function(v) v)

            For Each f As KeyValuePair(Of String, FileInfo) In files

                log("Processing file " & f.Key)
                store = "S" & f.Key.Split(".")(1)
                line = ""

                Using sr As StreamReader = File.OpenText(f.Value.FullName)
                    line = sr.ReadLine()
                End Using

                If ValidateFile(f.Key, line) Then
                    Try
                        co_odno = line.Split(",")(0)
                        status = line.Split(",")(1)
                        If DB.T1FILES_INTO_941.UpdateIST(co_odno, status) Then
                            File.Move(f.Value.FullName, setup.BakDir & "\" & f.Value.Name)

                            If status.ToUpper() = "C" Then
                                'send an email if the IST is completed
                                Try
                                    ISTConsoleNotification.T1FILES_INTO_941_Notification(co_odno, store)
                                Catch ex As Exception
                                    'eat any email sending error
                                    log("T1FILES_INTO_941 - There was a problem sending the email." & vbNewLine & ex.Message)
                                End Try
                            End If

                            log("Processing file " & f.Key & " complete")
                            successFiles += 1

                        Else
                            File.Move(f.Value.FullName, setup.BakDir & "\" & f.Value.Name & ".err")
                            log("Processing file " & f.Key & " - database update failed.")

                        End If
                    Catch ex As ISTException
                        File.Move(f.Value.FullName, setup.BakDir & "\" & f.Value.Name & ".err")
                        log("ERROR: " & ex.ErrorCode & " - " & ex.Message & IIf(Not ex.InnerException Is Nothing, " - " & ex.InnerException.Message, ""), False)

                    End Try
                Else
                    File.Move(f.Value.FullName, setup.BakDir & "\" & f.Value.Name & ".err")
                    log("Processing file " & f.Key & " - validation failed - File Name: '" & f.Key & "', File Contents: '" & line & "'")

                End If

            Next
        Catch ex As ISTException
            log("ERROR: " & ex.ErrorCode & " - " & ex.Message & IIf(Not ex.InnerException Is Nothing, " - " & ex.InnerException.Message, ""), False)

        Catch ex As Exception
            log("There was an error: " & ex.Message, False)

        Finally
            LogService.WriteLog("Finished 941ISTConsole." & prefix & ".", LogFileType.LogFile)
            log("Finished 941ISTConsole." & prefix & ", processed: " & successFiles.ToString() & " of " & IIf(Not files Is Nothing, files.Keys.Count.ToString(), 0) & " T1 files.", True)

        End Try

        Return logMsg.ToString()

    End Function

    Private Function ValidateFile(ByVal fileName As String, line As String) As Boolean
        Dim ASNNumFromFile As String
        Dim ASNNumFromLine As String
        Dim status As String

        If String.IsNullOrEmpty(line) Then
            Return False
        End If

        ASNNumFromFile = fileName.Split(".")(3)
        ASNNumFromLine = line.Split(",")(0)
        status = line.Split(",")(1)

        If ASNNumFromFile <> ASNNumFromLine Then
            Return False
        ElseIf setup.ValidStatus.Contains(status) = False Then
            Return False
        End If

        Return True

    End Function

    ''' <summary>
    ''' object to hold the variables used by the ASN out process
    ''' </summary>
    ''' <remarks></remarks>
    Private Class SetupEnv

        Public Environment As String
        Public ToStore As String
        Public InDir As String
        Public BakDir As String
        Public LogDir As String
        Public LogFileName As String
        Public FileMask As String
        Public ValidStatus As String

        Public Sub New()

            Environment = ConfigurationManager.AppSettings.Get("ENV")
            ToStore = ConfigurationManager.AppSettings.Get("STORE")
            InDir = ConfigurationManager.AppSettings.Get(prefix & "_INDIR")
            BakDir = ConfigurationManager.AppSettings.Get(prefix & "_BAKDIR")
            LogDir = ConfigurationManager.AppSettings.Get("LogsDir")
            FileMask = ConfigurationManager.AppSettings.Get(prefix & "_FILE_MASK")
            ValidStatus = ConfigurationManager.AppSettings.Get(prefix & "_VALID_STATUS")
            LogFileName = Config.LogsDir & prefix & "_" & LogService.GetDateTimeSufix(DateTimeSufixType.JustDate) & "." & Config.FileExtForLogs

            'make sure we have 4 digits leftpadded with zero
            ToStore = Utils.Get4DigitStoreNumber(ToStore)

        End Sub

    End Class

End Class
