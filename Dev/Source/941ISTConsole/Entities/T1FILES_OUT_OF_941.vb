﻿Imports System.Configuration
Imports System.Text
Imports System.IO

Public Class T1FILES_OUT_OF_941

    Private Shared prefix As String = "T1FILES_OUT_OF_941"
    Private setup As SetupEnv
    Private logMsg As StringBuilder

    Public Structure T1Status
        Public Shared Arrived As String = "A"
        Public Shared Complete As String = "C"
        Public Shared Void As String = "V"
        Public Shared Failed As String = "F"
    End Structure

    Public Sub New()

        setup = New SetupEnv()
        logMsg = New StringBuilder()

    End Sub

    ''' <summary>
    ''' Log writer
    ''' </summary>
    ''' <param name="msg">The message to log</param>
    ''' <param name="includeTime">optional parameter to append timestamp to the message</param>
    ''' <remarks></remarks>
    Protected Friend Sub log(ByVal msg As String, _
                             Optional ByVal includeTime As Boolean = False)

        logMsg.AppendFormat("{0}{1}{2}", msg, IIf(includeTime, " - " & Now, ""), Environment.NewLine)
        LogService.WriteLog(msg, Nothing, setup.LogFileName)

    End Sub

    Public Function CreateCompleteT1Files() As String
        Dim dt As DataTable

        Try
            dt = DB.T1FILES_OUT_OF_941.GetCompletedOrders()

            For Each order As DataRow In dt.Rows
                Try
                    CreateT1File(order("VD_NO").ToString().Replace("S", ""), order("URL_NO").ToString().Replace(order("VD_NO"), ""), T1Status.Complete)
                Catch ex As Exception
                    'eat the error so we can continue on in the loop
                End Try
            Next

        Catch ex As ISTException
            log("Error: " & ex.ErrorCode & " - There was a problem writing the T1 file(s) - " & ex.Message)
        Catch ex As Exception
            log("Error: There was a problem writing the T1 file(s) - " & ex.Message)
        End Try

        Return logMsg.ToString()

    End Function

    Public Function CreateT1File(ByVal toStore As String, _
                                 ByVal co_odno As String, _
                                 ByVal status As String) As Boolean
        Dim fileName As StringBuilder = New StringBuilder()
        Dim testPath As String

        Try
            fileName.Append(Utils.Get4DigitStoreNumber(toStore))
            fileName.Append(".")
            fileName.Append(setup.FromStore)
            fileName.Append(".")
            fileName.Append("t1")
            fileName.Append(".")
            fileName.Append(co_odno.PadLeft(8, "0"))
            fileName.Append(".")
            fileName.Append(Now.ToString("yyMMddhhmm"))
            fileName.Append(".")
            fileName.Append("sts")

            testPath = fileName.ToString().Substring(0, 22) & "*.sts"
            'check the T1 hasn't already been extracted, if not, then write it
            If T1FileAlreadySent(testPath, status) = False Then 'Directory.GetFiles(setup.BakDir, testPath).Length = 0 Then
                Using T1out As StreamWriter = File.CreateText(setup.WrkDir & "\" & fileName.ToString())
                    T1out.WriteLine(co_odno.PadLeft(8, "0") & "," & status)
                End Using

                File.Copy(setup.WrkDir & "\" & fileName.ToString(), setup.OutDir & "\" & fileName.ToString())
                File.Move(setup.WrkDir & "\" & fileName.ToString(), setup.BakDir & "\" & fileName.ToString())

                log("T1 created: " & fileName.ToString())
            End If

        Catch ex As Exception
            log("There was a problem writing the T1 file for order '" & co_odno & "'." & Environment.NewLine & ex.Message)
            Throw New ISTException(10, "There was a problem writing the T1 file '" & fileName.ToString() & "'.", ex)
        End Try

        Return True

    End Function

    Public Function T1FileAlreadySent(ByVal testPath As String, ByVal status As String) As Boolean
        Dim files() As String
        Dim line As String
        Dim sent As Boolean = False

        files = Directory.GetFiles(setup.BakDir, testPath)
        If files.Length = 0 Then
            Return False
        Else
            For Each fileName As String In files
                Using sr As StreamReader = File.OpenText(fileName)
                    line = sr.ReadLine()
                End Using
                If line.Length > 0 AndAlso line.Split(",")(1) = status Then
                    sent = True
                    Exit For
                End If
            Next
        End If

        Return sent

    End Function

    ''' <summary>
    ''' object to hold the variables used by the ASN out process
    ''' </summary>
    ''' <remarks></remarks>
    Private Class SetupEnv

        Public Environment As String
        Public FromStore As String
        Public WrkDir As String
        Public BakDir As String
        Public OutDir As String
        Public LogDir As String
        Public LogFileName As String

        Public Sub New()

            Environment = ConfigurationManager.AppSettings.Get("ENV")
            FromStore = ConfigurationManager.AppSettings.Get("STORE")
            WrkDir = ConfigurationManager.AppSettings.Get(prefix & "_WRKDIR")
            BakDir = ConfigurationManager.AppSettings.Get(prefix & "_BAKDIR")
            OutDir = ConfigurationManager.AppSettings.Get(prefix & "_OUTDIR")
            LogDir = ConfigurationManager.AppSettings.Get("LogsDir")
            LogFileName = Config.LogsDir & prefix & "_" & LogService.GetDateTimeSufix(DateTimeSufixType.JustDate) & "." & Config.FileExtForLogs

            'make sure we have 4 digits leftpadded with zero
            FromStore = Utils.Get4DigitStoreNumber(FromStore)

        End Sub

    End Class

End Class
