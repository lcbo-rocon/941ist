﻿Public Class ASNRecords

    Public Shared Function CreateASNRecords() As String

        Dim logMsg As String = String.Empty

        Try

            'Invoicing all IST Orders
            Dim respBack As ResponseBack(Of Object) = DALProxy.CreateASNRecords()
            logMsg += respBack.ToString()

            Select Case respBack.RespCode

                Case ResponseCode.Success
                    LogService.WriteLog(String.Format("CreateASNRecords {0}.", logMsg), LogFileType.LogFile)

                Case ResponseCode.NotSet
                    LogService.WriteLog(String.Format("CreateASNRecords {0}!!!.", logMsg), LogFileType.LogFile)

                Case ResponseCode.Failure
                    LogService.WriteLog(String.Format("CreateASNRecords {0}!!!.", logMsg), LogFileType.ErrFile)

                Case ResponseCode.SQLError
                    LogService.WriteLog(String.Format("CreateASNRecords {0}!!!.", logMsg), LogFileType.ErrFile)


            End Select


        Catch ex As Exception
            logMsg += "ISTIntegratedConsoles.CreateASNRecords()->" & vbNewLine & _
                               "ERROR:" & ex.ToString()

            LogService.WriteLog(logMsg, LogFileType.ErrFile)


        End Try

        Return logMsg

    End Function


End Class
