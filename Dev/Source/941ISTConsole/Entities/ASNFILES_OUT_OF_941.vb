﻿Imports System.Configuration
Imports System.IO
Imports System.Text

''' <summary>
''' Object to extract from the database and write the ASN files
''' </summary>
''' <remarks></remarks>
Public Class ASNFILES_OUT_OF_941

    Private Shared prefix As String = "ASNFILES_OUT_OF_941"
    Private setup As SetupEnv
    Private logMsg As StringBuilder

    Public Sub New()

        setup = New SetupEnv()
        logMsg = New StringBuilder()

    End Sub

    ''' <summary>
    ''' Log writer
    ''' </summary>
    ''' <param name="msg">The message to log</param>
    ''' <param name="includeTime">optional parameter to append timestamp to the message</param>
    ''' <remarks></remarks>
    Protected Friend Sub log(ByVal msg As String, _
                             Optional ByVal includeTime As Boolean = False)

        logMsg.AppendFormat("{0}{1}{2}", msg, IIf(includeTime, " - " & Now, ""), Environment.NewLine)
        LogService.WriteLog(msg, Nothing, setup.LogFileName)

    End Sub

    ''' <summary>
    ''' Main method to extract the ASN files
    ''' </summary>
    ''' <param name="co_odno">optional order number to extract. If not specified, will extract all ASN files</param>
    ''' <returns>the resultant message</returns>
    ''' <remarks></remarks>
    Public Function CreateASNFiles(ByVal co_odno As String) As String
        Dim dt As DataTable
        Dim asn As ASN
        Dim numFiles As Int32 = 0
        Dim successfulWrites As Int32 = 0

        LogService.WriteLog("Starting 941ISTConsole." & prefix, LogFileType.LogFile)
        log("*******************************************", False)
        log("Starting 941ISTConsole." & prefix, True)
        log("Env: " & setup.Environment)
        log("Work directory: " & setup.WrkDir)
        log("Override Parameter: CO_ODNO = '" & co_odno & "'")

        Try
            'get the ASN header info from the database
            dt = DB.ASNFILES_OUT_OF_941.getASNList(co_odno)
            numFiles = dt.Rows.Count

            For Each ASNRow In dt.Rows
                Try
                    log("Processing started - " & ASNRow("CO_ODNO"), True)
                    asn = New ASN(setup)

                    If asn.GetData(ASNRow) = False Then 'retrieve and create the ASN info
                        Throw New ISTException(1000, "There was a problem retrieving the ASN file information.")

                    ElseIf asn.WriteFile() = False Then 'write the ASN flie
                        Throw New ISTException(2000, "There was a problem writing the ASN file information.")

                    End If

                    log("Processing finished - " & ASNRow("CO_ODNO"), True)
                    successfulWrites += 1

                Catch ex As ISTException
                    Throw ex

                Catch ex As Exception
                    Throw New ISTException(1, "There was a problem creating the ASN files.", ex)

                End Try
            Next
        Catch ex As ISTException
            log("ERROR: " & ex.ErrorCode & " - " & ex.Message & IIf(Not ex.InnerException Is Nothing, " - " & ex.InnerException.Message, ""), False)

        Catch ex As Exception
            log("There was an error: " & ex.Message, False)

        Finally
            LogService.WriteLog("Finished 941ISTConsole." & prefix & ".", LogFileType.LogFile)
            log("Finished 941ISTConsole." & prefix & ", processed: " & successfulWrites.ToString() & " of " & numFiles.ToString() & " ASN records.", True)

        End Try

        Return logMsg.ToString()

    End Function

    ''' <summary>
    ''' object to hold the variables used by the ASN out process
    ''' </summary>
    ''' <remarks></remarks>
    Private Class SetupEnv

        Public Environment As String
        Public FromStore As String
        Public WrkDir As String
        Public OutDir As String
        Public BakDir As String
        Public LogDir As String
        Public LogFileName As String

        Public Sub New()

            Environment = ConfigurationManager.AppSettings.Get("ENV")
            FromStore = ConfigurationManager.AppSettings.Get(prefix & "_FROMSTORE")
            WrkDir = ConfigurationManager.AppSettings.Get(prefix & "_WRKDIR")
            OutDir = ConfigurationManager.AppSettings.Get(prefix & "_OUTDIR")
            BakDir = ConfigurationManager.AppSettings.Get(prefix & "_BAKDIR")
            LogDir = ConfigurationManager.AppSettings.Get("LogsDir")
            LogFileName = Config.LogsDir & prefix & "_" & LogService.GetDateTimeSufix(DateTimeSufixType.JustDate) & "." & Config.FileExtForLogs

            'make sure we have 4 digits leftpadded with zero
            FromStore = Utils.Get4DigitStoreNumber(FromStore)

        End Sub

    End Class

    ''' <summary>
    ''' object to represent an entire ASN
    ''' </summary>
    ''' <remarks></remarks>
    Private Class ASN

        Private _setup As SetupEnv
        Private _co_odno As String
        Private _fileHeader As FILE_Header
        Private _fileTrailer As FILE_Trailer
        Private _header As FILE_Record0
        Private _items As SortedList(Of Int32, FILE_Record1)

        Public Sub New(ByVal setup As SetupEnv)

            _setup = setup
            _items = New SortedList(Of Int32, FILE_Record1)

        End Sub

        ''' <summary>
        ''' method to retrieve the data from the database and create the ASN file information
        ''' </summary>
        ''' <param name="ASNRow"></param>
        ''' <returns>true if successful, false if failed</returns>
        ''' <remarks></remarks>
        Protected Friend Function GetData(ByVal ASNRow As DataRow) As Boolean
            Dim items As DataTable
            Dim rec1 As FILE_Record1

            Try
                'generate the file header record
                _fileHeader = New FILE_Header()
                _fileHeader.From_Store = Convert.ToInt16(_setup.FromStore)
                _fileHeader.To_Store = Convert.ToInt16(ASNRow("SEND_TO"))
                _fileHeader.ISTDate = ASNRow("INV_DT")

                'generate the order header recrod
                _header = New FILE_Record0()
                _header.order_number = Right("00000000" & ASNRow("CO_ODNO").ToString.Trim, 8)
                _header.From_Store = _fileHeader.From_Store
                _header.IST_Date = _fileHeader.ISTDate

                'generate the order item record(s)
                items = DB.ASNFILES_OUT_OF_941.GetASNItems(ASNRow("CO_ODNO"))
                For Each item As DataRow In items.Rows
                    If item("PICKED_QTY") <= 0 Then
                        'it should never get here based on the SQL, but just in case, we handle this
                        'becuase it will blow up the downstream systems
                        Throw New ISTException(1005, "There was a zero PICK_QTY item, which is not allowed.")

                    Else
                        rec1 = New FILE_Record1()
                        rec1.item_number = item("ITEM").ToString
                        rec1.quantity = item("PICKED_QTY")

                        _items.Add(_items.Count, rec1)

                    End If
                Next

                'generate the file trailer record
                _fileTrailer = New FILE_Trailer()
                _fileTrailer.total = _items.Count + 1 'add 1 to account for the header record

            Catch ex As ISTException
                Throw ex

            Catch ex As Exception
                Throw New ISTException(1009, "There was a problem getting the information from the database.", ex)

            End Try

            Return True

        End Function

        ''' <summary>
        ''' method to write the ASN files
        ''' </summary>
        ''' <returns>true if successful, false if failed</returns>
        ''' <remarks></remarks>
        Protected Friend Function WriteFile() As Boolean
            Dim ASNFileName As String

            Try
                ASNFileName = Right("0000" & _fileHeader.To_Store, 4) & "." & _
                              Right("0000" & _fileHeader.From_Store, 4) & "." & _
                              "s4" & "." & _
                              Right("00000000" & _header.order_number, 8) & "." & _
                              Now.ToString("yyMMddhhmm") & "." & _
                              "sts"

                If DB.ASNFILES_OUT_OF_941.UpdateASNCreationDate(System.Convert.ToInt64(_header.order_number), ASNFileName) Then
                    Try
                        Using ASNFile As StreamWriter = File.CreateText(_setup.WrkDir & "\" & ASNFileName)

                            ASNFile.WriteLine(_fileHeader.ToString())
                            ASNFile.WriteLine(_header.ToString())
                            For Each item As FILE_Record1 In _items.Values
                                ASNFile.WriteLine(item.ToString())
                            Next
                            ASNFile.WriteLine(_fileTrailer.ToString())

                        End Using
                    Catch ex As Exception
                        Throw New ISTException(2001, "There was a problem writing the ASN file.")

                    End Try
                    Try
                        'put the files into the proper folders
                        File.Copy(_setup.WrkDir & "\" & ASNFileName, _setup.BakDir & "\" & ASNFileName)
                        File.Move(_setup.WrkDir & "\" & ASNFileName, _setup.OutDir & "\" & ASNFileName)

                    Catch ex As Exception
                        Throw New ISTException(2002, "There was a problem copying the ASN files to the bakdir and outdir.")

                    End Try
                Else
                    Throw New ISTException(2003, "There was a problem updating the ASN creation date.")

                End If
            Catch ex As ISTException
                Throw ex

            Catch ex As Exception
                Throw New ISTException(2004, "There was a problem during the ASN WriteFile procedure.", ex)

            End Try

            Return True

        End Function

    End Class

    ''' <summary>
    ''' Object to represent the Header row in the file
    ''' </summary>
    ''' <remarks></remarks>
    Private Class FILE_Header

        Private sHeader As String
        Public ReadOnly Property Header() As String
            Get
                Return sHeader
            End Get
        End Property

        Private sI7 As String
        Public ReadOnly Property I7() As String
            Get
                Return sI7
            End Get
        End Property

        Private sISTDate_YYMMDD As String
        Public Property ISTDate() As String
            Get
                Return sISTDate_YYMMDD
            End Get
            Set(ByVal value As String)
                sISTDate_YYMMDD = value
            End Set
        End Property

        Private sFiller1 As String
        Public ReadOnly Property Filler1() As String
            Get
                Return sFiller1
            End Get
        End Property

        Private sTo_Store As String
        Public Property To_Store() As String
            Get
                Return sTo_Store
            End Get
            Set(ByVal value As String)
                sTo_Store = value
            End Set
        End Property


        Private sFiller2 As String
        Public ReadOnly Property Filler2() As String
            Get
                Return sFiller2
            End Get
        End Property

        Private sFrom_Store As String
        Public Property From_Store() As String
            Get
                Return sFrom_Store
            End Get
            Set(ByVal value As String)
                sFrom_Store = value
            End Set
        End Property

        Private sFiller3 As String
        Public ReadOnly Property Filler3() As String
            Get
                Return sFiller3
            End Get
        End Property

        Private sLast As String
        Public ReadOnly Property Last() As String
            Get
                Return sLast
            End Get
        End Property

        Private sInlist As String
        Public ReadOnly Property Inlist() As String
            Get
                Return sInlist
            End Get
        End Property

        Sub New()
            sHeader = Chr(34) & "#HEADER#" & Chr(34)
            sI7 = Chr(34) & "I7" & Chr(34)
            sFiller1 = Chr(34) & "0" & Chr(34)
            sFiller2 = Chr(34) & Chr(34)
            sFiller3 = Chr(34) & Chr(34)
            sLast = Chr(34) & "##LAST##" & Chr(34)
            sInlist = Chr(34) & "#INLIST#" & Chr(34)
        End Sub

        Public Overrides Function ToString() As String
            Dim str As String = ""
            str = Header & "," & _
                I7 & "," & _
                ISTDate & "," & _
                Filler1 & "," & _
                Chr(34) & From_Store & Chr(34) & "," & _
                Filler2 & "," & _
                Chr(34) & To_Store & Chr(34) & "," & _
                Filler3 & "," & _
                Last & "," & _
                Inlist

            Return str
        End Function
    End Class

    ''' <summary>
    ''' Object to represent the Trailer row in the file
    ''' </summary>
    ''' <remarks></remarks>
    Private Class FILE_Trailer

        Private sTrailer As String
        Public ReadOnly Property trailer() As String
            Get
                Return sTrailer
            End Get
        End Property

        Private sFiller As String
        Public ReadOnly Property Filler1() As String
            Get
                Return sFiller
            End Get
        End Property

        ''' <summary>
        ''' Total lines between header and trailer
        ''' </summary>
        ''' <remarks></remarks>
        Private iTotal As Integer
        Public Property total() As Integer
            Get
                Return iTotal
            End Get
            Set(ByVal value As Integer)
                iTotal = value
            End Set
        End Property


        Private iFiller As Integer
        Public ReadOnly Property Filler2() As Integer
            Get
                Return iFiller
            End Get
        End Property

        Sub New()
            sTrailer = Chr(34) & "#TRAILER#" & Chr(34)
            sFiller = Chr(34) & Chr(34)
            iFiller = 0
        End Sub

        Public Overrides Function ToString() As String
            Dim str As String = ""
            str = trailer & "," & _
            Filler1 & "," & _
            total & "," & _
            Filler2
            Return str
        End Function
    End Class

    ''' <summary>
    ''' Object to represent the ASN header information (i.e. record zero)
    ''' </summary>
    ''' <remarks></remarks>
    Private Class FILE_Record0

        Private sRecordType As String
        Public ReadOnly Property record_type() As String
            Get
                Return sRecordType
            End Get
        End Property


        Private sISTOrderNumber As String
        Public Property order_number() As String
            Get
                Return sISTOrderNumber
            End Get
            Set(ByVal value As String)
                sISTOrderNumber = value
            End Set
        End Property


        Private sFrom_Store As String
        Public Property From_Store() As String
            Get
                Return sFrom_Store
            End Get
            Set(ByVal value As String)
                sFrom_Store = value
            End Set
        End Property


        Private sIST_Date_YYMMDD As String
        Public Property IST_Date() As String
            Get
                Return sIST_Date_YYMMDD
            End Get
            Set(ByVal value As String)
                sIST_Date_YYMMDD = value
            End Set
        End Property

        Private sFiller As String
        Public Property Filler() As String
            Get
                Return sFiller
            End Get
            Set(ByVal value As String)
                sFiller = value
            End Set
        End Property

        Sub New()
            sRecordType = "0"
            sFiller = Chr(34) & Chr(34)
        End Sub

        Public Overrides Function ToString() As String
            Dim str As String = ""
            str = record_type & "," & _
            Chr(34) & order_number & Chr(34) & "," & _
            Chr(34) & From_Store & Chr(34) & "," & _
            IST_Date & "," & _
            sFiller
            Return str
        End Function

    End Class

    ''' <summary>
    ''' Object to represent the ASN item information (i.e. record one)
    ''' </summary>
    ''' <remarks></remarks>
    Private Class FILE_Record1

        Private sRecordType As String
        Public ReadOnly Property record_type() As String
            Get
                Return sRecordType
            End Get
        End Property


        Private iItem_Number As Integer
        Public Property item_number() As Integer
            Get
                Return iItem_Number
            End Get
            Set(ByVal value As Integer)
                iItem_Number = value
            End Set
        End Property

        ''' <summary>
        ''' Flag to indicate quantity is in bottle or cases 
        ''' 0 if bottle, 1 if case
        ''' </summary>
        ''' <remarks>Default 0</remarks>
        Private iBottle_Case_Flag As Integer
        Public Property bottle_case_flag() As Integer
            Get
                Return iBottle_Case_Flag
            End Get
            Set(ByVal value As Integer)
                iBottle_Case_Flag = value
            End Set
        End Property


        Private iQuantity As Integer
        Public Property quantity() As Integer
            Get
                Return iQuantity
            End Get
            Set(ByVal value As Integer)
                iQuantity = value
            End Set
        End Property

        Sub New()
            sRecordType = "1"
            bottle_case_flag = 0
        End Sub

        Public Overrides Function ToString() As String
            Dim str As String = ""

            str = record_type & "," & _
            item_number & "," & _
            bottle_case_flag & "," & _
            quantity

            Return str
        End Function

    End Class

End Class

