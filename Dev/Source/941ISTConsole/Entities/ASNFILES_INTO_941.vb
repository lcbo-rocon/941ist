﻿Imports System.Configuration
Imports System.IO
Imports System.Text


Public Class ASNFILES_INTO_941

    Private Shared prefix As String = "ASNFILES_INTO_941"
    Private setup As SetupEnv
    Private logMsg As StringBuilder

    Public Sub New()

        setup = New SetupEnv()
        logMsg = New StringBuilder()

    End Sub

    ''' <summary>
    ''' Log writer
    ''' </summary>
    ''' <param name="msg">The message to log</param>
    ''' <param name="includeTime">optional parameter to append timestamp to the message</param>
    ''' <remarks></remarks>
    Protected Friend Sub log(ByVal msg As String, _
                             Optional ByVal includeTime As Boolean = False)

        logMsg.AppendFormat("{0}{1}{2}", msg, IIf(includeTime, " - " & Now, ""), Environment.NewLine)
        LogService.WriteLog(msg, Nothing, setup.LogFileName)

    End Sub

    Public Function ProcessASNFiles() As String

        Dim files As Dictionary(Of String, FileInfo)
        Dim lines As List(Of String)
        Dim line As String = ""
        Dim successFiles As Int32 = 0
        Dim ws As WS_ASN_INTO_941.ApplyISTToPO
        Dim resp As String
        Dim respObj As RSGBase.RSGResponse
        Dim T1File As T1FILES_OUT_OF_941
        Dim co_odno As String
        Dim store As String

        LogService.WriteLog("Starting 941ISTConsole." & prefix, LogFileType.LogFile)
        log("*******************************************", False)
        log("Starting 941ISTConsole." & prefix, True)
        log("Env: " & setup.Environment)
        log("Work directory: " & setup.InDir)

        Try

            files = New DirectoryInfo(setup.InDir).GetFiles(setup.FileMask).ToDictionary(Function(k) k.Name, Function(v) v)

            ws = New WS_ASN_INTO_941.ApplyISTToPO()
            ws.UseDefaultCredentials = True

            For Each f As KeyValuePair(Of String, FileInfo) In files

                log("Processing file " & f.Key)
                store = f.Key.Split(".")(1)
                co_odno = "S" & store.PadLeft(4, "0") & Convert.ToInt64(f.Key.Split(".")(3)).ToString().PadLeft(7, "0")

                lines = New List(Of String)()

                Using sr As New StreamReader(f.Value.FullName)
                    While (InlineAssignHelper(line, sr.ReadLine())) IsNot Nothing
                        lines.Add(line)
                    End While
                End Using

                Try
                    resp = ws.Apply(f.Value.Name, lines.ToArray())
                    respObj = New RSGBase.RSGResponse(resp)

                    If respObj.IsSuccessful Then
                        File.Move(f.Value.FullName, setup.BakDir & "\" & f.Value.Name)

                        T1File = New T1FILES_OUT_OF_941()
                        T1File.CreateT1File(f.Value.FullName.Split(".")(1), f.Value.FullName.Split(".")(3), T1FILES_OUT_OF_941.T1Status.Arrived)
                        Try
                            'send an email when an IST comes in
                            ISTConsoleNotification.ASNFILES_INTO_941_Notification(co_odno, store)
                        Catch ex As Exception
                            'eat any email sending error
                            log("ASNFILES_INTO_941 - There was a problem sending the email." & vbNewLine & ex.Message)
                        End Try
                        log("Processing file " & f.Key & " complete")
                        successFiles += 1
                    Else
                        Try
                            ISTConsoleNotification.AlertErrors("ASNFILES_INTO_941", "ProcessASNFiles", co_odno, store, f.Key, respObj.ResponseCode, respObj.Details)
                        Catch ex As Exception
                            'eat the error
                        End Try
                        File.Move(f.Value.FullName, setup.BakDir & "\" & f.Value.Name & ".err")
                        log("Processing file " & f.Key & " FAILED - " & IIf(String.IsNullOrEmpty(respObj.Details), "Error from web service", respObj.Details) & ".")
                    End If
                Catch ex As Exception
                    File.Move(f.Value.FullName, setup.BakDir & "\" & f.Value.Name & ".err")
                    log("ERROR: " & ex.Message, False)
                End Try

            Next
        Catch ex As ISTException
            log("ERROR: " & ex.ErrorCode & " - " & ex.Message & IIf(Not ex.InnerException Is Nothing, " - " & ex.InnerException.Message, ""), False)

        Catch ex As Exception
            log("There was an error: " & ex.Message, False)

        Finally
            LogService.WriteLog("Finished 941ISTConsole." & prefix & ".", LogFileType.LogFile)
            log("Finished 941ISTConsole." & prefix & ", processed: " & successFiles.ToString() & " of " & IIf(Not files Is Nothing, files.Keys.Count.ToString(), 0) & " ASN files.", True)

        End Try

        Return logMsg.ToString()

    End Function

    Private Shared Function InlineAssignHelper(Of T)(ByRef target As T, value As T) As T
        target = value
        Return value
    End Function

    ''' <summary>
    ''' object to hold the variables used by the ASN out process
    ''' </summary>
    ''' <remarks></remarks>
    Private Class SetupEnv

        Public Environment As String
        Public ToStore As String
        Public InDir As String
        Public OutDir As String
        Public BakDir As String
        Public LogDir As String
        Public LogFileName As String
        Public FileMask As String

        Public Sub New()

            Environment = ConfigurationManager.AppSettings.Get("ENV")
            ToStore = ConfigurationManager.AppSettings.Get("STORE")
            InDir = ConfigurationManager.AppSettings.Get(prefix & "_INDIR")
            OutDir = ConfigurationManager.AppSettings.Get(prefix & "_OUTDIR")
            BakDir = ConfigurationManager.AppSettings.Get(prefix & "_BAKDIR")
            LogDir = ConfigurationManager.AppSettings.Get("LogsDir")
            FileMask = ConfigurationManager.AppSettings.Get(prefix & "_FILE_MASK")
            LogFileName = Config.LogsDir & prefix & "_" & LogService.GetDateTimeSufix(DateTimeSufixType.JustDate) & "." & Config.FileExtForLogs

            'make sure we have 4 digits leftpadded with zero
            ToStore = Utils.Get4DigitStoreNumber(ToStore)

        End Sub

    End Class

End Class
