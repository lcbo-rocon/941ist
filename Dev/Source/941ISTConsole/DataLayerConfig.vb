﻿Imports System.Configuration

Imports System.Data
Imports System.Data.OleDb


Public Module DataLayerConfig

#Region "Properties"

    Public ReadOnly Property OracleOleDbConnString As String
        Get
            Return GetConnString(ConfigurationManager.AppSettings("ENV"))
        End Get
    End Property

    Public ReadOnly Property DBConn As IDbConnection
        Get
            Dim conn As IDbConnection = New OleDbConnection(OracleOleDbConnString)
            Return conn
        End Get
    End Property

#End Region

#Region "Functions"


    Public Function GetAppSettings(key As String) As String

        Return ConfigurationManager.AppSettings(key).ToString()

    End Function

    Public Function GetConnString(key As String) As String

        Return ConfigurationManager.ConnectionStrings(key).ToString()

    End Function

#End Region

End Module