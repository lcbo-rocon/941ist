﻿Imports System
Imports System.Collections.Generic
Imports System.Linq
Imports System.Diagnostics
Imports System.Security.Permissions
Imports System.Threading
Imports System.Windows



Public Module Program

    Public ExceptionAtEventLog As String = "941ISTIntegratedConsoles"

    ''' <summary>
    ''' Entry Point for the Application
    ''' </summary>
    ''' <param name="cmdArgs">
    ''' r/RoutineName     r: routine to run.
    ''' asnrecord: ASN Record Routine
    ''' asnfile: ASN File Routine
    ''' </param>
    ''' <remarks></remarks>
    <MTAThread()>
    <SecurityPermission(SecurityAction.Demand, Flags:=SecurityPermissionFlag.ControlAppDomain)>
    Public Sub Main(ByVal cmdArgs As String())

        'Add the event handler for handling UI thread exceptions to the event
        AddHandler Application.ThreadException, AddressOf Application_UIThreadException

        'Set the unhandled exception mode to force all windows forms errors to go through this handler
        Application.SetUnhandledExceptionMode(UnhandledExceptionMode.CatchException)

        'Add the event handler for handling Non-UI thread exceptions to the event
        AddHandler AppDomain.CurrentDomain.UnhandledException, AddressOf CurrentDomain_UnhandledException

        Application.EnableVisualStyles()
        Application.SetCompatibleTextRenderingDefault(False)

        'finnally launching the form we want
        Application.Run(New MainForm(cmdArgs))


    End Sub



#Region "HelperMethods"

    Public Sub CurrentDomain_UnhandledException(sender As Object, e As UnhandledExceptionEventArgs)

        Dim logMsg As String = String.Empty
        Dim ex As Exception = CType(e.ExceptionObject, Exception)
        Dim statErrMsg As String = "ISTIntegratedConsoles CurrentDomain_UnhandledException: An application error occurred. " & vbNewLine & ex.ToString()

        Try
            logMsg = statErrMsg & "\n\nStack Trace:\n" & ex.StackTrace
            'Write log here
            WriteOnEventLog(logMsg)

        Catch ex2 As NotImplementedException

            Try
                MessageBox.Show("Fatal Non-UI Error", "Fatal Non-UI Error. Could not write the error to the event log. Reason: " & ex2.ToString(), MessageBoxButtons.OK, MessageBoxIcon.Stop)
            Finally
                'exiting application
                Application.Exit()
            End Try

        Finally
            'exiting application
            Application.Exit()
        End Try

        MessageBox.Show("Fatal Non-UI Error", "Fatal Non-UI Error. Could not write the error to the event log. Reason: " & ex.ToString(), MessageBoxButtons.OK, MessageBoxIcon.Stop)
        Application.Exit()

    End Sub

    Public Sub Application_UIThreadException(sender As Object, e As ThreadExceptionEventArgs)

        Dim logMsg As String = String.Empty
        Dim ex As Exception = e.Exception
        Dim statErrMsg As String = "ISTIntegratedConsoles Application_UIThreadException: An application error occurred. " & vbNewLine & ex.ToString()

        Try
            logMsg = statErrMsg & "\n\nStack Trace:\n" & ex.StackTrace
            'Write log here
            WriteOnEventLog(logMsg)

        Catch ex2 As Exception

            Try
                MessageBox.Show("Fatal Non-UI Error", "Fatal Non-UI Error. Could not write the error to the event log. Reason: " & ex2.ToString(), MessageBoxButtons.OK, MessageBoxIcon.Stop)
            Finally
                'exiting application
                Application.Exit()
            End Try

        Finally
            'exiting application
            Application.Exit()
        End Try

    End Sub

    Public Sub WriteOnEventLog(logMessage As String)

        'making sure we can access the event log
        If EventLog.SourceExists(ExceptionAtEventLog) = False Then
            EventLog.CreateEventSource(ExceptionAtEventLog, "Application")
        End If

        'create and EventLog instance and assign its source
        Using evLog As EventLog = New EventLog()
            evLog.Source = ExceptionAtEventLog
            evLog.WriteEntry(logMessage)
        End Using

    End Sub

#End Region



End Module
