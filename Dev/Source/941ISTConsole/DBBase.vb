﻿Imports Microsoft.VisualBasic
Imports System
Imports System.Collections
Imports System.Linq
Imports System.Text
Imports System.Configuration

Imports System.Data
Imports System.Data.OleDb

Imports System.Transactions

Imports System.ComponentModel
Imports System.Data.OracleClient

Public Class DBBase

    Public Shared Function GetDBConnection() As IDbConnection

        Dim conn As IDbConnection = DataLayerConfig.DBConn
        Return conn

    End Function

    Public Overloads Shared Function GetDataCommand() As IDbCommand

        Dim cmd As IDbCommand = New OracleCommand()
        Return cmd

    End Function

    Public Overloads Shared Function GetDataCommand(ByVal cmdText As String) As IDbCommand

        Dim cmd As IDbCommand = New OracleCommand(cmdText)
        Return cmd

    End Function

    Public Overloads Shared Function GetDataCommand(ByVal cmdText As String, ByRef connection As IDbConnection) As IDbCommand

        Dim cmd As IDbCommand = New OracleCommand(cmdText, connection)
        Return cmd

    End Function

    Public Overloads Shared Function GetDataCommand(ByVal cmdText As String, ByRef connection As IDbConnection, ByRef transaction As IDbTransaction) As IDbCommand

        Dim cmd As IDbCommand = New OracleCommand(cmdText, connection, transaction)
        Return cmd

    End Function

    Public Shared Function GetDataAdapter(cmd As IDbCommand) As IDataAdapter

        Dim adptr As IDataAdapter = New OleDbDataAdapter(cmd)
        Return adptr

    End Function

    Public Shared Function GetParameter() As IDataParameter

        Dim param As IDataParameter = New OleDbParameter()
        Return param

    End Function

    ''' <summary>
    ''' This class was created due to this article: http://blogs.msdn.com/b/dbrowne/archive/2010/06/03/using-new-transactionscope-considered-harmful.aspx
    ''' </summary>
    ''' <remarks></remarks>
    Class TransactionUtils
        Public Shared Function CreateTransactionScope() As TransactionScope
            Dim transactionOptions = New TransactionOptions()
            transactionOptions.IsolationLevel = System.Transactions.IsolationLevel.ReadCommitted
            transactionOptions.Timeout = TransactionManager.MaximumTimeout
            Return New TransactionScope(TransactionScopeOption.Required, transactionOptions)

        End Function
    End Class

End Class

