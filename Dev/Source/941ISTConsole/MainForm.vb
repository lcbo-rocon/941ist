﻿Imports System
Imports System.Threading
Imports System.Timers
Imports System.Linq
Imports System.ComponentModel
Imports System.Xml.Linq


Imports System.Runtime.InteropServices


''' <summary>
''' Entry Point for the MainFor
''' Expected parameters are:
''' r/RoutineName     r: routine to run.
''' asnrecord: ASN Record Routine
''' asnfile: ASN File Routine
''' </summary>
''' <remarks></remarks>
Public Class MainForm

    Private _EnableUI As Boolean = True
    Private logBldrDic As New Dictionary(Of LogBuilderType, VisualLogBuilder)
    Private Delegate Sub DelegateVisualLogCallBack(msg As String, txtBxCntrl As TextBox, logBldrType As LogBuilderType)
    Private Delegate Sub DelegateControlTextCallBack(msg As String, cntrl As Control)
    Private processWorker As BackgroundWorker = Nothing

    Protected Friend Property ManualMode As Boolean
        Get
            Return Me._EnableUI
        End Get
        Private Set(value As Boolean)
            Me._EnableUI = value
        End Set
    End Property


    <System.Runtime.InteropServices.DllImport("kernel32.dll")>
    Private Shared Function AllocConsole() As Boolean
    End Function


    Public Sub New(ByVal cmdArgs As String())

        ' This call is required by the designer.
        InitializeComponent()

        If cmdArgs.Count > 0 Then
            Dim joinedArr As String = String.Join(", ", cmdArgs)

            LogService.WriteLog(String.Format("Automatic Startup Initiated [{0}]", joinedArr), LogFileType.LogFile)
            Dim routine As RoutineType = ArgumentsTranslator.GetRoutine(cmdArgs)
            Me.RunAutomatedProcess(routine, cmdArgs)

        Else

            ' Add any initialization after the InitializeComponent() call.
            Me.InitializeGUI()

        End If

    End Sub

    Private Sub RunAutomatedProcess(routine As RoutineType, ByVal cmdArgs As String())

        Select Case routine

            Case RoutineType.ASNRecord
                Me.TabMain.SelectedTab = Me.TabPageASNRecords
                Me.InitalizeASNRecordsTab()
                Me.CreateASNRecords(True)

            Case RoutineType.ASNFile
                Me.TabMain.SelectedTab = Me.TabPageASNFilesOutOf941
                Me.InitalizeASNFilesOutOf941Tab()
                Me.CreateASNFilesOutOf941(True, cmdArgs)

            Case RoutineType.T1FilesInto941
                Me.TabMain.SelectedTab = Me.TabPageT1FilesInto941
                Me.InitalizeT1FilesInto941Tab()
                Me.ImportT1FilesInto941(True)

            Case RoutineType.ASNFilesInto941
                Me.TabMain.SelectedTab = Me.TabPageASNFilesInto941
                Me.InitalizeASNFilesInto941Tab()
                Me.ImportASNFilesInto941(True)

            Case RoutineType.T1FilesOutOf941
                Me.TabMain.SelectedTab = Me.TabPageT1FilesOutOf941
                Me.InitalizeT1FilesOutOf941Tab()
                Me.CreateT1FilesOutOf941(True)

            Case RoutineType.NotSet
                'do nothing
                Me.DealWithNonSetWhenOnAutoMode(True, cmdArgs)

        End Select



    End Sub

    Private Sub InitializeGUI()

        'Initalize tabs
        Me.InitalizeASNRecordsTab()
        Me.InitalizeASNFilesOutOf941Tab()
        Me.InitalizeT1FilesInto941Tab()
        Me.InitalizeASNFilesInto941Tab()
        Me.InitalizeT1FilesOutOf941Tab()

    End Sub



#Region "ASNRecords Area"

    Private Sub InitalizeASNRecordsTab()

        Dim visualLogBuilder As New VisualLogBuilder(Config.MaxNumLogMsgsToDisplay, LogBuilderType.ASNRecord)
        Me.logBldrDic.Add(visualLogBuilder.LogBuilderType, visualLogBuilder)

    End Sub

    Private Sub CreateASNRecords(automateProcess As Boolean)

        If automateProcess = True Then

            Me.processWorker = New BackgroundWorker()
            AddHandler Me.processWorker.RunWorkerCompleted, AddressOf Me.RutineHasBeenCompleted_EventHandler
            Me.processWorker.WorkerSupportsCancellation = True
            AddHandler Me.processWorker.DoWork, AddressOf Me.RunCreateASNRecords_EventHandler
            Me.processWorker.RunWorkerAsync()

        Else

            Dim e As DoWorkEventArgs = New DoWorkEventArgs(Nothing)
            RunCreateASNRecords_EventHandler(Me, e)

        End If


    End Sub

    Private Sub btnCreateASNRecords_Click(sender As System.Object, e As System.EventArgs) Handles btnCreateASNRecords.Click

        Me.CreateASNRecords(False)

    End Sub

    Private Sub RunCreateASNRecords_EventHandler(sender As Object, e As DoWorkEventArgs)

        Dim returnMsg As String = ASNRecords.CreateASNRecords()
        Me.DisplayLog(returnMsg, Me.txtASNRecMsgLogs, LogBuilderType.ASNRecord)

    End Sub

#End Region


#Region "ASNFilesOutOf941 Area"

    Private Sub InitalizeASNFilesOutOf941Tab()

        Dim visualLogBuilder As New VisualLogBuilder(Config.MaxNumLogMsgsToDisplay, LogBuilderType.ASNFile)
        Me.logBldrDic.Add(visualLogBuilder.LogBuilderType, visualLogBuilder)

    End Sub

    Private Sub CreateASNFilesOutOf941(automateProcess As Boolean, cmdArgs As String())

        If automateProcess = True Then

            Me.processWorker = New BackgroundWorker()
            AddHandler Me.processWorker.RunWorkerCompleted, AddressOf Me.RutineHasBeenCompleted_EventHandler
            Me.processWorker.WorkerSupportsCancellation = True
            AddHandler Me.processWorker.DoWork, AddressOf Me.RunCreateASNFilesOutOf941_EventHandler
            Me.processWorker.RunWorkerAsync(cmdArgs)

        Else

            Dim e As DoWorkEventArgs = New DoWorkEventArgs(cmdArgs)
            RunCreateASNFilesOutOf941_EventHandler(Me, e)

        End If


    End Sub

    Private Sub btnCreateASNFilesOutOf941_Click(sender As System.Object, e As System.EventArgs) Handles btnCreateASNFilesOutOf941.Click

        Me.CreateASNFilesOutOf941(False, Nothing)

    End Sub

    Private Sub RunCreateASNFilesOutOf941_EventHandler(sender As Object, e As DoWorkEventArgs)
        Dim asnFiles As ASNFILES_OUT_OF_941 = New ASNFILES_OUT_OF_941()
        Dim returnMsg As String
        Dim co_odno As String = Nothing

        If Not e.Argument Is Nothing AndAlso ArgumentsTranslator.KeyExist("co_odno", e.Argument) Then
            co_odno = ArgumentsTranslator.GetValue("co_odno", e.Argument)
        End If
        returnMsg = asnFiles.CreateASNFiles(co_odno)
        Me.DisplayLog(returnMsg, Me.txtASNFilesOutOf941MsgLogs, LogBuilderType.ASNFile)

    End Sub

#End Region

#Region "T1FilesInto941 Area"

    Private Sub InitalizeT1FilesInto941Tab()

        Dim visualLogBuilder As New VisualLogBuilder(Config.MaxNumLogMsgsToDisplay, LogBuilderType.T1FilesInto941)
        Me.logBldrDic.Add(visualLogBuilder.LogBuilderType, visualLogBuilder)

    End Sub

    Private Sub ImportT1FilesInto941(automateProcess As Boolean)

        If automateProcess = True Then

            Me.processWorker = New BackgroundWorker()
            AddHandler Me.processWorker.RunWorkerCompleted, AddressOf Me.RutineHasBeenCompleted_EventHandler
            Me.processWorker.WorkerSupportsCancellation = True
            AddHandler Me.processWorker.DoWork, AddressOf Me.RunImportT1FilesInto941_EventHandler
            Me.processWorker.RunWorkerAsync()

        Else

            Dim e As DoWorkEventArgs = New DoWorkEventArgs(Nothing)
            RunImportT1FilesInto941_EventHandler(Me, e)

        End If


    End Sub

    Private Sub btnImportT1FilesInto941_Click(sender As System.Object, e As System.EventArgs) Handles btnImportT1FilesInto941.Click

        Me.ImportT1FilesInto941(False)

    End Sub

    Private Sub RunImportT1FilesInto941_EventHandler(sender As Object, e As DoWorkEventArgs)
        Dim T1filesImport As T1FILES_INTO_941 = New T1FILES_INTO_941()
        Dim returnMsg As String = T1filesImport.ProcessT1Files()
        Me.DisplayLog(returnMsg, Me.txtImportT1FilesInto941MsgLogs, LogBuilderType.T1FilesInto941)

    End Sub

#End Region

#Region "ASNFilesInto941 Area"

    Private Sub InitalizeASNFilesInto941Tab()

        Dim visualLogBuilder As New VisualLogBuilder(Config.MaxNumLogMsgsToDisplay, LogBuilderType.ASNFilesInto941)
        Me.logBldrDic.Add(visualLogBuilder.LogBuilderType, visualLogBuilder)

    End Sub

    Private Sub ImportASNFilesInto941(automateProcess As Boolean)

        If automateProcess = True Then

            Me.processWorker = New BackgroundWorker()
            AddHandler Me.processWorker.RunWorkerCompleted, AddressOf Me.RutineHasBeenCompleted_EventHandler
            Me.processWorker.WorkerSupportsCancellation = True
            AddHandler Me.processWorker.DoWork, AddressOf Me.RunImportASNFilesInto941_EventHandler
            Me.processWorker.RunWorkerAsync()

        Else

            Dim e As DoWorkEventArgs = New DoWorkEventArgs(Nothing)
            RunImportASNFilesInto941_EventHandler(Me, e)

        End If


    End Sub

    Private Sub btnImportASNFilesInto941_Click(sender As System.Object, e As System.EventArgs) Handles btnImportASNFilesInto941.Click

        Me.ImportASNFilesInto941(False)

    End Sub

    Private Sub RunImportASNFilesInto941_EventHandler(sender As Object, e As DoWorkEventArgs)
        Dim ASNfilesImport As ASNFILES_INTO_941 = New ASNFILES_INTO_941()
        Dim returnMsg As String = ASNfilesImport.ProcessASNFiles()
        Me.DisplayLog(returnMsg, Me.txtImportASNFilesInto941MsgLogs, LogBuilderType.ASNFilesInto941)

    End Sub

#End Region

#Region "T1FilesOutOf941 Area"

    Private Sub InitalizeT1FilesOutOf941Tab()

        Dim visualLogBuilder As New VisualLogBuilder(Config.MaxNumLogMsgsToDisplay, LogBuilderType.T1FilesOutOf941)
        Me.logBldrDic.Add(visualLogBuilder.LogBuilderType, visualLogBuilder)

    End Sub

    Private Sub CreateT1FilesOutOf941(automateProcess As Boolean)

        If automateProcess = True Then

            Me.processWorker = New BackgroundWorker()
            AddHandler Me.processWorker.RunWorkerCompleted, AddressOf Me.RutineHasBeenCompleted_EventHandler
            Me.processWorker.WorkerSupportsCancellation = True
            AddHandler Me.processWorker.DoWork, AddressOf Me.RunCreateT1FilesOutOf941_EventHandler
            Me.processWorker.RunWorkerAsync()

        Else

            Dim e As DoWorkEventArgs = New DoWorkEventArgs(Nothing)
            RunCreateT1FilesOutOf941_EventHandler(Me, e)

        End If


    End Sub

    Private Sub btnCreateT1FilesOutOf941_Click(sender As System.Object, e As System.EventArgs) Handles btnCreateT1FilesOutOf941.Click

        Me.CreateT1FilesOutOf941(False)

    End Sub

    Private Sub RunCreateT1FilesOutOf941_EventHandler(sender As Object, e As DoWorkEventArgs)
        Dim T1Files As T1FILES_OUT_OF_941 = New T1FILES_OUT_OF_941()
        Dim returnMsg As String = T1Files.CreateCompleteT1Files()
        Me.DisplayLog(returnMsg, Me.txtT1FilesOutOf941MsgLogs, LogBuilderType.T1FilesOutOf941)

    End Sub

#End Region

#Region "Routine NotSet Area"

    Private Sub DealWithNonSetWhenOnAutoMode(automateProcess As Boolean, ByVal cmdArgs As String())

        Dim joinedArr As String = String.Join(", ", cmdArgs)

        If automateProcess = True Then

            Me.processWorker = New BackgroundWorker()
            AddHandler Me.processWorker.RunWorkerCompleted, AddressOf Me.RutineHasBeenCompleted_EventHandler
            Me.processWorker.WorkerSupportsCancellation = True
            AddHandler Me.processWorker.DoWork, AddressOf Me.RunNothing_EventHandler
            Me.processWorker.RunWorkerAsync(joinedArr)

        End If

    End Sub

    Private Sub RunNothing_EventHandler(sender As Object, e As DoWorkEventArgs)

        Dim newMsg As String = String.Format("RoutineType.NotSet: nothing to run for [{0}]", e.Argument.ToString())

        If Me.lblMsgAtMainTab.InvokeRequired Then
            Dim dlgte As DelegateControlTextCallBack = AddressOf SetTextAsynchronically_Handler
            Try
                Me.Invoke(dlgte, New Object() {newMsg, Me.lblMsgAtMainTab})
            Catch ex As Exception
                LogService.WriteLog("RunNothing_EventHandler ErrMsg: " + Err.ToString(), LogFileType.ErrFile)
            End Try
        Else

            LogService.WriteLog("RunNothing_EventHandler was triggered ", LogFileType.LogFile)
            Me.lblMsgAtMainTab.Text = newMsg
            Me.Refresh()

        End If

    End Sub

#End Region


#Region "End of Processes Area"

    Private Sub RutineHasBeenCompleted_EventHandler(sender As Object, e As RunWorkerCompletedEventArgs)
        Thread.Sleep(Config.TimerWaitBeforeCloseForm)
        LogService.WriteLog("Application is closing", LogFileType.LogFile)
        Me.Close()
    End Sub

#End Region


#Region "Display Logs Area"


    Private Sub DisplayLog(newMsg As String, txtBxCntrl As TextBox, logBldrType As LogBuilderType)

        'if the txbx requires it to be done asynchronically go this way
        If txtBxCntrl.InvokeRequired Then
            Dim dlgte As DelegateVisualLogCallBack = AddressOf AppendTextToVisualMsgLog_Handler
            Try
                Me.Invoke(dlgte, New Object() {newMsg, txtBxCntrl, logBldrType})
            Catch ex As Exception
                LogService.WriteLog("DisplayLog ErrMsg: ", LogFileType.ErrFile)
            End Try

        Else ' otherwise just call the handler directly
            Me.AppendTextToVisualMsgLog_Handler(newMsg, txtBxCntrl, logBldrType)
        End If


    End Sub

    Private Sub AppendTextToVisualMsgLog_Handler(newMsg As String, txtBxCntrl As TextBox, logBldrType As LogBuilderType)

        If Not Me.logBldrDic Is Nothing Then
            If Me.logBldrDic.ContainsKey(logBldrType) Then

                Dim displayValue As String = Me.logBldrDic.Item(logBldrType).AppendNewMessage(newMsg)
                txtBxCntrl.Text = displayValue
                txtBxCntrl.Refresh()

            End If
        End If

    End Sub

    Private Sub SetTextAsynchronically_Handler(newMsg As String, cntrl As Control)

        LogService.WriteLog(String.Format("SetTextAsynchronically_Handler was triggered - [{0}]", newMsg), LogFileType.LogFile)
        cntrl.Text = newMsg
        Me.Refresh()

    End Sub


#End Region


End Class
