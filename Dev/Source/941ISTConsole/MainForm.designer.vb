﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class MainForm
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.TabMain = New System.Windows.Forms.TabControl()
        Me.TabPageMain = New System.Windows.Forms.TabPage()
        Me.lblMsgAtMainTab = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.TabPageASNRecords = New System.Windows.Forms.TabPage()
        Me.btnCreateASNRecords = New System.Windows.Forms.Button()
        Me.lblLogsCreateASNRecs = New System.Windows.Forms.Label()
        Me.txtASNRecMsgLogs = New System.Windows.Forms.TextBox()
        Me.TabPageASNFilesOutOf941 = New System.Windows.Forms.TabPage()
        Me.btnCreateASNFilesOutOf941 = New System.Windows.Forms.Button()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.txtASNFilesOutOf941MsgLogs = New System.Windows.Forms.TextBox()
        Me.TabPageT1FilesInto941 = New System.Windows.Forms.TabPage()
        Me.btnImportT1FilesInto941 = New System.Windows.Forms.Button()
        Me.lblImportT1FilesInto941 = New System.Windows.Forms.Label()
        Me.txtImportT1FilesInto941MsgLogs = New System.Windows.Forms.TextBox()
        Me.TabPageASNFilesInto941 = New System.Windows.Forms.TabPage()
        Me.btnImportASNFilesInto941 = New System.Windows.Forms.Button()
        Me.lblImportASNFilesInto941 = New System.Windows.Forms.Label()
        Me.txtImportASNFilesInto941MsgLogs = New System.Windows.Forms.TextBox()
        Me.TabPageT1FilesOutOf941 = New System.Windows.Forms.TabPage()
        Me.btnCreateT1FilesOutOf941 = New System.Windows.Forms.Button()
        Me.lblT1FilesOutOf941 = New System.Windows.Forms.Label()
        Me.txtT1FilesOutOf941MsgLogs = New System.Windows.Forms.TextBox()
        Me.TabMain.SuspendLayout()
        Me.TabPageMain.SuspendLayout()
        Me.TabPageASNRecords.SuspendLayout()
        Me.TabPageASNFilesOutOf941.SuspendLayout()
        Me.TabPageT1FilesInto941.SuspendLayout()
        Me.TabPageASNFilesInto941.SuspendLayout()
        Me.TabPageT1FilesOutOf941.SuspendLayout()
        Me.SuspendLayout()
        '
        'TabMain
        '
        Me.TabMain.Controls.Add(Me.TabPageMain)
        Me.TabMain.Controls.Add(Me.TabPageASNRecords)
        Me.TabMain.Controls.Add(Me.TabPageASNFilesOutOf941)
        Me.TabMain.Controls.Add(Me.TabPageT1FilesInto941)
        Me.TabMain.Controls.Add(Me.TabPageASNFilesInto941)
        Me.TabMain.Controls.Add(Me.TabPageT1FilesOutOf941)
        Me.TabMain.Location = New System.Drawing.Point(13, 13)
        Me.TabMain.Name = "TabMain"
        Me.TabMain.SelectedIndex = 0
        Me.TabMain.Size = New System.Drawing.Size(917, 500)
        Me.TabMain.TabIndex = 0
        '
        'TabPageMain
        '
        Me.TabPageMain.Controls.Add(Me.lblMsgAtMainTab)
        Me.TabPageMain.Controls.Add(Me.Label1)
        Me.TabPageMain.Location = New System.Drawing.Point(4, 22)
        Me.TabPageMain.Name = "TabPageMain"
        Me.TabPageMain.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPageMain.Size = New System.Drawing.Size(909, 474)
        Me.TabPageMain.TabIndex = 0
        Me.TabPageMain.Text = "Main"
        Me.TabPageMain.UseVisualStyleBackColor = True
        '
        'lblMsgAtMainTab
        '
        Me.lblMsgAtMainTab.AutoSize = True
        Me.lblMsgAtMainTab.ForeColor = System.Drawing.Color.FromArgb(CType(CType(192, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.lblMsgAtMainTab.Location = New System.Drawing.Point(227, 259)
        Me.lblMsgAtMainTab.Name = "lblMsgAtMainTab"
        Me.lblMsgAtMainTab.Size = New System.Drawing.Size(0, 13)
        Me.lblMsgAtMainTab.TabIndex = 1
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 16.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(225, 197)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(403, 26)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "Welcome to the IST Integrated Consoles"
        '
        'TabPageASNRecords
        '
        Me.TabPageASNRecords.Controls.Add(Me.btnCreateASNRecords)
        Me.TabPageASNRecords.Controls.Add(Me.lblLogsCreateASNRecs)
        Me.TabPageASNRecords.Controls.Add(Me.txtASNRecMsgLogs)
        Me.TabPageASNRecords.Location = New System.Drawing.Point(4, 22)
        Me.TabPageASNRecords.Name = "TabPageASNRecords"
        Me.TabPageASNRecords.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPageASNRecords.Size = New System.Drawing.Size(909, 474)
        Me.TabPageASNRecords.TabIndex = 1
        Me.TabPageASNRecords.Text = "ASN Records"
        Me.TabPageASNRecords.UseVisualStyleBackColor = True
        '
        'btnCreateASNRecords
        '
        Me.btnCreateASNRecords.Location = New System.Drawing.Point(753, 39)
        Me.btnCreateASNRecords.Name = "btnCreateASNRecords"
        Me.btnCreateASNRecords.Size = New System.Drawing.Size(131, 23)
        Me.btnCreateASNRecords.TabIndex = 2
        Me.btnCreateASNRecords.Text = "Create ASN Records"
        Me.btnCreateASNRecords.UseVisualStyleBackColor = True
        '
        'lblLogsCreateASNRecs
        '
        Me.lblLogsCreateASNRecs.AutoSize = True
        Me.lblLogsCreateASNRecs.Location = New System.Drawing.Point(19, 66)
        Me.lblLogsCreateASNRecs.Name = "lblLogsCreateASNRecs"
        Me.lblLogsCreateASNRecs.Size = New System.Drawing.Size(65, 13)
        Me.lblLogsCreateASNRecs.TabIndex = 1
        Me.lblLogsCreateASNRecs.Text = "Latest Logs:"
        '
        'txtASNRecMsgLogs
        '
        Me.txtASNRecMsgLogs.CausesValidation = False
        Me.txtASNRecMsgLogs.Location = New System.Drawing.Point(22, 82)
        Me.txtASNRecMsgLogs.Multiline = True
        Me.txtASNRecMsgLogs.Name = "txtASNRecMsgLogs"
        Me.txtASNRecMsgLogs.ReadOnly = True
        Me.txtASNRecMsgLogs.ScrollBars = System.Windows.Forms.ScrollBars.Both
        Me.txtASNRecMsgLogs.Size = New System.Drawing.Size(862, 336)
        Me.txtASNRecMsgLogs.TabIndex = 0
        '
        'TabPageASNFilesOutOf941
        '
        Me.TabPageASNFilesOutOf941.Controls.Add(Me.btnCreateASNFilesOutOf941)
        Me.TabPageASNFilesOutOf941.Controls.Add(Me.Label2)
        Me.TabPageASNFilesOutOf941.Controls.Add(Me.txtASNFilesOutOf941MsgLogs)
        Me.TabPageASNFilesOutOf941.Location = New System.Drawing.Point(4, 22)
        Me.TabPageASNFilesOutOf941.Name = "TabPageASNFilesOutOf941"
        Me.TabPageASNFilesOutOf941.Size = New System.Drawing.Size(909, 474)
        Me.TabPageASNFilesOutOf941.TabIndex = 2
        Me.TabPageASNFilesOutOf941.Text = "ASN Files Out of 941"
        Me.TabPageASNFilesOutOf941.UseVisualStyleBackColor = True
        '
        'btnCreateASNFilesOutOf941
        '
        Me.btnCreateASNFilesOutOf941.Location = New System.Drawing.Point(756, 48)
        Me.btnCreateASNFilesOutOf941.Name = "btnCreateASNFilesOutOf941"
        Me.btnCreateASNFilesOutOf941.Size = New System.Drawing.Size(131, 23)
        Me.btnCreateASNFilesOutOf941.TabIndex = 5
        Me.btnCreateASNFilesOutOf941.Text = "Create ASN Files"
        Me.btnCreateASNFilesOutOf941.UseVisualStyleBackColor = True
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(22, 75)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(65, 13)
        Me.Label2.TabIndex = 4
        Me.Label2.Text = "Latest Logs:"
        '
        'txtASNFilesOutOf941MsgLogs
        '
        Me.txtASNFilesOutOf941MsgLogs.CausesValidation = False
        Me.txtASNFilesOutOf941MsgLogs.Location = New System.Drawing.Point(25, 91)
        Me.txtASNFilesOutOf941MsgLogs.Multiline = True
        Me.txtASNFilesOutOf941MsgLogs.Name = "txtASNFilesOutOf941MsgLogs"
        Me.txtASNFilesOutOf941MsgLogs.ReadOnly = True
        Me.txtASNFilesOutOf941MsgLogs.ScrollBars = System.Windows.Forms.ScrollBars.Both
        Me.txtASNFilesOutOf941MsgLogs.Size = New System.Drawing.Size(862, 336)
        Me.txtASNFilesOutOf941MsgLogs.TabIndex = 3
        '
        'TabPageT1FilesInto941
        '
        Me.TabPageT1FilesInto941.Controls.Add(Me.btnImportT1FilesInto941)
        Me.TabPageT1FilesInto941.Controls.Add(Me.lblImportT1FilesInto941)
        Me.TabPageT1FilesInto941.Controls.Add(Me.txtImportT1FilesInto941MsgLogs)
        Me.TabPageT1FilesInto941.Location = New System.Drawing.Point(4, 22)
        Me.TabPageT1FilesInto941.Name = "TabPageT1FilesInto941"
        Me.TabPageT1FilesInto941.Size = New System.Drawing.Size(909, 474)
        Me.TabPageT1FilesInto941.TabIndex = 3
        Me.TabPageT1FilesInto941.Text = "T1 Files Into 941"
        Me.TabPageT1FilesInto941.UseVisualStyleBackColor = True
        '
        'btnImportT1FilesInto941
        '
        Me.btnImportT1FilesInto941.Location = New System.Drawing.Point(756, 48)
        Me.btnImportT1FilesInto941.Name = "btnImportT1FilesInto941"
        Me.btnImportT1FilesInto941.Size = New System.Drawing.Size(131, 23)
        Me.btnImportT1FilesInto941.TabIndex = 8
        Me.btnImportT1FilesInto941.Text = "Import T1 Files"
        Me.btnImportT1FilesInto941.UseVisualStyleBackColor = True
        '
        'lblImportT1FilesInto941
        '
        Me.lblImportT1FilesInto941.AutoSize = True
        Me.lblImportT1FilesInto941.Location = New System.Drawing.Point(22, 75)
        Me.lblImportT1FilesInto941.Name = "lblImportT1FilesInto941"
        Me.lblImportT1FilesInto941.Size = New System.Drawing.Size(65, 13)
        Me.lblImportT1FilesInto941.TabIndex = 7
        Me.lblImportT1FilesInto941.Text = "Latest Logs:"
        '
        'txtImportT1FilesInto941MsgLogs
        '
        Me.txtImportT1FilesInto941MsgLogs.CausesValidation = False
        Me.txtImportT1FilesInto941MsgLogs.Location = New System.Drawing.Point(25, 91)
        Me.txtImportT1FilesInto941MsgLogs.Multiline = True
        Me.txtImportT1FilesInto941MsgLogs.Name = "txtImportT1FilesInto941MsgLogs"
        Me.txtImportT1FilesInto941MsgLogs.ReadOnly = True
        Me.txtImportT1FilesInto941MsgLogs.ScrollBars = System.Windows.Forms.ScrollBars.Both
        Me.txtImportT1FilesInto941MsgLogs.Size = New System.Drawing.Size(862, 336)
        Me.txtImportT1FilesInto941MsgLogs.TabIndex = 6
        '
        'TabPageASNFilesInto941
        '
        Me.TabPageASNFilesInto941.Controls.Add(Me.btnImportASNFilesInto941)
        Me.TabPageASNFilesInto941.Controls.Add(Me.lblImportASNFilesInto941)
        Me.TabPageASNFilesInto941.Controls.Add(Me.txtImportASNFilesInto941MsgLogs)
        Me.TabPageASNFilesInto941.Location = New System.Drawing.Point(4, 22)
        Me.TabPageASNFilesInto941.Name = "TabPageASNFilesInto941"
        Me.TabPageASNFilesInto941.Size = New System.Drawing.Size(909, 474)
        Me.TabPageASNFilesInto941.TabIndex = 4
        Me.TabPageASNFilesInto941.Text = "ASN Files Into 941"
        Me.TabPageASNFilesInto941.UseVisualStyleBackColor = True
        '
        'btnImportASNFilesInto941
        '
        Me.btnImportASNFilesInto941.Location = New System.Drawing.Point(756, 48)
        Me.btnImportASNFilesInto941.Name = "btnImportASNFilesInto941"
        Me.btnImportASNFilesInto941.Size = New System.Drawing.Size(131, 23)
        Me.btnImportASNFilesInto941.TabIndex = 11
        Me.btnImportASNFilesInto941.Text = "Import ASN Files"
        Me.btnImportASNFilesInto941.UseVisualStyleBackColor = True
        '
        'lblImportASNFilesInto941
        '
        Me.lblImportASNFilesInto941.AutoSize = True
        Me.lblImportASNFilesInto941.Location = New System.Drawing.Point(22, 75)
        Me.lblImportASNFilesInto941.Name = "lblImportASNFilesInto941"
        Me.lblImportASNFilesInto941.Size = New System.Drawing.Size(65, 13)
        Me.lblImportASNFilesInto941.TabIndex = 10
        Me.lblImportASNFilesInto941.Text = "Latest Logs:"
        '
        'txtImportASNFilesInto941MsgLogs
        '
        Me.txtImportASNFilesInto941MsgLogs.CausesValidation = False
        Me.txtImportASNFilesInto941MsgLogs.Location = New System.Drawing.Point(25, 91)
        Me.txtImportASNFilesInto941MsgLogs.Multiline = True
        Me.txtImportASNFilesInto941MsgLogs.Name = "txtImportASNFilesInto941MsgLogs"
        Me.txtImportASNFilesInto941MsgLogs.ReadOnly = True
        Me.txtImportASNFilesInto941MsgLogs.ScrollBars = System.Windows.Forms.ScrollBars.Both
        Me.txtImportASNFilesInto941MsgLogs.Size = New System.Drawing.Size(862, 336)
        Me.txtImportASNFilesInto941MsgLogs.TabIndex = 9
        '
        'TabPageT1FilesOutOf941
        '
        Me.TabPageT1FilesOutOf941.Controls.Add(Me.btnCreateT1FilesOutOf941)
        Me.TabPageT1FilesOutOf941.Controls.Add(Me.lblT1FilesOutOf941)
        Me.TabPageT1FilesOutOf941.Controls.Add(Me.txtT1FilesOutOf941MsgLogs)
        Me.TabPageT1FilesOutOf941.Location = New System.Drawing.Point(4, 22)
        Me.TabPageT1FilesOutOf941.Name = "TabPageT1FilesOutOf941"
        Me.TabPageT1FilesOutOf941.Size = New System.Drawing.Size(909, 474)
        Me.TabPageT1FilesOutOf941.TabIndex = 5
        Me.TabPageT1FilesOutOf941.Text = "T1 Files Out Of 941"
        Me.TabPageT1FilesOutOf941.UseVisualStyleBackColor = True
        '
        'btnCreateT1FilesOutOf941
        '
        Me.btnCreateT1FilesOutOf941.Location = New System.Drawing.Point(756, 48)
        Me.btnCreateT1FilesOutOf941.Name = "btnCreateT1FilesOutOf941"
        Me.btnCreateT1FilesOutOf941.Size = New System.Drawing.Size(131, 23)
        Me.btnCreateT1FilesOutOf941.TabIndex = 14
        Me.btnCreateT1FilesOutOf941.Text = "Create T1 Files"
        Me.btnCreateT1FilesOutOf941.UseVisualStyleBackColor = True
        '
        'lblT1FilesOutOf941
        '
        Me.lblT1FilesOutOf941.AutoSize = True
        Me.lblT1FilesOutOf941.Location = New System.Drawing.Point(22, 75)
        Me.lblT1FilesOutOf941.Name = "lblT1FilesOutOf941"
        Me.lblT1FilesOutOf941.Size = New System.Drawing.Size(65, 13)
        Me.lblT1FilesOutOf941.TabIndex = 13
        Me.lblT1FilesOutOf941.Text = "Latest Logs:"
        '
        'txtT1FilesOutOf941MsgLogs
        '
        Me.txtT1FilesOutOf941MsgLogs.CausesValidation = False
        Me.txtT1FilesOutOf941MsgLogs.Location = New System.Drawing.Point(25, 91)
        Me.txtT1FilesOutOf941MsgLogs.Multiline = True
        Me.txtT1FilesOutOf941MsgLogs.Name = "txtT1FilesOutOf941MsgLogs"
        Me.txtT1FilesOutOf941MsgLogs.ReadOnly = True
        Me.txtT1FilesOutOf941MsgLogs.ScrollBars = System.Windows.Forms.ScrollBars.Both
        Me.txtT1FilesOutOf941MsgLogs.Size = New System.Drawing.Size(862, 336)
        Me.txtT1FilesOutOf941MsgLogs.TabIndex = 12
        '
        'MainForm
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(932, 515)
        Me.Controls.Add(Me.TabMain)
        Me.Name = "MainForm"
        Me.Text = "IST Integrated Consoles"
        Me.TabMain.ResumeLayout(False)
        Me.TabPageMain.ResumeLayout(False)
        Me.TabPageMain.PerformLayout()
        Me.TabPageASNRecords.ResumeLayout(False)
        Me.TabPageASNRecords.PerformLayout()
        Me.TabPageASNFilesOutOf941.ResumeLayout(False)
        Me.TabPageASNFilesOutOf941.PerformLayout()
        Me.TabPageT1FilesInto941.ResumeLayout(False)
        Me.TabPageT1FilesInto941.PerformLayout()
        Me.TabPageASNFilesInto941.ResumeLayout(False)
        Me.TabPageASNFilesInto941.PerformLayout()
        Me.TabPageT1FilesOutOf941.ResumeLayout(False)
        Me.TabPageT1FilesOutOf941.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents TabMain As System.Windows.Forms.TabControl
    Friend WithEvents TabPageMain As System.Windows.Forms.TabPage
    Friend WithEvents TabPageASNRecords As System.Windows.Forms.TabPage
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents lblLogsCreateASNRecs As System.Windows.Forms.Label
    Friend WithEvents txtASNRecMsgLogs As System.Windows.Forms.TextBox
    Friend WithEvents btnCreateASNRecords As System.Windows.Forms.Button
    Friend WithEvents lblMsgAtMainTab As System.Windows.Forms.Label
    Friend WithEvents TabPageASNFilesOutOf941 As System.Windows.Forms.TabPage
    Friend WithEvents btnCreateASNFilesOutOf941 As System.Windows.Forms.Button
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents txtASNFilesOutOf941MsgLogs As System.Windows.Forms.TextBox
    Friend WithEvents TabPageT1FilesInto941 As System.Windows.Forms.TabPage
    Friend WithEvents btnImportT1FilesInto941 As System.Windows.Forms.Button
    Friend WithEvents lblImportT1FilesInto941 As System.Windows.Forms.Label
    Friend WithEvents txtImportT1FilesInto941MsgLogs As System.Windows.Forms.TextBox
    Friend WithEvents TabPageASNFilesInto941 As System.Windows.Forms.TabPage
    Friend WithEvents btnImportASNFilesInto941 As System.Windows.Forms.Button
    Friend WithEvents lblImportASNFilesInto941 As System.Windows.Forms.Label
    Friend WithEvents txtImportASNFilesInto941MsgLogs As System.Windows.Forms.TextBox
    Friend WithEvents TabPageT1FilesOutOf941 As System.Windows.Forms.TabPage
    Friend WithEvents btnCreateT1FilesOutOf941 As System.Windows.Forms.Button
    Friend WithEvents lblT1FilesOutOf941 As System.Windows.Forms.Label
    Friend WithEvents txtT1FilesOutOf941MsgLogs As System.Windows.Forms.TextBox

End Class
