﻿Imports Microsoft.VisualBasic
Imports System.IO
Imports System.Threading


Public Class LogServiceMODIFIED


    Public Overloads Shared Function WriteLog(ByVal lineToLog As String, ByVal logFile As String) As Boolean
        Dim mutexLog As Mutex = Nothing
        Dim filePath As String = String.Empty

        If EnsureLogFile(logFile) Then

            mutexLog = New Mutex(False, "mutexEventLog")

            Try

                If Not String.IsNullOrEmpty(filePath) Then

                    'Making sure one thread at the time
                    mutexLog.WaitOne()

                    'Creating a filestream that does not lock the file
                    Using fs As FileStream = New FileStream(filePath, FileMode.Append, FileAccess.Write, FileShare.ReadWrite)

                        'Initializing a streamWriter within a Using clause
                        Using sw As StreamWriter = New StreamWriter(fs)   ' could also be sw = File.AppendText(filePath) but prefer to use a fs

                            Dim timedLineLog As String = String.Format("{0}: {1}", GetDateTimeSufix(DateTimeSufixType.AllDateTime), lineToLog)

                            sw.WriteLine(timedLineLog)
                            sw.Flush()
                            sw.Close()

                        End Using

                    End Using

                End If

            Finally

                'releasing the resource
                mutexLog.ReleaseMutex()

            End Try


        End If


        Return True
    End Function

    Public Overloads Shared Function WriteLog(ByVal lineToLog As String, ByVal logType As LogFileType) As Boolean

        Return WriteLog(lineToLog, GetLogFileName(logType))

    End Function


    Public Overloads Shared Function EnsureLogFile(ByVal logFile As String) As Boolean
        Dim flInfo As FileInfo = New FileInfo(logFile)
        'ensure directory exist
        flInfo.Directory.Create()

        If File.Exists(logFile) = False Then
            Dim mutexFile As Mutex = New Mutex(False, "mutexFileCreation")

            Try

                'Making sure one thread at the time
                mutexFile.WaitOne()

                Using sw As StreamWriter = File.AppendText(logFile)

                    sw.Flush()
                    sw.Close()

                End Using

            Finally

                mutexFile.ReleaseMutex()

            End Try

        End If

        Dim returnValue As Boolean = File.Exists(logFile)
        Return returnValue
    End Function

    Public Overloads Shared Function EnsureLogFile(ByVal logType As LogFileType, ByRef filePath As String) As Boolean

        Dim fileName As String = GetLogFileName(logType)
        filePath = Config.LogsDir & fileName

        Return EnsureLogFile(filePath)

    End Function

    Public Shared Function GetLogFileName(ByVal logType As LogFileType) As String
        Dim strName As String = Config.BaseLogFileName
        Dim strDate As String = GetDateTimeSufix(DateTimeSufixType.JustDate)
        Dim strExt As String = GetLogFileExtension(logType)

        Dim fileName As String = String.Format("{0}_{1}.{2}", strName, strDate, strExt)

        Return fileName

    End Function

    Public Shared Function GetDateTimeSufix(sufixType As DateTimeSufixType) As String
        Dim returnValue As String = String.Empty

        Select Case sufixType

            Case DateTimeSufixType.JustDate
                returnValue = FillInt(System.DateTime.Now().Year) & FillInt(System.DateTime.Now().Month) & FillInt(System.DateTime.Now().Day)

            Case DateTimeSufixType.JustTime
                returnValue = FillInt(System.DateTime.Now().Hour) & FillInt(System.DateTime.Now().Minute) & FillInt(System.DateTime.Now().Second)

            Case DateTimeSufixType.AllDateTime
                returnValue = DateTime.Now().ToString("MMM dd, yyyy H:mm.ss.ffff")

        End Select

        Return returnValue
    End Function

    Public Shared Function FillInt(num As Int32, Optional totalWidth As Int32 = 2) As String

        Dim returnValue As String = String.Empty
        Dim pad As Char = "0"c

        Dim numStr As String = num.ToString()
        returnValue = numStr.PadLeft(totalWidth, pad)

        Return returnValue

    End Function

    Public Shared Function GetLogFileExtension(ByVal logType As LogFileType) As String

        Dim returnValue As String = Config.FileExtForLogs


        Select Case logType

            Case LogFileType.ErrFile
                returnValue = Config.FileExtForErrors

            Case LogFileType.LogFile
                returnValue = Config.FileExtForLogs

        End Select

        Return returnValue

    End Function


End Class



