﻿Imports Microsoft.VisualBasic
Imports System.Configuration
Imports System.Linq

Public Module Config

    Private configFile As Configuration = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None)
    Private appSettSec As AppSettingsSection = configFile.AppSettings

    Public ReadOnly Property LogsDir As String
        Get
            Return GetSettingsValue("LogsDir")
        End Get
    End Property

    Public ReadOnly Property BaseLogFileName As String
        Get
            Return GetSettingsValue("BaseLogFileName")
        End Get
    End Property

    Public ReadOnly Property FileExtForLogs As String
        Get
            Return GetSettingsValue("FileExtForLogs")
        End Get
    End Property

    Public ReadOnly Property FileExtForErrors As String
        Get
            Return GetSettingsValue("FileExtForErrors")
        End Get
    End Property

    Public ReadOnly Property MaxNumLogMsgsToDisplay As String
        Get
            Return GetSettingsValue("MaxNumLogMsgsToDisplay")
        End Get
    End Property

    Public ReadOnly Property TimerWaitBeforeCloseForm As Int32
        Get
            Dim returnValue As Int32
            If Not Int32.TryParse(GetSettingsValue("TimerWaitBeforeCloseForm"), returnValue) Then
                returnValue = 5000
            End If

            Return returnValue

        End Get
    End Property

    Public ReadOnly Property ENV As String
        Get
            Return GetSettingsValue("ENV")
        End Get
    End Property

    Public ReadOnly Property STORE As String
        Get
            Return GetSettingsValue("STORE")
        End Get
    End Property


#Region "Managing App.config"

    Public Function GetSettingsValue(key As String) As String

        Return ConfigurationManager.AppSettings(key).ToString()

    End Function

    Public Function GetConnValue(key As String) As String

        Return ConfigurationManager.ConnectionStrings(key).ToString()

    End Function

    Public Function ChangeAppSetting(key As ManagedKey, value As String)

        Dim returnValue As Boolean = False

        If appSettSec.Settings.AllKeys.Contains(key.ToString()) Then

            appSettSec.Settings(key.ToString()).Value = value
            configFile.Save(ConfigurationSaveMode.Modified)
            ConfigurationManager.RefreshSection("appSettings")
            returnValue = True
        End If

        Return returnValue

    End Function


#End Region



End Module
