﻿
Imports System.Linq
Imports System.ComponentModel
Imports System.Xml.Linq

Public Class ArgumentsTranslator

    Public Const KeyRoutine As String = "r/"
    Public Const spliter As String = "/"

    Public Shared Function GetRoutine(ByVal cmdArgs As String()) As RoutineType

        Dim returnValue As RoutineType = RoutineType.NotSet
        Dim parsedValue As RoutineType = Nothing

        Try

            Dim result As String = GetValue(KeyRoutine, cmdArgs)
            parsedValue = CType([Enum].Parse(GetType(RoutineType), result, True), RoutineType)

        Catch ex As Exception

            parsedValue = RoutineType.NotSet

        Finally

            If Not parsedValue = Nothing AndAlso parsedValue <> RoutineType.NotSet Then
                returnValue = parsedValue
            End If

        End Try

        Return returnValue

    End Function

    Public Shared Function KeyExist(ByVal key As String, ByVal cmdArgs As String()) As Boolean

        Dim returnValue As Boolean = False
        Dim cmdList As List(Of String) = cmdArgs.ToList()

        Dim result As IEnumerable(Of String) = cmdList.Where(Function(s) (s.StartsWith(key, StringComparison.CurrentCultureIgnoreCase)))
        returnValue = result.Count > 0

        Return returnValue

    End Function

    Public Shared Function GetValue(ByVal key As String, ByVal cmdArgs As String()) As String

        Dim returnValue As String = String.Empty
        Dim cmdList As List(Of String) = cmdArgs.ToList()

        Dim result As String = cmdList.First(Function(s) (s.StartsWith(key, StringComparison.CurrentCultureIgnoreCase)))
        If Not String.IsNullOrEmpty(result) Then

            Dim keyValuePair As String() = result.Split(spliter)
            If keyValuePair.Count > 1 Then
                returnValue = keyValuePair(1)
            End If

        End If

        Return returnValue

    End Function

    Public Shared Function GetDictionary(ByVal cmdArgs As String()) As Dictionary(Of String, String)

        Dim returnDict As Dictionary(Of String, String) = New Dictionary(Of String, String)

        For Each arg In cmdArgs

            Dim keyValuePair As String() = arg.Split(spliter)
            If keyValuePair.Count > 1 Then

                returnDict.Add(keyValuePair(0), keyValuePair(1))

            ElseIf keyValuePair.Count = 1 Then

                returnDict.Add(keyValuePair(0), String.Empty)

            End If

        Next

        Return returnDict

    End Function

End Class
