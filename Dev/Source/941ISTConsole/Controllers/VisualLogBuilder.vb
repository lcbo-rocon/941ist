﻿Imports System
Imports System.Collections.Generic
Imports System.Linq
Imports System.Text
Imports System.IO


Public Class VisualLogBuilder

    Private stringList As List(Of String) = New List(Of String)()
    Private numLogMsgs As Int32 = 0
    Private _LogBuilderType As LogBuilderType = Nothing

    Public Sub New(maxNumLogMsgs As Int32, logBldrType As LogBuilderType)
        Me.numLogMsgs = maxNumLogMsgs
        Me._LogBuilderType = logBldrType
    End Sub

    Public ReadOnly Property LogBuilderType As LogBuilderType
        Get
            Return Me._LogBuilderType
        End Get
    End Property

    Public Function AppendNewMessage(newMsg As String) As String

        Dim time As String = DateTime.Now.ToString("MMM dd, yyyy H:mm.ss.ffff")
        newMsg = newMsg.Replace("\0", "")
        Dim newMsg2 As String = String.Format("{0}: {1}", time, newMsg)

        If stringList.Count < numLogMsgs Then
            stringList.Add(newMsg2)
        Else
            stringList.RemoveAt(0)
            stringList.Add(newMsg2)
        End If

        Dim reversedList As List(Of String) = stringList.ToList()
        reversedList.Reverse()

        Dim returnValue As String = String.Join(Environment.NewLine, reversedList.ToArray())
        Return returnValue

    End Function



End Class
