﻿Imports System.Configuration
Imports System.Net.Mail

Public Class ISTConsoleNotification

    Private Shared sFrom As String = "ISTAdmin@lcbo.com"

    Public Shared Function T1FILES_INTO_941_Notification(ByVal co_odno As String, _
                                                         ByVal store As String) As Boolean

        Dim sMsg As String = ""
        Dim userList As New SortedList
        Dim tmpUser As RSGBase.RSGAD

        Dim sTo As String = ""
        Dim sCC As String = ""
        Dim T1NotifyUsers As String = ConfigurationManager.AppSettings("EMAIL_USERS_T1FILES_INTO_941")

        Try
            If Config.ENV <> "PROD" Then
                sMsg = "THIS IS A TEST .... FROM ENVIRONMENT " & Config.ENV
            End If

            If T1NotifyUsers.Trim <> "" Then
                For Each strUser As String In T1NotifyUsers.Split(",")
                    If strUser.IndexOf("@") > 0 Then
                        'use the email address in the config list
                        userList.Add(strUser, strUser)
                    Else
                        tmpUser = New RSGBase.RSGAD(strUser)
                        Try
                            userList.Add(tmpUser.UserEmail, tmpUser.UserEmail)
                        Catch ex As Exception
                            'ignore duplicates
                        End Try
                    End If
                Next
            End If

            For Each strUser As String In userList.GetValueList
                If sTo.Trim <> "" Then
                    sTo = sTo & ","
                End If
                sTo = sTo & strUser
            Next

            If sTo.Trim = "" Then
                Throw New ISTException(3001, "THERE ARE NO USERS LISTED!!!")
            Else
                SendMail(sTo, sCC, sFrom, "IST Order " & co_odno & " to store " & store & " has been completed", sMsg)
            End If

            Return True
        Catch ex As ISTException
            Throw ex
        Catch ex As Exception
            Throw New ISTException(3001, "There was a problem sending the IST Order completed email.", ex)
        End Try

    End Function

    Public Shared Function ASNFILES_INTO_941_Notification(ByVal co_odno As String, _
                                                          ByVal store As String) As Boolean

        Dim sMsg As String = ""
        Dim userList As New SortedList
        Dim tmpUser As RSGBase.RSGAD

        Dim sTo As String = ""
        Dim sCC As String = ""
        Dim ASNNotifyUsers As String = ConfigurationManager.AppSettings("EMAIL_USERS_ASNFILES_INTO_941")

        Try
            If Config.ENV <> "PROD" Then
                sMsg = "THIS IS A TEST .... FROM ENVIRONMENT " & Config.ENV
            End If

            If ASNNotifyUsers.Trim <> "" Then
                For Each strUser As String In ASNNotifyUsers.Split(",")
                    If strUser.IndexOf("@") > 0 Then
                        'use the email address in the config list
                        userList.Add(strUser, strUser)
                    Else
                        'get the email address from the user id
                        tmpUser = New RSGBase.RSGAD(strUser)
                        Try
                            userList.Add(tmpUser.UserEmail, tmpUser.UserEmail)
                        Catch ex As Exception
                            'ignore duplicates
                        End Try
                    End If
                Next
            End If

            For Each strUser As String In userList.GetValueList
                If sTo.Trim <> "" Then
                    sTo = sTo & ","
                End If
                sTo = sTo & strUser
            Next

            If sTo.Trim = "" Then
                Throw New ISTException(3001, "THERE ARE NO USERS LISTED!!!")
            Else
                SendMail(sTo, sCC, sFrom, "IST Order " & co_odno & " from store " & store & " has been shipped", sMsg)
            End If

            Return True
        Catch ex As ISTException
            Throw ex
        Catch ex As Exception
            Throw New ISTException(3001, "There was a problem sending the IST Order received email.", ex)
        End Try

    End Function

    Public Shared Function AlertErrors(ByVal processName As String, _
                                       ByVal stepName As String, _
                                       ByVal co_odno As String, _
                                       ByVal store As String, _
                                       ByVal fileName As String, _
                                       ByVal errorCode As Int32, _
                                       ByVal errorMessage As String) As Boolean
        Dim sMsg As String = ""
        Dim userList As New SortedList
        Dim tmpUser As RSGBase.RSGAD

        Dim sTo As String = ""
        Dim sCC As String = ""
        Dim ErrorsNotifyUsers As String = ConfigurationManager.AppSettings("EMAIL_USERS_ERRORS")

        Try
            If Config.ENV <> "PROD" Then
                sMsg = "THIS IS A TEST .... FROM ENVIRONMENT " & Config.ENV
            End If
            sMsg += vbCrLf & IIf(String.IsNullOrEmpty(processName), "", processName & " - ") & IIf(String.IsNullOrEmpty(stepName), "", stepName)
            sMsg += vbCrLf & "Order: " & co_odno
            sMsg += vbCrLf & "Store: " & store
            sMsg += IIf(String.IsNullOrEmpty(fileName), "", vbCrLf & fileName)
            sMsg += vbCrLf & "ERROR CODE: " & errorCode.ToString()
            sMsg += vbCrLf & "ERROR MESSAGE: " & errorMessage

            If ErrorsNotifyUsers.Trim <> "" Then
                For Each strUser As String In ErrorsNotifyUsers.Split(",")
                    If strUser.IndexOf("@") > 0 Then
                        'use the email address in the config list
                        userList.Add(strUser, strUser)
                    Else
                        'get the email address from the user id
                        tmpUser = New RSGBase.RSGAD(strUser)
                        Try
                            userList.Add(tmpUser.UserEmail, tmpUser.UserEmail)
                        Catch ex As Exception
                            'ignore duplicates
                        End Try
                    End If
                Next
            End If

            For Each strUser As String In userList.GetValueList
                If sTo.Trim <> "" Then
                    sTo = sTo & ","
                End If
                sTo = sTo & strUser
            Next

            If sTo.Trim = "" Then
                Throw New ISTException(3001, "THERE ARE NO USERS LISTED!!!")
            Else
                SendMail(sTo, sCC, sFrom, "IST Order processing ERROR", sMsg)
            End If

            Return True
        Catch ex As ISTException
            Throw ex
        Catch ex As Exception
            Throw New ISTException(3001, "There was a problem sending the IST Order received email.", ex)
        End Try
    End Function

    Public Shared Function SendMail(ByVal sMailTo As String, _
                                    ByVal sCCTo As String, _
                                    ByVal sMailFrom As String, _
                                    ByVal sSubject As String, _
                                    ByVal sBody As String) As Boolean

        Dim mail_server As String = ConfigurationManager.AppSettings("MAIL_SERVER")
        Dim mail_port As String = ConfigurationManager.AppSettings("MAIL_PORT")
        Dim mail As New MailMessage
        Dim SMTP As System.Net.Mail.SmtpClient = Nothing

        Try
            SMTP = New SmtpClient(mail_server, mail_port)
            mail.From = New MailAddress(sMailFrom)
            mail.To.Add(sMailTo)
            If String.IsNullOrEmpty(sCCTo) = False Then
                mail.CC.Add(sCCTo)
            End If

            'set content
            mail.Subject = (System.Environment.MachineName) & " - " & sSubject
            mail.Body = sBody

            SMTP.Send(mail)
        Catch ex As Exception
            Throw New ISTException(3000, "There was a problem sending the email." & vbNewLine & ex.Message, ex)
        End Try

        Return True

    End Function

End Class
