﻿Public Class Utils

    Public Shared Function Get4DigitStoreNumber(ByVal store As String) As String

        If String.IsNullOrEmpty(store) OrElse IsNumeric(store) = False Then
            Throw New ISTException(500, "Store " & store & " cannot be formatted as 4 digit number.")
        End If

        Return System.Convert.ToInt32(store).ToString().PadLeft(4, "0")

    End Function

End Class
