﻿Imports System.Runtime.Serialization

Public Class OracleNoConnectionException
    Inherits Exception
    Implements ISerializable

    Public Sub New()
        MyBase.New()
    End Sub

    Public Sub New(ByVal message As String)
        MyBase.New(message)

    End Sub

    Public Sub New(ByVal message As String, ByVal inner As Exception)
        MyBase.New(message, inner)
    End Sub

    Public Sub New(ByVal info As SerializationInfo, ByVal context As StreamingContext)
        MyBase.New(info, context)
    End Sub
End Class

Public Class GLOutOfBalanceException
    Inherits Exception
    Implements ISerializable

    Public Sub New()
        MyBase.New()
    End Sub

    Public Sub New(ByVal message As String)
        MyBase.New(message)

    End Sub

    Public Sub New(ByVal message As String, ByVal inner As Exception)
        MyBase.New(message, inner)
    End Sub

    Public Sub New(ByVal info As SerializationInfo, ByVal context As StreamingContext)
        MyBase.New(info, context)
    End Sub
End Class

Public Class ZeroAmountException
    Inherits Exception
    Implements ISerializable

    Public Sub New()
        MyBase.New()
    End Sub

    Public Sub New(ByVal message As String)
        MyBase.New(message)

    End Sub

    Public Sub New(ByVal message As String, ByVal inner As Exception)
        MyBase.New(message, inner)
    End Sub

    Public Sub New(ByVal info As SerializationInfo, ByVal context As StreamingContext)
        MyBase.New(info, context)
    End Sub
End Class

Public Class ISTInvoiceException
    Inherits Exception
    Implements ISerializable

    Public Sub New()
        MyBase.New()
    End Sub

    Public Sub New(ByVal message As String)
        MyBase.New(message)

    End Sub

    Public Sub New(ByVal message As String, ByVal inner As Exception)
        MyBase.New(message, inner)
    End Sub

    Public Sub New(ByVal info As SerializationInfo, ByVal context As StreamingContext)
        MyBase.New(info, context)
    End Sub
End Class

Public Class ISTException
    Inherits Exception
    Implements ISerializable

    Private _errorCode As Int32
    Private _dateTime As DateTime

    Public ReadOnly Property ErrorCode() As Int32
        Get
            Return _errorCode
        End Get
    End Property

    Public Sub New(ByVal ISTErrorCode As Int32)
        MyBase.New()
        _errorCode = ISTErrorCode
        _dateTime = Now()
    End Sub

    Public Sub New(ByVal ISTErrorCode As Int32, ByVal message As String)
        MyBase.New(message)
        _errorCode = ISTErrorCode
        _dateTime = Now()
    End Sub

    Public Sub New(ByVal ISTErrorCode As Int32, ByVal message As String, ByVal inner As Exception)
        MyBase.New(message, inner)
        _errorCode = ISTErrorCode
        _dateTime = Now()
    End Sub

    ' This constructor is needed for serialization.
    Protected Sub New(ByVal ISTErrorCode As Int32, ByVal info As SerializationInfo, ByVal context As StreamingContext)
        MyBase.New(info, context)
        _errorCode = ISTErrorCode
        _dateTime = Now()
    End Sub

End Class


