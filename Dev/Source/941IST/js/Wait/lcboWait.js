﻿(function ($) {
    var lcboGenericWaitDlg = null;

    window.lcboWaitMsg = {
        show: function () {
            if (!lcboGenericWaitDlg) {
                lcboGenericWaitDlg = $('<div style="display: none"><div class="lcboMessageWait"><span>Please Wait ...</span></div></div>').appendTo("body").dialog({
                    autoOpen: false,
                    position: "center",
                    height: "auto",
                    width: "auto",
                    minHeight: 30,
                    modal: true,
                    resizable: false,
                    closeOnEscape: false,
                    dialogClass: 'lcboMessageContainer',
                    title: ''
                });
            }
            lcboGenericWaitDlg.dialog("open");
        },
        hide: function () { lcboGenericWaitDlg.dialog("close"); }
    };

})(jQuery);