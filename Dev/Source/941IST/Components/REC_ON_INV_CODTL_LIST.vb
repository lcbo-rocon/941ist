Imports Microsoft.VisualBasic
Imports System.Data
Imports System.Data.OleDb
Imports System.Data.OracleClient

Public Class REC_ON_INV_CODTL_LIST
    Inherits SortedList
    Public ReadOnly Property on_inv_codtl_list() As DataTable
        Get
            Dim dt As New DataTable
            Dim dr As DataRow = Nothing

            With dt.Columns
                .Add("INV_NO")
                .Add("CO_ODNO")
                .Add("CO_INV_TYPE")
                .Add("COD_LINE")
                .Add("PRICE")
                .Add("EXTD_PRICE")
                .Add("TTL_CASES")
                .Add("TTL_UNITS")
                .Add("ITEM")
                .Add("PACKAGING_CODE")
                .Add("DEPOSIT_PRICE")
                .Add("QTY")
                .Add("ITEM_CATEGORY")
                .Add("ORIG_PRICE")
                .Add("PRICE_CHG_COMMENT")
                .Add("RETAIL_PRICE")
                .Add("ORIG_RETAIL_PRICE")
                '.Add("DELIVERY2_CHG")
                '.Add("DELIVERY2_TAX1")
                '.Add("DELIVERY2_TAX2")
                '.Add("DELIVERY2_POST_DT")
                '.Add("LINK_CO_ODNO")
            End With

            For Each rec As REC_ON_INV_CODTL In Me.GetValueList
                dr = dt.NewRow
                dr("INV_NO") = rec.inv_no
                dr("CO_ODNO") = rec.co_odno
                dr("CO_INV_TYPE") = rec.co_inv_type
                dr("COD_LINE") = rec.cod_line
                dr("PRICE") = rec.Price
                dr("EXTD_PRICE") = rec.extd_price
                dr("TTL_CASES") = rec.ttl_cases
                dr("TTL_UNITS") = rec.ttl_units
                dr("ITEM") = rec.item
                dr("PACKAGING_CODE") = rec.packaging_code
                dr("DEPOSIT_PRICE") = rec.deposit_price
                dr("QTY") = rec.qty
                dr("ITEM_CATEGORY") = rec.item_category
                dr("ORIG_PRICE") = rec.orig_price
                dr("PRICE_CHG_COMMENT") = rec.price_chg_comment
                dr("RETAIL_PRICE") = rec.retail_price
                dr("ORIG_RETAIL_PRICE") = rec.orig_retail_price

                dt.Rows.Add(dr)
            Next
            Return dt
        End Get
    End Property

    Private sCo_odno As String
    Public Property co_odno() As String
        Get
            Return sCo_odno
        End Get
        Set(ByVal value As String)
            sCo_odno = value
            For Each rec As REC_ON_INV_CODTL In Me.GetValueList
                rec.co_odno = value
            Next
        End Set
    End Property

    Private iInv_no As Integer
    Public Property inv_no() As Integer
        Get
            Return iInv_no
        End Get
        Set(ByVal value As Integer)
            iInv_no = value
            For Each rec As REC_ON_INV_CODTL In Me.GetValueList
                rec.inv_no = value
            Next
        End Set
    End Property
    Sub New()
        MyBase.New()
        co_odno = ""
        inv_no = 0
    End Sub
    Sub New(ByVal sOrderNum As String, ByVal sCoInvType As String)
        Me.New()
        Dim dtcodtl As DataTable = DB.GetONINVCODTL(sOrderNum, sCoInvType)
        Dim rec As REC_ON_INV_CODTL = Nothing
        For Each dr As DataRow In dtcodtl.Rows
            rec = New REC_ON_INV_CODTL(dr)
            Me.Add(rec.cod_line, rec)
        Next
        If dtcodtl.Rows.Count > 0 Then
            co_odno = dtcodtl.Rows(0).Item("CO_ODNO")
            inv_no = dtcodtl.Rows(0).Item("INV_NO")
        End If
    End Sub

    Sub New(ByVal dtCodtl As DataTable)
        Me.New()
        Dim rec As REC_ON_INV_CODTL = Nothing
        For Each dr As DataRow In dtCodtl.Rows
            rec = New REC_ON_INV_CODTL(dr)
            Me.Add(rec.cod_line, rec)
        Next
        If dtCodtl.Rows.Count > 0 Then
            co_odno = dtCodtl.Rows(0).Item("CO_ODNO")
            inv_no = dtCodtl.Rows(0).Item("INV_NO")
        End If

    End Sub

    Public Overloads Sub Add(ByVal iCod_Line As Integer, ByVal obj As REC_ON_INV_CODTL)
        Try
            MyBase.Add(iCod_Line, obj)
        Catch ex As Exception
            Throw New Exception("REC_ON_INV_CODTL_LIST.ADD(" & iCod_Line & ", REC_ON_INV_CODTL) " & ex.Message.ToString)
        End Try

    End Sub

    Public Function GetOrderLine(ByVal cod_line As Integer) As REC_ON_INV_CODTL
        Return Item(cod_line)
    End Function

    Public Function GetOrderLine(ByVal item As String) As REC_ON_INV_CODTL
        Dim rec As REC_ON_INV_CODTL = Nothing
        For Each itm As REC_ON_INV_CODTL In Me.GetValueList
            If itm.item.Trim = item.Trim Then
                rec = itm
            End If
        Next
        Return rec
    End Function

    Public Sub Update(ByVal oracmd As OracleCommand)
        For Each rec As REC_ON_INV_CODTL In Me.GetValueList
            If rec.action = "A" Or rec.action = "U" Then
                rec.Update(oracmd)
            End If
        Next
    End Sub


End Class
