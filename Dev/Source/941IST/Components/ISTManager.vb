﻿Imports _941IST.IST_ORDER

Public Class ISTManager

    Public Shared Function GetISTList(ByVal co_odno As String) As DataTable
        Try
            Return DB.GetISTList(co_odno)
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Shared Function GetIST(ByVal co_odno As String) As IST_ORDER
        Return New IST_ORDER(co_odno)
    End Function

    Public Shared Function GetISTReport() As DataTable
        Try
            Return DB.GetISTReport()
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Shared Function CreateIST(ByVal IstFromWeb As IST_FROM_WEB) As IST_ORDER
        Dim IST As IST_ORDER

        Try
            If IST_STORES.IsValidStore(IstFromWeb.to_store) = False Then
                Throw New ISTException(1000, "Store " & IstFromWeb.to_store & " is not valid.")
            End If
            IST = New IST_ORDER()
            IST.action = "A"
            IST.IST_Type = "STORE STOCK"
            IST.to_store = IstFromWeb.to_store
            IST.order_reqd_dt = IstFromWeb.order_reqd_dt
            IST.submitted_by = IstFromWeb.submittedBy
            IST.status = IST_STATUS.NEW_EDIT
            IST.pu_order_fl = IstFromWeb.pu_order_fl
            IST.last_update_by = IstFromWeb.submittedBy
            IST.last_update_dt = Now
            IST.comments_line1 = IstFromWeb.comments1.Trim()
            IST.comments_line2 = IstFromWeb.comments2.Trim()

            If DB.CreateIST(IST) = False Then
                Throw New ISTException(1001, "There was a problem creating the IST in the database.")
            End If

            Return IST

        Catch ex As Exception
            Throw ex
        End Try

        Return Nothing

    End Function

    Public Shared Function UpdateIST(ByVal IstFromWeb As IST_FROM_WEB) As Boolean
        Dim IstOrder As IST_ORDER

        Try
            IstOrder = ISTManager.GetIST(IstFromWeb.co_odno)

            If IstFromWeb.to_store.Trim() <> IstOrder.to_store AndAlso IST_STORES.IsValidStore(IstFromWeb.to_store.Trim()) = False Then
                Throw New ISTException(1000, "Invalid store specified.")
            End If

            If IstOrder.to_store <> IstFromWeb.to_store OrElse
                IstOrder.order_reqd_dt <> IstFromWeb.order_reqd_dt OrElse
                IstOrder.comments_line1 <> IstFromWeb.comments1.Trim() OrElse
                IstOrder.comments_line2 <> IstFromWeb.comments2.Trim() OrElse
                IstOrder.pu_order_fl <> IstFromWeb.pu_order_fl Then

                IstOrder.action = "U"
                IstOrder.to_store = IstFromWeb.to_store
                IstOrder.order_reqd_dt = IstFromWeb.order_reqd_dt
                IstOrder.comments_line1 = IstFromWeb.comments1.Trim()
                IstOrder.comments_line2 = IstFromWeb.comments2.Trim()
                IstOrder.pu_order_fl = IstFromWeb.pu_order_fl
            End If

            If DB.UpdateIST(IstOrder, IstFromWeb) = False Then
                Throw New ISTException(1001, "There was a problem updating the IST order.")
            End If
        Catch ex As Exception
            Throw ex
        End Try

        Return True

    End Function

    Public Shared Function ResetIST(ByVal IstFromWeb As IST_FROM_WEB) As Boolean
        Dim IstOrder As IST_ORDER
        Dim result As Boolean

        Try
            IstOrder = ISTManager.GetIST(IstFromWeb.co_odno)

            If IstOrder.status.ToUpper.StartsWith("NEW") Then
                result = DB.CancelIST(IstOrder, IstFromWeb.submittedBy)
            Else
                result = DB.RevertToPreviousStateIST(IstOrder, IstFromWeb.submittedBy)
            End If

            If result = False Then
                Throw New ISTException(1001, "There was a problem resetting the IST order.")
            End If
        Catch ex As Exception
            Throw ex
        End Try

        Return result

    End Function

    Public Shared Function CancelIST(ByVal co_odno As String, ByVal submittedBy As String) As Boolean
        Dim IstOrder As IST_ORDER
        Dim result As Boolean

        Try
            IstOrder = ISTManager.GetIST(co_odno)

            result = DB.CancelIST(IstOrder, submittedBy)

            If result = False Then
                Throw New ISTException(1001, "There was a problem cancelling the IST order.")
            End If
        Catch ex As Exception
            Throw ex
        End Try

        Return result

    End Function

    Public Shared Function RecallIST(ByVal co_odno As String, ByVal submittedBy As String) As Boolean
        Dim IstOrder As IST_ORDER
        Dim result As Boolean
        Dim dt As DataTable

        Try
            'check if IST is recallable
            'it's possible it's been traffic planned (not in ON_COHDR any more) so deny recalling
            dt = DB.GetISTList(co_odno)
            If dt.Rows.Count = 0 OrElse dt.Rows(0).Item("RECALLABLE") = "N" Then
                Throw New ISTException(2001, "The IST cannot be recalled, it has already been traffic planned.")
            End If

            IstOrder = ISTManager.GetIST(co_odno)

            result = DB.RecallIST(IstOrder, submittedBy)

            If result = False Then
                Throw New ISTException(1001, "There was a problem recalling the IST order.")
            End If
        Catch ex As Exception
            Throw ex
        End Try

        Return result

    End Function

    Public Shared Function ISTRequestApproval(ByVal co_odno As String, ByVal submittedBy As String) As Boolean
        Dim IstOrder As IST_ORDER

        Try
            If Utils.IsApprovalRequired = False Then
                Return ISTApprove(co_odno, submittedBy, True)
            Else
                IstOrder = New IST_ORDER(co_odno)

                If IstOrder.Num_Of_Units = 0 Then
                    Throw New ISTException(50, "The order is empty, nothing to approve.")
                End If

                If DB.ISTRequestApproval(IstOrder, submittedBy) Then
                    Try
                        If ISTNotification.RequestApprovalNotification(submittedBy, IstOrder) = False Then
                            Throw New ISTException(100, "There was a problem sending the email notification.")
                        End If
                    Catch ex As Exception
                        'TODO: currently we eat the email exception, maybe we shouldn't???
                    End Try
                Else
                    Throw New ISTException(101, "There was a problem requesting approval for the order.")
                End If
            End If
        Catch ex As Exception
            Throw ex
        End Try

        Return True

    End Function

    Public Shared Function ISTApprove(ByVal co_odno As String, ByVal submittedBy As String, ByVal forceSubmit As Boolean) As Boolean
        Dim IstOrder As IST_ORDER

        Try
            IstOrder = New IST_ORDER(co_odno)

            If IstOrder.Num_Of_Units = 0 Then
                Throw New ISTException(50, "The order is empty, nothing to approve.")
            End If

            If DB.ISTApprove(IstOrder, submittedBy, forceSubmit) = False Then
                Throw New ISTException(100, "There was a problem submitting the order.")
            End If
        Catch ex As Exception
            Throw ex
        End Try

        Return True

    End Function

    Public Shared Function AddItem(ByVal co_odno As String, _
                                   ByVal item As String, _
                                   ByVal qty As Int32, _
                                   ByVal submittedBy As String, _
                                   ByVal insufficientQtyOverride As String) As Boolean
        Dim dt As DataTable
        Dim uncommittedQty As Int32
        Dim ist As IST_ORDER

        Try
            'make sure item is valid and exists
            Try
                'check if valid
                If String.IsNullOrEmpty(item) Then
                    Throw New ISTException(1002, "Item '" & item & "' is invalid.")
                End If
                item = item.Trim()

                'check if item exists
                dt = DB.GetItemInfo(item)
            Catch ex As Exception
                Throw ex
            End Try

            'make sure qty is valid
            Try
                uncommittedQty = DB.GetItemUncommittedQty(item)
                If qty <= 0 Then
                    Throw New ISTException(1003, "Quantity '" & qty.ToString() & "' is invalid.")
                ElseIf qty > uncommittedQty AndAlso insufficientQtyOverride <> "Y" Then
                    Throw New ISTException(1004, "Quantity '" & qty.ToString() & "' is greater than the uncommitted quantity.")
                End If
            Catch ex As Exception
                Throw ex
            End Try

            ist = GetIST(co_odno)

            If ist.itemExist(item) Then
                Throw New ISTException(1006, "Item '" & item & "' already exists in order " & ist.co_odno & ".")
            Else
                Try
                    DB.AddItem(ist, item, qty, submittedBy)
                Catch ex As Exception
                    Throw New ISTException(1007, "There was a problem adding the item to the order.", ex.InnerException)
                End Try
            End If
        Catch ex As Exception
            Throw ex
        End Try

        Return True

    End Function

    Public Shared Function UpdateItem(ByVal co_odno As String, _
                                      ByVal cod_line As String, _
                                      ByVal item As String, _
                                      ByVal qty As Int32, _
                                      ByVal origQty As Int32, _
                                      ByVal submittedBy As String, _
                                      ByVal insufficientQtyOverride As String) As Boolean
        Dim uncommittedQty As Int32
        Dim ist As IST_ORDER

        Try

            Try
                uncommittedQty = DB.GetItemUncommittedQty(item)
                If qty <> 0 AndAlso uncommittedQty < (qty - origQty) AndAlso insufficientQtyOverride <> "Y" Then
                    Throw New ISTException(1004, "Quantity '" & qty.ToString() & "' is greater than the uncommitted quantity.")
                End If

                ist = GetIST(co_odno)

                DB.UpdateItem(ist, cod_line, qty, submittedBy)

            Catch ex As Exception
                Throw ex
            End Try

        Catch ex As Exception
            Throw ex
        End Try

        Return True

    End Function



End Class
