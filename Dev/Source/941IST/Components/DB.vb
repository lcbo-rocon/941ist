﻿Imports System.Data.OleDb
Imports System.Data.OracleClient

Public Class DB

    ' Public Shared Function Connection(ByVal connKey As String) As OleDbConnection
    '     Return New OleDbConnection(ConfigurationManager.ConnectionStrings(ConfigurationManager.AppSettings.Item("ENVIRONMENT") & "_" & connKey).ConnectionString)
    ' End Function

    Public Shared Function Connection(ByVal connKey As String) As IDbConnection
        Return New OracleConnection(ConfigurationManager.ConnectionStrings(ConfigurationManager.AppSettings.Item("ENVIRONMENT") & "_" & connKey).ConnectionString)
    End Function

    Public Shared Function GetISTList(ByVal co_odno As String) As DataTable
        Dim dt As DataTable
        Dim sql As StringBuilder = New StringBuilder()

        Try
            sql.Append("SELECT ")
            sql.Append("IH.INV_NO, ")
            sql.Append("IH.CO_ODNO, ")
            sql.Append("IH.TO_STORE, ")
            sql.Append("IH.ORIG_INV_NO, ")
            sql.Append("IH.ORIG_CO_ODNO, ")
            sql.Append("TO_CHAR(IH.ORDER_DT_TM, 'YYYYMMDD') AS ORDER_DT_TM, ")
            sql.Append("TO_CHAR(IH.ORDER_REQD_DT, 'YYYYMMDD') AS ORDER_REQD_DT, ")
            sql.Append("IH.IST_TYPE, ")
            sql.Append("IH.PU_ORDER_FL, ")
            sql.Append("IH.NUM_OF_PROD, ")
            sql.Append("IH.NUM_OF_UNITS, ")
            sql.Append("IH.SUBMITTED_BY, ")
            sql.Append("IH.AUTHORIZED_BY, ")
            sql.Append("IH.COMMENTS_LINE1, ")
            sql.Append("IH.COMMENTS_LINE2, ")
            sql.Append("IH.STATUS, ")
            sql.Append("IH.REQUEST_AUTH_USER, ")
            sql.Append("TO_CHAR(IH.REQUEST_AUTH_DT, 'YYYYMMDD') AS REQUEST_AUTH_DT, ")
            sql.Append("TO_CHAR(IH.LAST_SAVED_DT, 'YYYYMMDD') AS LAST_SAVED_DT, ")
            sql.Append("IH.LAST_SAVED_BY, ")
            sql.Append("TO_CHAR(IH.LAST_UPDATE_DT, 'YYYYMMDD') AS LAST_UPDATE_DT, ")
            sql.Append("IH.LAST_UPDATE_BY, ")
            sql.Append("'E' as ACTION, ")
            sql.Append("DECODE(IH.STATUS,'SUBMITTED','true','INVOICED','true','false') as APPROVED, ")
            sql.Append("CASE WHEN IH.STATUS = 'SAVED' AND SYSDATE - LAST_SAVED_DT > " & ConfigurationManager.AppSettings.Get("SAVED_THRESHOLD_DAYS") & " THEN 1 ELSE 0 END as SAVED_OLD, ")
            sql.Append("CASE WHEN IH.STATUS = 'APPROVAL REQUESTED' AND SYSDATE - REQUEST_AUTH_DT > " & ConfigurationManager.AppSettings.Get("AUTH_THRESHOLD_DAYS") & " THEN 1 ELSE 0 END as REQ_AUTH_OLD, ")
            sql.Append("TO_CHAR(IH.ASN_SENT, 'YYYYMMDD') AS ASN_SENT, ")
            sql.Append("IH.T1_STATUS, ")
            sql.Append("CASE WHEN OCH.CO_STATUS IS NOT NULL THEN 'Y' ELSE 'N' END AS RECALLABLE ")
            sql.Append("FROM DBO.LCBO_IST_HDR IH LEFT OUTER JOIN ")
            sql.Append("DBO.ON_COHDR OCH ON OCH.CO_ODNO = IH.CO_ODNO AND OCH.CO_STATUS = 'O' ")
            If String.IsNullOrEmpty(co_odno) = False Then
                sql.Append("WHERE IH.CO_ODNO = '" & co_odno & "' AND IH.ORDER_DT_TM > (SYSDATE - 1000) ")
            Else
                sql.Append("WHERE IH.ORDER_DT_TM > (SYSDATE - 1000) ")
            End If
            sql.Append("ORDER BY CO_ODNO DESC")

            dt = GetDataTable("941", sql.ToString())

        Catch ex As Exception
            Throw New Exception("DB.GetISTList()" & vbNewLine & _
                                "qry:" & sql.ToString() & vbNewLine & _
                                "ERROR:" & ex.Message)
        End Try

        Return dt

    End Function

    Public Shared Function GetStoreListFromSTNA() As DataTable
        Dim dt As DataTable
        Dim sql As StringBuilder = New StringBuilder()

        'Return GetData("STNA", "SELECT LCTN_NO, NVL(LCTN_NM, ' ') AS LCTN_NM FROM POSJOB.STORE WHERE LCTN_STAT_CD <> 'C' ORDER BY LCTN_NO")

        Try
            sql.Append("Select lpad(LCTN_NO,5,'0') as LCTN_NO ")
            sql.Append(", LCTN_CLAS_CD ")
            sql.Append(", LCTN_DSTT_CD ")
            sql.Append(", LCTN_TYPE_CD ")
            sql.Append(", LCTN_STAT_CD ")
            sql.Append(", LCTN_NM ")
            sql.Append(", LCTN_ADDR_LINE_1 ")
            sql.Append(", LCTN_ADDR_LINE_2 ")
            sql.Append(", CITY_NM ")
            sql.Append(", PSTL_CD ")
            sql.Append(", NTSX_DESC ")
            sql.Append(", PHNE_AREA_CD ")
            sql.Append(", PHNE_1_NO ")
            sql.Append(", PHNE_2_NO ")
            sql.Append(", LCBO_WEB AS WEB_DISPLAY ")
            sql.Append("from POSJOB.STORE ")
            sql.Append("WHERE LCTN_STAT_CD IN ('A','I') ")
            'sql.Append("AND LCTN_TYPE_CD NOT IN ('D') ")
            sql.Append("ORDER BY LCTN_NO ASC ")

            dt = GetDataTable("STNA", sql.ToString())

        Catch ex As Exception
            Throw New Exception("DB.GetStoreListFromSTNA()" & vbNewLine & _
                                "qry:" & sql.ToString() & vbNewLine & _
                                "ERROR:" & ex.Message)
        End Try

        Return dt

    End Function

    Public Shared Function GetStoreListFromCustomers() As DataTable
        Dim dt As New DataTable
        Dim sql As StringBuilder = New StringBuilder()

        sql.Append("Select ")
        sql.Append("SUBSTR(TRIM(CU_NO), -5,5) AS CU_NO ")
        sql.Append(", PROV_SH ")
        sql.Append(", CU_NAME ")
        sql.Append(", ADD1_SH ")
        sql.Append(", ADD2_SH ")
        sql.Append(", CITY_SH ")
        sql.Append(", COUNTRY ")
        sql.Append(", POST_CODE ")
        sql.Append(", PHONE_NU ")
        sql.Append(", FAX_NU ")
        sql.Append(", CONTACT ")
        sql.Append(", EMAIL ")
        sql.Append(", CU_TYPE ")
        sql.Append(", ACTIVE_FL ")
        sql.Append(", CREDIT_AMOUNT ")
        sql.Append(", PU_ORDER_FL ")
        sql.Append("from DBO.ON_CUST_MAS where cu_type = 'IST' ")
        sql.Append("order by CU_NO")

        Try
            dt = GetDataTable("941", sql.ToString())

        Catch ex As Exception
            Throw New Exception("DB.GetStoreListFromCustomers()" & vbNewLine & _
                                "qry:" & sql.ToString() & vbNewLine & _
                                "ERROR:" & ex.Message)
        End Try

        Return dt

    End Function

    Public Shared Function GetLCBOCalendar() As DataTable
        Dim dt As New DataTable
        Dim sql As StringBuilder = New StringBuilder()

        sql.Append("Select ")
        sql.Append("CUT_OFF_DATETIME ")
        sql.Append(", DELIVERY_DATE ")
        sql.Append(", STATUS ")
        sql.Append("from DBO.LCBO_CALENDAR ")
        sql.Append("ORDER BY DELIVERY_DATE DESC, CUT_OFF_DATETIME ASC")

        Try
            dt = GetDataTable("941", sql.ToString())

        Catch ex As Exception
            Throw New Exception("DB.GetLCBOCalendar()" & vbNewLine & _
                                "qry:" & sql.ToString() & vbNewLine & _
                                "ERROR:" & ex.Message)
        End Try

        Return dt
    End Function

    Public Shared Function GetNewOrderNo(ByRef cmd As OracleCommand) As String
        Dim iNewOrderNum As Integer = 0
        Dim iOrderNum As Integer = 0

        Dim sQry As String = "UPDATE DBO.ON_CUST_ORDER_SEQ SET SEQ_NO=SEQ_NO"

        Try
            cmd.CommandText = sQry
            cmd.Parameters.Clear()
            If cmd.ExecuteNonQuery <= 0 Then
                Throw New Exception("ERROR LOCKING DBO.RETURN_ORDER_NO_TBL FOR UPDATING")
            End If

            sQry = "SELECT SEQ_NO FROM DBO.ON_CUST_ORDER_SEQ WHERE ROWNUM = 1"
            cmd.CommandText = sQry
            cmd.Parameters.Clear()
            Dim oraReader As OracleDataReader = cmd.ExecuteReader

            While oraReader.Read
                iOrderNum = oraReader("SEQ_NO")
            End While
            oraReader.Close()

            iNewOrderNum = iOrderNum + 1
            If iNewOrderNum >= 9999999 Then
                iNewOrderNum = 100000
            End If

            sQry = "Update DBO.ON_CUST_ORDER_SEQ SET SEQ_NO = " & iNewOrderNum
            cmd.CommandText = sQry
            cmd.Parameters.Clear()

            If cmd.ExecuteNonQuery <= 0 Then
                Throw New Exception("Error updating new order number!")
            End If

        Catch ex As Exception
            Throw New Exception("DALROC.getNewOrderNo error->" & ex.Message.ToString)
        End Try


        'this was returning the following:      Return Right("0000000" & iOrderNum.ToString & "0", 8)  which procudes an Oracle exception as primary key already exists
        iNewOrderNum = Right("0000000" & iOrderNum.ToString() & "0", 8)
        Return iNewOrderNum

    End Function

    Shared Function getNewInvNo(ByRef cmd As OracleCommand) As Integer
        Dim iNewInvNo As Integer = 0
        Dim iInvNo As Integer = 0

        Dim sQry As String = "UPDATE DBO.ON_INV_SEQ SET INV_SEQ_NO=INV_SEQ_NO"

        Try
            cmd.CommandText = sQry
            cmd.Parameters.Clear()
            If cmd.ExecuteNonQuery <= 0 Then
                Throw New Exception("ERROR LOCKING DBO.ON_INV_SEQ FOR UPDATING")
            End If

            sQry = "SELECT INV_SEQ_NO FROM DBO.ON_INV_SEQ WHERE ROWNUM = 1"
            cmd.CommandText = sQry
            cmd.Parameters.Clear()
            Dim oraReader As OracleDataReader = cmd.ExecuteReader

            While oraReader.Read
                iInvNo = oraReader("INV_SEQ_NO")
            End While
            oraReader.Close()

            iNewInvNo = iInvNo + 1
            If iNewInvNo >= 9999999999 Then
                iNewInvNo = 1
            End If

            sQry = "Update DBO.ON_INV_SEQ SET INV_SEQ_NO = " & iNewInvNo

            cmd.CommandText = sQry
            cmd.Parameters.Clear()
            If cmd.ExecuteNonQuery <= 0 Then
                Throw New Exception("Error updaing new return order number!")
            End If

        Catch ex As Exception
            Throw New Exception("DALROC.getNewInvNo error->" & ex.Message.ToString)
        End Try

        Return iInvNo
    End Function

    Public Shared Function GetItemInfo(ByVal item As String) As DataTable
        Dim dt As DataTable
        Dim sql As StringBuilder = New StringBuilder()

        Try
            item = item.Trim.PadRight(20)

            sql.Append("SELECT T1.ITEM, ")
            sql.Append("T1.UNIT_PRICE, ")
            sql.Append("NVL(T1.CASE_SZ,1) AS CASE_SZ, ")
            sql.Append("T1.ITEM_CATEGORY, ")
            sql.Append("T1.IM_DESC, ")
            sql.Append("T1.PACK_FACTOR, ")
            sql.Append("T1.PACKAGING_CODE, ")
            sql.Append("T2.ATTR6 AS DEPOSIT_PRICE ")
            sql.Append("FROM DBO.IMMAS T1 INNER JOIN DBO.ON_CUST_CATALOG T2 ON T1.ITEM = T2.ITEM AND T1.PACKAGING_CODE = T2.PACKAGING_CODE ")
            sql.Append("WHERE T1.ITEM = '")
            sql.Append(item)
            sql.Append("'")

            dt = GetDataTable("941", sql.ToString())
            If dt.Rows.Count > 0 Then
                Return dt
            Else
                Throw New Exception("Item " & item.Trim() & " not found.")
            End If
        Catch ex As Exception
            Throw ex
        End Try

    End Function

    Public Shared Function GetItemUncommittedQty(ByVal item As String) As Integer
        Dim dsOH As DataSet
        Dim dsRR As DataSet
        Dim OH As Int32
        Dim reserved As Int32
        Dim requested As Int32

        Try
            dsOH = GetItemOnHandQty(item)
            dsRR = GetItemReserveRequested(item)

            OH = dsOH.Tables(0).Rows(0).Item(0)
            reserved = dsRR.Tables(0).Rows(0).Item(1)
            requested = dsRR.Tables(0).Rows(0).Item(2)

            Return OH - reserved - requested
        Catch ex As Exception
            Throw ex
        End Try

    End Function

    Public Shared Function GetItemOnHandQty(ByVal item As String) As DataSet
        Dim ds As DataSet
        Dim sql As StringBuilder = New StringBuilder()

        Try
            item = Convert.ToInt32(item).ToString().PadRight(20)

            sql.Append("SELECT NVL(SUM(QUANTITY),0) AS QOH FROM ")
            sql.Append("( SELECT RTRIM(RP.LO_LOCA,' ') AS LO_LOCA ")
            sql.Append(", RTRIM(RP.TICKET_ID,' ')      AS TICKET_ID ")
            sql.Append(", RTRIM(RP.TICKET_TYPE,' ')    AS TICKET_TYPE ")
            sql.Append(", RP.QUANTITY                  AS QUANTITY ")
            sql.Append(", 0                            AS RESERVE_FOR_PICKING_QTY ")
            sql.Append(", RTRIM(IM.UNIT_OF_MEASURE,' ') AS UNIT_OF_MEASURE ")
            sql.Append(", RTRIM(RP.LOT_NUMBER,' ')     AS LOT_NUMBER ")
            sql.Append("FROM RECEIVED_PALLET RP ")
            sql.Append(", CONTAINER CONT ")
            sql.Append(", IMMAS IM ")
            sql.Append(", PALLET_DESC PD ")
            sql.Append("WHERE RP.ITEM = ")
            sql.Append("'" & item & "'")
            sql.Append("AND RP.PACKAGING_CODE = ' ' ")
            sql.Append("AND PD.TICKET_ID = RP.TICKET_ID ")
            sql.Append("AND IM.ITEM = RP.ITEM ")
            sql.Append("AND IM.PACKAGING_CODE = RP.PACKAGING_CODE ")
            sql.Append("AND RP.TICKET_TYPE != 'DEL' ")
            sql.Append("And RP.QUARANTINE_Y_OR_N = 'N' ")
            sql.Append("And RP.BLOCK_Y_OR_N      = 'N' ")
            sql.Append("AND RP.TICKET_ID = PD.TICKET_ID ")
            sql.Append("AND NVL(RP.CONTAINER_ID, RPAD(' ', 19, NULL)) = CONT.CONTAINER_ID (+) ")
            sql.Append("        UNION ")
            sql.Append("SELECT RTRIM(LOCA.LO_LOCA,' ') ")
            sql.Append(", RTRIM(LOCA.TICKET_ID,' ') ")
            sql.Append(", RTRIM(LOCA.TICKET_TYPE,' ') ")
            sql.Append(", LOCA.QUANTITY ")
            sql.Append(", LOCA.RESERVE_FOR_PICKING_QTY ")
            sql.Append(", RTRIM(IM.UNIT_OF_MEASURE,' ') ")
            sql.Append(", RTRIM(LOCA.LOT_NUMBER,' ') ")
            sql.Append("FROM LOCA_PALLET LOCA ")
            sql.Append(", CONTAINER CONT ")
            sql.Append(", IMMAS IM ")
            sql.Append(", PALLET_DESC PD ")
            sql.Append("WHERE LOCA.ITEM = ")
            sql.Append("'" & item & "'")
            sql.Append("AND LOCA.PACKAGING_CODE = ' ' ")
            sql.Append("AND PD.TICKET_ID = LOCA.TICKET_ID ")
            sql.Append("AND IM.ITEM = LOCA.ITEM ")
            sql.Append("AND IM.PACKAGING_CODE = LOCA.PACKAGING_CODE ")
            sql.Append("AND LOCA.TICKET_TYPE != 'DEL' ")
            sql.Append("And LOCA.QUARANTINE_Y_OR_N = 'N' ")
            sql.Append("And LOCA.BLOCK_Y_OR_N      = 'N' ")
            sql.Append("AND LOCA.TICKET_ID = PD.TICKET_ID ")
            sql.Append("AND NVL(LOCA.CONTAINER_ID, RPAD(' ', 19, NULL)) = CONT.CONTAINER_ID (+) )  T1 ")
            sql.Append("WHERE T1.TICKET_ID NOT IN (SELECT T1.TICKET_ID FROM SA_TICKET T2 WHERE T1.TICKET_ID = T2.TICKET_ID) ")

            ds = GetData("941", sql.ToString())

        Catch ex As Exception
            Throw New Exception("DB.GetItemOnHandQty()" & vbNewLine &
                                "qry:" & sql.ToString() & vbNewLine &
                                "ERROR:" & ex.Message)
        End Try

        Return ds

    End Function

    Public Shared Function GetItemReserveRequested(ByVal item As String) As DataSet
        Dim ds As DataSet
        Dim sql As StringBuilder = New StringBuilder()

        Try
            item = Convert.ToInt32(item).ToString().PadRight(20)

            sql.Append("SELECT ITEM, SUM(RESERVE) AS RESERVE, SUM(REQUESTED) AS REQUESTED, SUM(OVERPICKED) AS OVERPICKED FROM ( ")
            sql.Append("Select item ")
            sql.Append(",  NVL(SUM( NVL( d.PLANNED_QTY, 0 ) - NVL( d.PICKED_QTY, 0 ) ), 0 ) AS RESERVE ")
            sql.Append(",  NVL(SUM( NVL( d.COD_QTY, 0 ) - NVL( d.PLANNED_QTY   , 0 ) ), 0 ) as REQUESTED ")
            sql.Append(",  NVL(SUM((NVL(d.PICKED_QTY,0) - NVL(d.PLANNED_QTY,0)) * (1-SIGN(1-SIGN( NVL(d.PICKED_QTY,0) - NVL(d.PLANNED_QTY,0) )))), 0 ) AS OVERPICKED ")
            sql.Append("FROM CODTL d, COHDR h ")
            sql.Append("WHERE d.ITEM = ")
            sql.Append("'" & item & "'")
            sql.Append("AND d.PACKAGING_CODE  = ' ' ")
            sql.Append("AND d.CO_ODNO = h.CO_ODNO ")
            sql.Append("AND NVL(d.DELETED_FL, 'N') != 'R' ")
            ' sql.Append("AND CO_STATUS IN ('O','R','S') ")
            sql.Append("AND ( h.END_OF_ORDER_FL  = 'N' or h.END_OF_ORDER_FL is null ) ")
            sql.Append("GROUP BY ITEM ")
            sql.Append("UNION ALL ")
            sql.Append("Select item ")
            sql.Append(",0 AS RESERVE ")
            sql.Append(",  NVL(SUM( NVL( d.TOTAL_ALLOCATED_QTY, 0 )  ), 0 ) as REQUESTED ")
            sql.Append(",  0 AS OVERPICKED ")
            sql.Append("FROM ON_CODTL d, ON_COHDR h ")
            sql.Append("WHERE d.ITEM = ")
            sql.Append("'" & item & "'")
            sql.Append("AND d.PACKAGING_CODE  = ' ' ")
            sql.Append("AND h.CO_STATUS IN ('O','W') ")
            sql.Append("AND d.CO_ODNO = h.CO_ODNO ")
            sql.Append("AND NVL(d.DELETED_FL, 'x') != 'R' ")
            sql.Append("GROUP BY ITEM ")
            sql.Append("UNION ALL ")
            sql.Append("SELECT ")
            sql.Append("'" & item & "'")
            sql.Append(",0 ")
            sql.Append(",0 ")
            sql.Append(",0 ")
            sql.Append("FROM DUAL ")
            sql.Append(") ")
            sql.Append("GROUP BY ITEM ")

            ds = GetData("941", sql.ToString())

        Catch ex As Exception
            Throw New Exception("DB.GetItemReserveRequested()" & vbNewLine &
                                "qry:" & sql.ToString() & vbNewLine &
                                "ERROR:" & ex.Message)
        End Try

        Return ds

    End Function

    Public Shared Function GetOnInvoiceInfo(ByVal inv_no As Integer) As DataRow
        Dim ds As DataSet
        Dim sql As StringBuilder = New StringBuilder()

        Try
            sql.Append("Select INV_NO ")
            sql.Append(", INV_CLI ")
            sql.Append(", SITE_CD ")
            sql.Append(", INV_DT ")
            sql.Append(", INV_STATUS ")
            sql.Append(", INV_TYPE ")
            sql.Append(", TRAN_DT ")
            sql.Append(", AMOUNT_PAID ")
            sql.Append(", PAID_FL ")
            sql.Append(", INV_TRANS_NO ")
            sql.Append(", PAID_DT ")
            sql.Append(", INV_CU_NO ")
            sql.Append(", ARCHIVE_DT ")
            sql.Append(", 'E' as ACTION ")
            sql.Append("from DBO.ON_INVOICE ")
            sql.Append("WHERE INV_NO = ")
            sql.Append(inv_no)

            ds = GetData("941", sql.ToString())
            If ds.Tables.Count = 0 Then
                Return Nothing
            Else
                Return ds.Tables(0).Rows(0)
            End If

        Catch ex As Exception
            Throw New Exception("DB.GetONInvoiceInfo(" & inv_no & ")" & vbNewLine &
                                "qry:" & sql.ToString() & vbNewLine &
                                "ERROR:" & ex.Message)
        End Try

    End Function

    Public Shared Function GetOnInvCoInfo(ByVal co_odno As String, Optional ByVal cotype As String = "E") As DataRow
        Dim dt As DataTable
        Dim dr As DataRow = Nothing
        Dim sql As StringBuilder = New StringBuilder()

        Try
            sql.Append("Select INV_NO ")
            sql.Append(", CO_ODNO ")
            sql.Append(", CO_INV_TYPE ")
            sql.Append(", INV_AMT ")
            sql.Append(", DELIVERY_CHG ")
            sql.Append(", DELIVERY_TAX1 ")
            sql.Append(", DELIVERY_TAX2 ")
            sql.Append(", TTL_FULL_CASES ")
            sql.Append(", TTL_PART_CASES ")
            sql.Append(", INV_DT ")
            sql.Append(", UPLOAD1_DT ")
            sql.Append(", UPLOAD2_DT ")
            sql.Append(", EM_NO ")
            sql.Append(", ACC_POST_DT ")
            sql.Append(", DELIVERY2_CHG ")
            sql.Append(", DELIVERY2_TAX1 ")
            sql.Append(", DELIVERY2_TAX2 ")
            sql.Append(", DELIVERY2_POST_DT ")
            sql.Append(", LINK_CO_ODNO  ")
            sql.Append(", 'E' AS ACTION ")
            sql.Append("from DBO.ON_INV_CO ")
            sql.Append("WHERE co_odno = '")
            sql.Append(co_odno.Trim.PadRight(16))
            sql.Append("'")
            sql.Append("AND co_inv_type = '" & cotype & "'")


            dt = GetDataTable("941", sql.ToString())
            If dt.Rows.Count >= 0 Then
                dr = dt.Rows(0)
            End If

        Catch ex As Exception
            Throw New Exception("DB.GetOnInvCoInfo(" & co_odno & ")" & vbNewLine &
                                "qry:" & sql.ToString() & vbNewLine &
                                "ERROR:" & ex.Message)
        End Try

        Return dr

    End Function

    Public Shared Function GetONINVCODTL(ByVal sOrderNum As String, ByVal sOrderType As String) As DataTable
        Dim dt As DataTable
        Dim sql As StringBuilder = New StringBuilder()

        Try
            sql.Append("Select ITM.INV_NO          ")
            sql.Append(", ITM.CO_ODNO              ")
            sql.Append(", ITM.CO_INV_TYPE          ")
            sql.Append(", ITM.COD_LINE             ")
            sql.Append(", ITM.PRICE                ")
            sql.Append(", ITM.EXTD_PRICE           ")
            sql.Append(", ITM.TTL_CASES            ")
            sql.Append(", ITM.TTL_UNITS            ")
            sql.Append(", ITM.ITEM                 ")
            sql.Append(", ITM.PACKAGING_CODE       ")
            sql.Append(", ITM.DEPOSIT_PRICE        ")
            sql.Append(", ITM.QTY                  ")
            sql.Append(", NVL(ITM.ITEM_CATEGORY,' ') AS ITEM_CATEGORY ")
            sql.Append(", ITM.ORIG_PRICE           ")
            sql.Append(", NVL(ITM.PRICE_CHG_COMMENT,' ') AS PRICE_CHG_COMMENT ")
            sql.Append(", NVL(ITM.RETAIL_PRICE,0) as RETAIL_PRICE  ")
            sql.Append(", NVL(ITM.ORIG_RETAIL_PRICE,0) AS ORIG_RETAIL_PRICE ")
            sql.Append(", 'E' AS ACTION ")
            sql.Append("FROM DBO.ON_INV_CODTL ITM  ")
            sql.Append("WHERE ITM.CO_ODNO = '")
            sql.Append(sOrderNum)
            sql.Append("' ")
            sql.Append("AND ITM.CO_INV_TYPE = '")
            sql.Append(sOrderType)
            sql.Append("' ")
            sql.Append("ORDER BY ITM.COD_LINE ASC ")


            dt = GetDataTable("941", sql.ToString())

        Catch ex As Exception
            Throw New Exception("DB.GetONINVCODTL(" & sOrderNum & "," & sOrderType & ")" & vbNewLine &
                                "Qry: " & sql.ToString() & vbNewLine &
                                "Error Msg: " & ex.Message.ToString)
        End Try

        Return dt
    End Function

    Public Shared Function GetCOHDRInfo(ByVal sOrderNum As String) As DataRow
        Dim dr As DataRow = Nothing
        Dim sql As StringBuilder = New StringBuilder()


        Try
            sql.Append("Select HDR.CO_ODNO ")
            sql.Append(", HDR.CO_STATUS ")
            sql.Append(", HDR.ROUTE_CODE ")
            sql.Append(", NVL(HDR.PRIOR_NAME,' ') AS PRIOR_NAME ")
            sql.Append(", HDR.CU_NO ")
            sql.Append(", HDR.SHIP_TO ")
            sql.Append(", HDR.ORDER_DT_TM ")
            sql.Append(", HDR.ORDER_REQD_DT ")
            sql.Append(", HDR.ORDER_TYPE ")
            sql.Append(", HDR.TRANSPORT_MODE ")
            sql.Append(", HDR.ORDER_CANCELED_Y_OR_N ")
            sql.Append(", HDR.STOP_SEQ_NO ")
            sql.Append(", NVL(HDR.COHDR_COMMENT ,' ') AS COHDR_COMMENT ")
            sql.Append(", HDR.END_OF_ORDER_FL ")
            sql.Append(", NVL(HDR.PAY_METHOD, ' ') AS PAY_METHOD ")
            sql.Append(", NVL(TO_CHAR(HDR.ORDER_DEL_DT, 'YYYY-MM-DD HH24:MI:SS'),' ') AS ORDER_DEL_DT ")
            sql.Append(", HDR.TOTAL_CASES ")
            sql.Append(", NVL(HDR.TRAFFIC_REF_NO,' ') AS TRAFFIC_REF_NO ")
            sql.Append(", HDR.ENTRY_FL ")
            sql.Append(", HDR.PU_ORDER_FL ")
            sql.Append(", HDR.DELIVERY_FL ")
            sql.Append(", 'E' AS ACTION ")
            sql.Append("from DBO.COHDR HDR ")
            sql.Append("WHERE HDR.CO_ODNO = ")
            sql.Append("'" & sOrderNum & "' ")

            dr = GetDataRow("941", sql.ToString())

            If dr Is Nothing Then
                Throw New Exception("No records found!")
            End If

        Catch ex As Exception
            Throw New Exception("DB.GetCOHDRInfo(" & sOrderNum & ") " & vbNewLine &
                                "qry: " & sql.ToString() & vbNewLine &
                                "ERROR Msg: " & ex.Message.ToString)
        End Try

        Return dr
    End Function

    Public Shared Function getCODTL(ByVal sOrderNum As String) As DataTable
        Dim dt As DataTable
        Dim sql As StringBuilder = New StringBuilder()

        Try
            sql.Append("Select CO_ODNO ")
            sql.Append(", COD_LINE ")
            sql.Append(", ITEM ")
            sql.Append(", PICKED_QTY ")
            sql.Append(", PACKAGING_CODE ")
            sql.Append(", UNIT_PRICE ")
            sql.Append(", COD_QTY ")
            sql.Append(", TOTAL_ALLOCATED_QTY ")
            sql.Append(", PLANNED_QTY ")
            sql.Append(", USE_BLOCKED_STOCK_FL ")
            sql.Append(", SHIPPED_QTY ")
            sql.Append(", DELETED_FL ")
            sql.Append(", PICK_PLANNED_Y_OR_N ")
            sql.Append(", PICKED_CASES ")
            sql.Append(", PICKED_UNITS ")
            sql.Append(", 'E' AS ACTION ")
            sql.Append("from DBO.CODTL ")
            sql.Append("WHERE co_odno = '")
            sql.Append(sOrderNum.Trim.PadRight(16))
            sql.Append("' ")
            sql.Append("ORDER BY COD_LINE ASC")

            dt = GetDataTable("941", sql.ToString())

        Catch ex As Exception
            Throw New Exception("DB.GetCODTL(" & sOrderNum & ")" & vbNewLine &
                                "Qry: " & sql.ToString() & vbNewLine &
                                "Error Msg: " & ex.Message.ToString)
        End Try

        Return dt
    End Function

    Public Shared Function GetONCOHDRInfo(ByVal sOrderNum As String) As DataRow
        Dim dt As DataTable
        Dim dr As DataRow
        Dim sql As StringBuilder = New StringBuilder()

        Try
            sql.Append("Select CO_ODNO ")
            sql.Append(", CO_STATUS ")
            sql.Append(", ROUTE_CODE ")
            sql.Append(", ROUTE_STOP ")
            sql.Append(", PRIOR_NAME ")
            sql.Append(", CU_NO ")
            sql.Append(", SHIP_TO ")
            sql.Append(", ORDER_DT_TM ")
            sql.Append(", ORDER_REQD_DT ")
            sql.Append(", SHIP_REQD_DT ")
            sql.Append(", ORDER_TYPE ")
            sql.Append(", CARRIER ")
            sql.Append(", TIMESTAMP ")
            sql.Append(", ORDER_DEL_DT ")
            sql.Append(", TRAFFIC_REF_NO ")
            sql.Append(", SITE ")
            sql.Append(", PALLET_CUBE ")
            sql.Append(", HOST_ODNO ")
            sql.Append(", CLI_NO ")
            sql.Append(", SERVICE_LEVEL ")
            sql.Append(", EMPLOYEE_NUMBER ")
            sql.Append(", PAY_METHOD ")
            sql.Append(", CU_NAME ")
            sql.Append(", PU_ORDER_FL ")
            sql.Append(", DELIVERY_FL ")
            sql.Append(", ENTRY_FL  ")
            sql.Append(", 'E' AS ACTION ")
            sql.Append("from DBO.ON_COHDR ")
            sql.Append("WHERE CO_ODNO = '")
            sql.Append(sOrderNum.Trim.PadRight(16))
            sql.Append("'")

            dt = GetDataTable("941", sql.ToString())

            If dt.Rows.Count > 0 Then
                dr = dt.Rows(0)
            Else
                Throw New Exception("No records found!")
            End If

        Catch ex As Exception
            Throw New Exception("DB.GetONCOHDRInfo(" & sOrderNum & ") " & vbNewLine &
                                "qry: " & sql.ToString() & vbNewLine &
                                "ERROR Msg: " & ex.Message.ToString)
        End Try

        Return dr
    End Function

    Public Shared Function GetONCODTL(ByVal sOrderNum As String) As DataTable
        Dim dt As DataTable
        Dim sql As StringBuilder = New StringBuilder()

        Try
            sql.Append("Select CO_ODNO ")
            sql.Append(", COD_LINE ")
            sql.Append(", ITEM ")
            sql.Append(", PACKAGING_CODE ")
            sql.Append(", COD_QTY ")
            sql.Append(", LOT_NUMBER ")
            sql.Append(", USE_BLOCKED_STOCK_FL ")
            sql.Append(", TIMESTAMP ")
            sql.Append(", EXPIRY_DT ")
            sql.Append(", ROUTE_CODE ")
            sql.Append(", ROUTE_STOP ")
            sql.Append(", CARRIER ")
            sql.Append(", ORDER_REQD_DT ")
            sql.Append(", SHIP_REQD_DT ")
            sql.Append(", SITE ")
            sql.Append(", TOTAL_ALLOCATED_QTY ")
            sql.Append(", SINGLE_IM_WEIGHT ")
            sql.Append(", SINGLE_IM_CUBE ")
            sql.Append(", PICKED_QTY ")
            sql.Append(", DELETED_FL ")
            sql.Append(", 'E' AS ACTION ")
            sql.Append(", UNIT_PRICE from DBO.ON_CODTL ")
            sql.Append("WHERE co_odno = '")
            sql.Append(sOrderNum.Trim.PadRight(16))
            sql.Append("'")
            sql.Append("ORDER BY COD_LINE ASC")

            dt = GetDataTable("941", sql.ToString())

        Catch ex As Exception
            Throw New Exception("DB.GetONCODTL(" & sOrderNum & ")" & vbNewLine &
                                "Qry: " & sql.ToString() & vbNewLine &
                                "Error Msg: " & ex.Message.ToString)
        End Try

        Return dt
    End Function

    Public Shared Function GetISTHeaderInfo(ByVal co_odno As String) As DataTable
        Dim dt As New DataTable
        Dim sql As StringBuilder = New StringBuilder()

        Try
            sql.Append("Select INV_NO ")
            sql.Append(", CO_ODNO ")
            sql.Append(", TO_STORE ")
            sql.Append(", ORIG_INV_NO ")
            sql.Append(", ORIG_CO_ODNO ")
            sql.Append(", ORDER_DT_TM ")
            sql.Append(", ORDER_REQD_DT ")
            sql.Append(", IST_TYPE ")
            sql.Append(", PU_ORDER_FL")
            sql.Append(", NUM_OF_PROD ")
            sql.Append(", NUM_OF_UNITS ")
            sql.Append(", SUBMITTED_BY ")
            sql.Append(", AUTHORIZED_BY ")
            sql.Append(", COMMENTS_LINE1 ")
            sql.Append(", COMMENTS_LINE2 ")
            sql.Append(", STATUS ")
            sql.Append(", REQUEST_AUTH_USER ")
            sql.Append(", REQUEST_AUTH_DT ")
            sql.Append(", LAST_SAVED_DT ")
            sql.Append(", LAST_SAVED_BY ")
            sql.Append(", LAST_UPDATE_DT ")
            sql.Append(", LAST_UPDATE_BY ")
            sql.Append(",'E' as ACTION ")
            sql.Append(", DECODE(STATUS,'SUBMITTED','true','INVOICED','true','false') as APPROVED ")
            sql.Append("from DBO.LCBO_IST_HDR ")
            sql.Append("WHERE CO_ODNO = '")
            sql.Append(co_odno)
            sql.Append("'")

            dt = GetDataTable("941", sql.ToString())

        Catch ex As Exception
            Throw New Exception("DB.GetISTHeaderInfo(" & co_odno & ")" & vbNewLine &
                                "qry:" & sql.ToString() & vbNewLine &
                                "ERROR:" & ex.Message)
        End Try

        Return dt
    End Function

    Public Shared Function GetISTDetails(ByVal co_odno As String) As DataTable
        Dim dt As New DataTable
        Dim sql As StringBuilder = New StringBuilder()

        Try
            sql.Append("Select INV_NO ")
            sql.Append(", CO_ODNO ")
            sql.Append(", COD_LINE ")
            sql.Append(", ITEM ")
            sql.Append(", ITEM_DESC ")
            sql.Append(", PACKAGING_CODE ")
            sql.Append(", ORIG_QTY ")
            sql.Append(", SAVED_COD_QTY ")
            sql.Append(", COD_QTY ")
            sql.Append(", DELETED_FL ")
            sql.Append(", UPDATE_BY ")
            sql.Append(", UPDATE_DT ")
            sql.Append("from DBO.LCBO_IST_DTL ")
            sql.Append("where co_odno = '")
            sql.Append(co_odno.Trim.PadRight(16))
            sql.Append("' ")
            sql.Append("order by cod_line asc ")

            dt = GetDataTable("941", sql.ToString())

        Catch ex As Exception
            Throw New Exception("DB.GetISTDetails(" & co_odno & ")" & vbNewLine &
                                "qry:" & sql.ToString() & vbNewLine &
                                "ERROR:" & ex.Message)
        End Try

        Return dt
    End Function

    Public Shared Function CreateIST(ByRef ist As IST_ORDER) As Boolean
        Dim trans As OracleTransaction
        Dim result As Boolean = False

        Using conn As IDbConnection = Connection("941")
            Try
                conn.Open()
                trans = conn.BeginTransaction()
                Try
                    Using cmd As New OracleCommand("SELECT SYSDATE FROM DUAL", conn, trans)
                        ist.co_odno = DB.GetNewOrderNo(cmd)
                        ist.inv_no = DB.getNewInvNo(cmd)
                        ist.Update(cmd)
                    End Using
                    trans.Commit()
                    result = True
                Catch ex As Exception
                    trans.Rollback()
                    Throw ex
                End Try
            Catch ex As Exception
                Throw ex
            End Try
        End Using

        Return result
    End Function

    Public Shared Function UpdateIST(ByRef ist As IST_ORDER, ByVal istFromWeb As IST_FROM_WEB) As Boolean
        Dim trans As OracleTransaction
        Dim result As Boolean = False

        Using conn As IDbConnection = Connection("941")
            Try
                conn.Open()
                trans = conn.BeginTransaction()
                Try
                    Using cmd As New OracleCommand("SELECT SYSDATE FROM DUAL", conn, trans)
                        'update the item info
                        For Each itm As IST_FROM_WEB.LINE In istFromWeb.items
                            ist.UpdateItem(itm.COD_LINE, itm.COD_QTY, istFromWeb.submittedBy, cmd)
                        Next

                        'update the header info
                        ist.SaveOrder(istFromWeb.submittedBy, cmd)
                    End Using
                    trans.Commit()
                    result = True
                Catch ex As Exception
                    trans.Rollback()
                    Throw ex
                End Try
            Catch ex As Exception
                Throw ex
            End Try
        End Using

        Return result
    End Function

    Public Shared Function RevertToPreviousStateIST(ByRef ist As IST_ORDER, ByVal submittedBy As String) As Boolean
        Dim trans As OracleTransaction
        Dim result As Boolean = False

        Using conn As IDbConnection = Connection("941")
            Try
                conn.Open()
                trans = conn.BeginTransaction()
                Try
                    Using cmd As New OracleCommand("SELECT SYSDATE FROM DUAL", conn, trans)
                        ist.RevertSaveOrder(submittedBy, cmd)
                    End Using
                    trans.Commit()
                    result = True
                Catch ex As Exception
                    trans.Rollback()
                    Throw ex
                End Try
            Catch ex As Exception
                Throw ex
            End Try
        End Using

        Return result
    End Function

    Public Shared Function CancelIST(ByRef ist As IST_ORDER, ByVal submittedBy As String) As Boolean
        Dim trans As OracleTransaction
        Dim result As Boolean = False

        Using conn As IDbConnection = Connection("941")
            Try
                conn.Open()
                trans = conn.BeginTransaction()
                Try
                    Using cmd As New OracleCommand("SELECT SYSDATE FROM DUAL", conn, trans)
                        ist.CancelOrder(submittedBy, cmd)
                    End Using
                    trans.Commit()
                    result = True
                Catch ex As Exception
                    trans.Rollback()
                    Throw ex
                End Try
            Catch ex As Exception
                Throw ex
            End Try
        End Using

        Return result
    End Function

    Public Shared Function RecallIST(ByRef ist As IST_ORDER, ByVal submittedBy As String) As Boolean
        Dim trans As OracleTransaction
        Dim result As Boolean = False

        Using conn As IDbConnection = Connection("941")
            Try
                conn.Open()
                trans = conn.BeginTransaction()
                Try
                    Using cmd As New OracleCommand("SELECT SYSDATE FROM DUAL", conn, trans)
                        ist.RecallOrder(submittedBy, cmd)
                    End Using
                    trans.Commit()
                    result = True
                Catch ex As Exception
                    trans.Rollback()
                    Throw ex
                End Try
            Catch ex As Exception
                Throw ex
            End Try
        End Using

        Return result
    End Function

    Public Shared Function ISTRequestApproval(ByRef ist As IST_ORDER, ByVal submittedBy As String) As Boolean
        Dim trans As OracleTransaction
        Dim result As Boolean = False

        Using conn As IDbConnection = Connection("941")
            Try
                conn.Open()
                trans = conn.BeginTransaction()
                Try
                    Using cmd As New OracleCommand("SELECT SYSDATE FROM DUAL", conn, trans)
                        ist.SaveOrder(submittedBy, cmd)
                        ist.RequestForAuth(submittedBy, cmd)
                    End Using
                    trans.Commit()
                    result = True
                Catch ex As Exception
                    trans.Rollback()
                    Throw ex
                End Try
            Catch ex As Exception
                Throw ex
            End Try
        End Using

        Return result
    End Function

    Public Shared Function ISTApprove(ByRef ist As IST_ORDER, ByVal submittedBy As String, ByVal forceSubmit As Boolean) As Boolean
        Dim trans As OracleTransaction
        Dim result As Boolean = False

        Using conn As IDbConnection = Connection("941")
            Try
                conn.Open()
                trans = conn.BeginTransaction()
                Try
                    Using cmd As New OracleCommand("SELECT SYSDATE FROM DUAL", conn, trans)
                        If forceSubmit Then
                            ist.SaveOrder(submittedBy, cmd)
                            ist.RequestForAuth(submittedBy, cmd)
                        End If
                        ist.SubmitOrder(submittedBy, cmd)
                    End Using
                    trans.Commit()
                    result = True
                Catch ex As Exception
                    trans.Rollback()
                    Throw ex
                End Try
            Catch ex As Exception
                Throw ex
            End Try
        End Using

        Return result
    End Function

    Public Shared Function AddItem(ByRef ist As IST_ORDER, ByVal item As String, ByVal qty As Int32, ByVal userId As String) As Integer
        Dim trans As OracleTransaction
        Dim result As Integer

        Using conn As IDbConnection = Connection("941")
            Try
                conn.Open()
                trans = conn.BeginTransaction()
                Try
                    Using cmd As New OracleCommand("SELECT SYSDATE FROM DUAL", conn, trans)
                        result = ist.AddItem(item, qty, userId, cmd)
                    End Using
                    trans.Commit()
                Catch ex As Exception
                    trans.Rollback()
                    Throw ex
                End Try
            Catch ex As Exception
                Throw ex
            End Try
        End Using

        Return result

    End Function

    Public Shared Function UpdateItem(ByRef ist As IST_ORDER, ByVal cod_line As String, ByVal qty As Int32, ByVal userId As String) As Integer
        Dim trans As OracleTransaction
        Dim result As Integer

        Using conn As IDbConnection = Connection("941")
            Try
                conn.Open()
                trans = conn.BeginTransaction()
                Try
                    Using cmd As New OracleCommand("SELECT SYSDATE FROM DUAL", conn, trans)
                        result = ist.UpdateItem(cod_line, qty, userId, cmd)
                    End Using
                    trans.Commit()
                Catch ex As Exception
                    trans.Rollback()
                    Throw ex
                End Try
            Catch ex As Exception
                Throw ex
            End Try
        End Using

        Return result

    End Function


#Region "REPORTS"

    Public Shared Function GetISTReport() As DataTable
        Dim dt As DataTable
        Dim sql As StringBuilder = New StringBuilder()

        Try
            sql.Append("SELECT ")
            sql.Append("INV_NO, ")
            sql.Append("LI.CO_ODNO, ")
            sql.Append("TO_STORE, ")
            sql.Append("ORIG_INV_NO, ")
            sql.Append("ORIG_CO_ODNO, ")
            sql.Append("TO_CHAR(LI.ORDER_DT_TM, 'YYYYMMDD') AS ORDER_DT_TM, ")
            sql.Append("TO_CHAR(LI.ORDER_DT_TM, 'YYYY') AS ORDER_YEAR, ")
            sql.Append("TO_CHAR(LI.ORDER_DT_TM, 'MM') AS ORDER_MONTH, ")
            sql.Append("TO_CHAR(ORDER_REQD_DT, 'YYYYMMDD') AS ORDER_REQD_DT, ")
            sql.Append("IST_TYPE, ")
            sql.Append("PU_ORDER_FL, ")
            sql.Append("NUM_OF_PROD, ")
            sql.Append("NUM_OF_UNITS, ")
            'sql.Append("SUBMITTED_BY, ")
            'sql.Append("AUTHORIZED_BY, ")
            'sql.Append("COMMENTS_LINE1, ")
            'sql.Append("COMMENTS_LINE2, ")
            sql.Append("STATUS, ")
            'sql.Append("REQUEST_AUTH_USER, ")
            'sql.Append("TO_CHAR(REQUEST_AUTH_DT, 'YYYYMMDD') AS REQUEST_AUTH_DT, ")
            'sql.Append("TO_CHAR(LAST_SAVED_DT, 'YYYYMMDD') AS LAST_SAVED_DT, ")
            'sql.Append("LAST_SAVED_BY, ")
            sql.Append("TO_CHAR(LAST_UPDATE_DT, 'YYYYMMDD') AS LAST_UPDATE_DT, ")
            'sql.Append("LAST_UPDATE_BY, ")
            sql.Append("'E' as ACTION, ")
            'sql.Append("DECODE(STATUS,'SUBMITTED','true','INVOICED','true','false') as APPROVED, ")
            sql.Append("TO_CHAR(ASN_SENT, 'YYYYMMDD') AS ASN_SENT, ")
            sql.Append("T1_STATUS, ")
            sql.Append("TRAFFIC_PLANNED, ")
            sql.Append("PICK_PLANNED, ")
            sql.Append("EORD, ")
            sql.Append("INVOICED ")
            sql.Append("FROM DBO.LCBO_IST_HDR LI LEFT OUTER JOIN")
            sql.Append("( ")
            sql.Append("SELECT ")
            sql.Append("IST.*, ")
            sql.Append("CASE WHEN INV.CO_ODNO IS NULL THEN 'N' ELSE 'Y' END AS INVOICED ")
            sql.Append("FROM ")
            sql.Append("( ")
            sql.Append("select  ")
            sql.Append("co_odno, ")
            sql.Append("co_status, ")
            sql.Append("cu_no, ")
            sql.Append("order_dt_tm, ")
            sql.Append("'N' AS traffic_planned, ")
            sql.Append("'N' AS pick_planned, ")
            sql.Append("'N' AS eord ")
            sql.Append("from DBO.on_cohdr ")
            sql.Append("where ")
            sql.Append("ORDER_TYPE = 'IST' ")
            sql.Append("and order_dt_tm > (sysdate - 365) ")
            sql.Append("UNION ")
            sql.Append("select  ")
            sql.Append("co_odno, ")
            sql.Append("co_status, ")
            sql.Append("cu_no, ")
            sql.Append("order_dt_tm, ")
            sql.Append("'Y' AS traffic_planned, ")
            sql.Append("CASE WHEN co_status = 'C' THEN 'Y' ELSE 'N' END AS pick_planned, ")
            sql.Append("CASE WHEN co_status = 'C' AND END_OF_ORDER_FL = 'Y' THEN 'Y' ELSE 'N' END AS eord ")
            sql.Append("from DBO.cohdr ")
            sql.Append("where ")
            sql.Append("ORDER_TYPE = 'IST' ")
            sql.Append("and order_dt_tm > (sysdate - 365) ")
            sql.Append(") IST LEFT OUTER JOIN ")
            sql.Append("ON_INV_CO INV ON INV.CO_ODNO = IST.CO_ODNO AND INV.CO_INV_TYPE = 'A' ")
            sql.Append(") ORDERS ON ORDERS.CO_ODNO = LI.CO_ODNO ")
            sql.Append("ORDER BY LI.ORDER_DT_TM DESC ")

            dt = GetDataTable("941", sql.ToString())

        Catch ex As Exception
            Throw New Exception("DB.GetReportData()" & vbNewLine &
                                "qry:" & sql.ToString() & vbNewLine &
                                "ERROR:" & ex.Message)
        End Try

        Return dt

    End Function

#End Region

#Region "HELPERS"
    Public Shared Function GetData(ByVal connKey As String, ByVal sql As String) As DataSet
        Dim ds As New DataSet()
        Try
            Using conn As IDbConnection = Connection(connKey)
                conn.Open()
                Using cmd As New OracleCommand(sql, conn)
                    Using da As New OracleDataAdapter(cmd)
                        da.Fill(ds)
                    End Using
                End Using
            End Using
        Catch ex As Exception
            Throw ex
        End Try

        Return ds
    End Function

    Public Shared Function GetDataTable(ByVal connKey As String, ByVal sql As String) As DataTable
        Dim dt As DataTable = New DataTable()

        Try
            Using conn As IDbConnection = Connection(connKey)
                conn.Open()
                Using cmd As New OracleCommand(sql, conn)
                    Using da As New OracleDataAdapter(cmd)
                        da.Fill(dt)
                    End Using
                End Using
            End Using
        Catch ex As Exception
            Throw ex
        End Try

        Return dt

    End Function

    Public Shared Function GetDataRow(ByVal connKey As String, ByVal sql As String) As DataRow
        Dim dt As DataTable
        Dim dr As DataRow = Nothing

        Try
            dt = GetDataTable(connKey, sql)

            If dt.Rows.Count > 0 Then
                dr = dt.Rows(0)
            End If
        Catch ex As Exception
            Throw ex
        End Try

        Return dr

    End Function
#End Region


End Class
