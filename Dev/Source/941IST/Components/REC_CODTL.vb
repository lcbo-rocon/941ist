Imports Microsoft.VisualBasic
Imports System.Collections
Imports System.Data.OleDb
Imports System.Data
Imports System.Data.OracleClient

Public Class REC_CODTL
    Implements IComparable

    ''' <summary>
    ''' CO_ODNO,CHAR,16,,N,,
    ''' </summary>
    ''' <remarks></remarks>
    Private sCo_Odno As String
    Public Property co_odno() As String
        Get
            Return sCo_Odno
        End Get
        Set(ByVal value As String)
            sCo_Odno = value
        End Set
    End Property

    ''' <summary>
    ''' COD_LINE,NUMBER,10,,N,,
    ''' </summary>
    ''' <remarks></remarks>
    Private iCod_Line As Integer
    Public Property cod_line() As Integer
        Get
            Return iCod_Line
        End Get
        Set(ByVal value As Integer)
            iCod_Line = value
        End Set
    End Property

    ''' <summary>
    ''' ITEM,CHAR,20,,N,,
    ''' </summary>
    ''' <remarks></remarks>
    Private sItem As String
    Public Property item() As String
        Get
            Return sItem
        End Get
        Set(ByVal value As String)
            sItem = value
        End Set
    End Property

    ''' <summary>
    ''' PICKED_QTY,NUMBER,10,,N,,0
    ''' </summary>
    ''' <remarks></remarks>
    Private iPicked_Qty As Integer
    Public Property picked_qty() As Integer
        Get
            Return iPicked_Qty
        End Get
        Set(ByVal value As Integer)
            iPicked_Qty = value
        End Set
    End Property

    ' PACKAGING_CODE,CHAR,1,,N,,' '

    ''' <summary>
    ''' UNIT_PRICE,NUMBER,14,2,Y,,
    ''' </summary>
    ''' <remarks></remarks>
    Private dUnit_Price As Decimal
    Public Property Unit_Price() As Decimal
        Get
            Return dUnit_Price
        End Get
        Set(ByVal value As Decimal)
            dUnit_Price = value
        End Set
    End Property

    ''' <summary>
    ''' COD_QTY,NUMBER,10,,N,,
    ''' </summary>
    ''' <remarks></remarks>
    Private iCod_Qty As Integer
    Public Property cod_qty() As Integer
        Get
            Return iCod_Qty
        End Get
        Set(ByVal value As Integer)
            iCod_Qty = value
        End Set
    End Property

    ''' <summary>
    ''' TOTAL_ALLOCATED_QTY,NUMBER,10,,N,,0
    ''' </summary>
    ''' <remarks></remarks>
    Private iTotal_Allocated_Qty As Integer
    Public Property total_allocated_qty() As Integer
        Get
            Return iTotal_Allocated_Qty
        End Get
        Set(ByVal value As Integer)
            iTotal_Allocated_Qty = value
        End Set
    End Property

    ''' <summary>
    ''' PLANNED_QTY,NUMBER,10,,N,,0
    ''' </summary>
    ''' <remarks></remarks>
    Private iPlanned_Qty As Integer
    Public Property planned_qty() As Integer
        Get
            Return iPlanned_Qty
        End Get
        Set(ByVal value As Integer)
            iPlanned_Qty = value
        End Set
    End Property

    'LOT_NUMBER,CHAR,15,,Y,,

    ''' <summary>
    ''' default to 'N'
    ''' </summary>
    ''' <remarks></remarks>
    Private sUse_Blocked_Stock_Fl As String
    Public Property use_blocked_stock_fl() As String
        Get
            Return sUse_Blocked_Stock_Fl
        End Get
        Set(ByVal value As String)
            sUse_Blocked_Stock_Fl = value
        End Set
    End Property

    ''' <summary>
    ''' SHIPPED_QTY,NUMBER,10,,N,,0
    ''' default 0
    ''' </summary>
    ''' <remarks></remarks>
    Private iShipped_Qty As Integer
    Public Property shipped_qty() As Integer
        Get
            Return iShipped_Qty
        End Get
        Set(ByVal value As Integer)
            iShipped_Qty = value
        End Set
    End Property

    'TIMESTAMP,DATE,7,,Y,,
    'FILL_FROM_FL,CHAR,1,,Y,,'N'
    'FILL_FROM_ITEM,CHAR,20,,Y,,
    'FILL_FROM_PACKAGING_CODE,CHAR,1,,Y,,' '
    'FILL_FROM_QTY,NUMBER,10,,Y,,
    'SALES_UNIT_OF_MEASURE,CHAR,4,,Y,,
    'CONVERSION_FACTOR,NUMBER,14,4,Y,,
    'PROCESS_REQUIRED_FL,CHAR,1,,Y,,
    'FILL_FROM_DESC_1,VARCHAR2,36,,Y,,
    'FILL_FROM_DESC_2,VARCHAR2,36,,Y,,
    'QTY_ORDERED_IN_PIECES,NUMBER,10,,Y,,
    'TEST_CERTIFICATE,CHAR,1,,Y,,
    'PICKED_ACTUAL_WEIGHT,NUMBER,14,4,Y,,0.0
    'LINE_CANCELED_Y_OR_N,CHAR,1,,Y,,
    'COD_AMOUNT,NUMBER,10,,Y,,
    'AUDIT_CODE,CHAR,1,,Y,,' '
    'COUNTRY_OF_ORGIN,VARCHAR2,20,,Y,,
    'OWNERSHIP_CODE,VARCHAR2,10,,Y,,
    'PRICE_CODE,VARCHAR2,1,,Y,,
    'MERCHANDISE_CLASS,VARCHAR2,1,,Y,,
    'PRICE_SYMBOL,VARCHAR2,1,,Y,,
    'SALES_DISCOUNT,VARCHAR2,2,,Y,,
    'AUDIT_REASON_CODE,VARCHAR2,4,,Y,,
    'LINE_COMMENT,VARCHAR2,20,,Y,,
    'AUDIT_REQUESTED_FL,CHAR,1,,Y,,'N'
    'EXPIRY_DT,DATE,7,,Y,,
    'UNCOMMITTED_QTY,NUMBER,10,,Y,,

    ''' <summary>
    ''' Default 'N'
    ''' DELETED_FL,CHAR,1,,Y,,
    ''' </summary>
    ''' <remarks></remarks>
    Private sDeleted_Fl As String
    Public Property deleted_fl() As String
        Get
            Return sDeleted_Fl
        End Get
        Set(ByVal value As String)
            sDeleted_Fl = value
        End Set
    End Property

    ''' <summary>
    ''' Default is 'Y'
    ''' If freight forwarded then 'N'
    ''' </summary>
    ''' <remarks></remarks>
    Private sPick_Planned_Y_or_N As String
    Public Property pick_planned_y_or_n() As String
        Get
            Return sPick_Planned_Y_or_N
        End Get
        Set(ByVal value As String)
            sPick_Planned_Y_or_N = value
        End Set
    End Property
    'STOCK_ROOM_NO,NUMBER,10,,Y,,
    'ACTIVE_PICK_PLAN_NU,NUMBER,10,,Y,,
    'SUBSTITUTE_FOR_LINE,NUMBER,10,,Y,,
    'NO_SUBS_FL,CHAR,1,,Y,,
    'SUBSTITUTE_FOR_ITEM,CHAR,20,,Y,,
    'SUBSTITUTE_FOR_PC,CHAR,1,,Y,,
    'NET_QTY,NUMBER,10,,Y,,
    'CROSSDOCK_PO_FL,CHAR,1,,N,,'N'
    'ALLOC_CD,CHAR,10,,Y,,

    ''' <summary>
    ''' PICKED_CASES,NUMBER,10,,Y,,0
    ''' </summary>
    ''' <remarks></remarks>
    Private iPicked_Cases As Integer
    Public Property picked_cases() As Integer
        Get
            Return iPicked_Cases
        End Get
        Set(ByVal value As Integer)
            iPicked_Cases = value
        End Set
    End Property

    ''' <summary>
    ''' PICKED_UNITS,NUMBER,10,,Y,,0
    ''' </summary>
    ''' <remarks></remarks>
    Private iPicked_Units As Integer
    Public Property picked_units() As Integer
        Get
            Return iPicked_Units
        End Get
        Set(ByVal value As Integer)
            iPicked_Units = value
        End Set
    End Property


    Private sAction As String
    Public Property action() As String
        Get
            Return sAction
        End Get
        Set(ByVal value As String)
            sAction = value
        End Set
    End Property

    Sub New()
        co_odno = ""
        cod_line = 0
        item = ""
        picked_qty = 0
        Unit_Price = 0
        cod_qty = 0
        total_allocated_qty = 0
        planned_qty = 0
        use_blocked_stock_fl = "N"
        shipped_qty = 0
        deleted_fl = "N"
        pick_planned_y_or_n = ""
        picked_cases = 0
        picked_units = 0
        action = "N"
    End Sub
    Sub New(ByVal oc As REC_ON_CODTL)
        Me.New()
        co_odno = oc.co_odno
        cod_line = oc.cod_line
        item = oc.item
        picked_qty = oc.picked_qty
        Unit_Price = oc.unit_price
        cod_qty = oc.cod_qty
        total_allocated_qty = oc.total_allocated_qty
        deleted_fl = oc.deleted_fl
    End Sub
    Sub New(ByVal dr As DataRow)
        Me.New()
        SetInfo(dr)
    End Sub
    Sub SetInfo(ByVal dr As DataRow)
        If IsDBNull(dr("CO_ODNO")) = False Then
            co_odno = dr("CO_ODNO")
        End If

        If IsDBNull(dr("COD_LINE")) = False Then
            cod_line = dr("COD_LINE")
        End If

        If IsDBNull(dr("ITEM")) = False Then
            item = dr("ITEM")
        End If

        If IsDBNull(dr("PICKED_QTY")) = False Then
            picked_qty = dr("PICKED_QTY")
        End If

        If IsDBNull(dr("UNIT_PRICE")) = False Then
            Unit_Price = dr("UNIT_PRICE")
        End If

        If IsDBNull(dr("COD_LINE")) = False Then
            cod_qty = dr("COD_QTY")
        End If

        If IsDBNull(dr("TOTAL_ALLOCATED_QTY")) = False Then
            total_allocated_qty = dr("TOTAL_ALLOCATED_QTY")
        End If

        If IsDBNull(dr("PLANNED_QTY")) = False Then
            planned_qty = dr("PLANNED_QTY")
        End If

        If IsDBNull(dr("USE_BLOCKED_STOCK_FL")) = False Then
            use_blocked_stock_fl = dr("USE_BLOCKED_STOCK_FL")
        End If

        If IsDBNull(dr("SHIPPED_QTY")) = False Then
            shipped_qty = dr("SHIPPED_QTY")
        End If

        If IsDBNull(dr("DELETED_FL")) = False Then
            deleted_fl = dr("DELETED_FL")
        End If

        If IsDBNull(dr("PICK_PLANNED_Y_OR_N")) = False Then
            pick_planned_y_or_n = dr("PICK_PLANNED_Y_OR_N")
        End If

        If IsDBNull(dr("PICKED_CASES")) = False Then
            picked_cases = dr("PICKED_CASES")
        End If

        If IsDBNull(dr("PICKED_UNITS")) = False Then
            picked_units = dr("PICKED_UNITS")
        End If

        If IsDBNull(dr("ACTION")) = False Then
            action = dr("ACTION")
        End If
    End Sub

    Public Function validate() As Boolean
        Dim bValid As Boolean = True

        bValid = (co_odno.Trim <> "")
        bValid = bValid AndAlso (cod_line > 0)
        bValid = bValid AndAlso (item.Trim <> "")
        bValid = bValid AndAlso (cod_qty >= 0)

        Return bValid

    End Function
    Sub Update(ByRef oraCmd As OracleCommand)
        Dim bValid As Boolean = validate()

        Dim sQry As String = ""
        If action = "A" Then
            sQry = "INSERT INTO DBO.CODTL (" & vbNewLine & _
                 "CO_ODNO " & vbNewLine & _
                 ",COD_LINE " & vbNewLine & _
                 ",ITEM " & vbNewLine & _
                 ",PICKED_QTY " & vbNewLine & _
                 ",UNIT_PRICE " & vbNewLine & _
                 ",COD_QTY " & vbNewLine & _
                 ",TOTAL_ALLOCATED_QTY " & vbNewLine & _
                 ",PLANNED_QTY " & vbNewLine & _
                 ",USE_BLOCKED_STOCK_FL " & vbNewLine & _
                 ",SHIPPED_QTY " & vbNewLine & _
                 ",DELETED_fL " & vbNewLine & _
                 ",PICK_PLANNED_Y_OR_N " & vbNewLine & _
                 ",PICKED_CASES " & vbNewLine & _
                 ",PICKED_UNITS " & vbNewLine & _
                 ") VALUES (" & vbNewLine & _
                 "'" & co_odno & "'" & vbNewLine & _
                 "," & cod_line & vbNewLine & _
                 ",'" & item & "'" & vbNewLine & _
                 "," & picked_qty & vbNewLine & _
                 "," & Unit_Price & vbNewLine & _
                 "," & cod_qty & vbNewLine & _
                 "," & total_allocated_qty & vbNewLine & _
                 "," & planned_qty & vbNewLine & _
                 ",'" & use_blocked_stock_fl & "'" & vbNewLine & _
                 "," & shipped_qty & vbNewLine & _
                 ",'" & deleted_fl & "'" & vbNewLine & _
                 ",'" & pick_planned_y_or_n & "'" & vbNewLine & _
                 "," & picked_cases & vbNewLine & _
                 "," & picked_units & vbNewLine & _
                 ")"
        ElseIf action = "U" Then
            sQry = "UPDATE INTO DBO.CODTL " & vbNewLine & _
                 " SET ITEM = '" & item & "'" & vbNewLine & _
                 ",PICKED_QTY = " & picked_qty & vbNewLine & _
                 ",UNIT_PRICE = " & Unit_Price & vbNewLine & _
                 ",COD_QTY = " & cod_qty & vbNewLine & _
                 ",TOTAL_ALLOCATED_QTY = " & total_allocated_qty & vbNewLine & _
                 ",PLANNED_QTY = " & planned_qty & vbNewLine & _
                 ",USE_BLOCKED_STOCK_FL = " & use_blocked_stock_fl & vbNewLine & _
                 ",SHIPPED_QTY = " & shipped_qty & vbNewLine & _
                 ",DELETED_fL = '" & deleted_fl & "'" & vbNewLine & _
                 ",PICK_PLANNED_Y_OR_N = '" & pick_planned_y_or_n & "'" & vbNewLine & _
                 ",PICKED_CASES = " & picked_cases & vbNewLine & _
                 ",PICKED_UNITS = " & picked_units & vbNewLine & _
                 " WHERE CO_ODNO = '" & co_odno.Trim.PadRight(16) & vbNewLine & _
                 " AND COD_LINE = " & cod_line
        Else
            'NOTHING TO DO
            Exit Sub
        End If

        Try
            If bValid = False Then
                Throw New Exception("Error validating CODTL record")
            End If
            oraCmd.CommandText = sQry
            oraCmd.Parameters.Clear()

            If oraCmd.ExecuteNonQuery <= 0 Then
                Throw New Exception("Error Inserting CODTL record query: " & sQry & "!")
            End If

        Catch ex As Exception
            Throw New Exception("CODTL.UPDATE->" & co_odno & "," & cod_line & ".Error Message:" & ex.Message.ToString)
        End Try

    End Sub

    Public Function CompareTo(ByVal obj As Object) As Integer Implements System.IComparable.CompareTo
        Dim b As REC_CODTL = obj
        Return cod_line.CompareTo(b.cod_line)
    End Function
End Class
'CO_ODNO,CHAR,16,,N,,
'COD_LINE,NUMBER,10,,N,,
'ITEM,CHAR,20,,N,,
'PICKED_QTY,NUMBER,10,,N,,0
'PACKAGING_CODE,CHAR,1,,N,,' '
'UNIT_PRICE,NUMBER,14,2,Y,,
'COD_QTY,NUMBER,10,,N,,
'TOTAL_ALLOCATED_QTY,NUMBER,10,,N,,0
'PLANNED_QTY,NUMBER,10,,N,,0
'LOT_NUMBER,CHAR,15,,Y,,
'USE_BLOCKED_STOCK_FL,CHAR,1,,Y,,'N'
'SHIPPED_QTY,NUMBER,10,,N,,0
'TIMESTAMP,DATE,7,,Y,,
'FILL_FROM_FL,CHAR,1,,Y,,'N'
'FILL_FROM_ITEM,CHAR,20,,Y,,
'FILL_FROM_PACKAGING_CODE,CHAR,1,,Y,,' '
'FILL_FROM_QTY,NUMBER,10,,Y,,
'SALES_UNIT_OF_MEASURE,CHAR,4,,Y,,
'CONVERSION_FACTOR,NUMBER,14,4,Y,,
'PROCESS_REQUIRED_FL,CHAR,1,,Y,,
'FILL_FROM_DESC_1,VARCHAR2,36,,Y,,
'FILL_FROM_DESC_2,VARCHAR2,36,,Y,,
'QTY_ORDERED_IN_PIECES,NUMBER,10,,Y,,
'TEST_CERTIFICATE,CHAR,1,,Y,,
'PICKED_ACTUAL_WEIGHT,NUMBER,14,4,Y,,0.0
'LINE_CANCELED_Y_OR_N,CHAR,1,,Y,,
'COD_AMOUNT,NUMBER,10,,Y,,
'AUDIT_CODE,CHAR,1,,Y,,' '
'COUNTRY_OF_ORGIN,VARCHAR2,20,,Y,,
'OWNERSHIP_CODE,VARCHAR2,10,,Y,,
'PRICE_CODE,VARCHAR2,1,,Y,,
'MERCHANDISE_CLASS,VARCHAR2,1,,Y,,
'PRICE_SYMBOL,VARCHAR2,1,,Y,,
'SALES_DISCOUNT,VARCHAR2,2,,Y,,
'AUDIT_REASON_CODE,VARCHAR2,4,,Y,,
'LINE_COMMENT,VARCHAR2,20,,Y,,
'AUDIT_REQUESTED_FL,CHAR,1,,Y,,'N'
'EXPIRY_DT,DATE,7,,Y,,
'UNCOMMITTED_QTY,NUMBER,10,,Y,,
'DELETED_FL,CHAR,1,,Y,,
'PICK_PLANNED_Y_OR_N,CHAR,1,,Y,,
'STOCK_ROOM_NO,NUMBER,10,,Y,,
'ACTIVE_PICK_PLAN_NU,NUMBER,10,,Y,,
'SUBSTITUTE_FOR_LINE,NUMBER,10,,Y,,
'NO_SUBS_FL,CHAR,1,,Y,,
'SUBSTITUTE_FOR_ITEM,CHAR,20,,Y,,
'SUBSTITUTE_FOR_PC,CHAR,1,,Y,,
'NET_QTY,NUMBER,10,,Y,,
'CROSSDOCK_PO_FL,CHAR,1,,N,,'N'
'ALLOC_CD,CHAR,10,,Y,,
'PICKED_CASES,NUMBER,10,,Y,,0
'PICKED_UNITS,NUMBER,10,,Y,,0
