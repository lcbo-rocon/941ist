﻿Public Class LCBO_CALENDAR

    Private Shared _cacheKey As String = "LCBO_CALENDAR"

    Public Shared Function GetCalendar() As DataTable
        Dim dtCalendar As DataTable

        Try
            dtCalendar = HttpContext.Current.Application(_cacheKey)

            If dtCalendar Is Nothing Then
                'cache needs to be populated
                dtCalendar = DB.GetLCBOCalendar()

                'get the calendar
                HttpContext.Current.Application(_cacheKey) = dtCalendar

            End If

            Return dtCalendar
        Catch ex As Exception
            Throw New ISTException(5000, "Error getting LCBO calendar information.")
        End Try

    End Function

    Public Shared Function GetDate(ByVal calendarDate As DateTime) As DataRow
        Dim dr() As DataRow

        Try
            dr = GetCalendar().Select("DELIVERY_DATE = #" & calendarDate.ToString("MM-dd-yyyy") & "# AND CUT_OFF_DATETIME >= #" & Now().ToString("MM-dd-yyyy HH:mm:ss") & "#")

            If dr.Count > 0 Then
                Return dr(0)
            Else
                Return Nothing
            End If
        Catch ex As Exception
            Throw New ISTException(5001, "LCBO_CALENDAR.GetDate" & ex.Message)
        End Try


    End Function

    Public Shared Function IsValidDate(ByVal calendarDate As DateTime) As Boolean
        Dim dr As DataRow

        Try
            dr = GetDate(calendarDate)
            Try
                If dr Is Nothing OrElse Convert.ToDateTime(dr("CUT_OFF_DATETIME")) < Now Then
                    Return False
                Else
                    Return True
                End If
            Catch ex As Exception
                Throw New ISTException(5000, "LCBO_CALENDAR.IsValidDate " & ex.Message)
            End Try
        Catch ex As ISTException
            Throw ex
        Catch ex As Exception
            Throw ex
        End Try

    End Function

End Class
