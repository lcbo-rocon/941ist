Imports Microsoft.VisualBasic
Imports System.Data
Imports System.Data.OleDb
Imports System.Data.OracleClient

Public Class REC_LCBO_IST_DTL
    Implements IComparable

    Private iInv_No As Integer
    Public Property inv_no() As Integer
        Get
            Return iInv_No
        End Get
        Set(ByVal value As Integer)
            iInv_No = value
        End Set
    End Property

    Private sCo_odno As String
    Public Property co_odno() As String
        Get
            Return sCo_odno
        End Get
        Set(ByVal value As String)
            sCo_odno = value
        End Set
    End Property

    Private iCOD_Line As Integer
    Public Property cod_line() As Integer
        Get
            Return iCOD_Line
        End Get
        Set(ByVal value As Integer)
            iCOD_Line = value
        End Set
    End Property

    Private sItem As String
    Public Property item() As String
        Get
            Return sItem
        End Get
        Set(ByVal value As String)
            sItem = value
        End Set
    End Property

    Private sItem_Desc As String
    Public Property item_desc() As String
        Get
            Return sItem_Desc
        End Get
        Set(ByVal value As String)
            sItem_Desc = value
        End Set
    End Property

    Private sPackaging_Code As String
    Public Property packaging_code() As String
        Get
            Return sPackaging_Code
        End Get
        Set(ByVal value As String)
            sPackaging_Code = value
        End Set
    End Property


    Private iOrig_Qty As Integer
    Public Property orig_qty() As Integer
        Get
            Return iOrig_Qty
        End Get
        Set(ByVal value As Integer)
            iOrig_Qty = value
        End Set
    End Property

    Private iSaved_COD_Qty As Integer
    Public Property saved_cod_qty() As Integer
        Get
            Return iSaved_COD_Qty
        End Get
        Set(ByVal value As Integer)
            iSaved_COD_Qty = value
        End Set
    End Property

    Private iCOD_Qty As Integer
    Public Property cod_qty() As Integer
        Get
            Return iCOD_Qty
        End Get
        Set(ByVal value As Integer)
            iCOD_Qty = value
        End Set
    End Property

    Private sDeleted_Fl As String
    Public Property deleted_fl() As String
        Get
            Return sDeleted_Fl
        End Get
        Set(ByVal value As String)
            sDeleted_Fl = value
        End Set
    End Property

    Private sLastSaved_By As String
    Public Property last_saved_by() As String
        Get
            Return sLastSaved_By
        End Get
        Set(ByVal value As String)
            sLastSaved_By = value
        End Set
    End Property

    Private dtLastSaved_Dt As DateTime
    Public Property last_saved_dt() As DateTime
        Get
            Return dtLastSaved_Dt
        End Get
        Set(ByVal value As DateTime)
            dtLastSaved_Dt = value
        End Set
    End Property


    Private sUpdate_By As String
    Public Property update_by() As String
        Get
            Return sUpdate_By
        End Get
        Set(ByVal value As String)
            sUpdate_By = value
        End Set
    End Property

    Private dtUpdate_Dt As DateTime
    Public Property update_dt() As DateTime
        Get
            Return dtUpdate_Dt
        End Get
        Set(ByVal value As DateTime)
            dtUpdate_Dt = value
        End Set
    End Property


    Private sAction As String
    Public Property action() As String
        Get
            Return sAction
        End Get
        Set(ByVal value As String)
            sAction = value
        End Set
    End Property

    Sub New()
        inv_no = 0
        co_odno = ""
        cod_line = 0
        item = ""
        item_desc = ""
        packaging_code = " "
        orig_qty = 0
        saved_cod_qty = 0
        cod_qty = 0
        deleted_fl = "N"
        update_by = ""
        update_dt = New DateTime
    End Sub

    Sub New(ByVal dtl As REC_LCBO_IST_DTL)
        MyBase.New()
        inv_no = dtl.inv_no
        co_odno = dtl.co_odno
        cod_line = dtl.cod_line
        item = dtl.item
        item_desc = dtl.item_desc
        packaging_code = dtl.packaging_code
        orig_qty = dtl.orig_qty
        saved_cod_qty = dtl.saved_cod_qty
        cod_qty = dtl.cod_qty
        deleted_fl = dtl.deleted_fl
        update_by = dtl.update_by
        update_dt = dtl.update_dt
    End Sub

    Sub New(ByVal dr As Data.DataRow)
        MyBase.New()
        SetInfo(dr)
    End Sub

    Sub SetInfo(ByVal dr As Data.DataRow)
        Try
            If IsDBNull(dr("inv_no")) = False Then
                inv_no = dr("inv_no")
            End If

            If IsDBNull(dr("co_odno")) = False Then
                co_odno = dr("co_odno")
            End If

            If IsDBNull(dr("cod_line")) = False Then
                cod_line = dr("cod_line")
            End If

            If IsDBNull(dr("item")) = False Then
                item = dr("item")
            End If

            If IsDBNull(dr("item_desc")) = False Then
                item_desc = dr("item_desc")
            End If

            If IsDBNull(dr("orig_qty")) = False Then
                orig_qty = dr("orig_qty")
            End If

            If IsDBNull(dr("packaging_code")) = False Then
                packaging_code = dr("packaging_code")
            End If

            If IsDBNull(dr("saved_cod_qty")) = False Then
                saved_cod_qty = dr("saved_cod_qty")
            End If

            If IsDBNull(dr("cod_qty")) = False Then
                cod_qty = dr("cod_qty")
            End If

            If IsDBNull(dr("deleted_fl")) = False Then
                deleted_fl = dr("deleted_fl")
            End If

            If IsDBNull(dr("update_by")) = False Then
                update_by = dr("update_by")
            End If

            If IsDBNull(dr("update_dt")) = False Then
                update_dt = dr("update_dt")
            End If

        Catch ex As Exception
            Throw New Exception("REC_LCBO_IST_DTL.SETINFO(dr)" & vbNewLine & _
                                "ERR: " & ex.Message.ToString)
        End Try
    End Sub

    Sub Update(ByVal oracmd As OracleCommand)

        Dim sQry As String = ""

        If action = "A" Then
            sQry = "INSERT INTO DBO.LCBO_IST_DTL (" & vbNewLine & _
                   " INV_NO " & vbNewLine & _
                   ",CO_ODNO " & vbNewLine & _
                   ",COD_LINE " & vbNewLine & _
                   ",ITEM " & vbNewLine & _
                   ",ITEM_DESC " & vbNewLine & _
                   ",PACKAGING_CODE " & vbNewLine & _
                   ",ORIG_QTY " & vbNewLine & _
                   ",SAVED_COD_QTY " & vbNewLine & _
                   ",COD_QTY " & vbNewLine & _
                   ",DELETED_FL " & vbNewLine & _
                   ",UPDATE_BY " & vbNewLine & _
                   ",UPDATE_DT " & vbNewLine & _
                   ") VALUES (" & vbNewLine & _
                   inv_no & vbNewLine & _
                   ",'" & co_odno & "'" & vbNewLine & _
                   "," & cod_line & vbNewLine & _
                   ",'" & item & "'" & vbNewLine & _
                   ",'" & item_desc.Replace("'", "''") & "'" & vbNewLine & _
                   ",'" & packaging_code & "'" & vbNewLine & _
                   "," & orig_qty & vbNewLine & _
                   "," & saved_cod_qty & vbNewLine & _
                   "," & cod_qty & vbNewLine & _
                   ",'" & deleted_fl & "'" & vbNewLine & _
                   ",'" & update_by & "'" & vbNewLine & _
                   ",SYSDATE)"
        ElseIf action = "U" Then
            sQry = "UPDATE DBO.LCBO_IST_DTL " & vbNewLine & _
                   " SET ORIG_QTY = " & orig_qty & vbNewLine & _
                   ",SAVED_COD_QTY = " & saved_cod_qty & vbNewLine & _
                   ",COD_QTY =  " & cod_qty & vbNewLine & _
                   ",DELETED_FL = '" & deleted_fl & "'" & vbNewLine & _
                   ",UPDATE_BY = '" & update_by & "'" & vbNewLine & _
                   ",UPDATE_DT = TO_DATE('" & update_dt.ToString("yyyy-MM-dd HH:mm:ss") & "','YYYY-MM-DD HH24:MI:SS')" & vbNewLine & _
                   " WHERE INV_NO = " & inv_no & vbNewLine & _
                   " AND CO_ODNO = '" & co_odno.Trim.PadRight(16) & "'" & vbNewLine & _
                   " AND COD_LINE = " & cod_line

        Else
            Exit Sub
        End If
        Try
            'If bValid = False Then
            '    Throw New Exception("Error validating CODTL record")
            'End If
            oracmd.CommandText = sQry
            oracmd.Parameters.Clear()

            If oracmd.ExecuteNonQuery <= 0 Then
                Throw New Exception("Error Inserting ON_CODTL record query: " & sQry & "!")
            End If

        Catch ex As Exception
            Throw New Exception("REC_LCBO_IST_DTL.Insert->" & co_odno & "," & cod_line & ".Error Message:" & ex.Message.ToString)
        End Try

    End Sub
    Public Function CompareTo(ByVal obj As Object) As Integer Implements System.IComparable.CompareTo

    End Function
End Class
