﻿Imports System.Net.Mail

Public Class ISTNotification

    Public Shared Function RequestApprovalNotification(ByVal sUser As String, ByVal objIST As IST_ORDER) As Boolean

        Dim sMsg As String = ""
        Dim userList As New SortedList
        Dim objSender As RSGBase.RSGAD = New RSGBase.RSGAD(sUser)
        Dim tmpUser As RSGBase.RSGAD
        Dim sFrom As String = "DSO_ISTAdmin@lcbo.com"
        Dim sTo As String = ""
        Dim sCC As String = ""
        Dim istApproverGroup As String = ConfigurationManager.AppSettings("IST_APPROVER_GROUP")
        Dim istApprovers As String = ConfigurationManager.AppSettings("IST_APPROVERS")

        Try
            If Utils.Environment <> "PROD" Then
                sMsg = "THIS IS A TEST .... FROM ENVIRONMENT " & Utils.Environment
            End If

            ''Get list of approves
            'If istApproverGroup.Trim <> "" Then
            '    For Each approverGroup As String In istApproverGroup.Split(",")
            '        For Each strUser As String In DAL_ROC.GetUserList(approverGroup).GetValueList
            '            Try
            '                If objSender.email.ToLower.Trim <> strUser.Trim.ToLower Then
            '                    userList.Add(strUser, strUser)
            '                End If
            '            Catch ex As Exception
            '                'ignore duplicates
            '            End Try
            '        Next
            '    Next
            'End If

            If istApprovers.Trim <> "" Then
                For Each strUser As String In ConfigurationManager.AppSettings("IST_APPROVERS").Split(",")
                    tmpUser = New RSGBase.RSGAD(strUser) 'DSOUser("LCBO\" & strUser.Replace("LCBO\", ""))
                    Try
                        If objSender.UserEmail.ToLower.Trim <> tmpUser.UserEmail.ToLower.Trim Then
                            userList.Add(tmpUser.UserEmail, tmpUser.UserEmail)
                        End If
                    Catch ex As Exception
                        'ignore duplicates
                    End Try

                Next
            End If


            sMsg = sMsg & vbNewLine & "Approval for DSO IST Order: " & objIST.co_odno.Trim & " have been requested." & vbNewLine & vbNewLine & _
            "Requested By: " & objSender.FirstName & " " & objSender.LastName & vbNewLine & _
            "Request Date: " & objIST.request_auth_dt & vbNewLine & _
            "IST Order No: " & objIST.co_odno & vbNewLine & _
            "IST Order Type: " & objIST.IST_Type & vbNewLine & _
            "To Store: " & objIST.to_store & vbNewLine & vbNewLine & _
            "Order Details: " & vbNewLine & _
            "-----------------------------------------------------------" & vbNewLine & _
            "ITEM                 ITEM DESCRIPTION               QTY    " & vbNewLine & _
            "-----------------------------------------------------------" & vbNewLine


            For Each DROW As DataRow In objIST.IST_DTL_List.Rows
                If DROW("SAVED_COD_QTY") > 0 Then
                    sMsg = sMsg & DROW("ITEM") & " " & DROW("ITEM_DESC") & "     " & DROW("SAVED_COD_QTY") & vbNewLine
                End If
            Next

            sMsg = sMsg & vbNewLine & vbNewLine & _
             "Please Logon to " & ConfigurationManager.AppSettings(Utils.Environment & "_" & "SITE") & " to approve this order." & vbNewLine & vbNewLine & _
             "DSO IST Admin "

            sCC = objSender.UserEmail
            For Each strUser As String In userList.GetValueList
                If sTo.Trim <> "" Then
                    sTo = sTo & ","
                End If
                sTo = sTo & strUser
            Next

            If sTo.Trim = "" Then
                Throw New ISTException(3001, "THERE ARE NO IST APPROVERS LISTED!!!")
            Else
                SendMail(sTo, sCC, sFrom, "DSO IST APPROVAL REQUEST", sMsg)
            End If

            Return True
        Catch ex As ISTException
            Throw ex
        Catch ex As Exception
            Throw New ISTException(3001, "There was a problem sending the approval email.", ex)
        End Try

    End Function

    Public Shared Function SendMail(ByVal sMailTo As String, _
                                    ByVal sCCTo As String, _
                                    ByVal sMailFrom As String, _
                                    ByVal sSubject As String, _
                                    ByVal sBody As String) As Boolean

        Dim mail_server As String = ConfigurationManager.AppSettings("MAIL_SERVER")
        Dim mail_port As String = ConfigurationManager.AppSettings("MAIL_PORT")
        Dim mail As New MailMessage
        Dim SMTP As System.Net.Mail.SmtpClient = Nothing

        Try
            SMTP = New SmtpClient(mail_server, mail_port)
            mail.From = New MailAddress(sMailFrom)
            mail.To.Add(sMailTo)
            mail.CC.Add(sCCTo)

            'set content
            mail.Subject = sSubject
            mail.Body = sBody

            SMTP.Send(mail)
        Catch ex As Exception
            Throw New ISTException(3000, "There was a problem sending the email.", ex)
        End Try

        Return True

    End Function

End Class
