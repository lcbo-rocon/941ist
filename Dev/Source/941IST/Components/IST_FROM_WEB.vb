﻿Imports System.Web.Script.Serialization
Imports System.Collections.Generic

<Serializable()> _
Public Class IST_FROM_WEB

    Public co_odno As String
    Public to_store As String
    Public order_reqd_dt As DateTime
    Public comments1 As String
    Public comments2 As String
    Public submittedBy As String
    Public status As String
    Public pu_order_fl As String
    Public items As New List(Of LINE)

    Public Class LINE
        Public COD_LINE As Int32
        Public ITEM As String
        Public COD_QTY As Int32
        Public ORIG_QTY As Int32
    End Class

End Class
