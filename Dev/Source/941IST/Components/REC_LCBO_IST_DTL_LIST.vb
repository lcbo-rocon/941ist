Imports Microsoft.VisualBasic
Imports System.Data
Imports System.Data.OleDb
Imports System.Data.OracleClient

Public Class REC_LCBO_IST_DTL_LIST
    Inherits SortedList

    Public ReadOnly Property IST_DTL_List() As DataTable
        Get
            Dim tbl As New DataTable
            With tbl.Columns
                .Add("INV_NO", GetType(System.Int32))
                .Add("CO_ODNO")
                .Add("COD_LINE", GetType(System.Int32))
                .Add("ITEM")
                .Add("ITEM_DESC")
                .Add("PACKAGING_CODE")
                .Add("ORIG_QTY", GetType(System.Int32))
                .Add("SAVED_COD_QTY", GetType(System.Int32))
                .Add("COD_QTY", GetType(System.Int32))
                .Add("DELETED_FL")
                .Add("UPDATE_BY")
                .Add("UPDATE_DT")
            End With

            Dim dr As DataRow = Nothing
            For Each dtl As REC_LCBO_IST_DTL In Me.GetValueList
                dr = tbl.NewRow
                dr("INV_NO") = dtl.inv_no
                dr("CO_ODNO") = dtl.co_odno
                dr("COD_LINE") = dtl.cod_line
                dr("ITEM") = dtl.item
                dr("ITEM_DESC") = dtl.item_desc
                dr("PACKAGING_CODE") = dtl.packaging_code
                dr("ORIG_QTY") = dtl.orig_qty
                dr("SAVED_COD_QTY") = dtl.saved_cod_qty
                dr("COD_QTY") = dtl.cod_qty
                dr("DELETED_FL") = dtl.deleted_fl
                dr("UPDATE_BY") = dtl.update_by
                dr("UPDATE_DT") = dtl.update_dt
                tbl.Rows.Add(dr)
            Next
            Return tbl
        End Get
    End Property


    Private sCo_odno As String
    Public Property co_odno() As String
        Get
            Return sCo_odno
        End Get
        Set(ByVal value As String)
            sCo_odno = value
            For Each rec As REC_LCBO_IST_DTL In Me.GetValueList
                rec.co_odno = value
            Next
        End Set
    End Property


    Private iInv_no As Integer
    Public Property inv_no() As Integer
        Get
            Return iInv_no
        End Get
        Set(ByVal value As Integer)
            iInv_no = value
            For Each rec As REC_LCBO_IST_DTL In Me.GetValueList
                rec.inv_no = value
            Next
        End Set
    End Property

    Sub New()
        MyBase.New()
        co_odno = ""
        inv_no = 0
    End Sub

    Sub New(ByVal sOrderNum As String)
        Me.New()
        Dim dtItems As DataTable = DB.GetISTDetails(sOrderNum)
        SetInfo(dtItems)
        If dtItems.Rows.Count > 0 Then
            co_odno = dtItems.Rows(0).Item("CO_ODNO")
            inv_no = dtItems.Rows(0).Item("INV_NO")
        End If
    End Sub

    Sub New(ByVal dtItems As DataTable)
        Me.New()
        SetInfo(dtItems)
    End Sub

    Sub SetInfo(ByVal dtItems As DataTable)
        Me.Clear()
        Dim rec As REC_LCBO_IST_DTL = Nothing
        For Each dr As DataRow In dtItems.Rows
            rec = New REC_LCBO_IST_DTL(dr)
            Me.Add(rec.cod_line, rec)
        Next
    End Sub

    Public Overloads Sub Add(ByVal iCod_Line As Integer, ByVal obj As REC_LCBO_IST_DTL)
        Try
            MyBase.Add(iCod_Line, obj)
        Catch ex As Exception
            Throw New Exception("REC_LCBO_IST_DTL_LIST.ADD(" & iCod_Line & ", REC_LCBO_IST_DTL) " & ex.Message.ToString)
        End Try

    End Sub

    Public Function GetOrderLine(ByVal cod_line As Integer) As REC_LCBO_IST_DTL
        Return Item(cod_line)
    End Function

    Public Function GetOrderLine(ByVal item As String) As REC_LCBO_IST_DTL
        Dim rec As REC_LCBO_IST_DTL = Nothing
        For Each itm As REC_LCBO_IST_DTL In Me.GetValueList
            If itm.item.Trim = item.Trim And itm.deleted_fl = "N" Then
                If itm.cod_qty <> 0 Then
                    rec = itm
                End If
            End If
        Next
        Return rec
    End Function

    Public Function nextOrderLine() As Integer
        Dim iLastCodLine As Integer = 0
        Dim oCount As Object = Me.IST_DTL_List.Compute("MAX(COD_LINE)", "1=1")

        If IsDBNull(oCount) = False Then
            iLastCodLine = oCount
        End If

        Return iLastCodLine + 1
    End Function

    Public Sub Update(ByVal oracmd As OracleCommand)
        For Each rec As REC_LCBO_IST_DTL In Me.GetValueList
            If rec.action = "A" Or rec.action = "U" Then
                rec.Update(oracmd)
            End If
        Next
    End Sub
End Class
