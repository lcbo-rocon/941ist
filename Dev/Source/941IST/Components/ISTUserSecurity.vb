﻿Imports System.Collections
Imports System.Collections.Generic

Public Class ISTUserSecurity
    Implements Interfaces.IRSGUserSecurity

    Private _UserId As String
    Private _UserList As List(Of String)
    Private _ApproverList As List(Of String)

    Public ReadOnly Property UserList() As List(Of String)
        Get
            Return _UserList
        End Get
    End Property

    Public ReadOnly Property ApproverList() As List(Of String)
        Get
            Return _ApproverList
        End Get
    End Property

    Public Sub New(ByVal userId As String)
        _UserId = userId
        _UserList = New List(Of String)
        _ApproverList = New List(Of String)

        LoadAccess()
    End Sub

    Public Function UserHasAccessToModule(ByVal ModuleName As String) As Boolean Implements Interfaces.IRSGUserSecurity.UserHasAccessToModule
        If ModuleName = "USER" And _UserList.Contains(_UserId) Then
            Return True
        ElseIf ModuleName = "APPROVER" And _ApproverList.Contains(_UserId) Then
            Return True
        End If
        Return False
    End Function


    Public Function IsAuthorized(ByVal ModuleName As String, ByVal redirect As Boolean) As Boolean Implements Interfaces.IRSGUserSecurity.IsAuthorized
        Dim validateAccess As Boolean = True
        Dim UserModuleAccess As Boolean = True

        If Not ConfigurationManager.AppSettings.Get("VALIDATEACCESS") Is Nothing AndAlso ConfigurationManager.AppSettings.Get("VALIDATEACCESS").ToUpper() = "FALSE" Then
            validateAccess = False
        End If

        UserModuleAccess = UserHasAccessToModule(ModuleName)

        If UserModuleAccess = False And validateAccess = True And redirect = True Then
            HttpContext.Current.Response.Redirect(ConfigurationManager.AppSettings("ACCESS_DENIED_URL"))
            HttpContext.Current.Response.End()
        End If

        Return UserModuleAccess

    End Function

    Private Sub LoadAccess()

        Try
            If ConfigurationManager.AppSettings.Get("IST_USERS") Is Nothing Then
                Throw New ISTException("1", "Error getting user access list.")
            End If

            _UserList = New List(Of String)(ConfigurationManager.AppSettings.Get("IST_USERS").ToUpper().Replace("LCBO\", "").Split(","))
        Catch ex As Exception
            Throw ex
        End Try

        Try
            If Not ConfigurationManager.AppSettings.Get("IST_APPROVERS") Is Nothing Then
                _ApproverList = New List(Of String)(ConfigurationManager.AppSettings.Get("IST_APPROVERS").ToUpper().Replace("LCBO\", "").Split(","))
            End If

        Catch ex As Exception
            Throw ex
        End Try
    End Sub


End Class
