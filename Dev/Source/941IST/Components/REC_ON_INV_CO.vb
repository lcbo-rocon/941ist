Imports Microsoft.VisualBasic
Imports System.Collections
Imports System.Data
Imports System.Data.OleDb
Imports System.Data.OracleClient

Public Class REC_ON_INV_CO
    Implements IComparable

    Private iInv_no As Integer
    Public Property inv_no() As Integer
        Get
            Return iInv_no
        End Get
        Set(ByVal value As Integer)
            iInv_no = value
        End Set
    End Property

    Private sCo_odno As String
    Public Property co_odno() As String
        Get
            Return sCo_odno
        End Get
        Set(ByVal value As String)
            sCo_odno = value
        End Set
    End Property

    ''' <summary>
    ''' E = Estimate
    ''' A = Actual
    ''' R = Return
    ''' </summary>
    ''' <remarks></remarks>
    Private sCo_Inv_Type As String
    Public Property co_inv_type() As String
        Get
            Return sCo_Inv_Type
        End Get
        Set(ByVal value As String)
            sCo_Inv_Type = value
        End Set
    End Property

    ''' <summary>
    ''' Total invoice amount including delivery charge
    ''' </summary>
    ''' <remarks></remarks>
    Private dInv_Amt As Decimal
    Public Property inv_amt() As Decimal
        Get
            Return dInv_Amt
        End Get
        Set(ByVal value As Decimal)
            dInv_Amt = value
        End Set
    End Property


    Private dDelivery_Chg As Decimal
    Public Property delivery_chg() As Decimal
        Get
            Return dDelivery_Chg
        End Get
        Set(ByVal value As Decimal)
            dDelivery_Chg = value
        End Set
    End Property

    ''' <summary>
    ''' currently not used. always 0
    ''' </summary>
    ''' <remarks></remarks>
    Private dDelivery_Tax1 As Decimal
    Public Property delivery_tax1() As Decimal
        Get
            Return dDelivery_Tax1
        End Get
        Set(ByVal value As Decimal)
            dDelivery_Tax1 = value
        End Set
    End Property

    ''' <summary>
    ''' HST on Delivery
    ''' </summary>
    ''' <remarks></remarks>
    Private dDelivery_Tax2 As Decimal
    Public Property delivery_tax2() As Decimal
        Get
            Return dDelivery_Tax2
        End Get
        Set(ByVal value As Decimal)
            dDelivery_Tax2 = value
        End Set
    End Property

    Private iTtl_Full_Cases As Integer
    Public Property ttl_full_cases() As Integer
        Get
            Return iTtl_Full_Cases
        End Get
        Set(ByVal value As Integer)
            iTtl_Full_Cases = value
        End Set
    End Property

    Private iTtl_Part_Cases As Integer
    Public Property ttl_part_cases() As Integer
        Get
            Return iTtl_Part_Cases
        End Get
        Set(ByVal value As Integer)
            iTtl_Part_Cases = value
        End Set
    End Property

    ''' <summary>
    ''' Invoice date time in YYYY-MM-DD HH:MI:SS format
    ''' </summary>
    ''' <remarks></remarks>
    Private sInv_Dt As String
    Public Property inv_dt() As DateTime
        Get
            Return sInv_Dt
        End Get
        Set(ByVal value As DateTime)
            sInv_Dt = value
        End Set
    End Property

    ''' <summary>
    ''' Date written to T5 in YYYY-MM-DD HH:MI:SS format
    ''' </summary>
    ''' <remarks></remarks>
    Private sUpload1_dt As String
    Public Property upload1_dt() As String
        Get
            Return sUpload1_dt
        End Get
        Set(ByVal value As String)
            sUpload1_dt = value
        End Set
    End Property

    ''' <summary>
    ''' Date written to C3 in YYYY-MM-DD HH:MI:SS format
    ''' currently not in use (consider using as flag to indicate date freight forward?)
    ''' </summary>
    ''' <remarks></remarks>
    Private sUpload2_dt As String
    Public Property upload2_dt() As String
        Get
            Return sUpload2_dt
        End Get
        Set(ByVal value As String)
            sUpload2_dt = value
        End Set
    End Property

    ''' <summary>
    ''' Employee id (rocon logon/domain logon) 
    ''' </summary>
    ''' <remarks></remarks>
    Private sEm_no As String
    Public Property em_no() As String
        Get
            Return sEm_no
        End Get
        Set(ByVal value As String)
            sEm_no = value
        End Set
    End Property

    ''' <summary>
    ''' Date written to GL Tables in YYYY-MM-DD HH:MI:SS
    ''' </summary>
    ''' <remarks></remarks>
    Private sAcc_Post_Dt As String
    Public Property acc_post_dt() As String
        Get
            Return sAcc_Post_Dt
        End Get
        Set(ByVal value As String)
            sAcc_Post_Dt = value
        End Set
    End Property

    Private sLink_Co_odno As String
    Public Property link_co_odno() As String
        Get
            Return sLink_Co_odno
        End Get
        Set(ByVal value As String)
            sLink_Co_odno = value
        End Set
    End Property


    Private sAction As String
    Public Property action() As String
        Get
            Return sAction
        End Get
        Set(ByVal value As String)
            sAction = value
        End Set
    End Property


    Sub New()
        inv_no = 0
        co_odno = ""
        co_inv_type = ""
        inv_amt = 0D
        delivery_chg = 0D
        delivery_tax1 = 0D
        delivery_tax2 = 0D
        ttl_full_cases = 0
        ttl_part_cases = 0
        inv_dt = New DateTime
        upload1_dt = ""
        upload2_dt = ""
        em_no = ""
        acc_post_dt = ""
        link_co_odno = ""
        action = "N"

    End Sub
    Sub New(ByVal drHeader As DataRow)
        Me.New()
        setInfo(drHeader)
    End Sub
    Sub New(ByVal sOrderNum As String, ByVal sCoInvType As String)
        Me.New()
        Dim dr As DataRow = DB.GetOnInvCoInfo(sOrderNum, sCoInvType)
        setInfo(dr)
    End Sub
    Public Sub setInfo(ByVal drHeader As DataRow)

        If IsDBNull(drHeader("inv_no")) = False Then
            inv_no = drHeader("INV_NO")
        End If

        If IsDBNull(drHeader("co_odno")) = False Then
            co_odno = drHeader("CO_ODNO")
        End If

        If IsDBNull(drHeader("co_inv_type")) = False Then
            co_inv_type = drHeader("CO_INV_TYPE")
        End If

        If IsDBNull(drHeader("inv_amt")) = False Then
            dInv_Amt = drHeader("INV_AMT")
        End If

        If IsDBNull(drHeader("delivery_chg")) = False Then
            delivery_chg = drHeader("DELIVERY_CHG")
        End If

        If IsDBNull(drHeader("delivery_tax1")) = False Then
            delivery_tax1 = drHeader("DELIVERY_TAX1")
        End If

        If IsDBNull(drHeader("delivery_tax2")) = False Then
            delivery_tax2 = drHeader("DELIVERY_TAX2")
        End If

        If IsDBNull(drHeader("ttl_full_cases")) = False Then
            ttl_full_cases = drHeader("TTL_FULL_CASES")
        End If

        If IsDBNull(drHeader("ttl_part_cases")) = False Then
            ttl_part_cases = drHeader("TTL_PART_CASES")
        End If

        If IsDBNull(drHeader("inv_dt")) = False Then
            inv_dt = drHeader("INV_DT")
        End If

        If IsDBNull(drHeader("upload1_dt")) = False Then
            upload1_dt = drHeader("UPLOAD1_DT")
        End If

        If IsDBNull(drHeader("upload2_dt")) = False Then
            upload2_dt = drHeader("UPLOAD2_DT")
        End If

        If IsDBNull(drHeader("em_no")) = False Then
            em_no = drHeader("EM_NO")
        End If

        If IsDBNull(drHeader("acc_post_dt")) = False Then
            acc_post_dt = drHeader("ACC_POST_DT")
        End If

        If IsDBNull(drHeader("link_co_odno")) = False Then
            link_co_odno = drHeader("LINK_CO_ODNO")
        End If

        If IsDBNull(drHeader("action")) = False Then
            action = drHeader("action")
        End If

    End Sub
    Public Function CompareTo(ByVal obj As Object) As Integer Implements System.IComparable.CompareTo
        Dim b As REC_ON_INV_CO = obj
        Return Me.co_odno.CompareTo(b.co_odno)
    End Function
    Function validate() As Boolean
        Dim bRetval As Boolean = True
        'check inv_no must be > 0
        If inv_no < 0 Then
            bRetval = False
        End If

        If co_odno.Trim = "" Then
            bRetval = False
        End If

        If co_inv_type.Trim = "" Then
            bRetval = False
        End If

        'If inv_amt <= 0 Then
        '    bRetval = False
        'End If

        If em_no.Trim = "" Then
            bRetval = False
        End If

        If link_co_odno.Trim = "" Then
            link_co_odno = co_odno
        End If

        Return bRetval
    End Function
    Sub Update(ByRef oraCmd As OracleCommand)
        Dim tmpDate As DateTime = Nothing
        Dim bValid As Boolean = validate()

        If upload1_dt.Trim <> "" Then
            If DateTime.TryParse(upload1_dt, tmpDate) = False Then
                Throw New Exception("ON_INVOICE.Insert->" & inv_no & ".Error Message:Invalid upload1_dt provided for insert:" & inv_dt)
            Else
                upload1_dt = tmpDate.ToString("yyyy-MM-dd HH:mm:ss")
            End If
        End If

        If upload2_dt.Trim <> "" Then
            If DateTime.TryParse(upload2_dt, tmpDate) = False Then
                Throw New Exception("ON_INVOICE.Insert->" & inv_no & ".Error Message:Invalid upload2_dt provided for insert:" & inv_dt)
            Else
                upload2_dt = tmpDate.ToString("yyyy-MM-dd HH:mm:ss")
            End If
        End If

        If acc_post_dt.Trim <> "" Then
            If DateTime.TryParse(acc_post_dt, tmpDate) = False Then
                Throw New Exception("ON_INVOICE.Insert->" & inv_no & ".Error Message:Invalid acc_post_dt provided for insert:" & inv_dt)
            Else
                acc_post_dt = tmpDate.ToString("yyyy-MM-dd HH:mm:ss")
            End If
        End If

        Dim sQry As String = ""

        If action = "A" Then
            sQry = "INSERT INTO DBO.ON_INV_CO (" & vbNewLine & _
                     " INV_NO " & vbNewLine & _
                     ",CO_ODNO " & vbNewLine & _
                     ",CO_INV_TYPE " & vbNewLine & _
                     ",INV_AMT " & vbNewLine & _
                     ",DELIVERY_CHG " & vbNewLine & _
                     ",DELIVERY_TAX1 " & vbNewLine & _
                     ",DELIVERY_TAX2 " & vbNewLine & _
                     ",TTL_FULL_CASES " & vbNewLine & _
                     ",TTL_PART_CASES " & vbNewLine & _
                     ",INV_DT " & vbNewLine & _
                     ",EM_NO " & vbNewLine & _
                     ",LINK_CO_ODNO " & vbNewLine & _
                     ") VALUES (" & vbNewLine & _
                     inv_no & vbNewLine & _
                     ",'" & co_odno & "'" & vbNewLine & _
                     ",'" & co_inv_type & "'" & vbNewLine & _
                     "," & inv_amt & vbNewLine & _
                     "," & delivery_chg & vbNewLine & _
                     "," & delivery_tax1 & vbNewLine & _
                     "," & delivery_tax2 & vbNewLine & _
                     "," & ttl_full_cases & vbNewLine & _
                     "," & ttl_part_cases & vbNewLine & _
                     ",TO_DATE('" & inv_dt.ToString("yyyy-MM-dd HH:mm:ss") & "','YYYY-MM-DD HH24:MI:SS') " & vbNewLine & _
                     ",'" & em_no & "'" & vbNewLine & _
                     ",'" & link_co_odno & "'" & vbNewLine & _
                     ")"
        ElseIf action = "U" Then
            sQry = "UPDATE DBO.ON_INV_CO " & vbNewLine & _
                 " SET CO_INV_TYPE = '" & co_inv_type & "'" & vbNewLine & _
                 ",INV_AMT = " & inv_amt & vbNewLine & _
                 ",DELIVERY_CHG = " & delivery_chg & vbNewLine & _
                 ",DELIVERY_TAX1 = " & delivery_tax1 & vbNewLine & _
                 ",DELIVERY_TAX2 = " & delivery_tax2 & vbNewLine & _
                 ",TTL_FULL_CASES = " & ttl_full_cases & vbNewLine & _
                 ",TTL_PART_CASES = " & ttl_part_cases & vbNewLine & _
                 ",INV_DT = TO_DATE('" & inv_dt.ToString("yyyy-MM-dd HH:mm:ss") & "','YYYY-MM-DD HH24:MI:SS') " & vbNewLine & _
                 ",EM_NO ='" & em_no & "'" & vbNewLine & _
                 ",LINK_CO_ODNO = '" & link_co_odno.Trim & "'" & vbNewLine & _
                 " WHERE INV_NO = " & inv_no & vbNewLine & _
                 " AND CO_ODNO = '" & co_odno.Trim.PadRight(16) & "'"
        Else
            'Nothing to do
            Exit Sub
        End If
        Try
            If bValid = False Then
                Throw New Exception("Error validating ON_INV_CO record")
            End If
            oraCmd.CommandText = sQry
            oraCmd.Parameters.Clear()

            If oraCmd.ExecuteNonQuery <= 0 Then
                Throw New Exception("Error Inserting ON_INV_CO record query: " & sQry & "!")
            End If

            If upload1_dt.Trim <> "" Then
                sQry = "UPDATE DBO.ON_INV_CO SET UPLOAD1_DT = TO_DATE('" & upload1_dt & "','YYYY-MM-DD HH24:MI:SS') " & vbNewLine & _
                       " WHERE INV_NO = " & inv_no & vbNewLine & _
                       " AND CO_ODNO = '" & co_odno & "'" & vbNewLine & _
                       " AND CO_INV_TYPE = '" & co_inv_type & "'"

                oraCmd.CommandText = sQry
                oraCmd.Parameters.Clear()

                If oraCmd.ExecuteNonQuery <= 0 Then
                    Throw New Exception("Error UPDATING ON_INV_CO.UPLOAD1_DT record query: " & sQry & "!")
                End If

            End If

            If upload2_dt.Trim <> "" Then
                sQry = "UPDATE DBO.ON_INV_CO SET UPLOAD2_DT = TO_DATE('" & upload2_dt & "','YYYY-MM-DD HH24:MI:SS') " & vbNewLine & _
                       " WHERE INV_NO = " & inv_no & vbNewLine & _
                       " AND CO_ODNO = '" & co_odno & "'" & vbNewLine & _
                       " AND CO_INV_TYPE = '" & co_inv_type & "'"

                oraCmd.CommandText = sQry
                oraCmd.Parameters.Clear()

                If oraCmd.ExecuteNonQuery <= 0 Then
                    Throw New Exception("Error UPDATING ON_INV_CO.UPLOAD2_DT record query: " & sQry & "!")
                End If

            End If

            If acc_post_dt.Trim <> "" Then
                sQry = "UPDATE DBO.ON_INV_CO SET ACC_POST_DT = TO_DATE('" & acc_post_dt & "','YYYY-MM-DD HH24:MI:SS') " & vbNewLine & _
                       " WHERE INV_NO = " & inv_no & vbNewLine & _
                       " AND CO_ODNO = '" & co_odno & "'" & vbNewLine & _
                       " AND CO_INV_TYPE = '" & co_inv_type & "'"

                oraCmd.CommandText = sQry
                oraCmd.Parameters.Clear()

                If oraCmd.ExecuteNonQuery <= 0 Then
                    Throw New Exception("Error UPDATING ON_INV_CO.ACC_POST_DT record query: " & sQry & "!")
                End If

            End If
        Catch ex As Exception
            Throw New Exception("ON_INV_CO.update->" & co_odno & ".Error Message:" & ex.Message.ToString)
        End Try

    End Sub
End Class



'INV_NO,NUMBER,10,,N,
'CO_ODNO,CHAR,16,,N,
'CO_INV_TYPE,CHAR,1,,N,
'INV_AMT,NUMBER,14,2,N,
'DELIVERY_CHG,NUMBER,14,2,Y,
'DELIVERY_TAX1,NUMBER,14,2,Y,
'DELIVERY_TAX2,NUMBER,14,2,Y,
'TTL_FULL_CASES,NUMBER,10,,Y,
'TTL_PART_CASES,NUMBER,10,,Y,
'INV_DT,DATE,7,,Y,
'UPLOAD1_DT,DATE,7,,Y,
'UPLOAD2_DT,DATE,7,,Y,
'EM_NO,CHAR,11,,Y,
'ACC_POST_DT,DATE,7,,Y,
'DELIVERY2_CHG,NUMBER,14,2,Y,
'DELIVERY2_TAX1,NUMBER,14,2,Y,
'DELIVERY2_TAX2,NUMBER,14,2,Y,
'DELIVERY2_POST_DT,DATE,7,,Y,
'LINK_CO_ODNO,CHAR,16,,Y,