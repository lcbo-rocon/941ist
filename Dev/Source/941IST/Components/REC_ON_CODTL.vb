Imports Microsoft.VisualBasic
Imports System.Data.OleDb
Imports System.Data
Imports System.Data.OracleClient

Public Class REC_ON_CODTL
    Implements IComparable
    Private sCo_Odno As String
    Public Property co_odno() As String
        Get
            Return sCo_Odno
        End Get
        Set(ByVal value As String)
            sCo_Odno = value
        End Set
    End Property

    Private iCod_line As Integer
    Public Property cod_line() As Integer
        Get
            Return iCod_line
        End Get
        Set(ByVal value As Integer)
            iCod_line = value
        End Set
    End Property

    Private sItem As String
    Public Property item() As String
        Get
            Return sItem
        End Get
        Set(ByVal value As String)
            sItem = value
        End Set
    End Property

    Private sPackagingCode As String
    Public Property packaging_code() As String
        Get
            Return sPackagingCode
        End Get
        Set(ByVal value As String)
            sPackagingCode = value
        End Set
    End Property

    Private iCOD_Qty As Integer
    Public Property cod_qty() As Integer
        Get
            Return iCOD_Qty
        End Get
        Set(ByVal value As Integer)
            iCOD_Qty = value
        End Set
    End Property

    'Lot_number char 15
    Private sUser_Blocked_stock_fl As String
    Public Property use_blocked_stock_fl() As String
        Get
            Return sUser_Blocked_stock_fl
        End Get
        Set(ByVal value As String)
            sUser_Blocked_stock_fl = value
        End Set
    End Property
    'TIMESTAMP,DATE,7
    'EXPIRY_DT,DATE,7
    'ROUTE_CODE,CHAR,4
    'ROUTE_STOP,NUMBER,10
    'CARRIER,CHAR,15
    'ORDER_REQD_DT,DATE,7
    'SHIP_REQD_DT,DATE,7
    'SITE,NUMBER,10


    Private iTotal_Allocated_Qty As Integer
    Public Property total_allocated_qty() As Integer
        Get
            Return iTotal_Allocated_Qty
        End Get
        Set(ByVal value As Integer)
            iTotal_Allocated_Qty = value
        End Set
    End Property

    'SINGLE_IM_WEIGHT,NUMBER,14,4
    'SINGLE_IM_CUBE,NUMBER,14,4

    Private iPicked_qty As Integer
    Public Property picked_qty() As Integer
        Get
            Return iPicked_qty
        End Get
        Set(ByVal value As Integer)
            iPicked_qty = value
        End Set
    End Property


    Private sDeleted_Fl As String
    Public Property deleted_fl() As String
        Get
            Return sDeleted_Fl
        End Get
        Set(ByVal value As String)
            sDeleted_Fl = value
        End Set
    End Property


    Private dUnitPrice As Decimal
    Public Property unit_price() As Decimal
        Get
            Return dUnitPrice
        End Get
        Set(ByVal value As Decimal)
            dUnitPrice = value
        End Set
    End Property


    Private sAction As String
    Public Property action() As String
        Get
            Return sAction
        End Get
        Set(ByVal value As String)
            sAction = value
        End Set
    End Property

    Sub New()
        co_odno = ""
        cod_line = 0
        item = ""
        packaging_code = " "
        cod_qty = 0
        use_blocked_stock_fl = "N"
        total_allocated_qty = 0
        picked_qty = 0
        deleted_fl = "N"
        unit_price = 0D
    End Sub

    Sub New(ByVal dr As DataRow)
        Me.New()
        SetInfo(dr)
    End Sub

    Sub SetInfo(ByVal dr As DataRow)
        If IsDBNull(dr("co_odno")) = False Then
            co_odno = dr("co_odno")
        End If

        If IsDBNull(dr("cod_line")) = False Then
            cod_line = dr("cod_line")
        End If

        If IsDBNull(dr("item")) = False Then
            item = dr("item")
        End If

        If IsDBNull(dr("packaging_code")) = False Then
            packaging_code = dr("packaging_code")
        End If

        If IsDBNull(dr("cod_qty")) = False Then
            cod_qty = dr("cod_qty")
        End If

        If IsDBNull(dr("total_allocated_qty")) = False Then
            total_allocated_qty = dr("total_allocated_qty")
        End If

        If IsDBNull(dr("picked_qty")) = False Then
            picked_qty = dr("picked_qty")
        End If

        If IsDBNull(dr("deleted_fl")) = False Then
            deleted_fl = dr("deleted_fl")
        End If

        If IsDBNull(dr("unit_price")) = False Then
            unit_price = dr("unit_price")
        End If

    End Sub

    Public Function validate() As Boolean
        Dim bValid As Boolean = True

        bValid = (co_odno.Trim <> "")
        bValid = bValid AndAlso (cod_line > 0)
        bValid = bValid AndAlso (item.Trim <> "")
        bValid = bValid AndAlso (cod_qty >= 0)

        Return bValid

    End Function

    Sub Update(ByVal oracmd As OracleCommand)
        Dim bValid As Boolean = validate()

        Dim sQry As String = ""
        If action = "A" Then
            sQry = "INSERT INTO DBO.ON_CODTL (" & vbNewLine & _
                 "CO_ODNO " & vbNewLine & _
                 ",COD_LINE " & vbNewLine & _
                 ",ITEM " & vbNewLine & _
                 ",COD_QTY " & vbNewLine & _
                 ",PACKAGING_CODE " & vbNewLine & _
                 ",USE_BLOCKED_STOCK_FL " & vbNewLine & _
                 ",TOTAL_ALLOCATED_QTY " & vbNewLine & _
                 ",PICKED_QTY " & vbNewLine & _
                 ",DELETED_FL " & vbNewLine & _
                 ",UNIT_PRICE " & vbNewLine & _
                 ") VALUES (" & vbNewLine & _
                 "'" & co_odno & "'" & vbNewLine & _
                 "," & cod_line & vbNewLine & _
                 ",'" & item & "'" & vbNewLine & _
                 "," & cod_qty & vbNewLine & _
                 ",'" & packaging_code & "'" & vbNewLine & _
                ",'" & use_blocked_stock_fl & "'" & vbNewLine & _
                "," & total_allocated_qty & vbNewLine & _
                "," & picked_qty & vbNewLine & _
                ",'" & deleted_fl & "'" & vbNewLine & _
                "," & unit_price & vbNewLine & _
                 ")"
        ElseIf action = "U" Then
            sQry = "UPDATE DBO.ON_CODTL " & vbNewLine & _
                 " SET ITEM = '" & item & "'" & vbNewLine & _
                 ",COD_QTY = " & cod_qty & vbNewLine & _
                 ",TOTAL_ALLOCATED_QTY = " & total_allocated_qty & vbNewLine & _
                 ",PICKED_QTY = " & picked_qty & vbNewLine & _
                 ",USE_BLOCKED_STOCK_FL = '" & use_blocked_stock_fl & "'" & vbNewLine & _
                 ",DELETED_FL = '" & deleted_fl & "'" & vbNewLine & _
                 ",UNIT_PRICE = " & unit_price & vbNewLine & _
                 " WHERE CO_ODNO = '" & co_odno.Trim.PadRight(16) & "'" & vbNewLine & _
                 " AND COD_LINE = " & cod_line
        ElseIf action = "D" Then
            sQry = "DELETE FROM DBO.ON_CODTL " & vbNewLine & _
            " WHERE CO_ODNO = '" & co_odno.Trim.PadRight(16) & "'" & vbNewLine & _
                             " AND COD_LINE = " & cod_line

        Else
            'NOTHING TO DO
            Exit Sub
        End If

        Try
            If bValid = False Then
                Throw New Exception("Error validating CODTL record")
            End If
            oracmd.CommandText = sQry
            oracmd.Parameters.Clear()

            If oracmd.ExecuteNonQuery <= 0 Then
                Throw New Exception("Error Inserting ON_CODTL record query: " & sQry & "!")
            End If

        Catch ex As Exception
            Throw New Exception("ON_CODTL.Insert->" & co_odno & "," & cod_line & ".Error Message:" & ex.Message.ToString)
        End Try

    End Sub

    Public Function CompareTo(ByVal obj As Object) As Integer Implements System.IComparable.CompareTo
        Dim b As REC_ON_CODTL = obj
        Return cod_line.CompareTo(b.cod_line)
    End Function








End Class
