Imports Microsoft.VisualBasic
Imports System.Data
Imports System.Data.OleDb
Imports System.Data.OracleClient

Public Class IST_ORDER

    Private ist_header As REC_LCBO_IST_HDR
    Private ist_detailsList As REC_LCBO_IST_DTL_LIST
    Private on_invoice As REC_ON_INVOICE
    Private on_inv_co As REC_ON_INV_CO
    Private on_inv_codtl As REC_ON_INV_CODTL_LIST
    Private on_cohdr As REC_ON_COHDR
    Private on_codtl As REC_ON_CODTL_LIST
    Private cohdr As REC_COHDR
    Private codtl As REC_CODTL_LIST


    Public Structure IST_STATUS
        Public Shared SAVED As String = "SAVED"
        Public Shared CANCELLED As String = "CANCELLED"
        Public Shared NEW_EDIT As String = "NEW-EDIT"
        Public Shared SAVED_EDIT As String = "SAVED-EDIT"
        Public Shared SUBMITTED As String = "SUBMITTED"
        Public Shared APPROVAL_REQUESTED As String = "APPROVAL REQUESTED"
        Public Shared INVOICED As String = "INVOICED"
    End Structure

    Public Property inv_no() As Integer
        Get
            Return ist_header.inv_no
        End Get
        Set(ByVal value As Integer)
            ist_header.inv_no = value
            ist_detailsList.inv_no = value
            on_invoice.inv_no = value
            on_inv_co.inv_no = value
            on_inv_codtl.inv_no = value
        End Set
    End Property

    Public Property co_odno() As String
        Get
            Return ist_header.co_odno
        End Get
        Set(ByVal value As String)
            ist_header.co_odno = value
            ist_detailsList.co_odno = value
            on_inv_co.co_odno = value
            on_inv_codtl.co_odno = value

            If on_cohdr Is Nothing = False Then
                on_cohdr.co_odno = value
                on_codtl.co_odno = value
            End If

            If cohdr Is Nothing = False Then
                cohdr.co_odno = value
                codtl.co_odno = value
            End If

            If Me.IST_Type <> "REPLACEMENTS" Then
                on_inv_co.link_co_odno = co_odno
            End If

        End Set
    End Property

    Public Property to_store() As String
        Get
            Return ist_header.to_store
        End Get
        Set(ByVal value As String)
            value = value.PadLeft(5, "0")
            ist_header.to_store = value
            on_invoice.inv_cu_no = value

            If on_cohdr Is Nothing = False Then
                on_cohdr.cu_no = value
                on_cohdr.ship_to = value
            End If

            If cohdr Is Nothing = False Then
                cohdr.cu_no = value
                cohdr.ship_to = value
            End If
        End Set
    End Property
    Public Property orig_inv_no() As Integer
        Get
            Return ist_header.inv_no
        End Get
        Set(ByVal value As Integer)
            ist_header.orig_inv_no = value
        End Set
    End Property

    Public Property orig_co_odno() As String
        Get
            Return ist_header.orig_co_odno
        End Get
        Set(ByVal value As String)
            ist_header.orig_co_odno = value

            If IST_Type = "REPLACEMENTS" Then
                on_inv_co.link_co_odno = orig_co_odno
            End If
        End Set
    End Property

    Public Property order_dt_tm() As DateTime
        Get
            Return ist_header.order_dt_tm
        End Get
        Set(ByVal value As DateTime)
            ist_header.order_dt_tm = value
            If on_cohdr Is Nothing = False Then
                on_cohdr.order_dt_tm = value
            End If
            If cohdr Is Nothing = False Then
                cohdr.order_dt_tm = value
            End If
        End Set
    End Property

    Public Property order_reqd_dt() As DateTime
        Get
            Return ist_header.order_reqd_dt
        End Get
        Set(ByVal value As DateTime)
            ist_header.order_reqd_dt = value
            If on_cohdr Is Nothing = False Then
                on_cohdr.order_reqd_dt = value
            End If
            If cohdr Is Nothing = False Then
                cohdr.order_reqd_dt = value
            End If
        End Set
    End Property

    Public Property IST_Type() As String
        Get
            Return ist_header.ist_type
        End Get
        Set(ByVal value As String)
            ist_header.ist_type = value
        End Set
    End Property

    Public Property pu_order_fl() As String
        Get
            Return ist_header.pu_order_fl
        End Get
        Set(ByVal value As String)
            ist_header.pu_order_fl = value

            If on_cohdr Is Nothing = False Then
                on_cohdr.pu_order_fl = value
            End If

            If cohdr Is Nothing = False Then
                cohdr.pu_order_fl = value
            End If
        End Set
    End Property

    Public Property Num_Of_Prod() As Integer
        Get
            Return ist_header.num_of_prod
        End Get
        Set(ByVal value As Integer)
            ist_header.num_of_prod = value
        End Set
    End Property

    Public Property Num_Of_Units() As Integer
        Get
            Return ist_header.num_of_units
        End Get
        Set(ByVal value As Integer)
            ist_header.num_of_units = value
        End Set
    End Property

    Public Property submitted_by() As String
        Get
            Return ist_header.submitted_by
        End Get
        Set(ByVal value As String)
            ist_header.submitted_by = value
            on_inv_co.em_no = value
        End Set
    End Property

    Public Property authorized_by() As String
        Get
            Return ist_header.authorized_by
        End Get
        Set(ByVal value As String)
            ist_header.authorized_by = value
        End Set
    End Property

    Public Property comments_line1() As String
        Get
            Return ist_header.comments_line1
        End Get
        Set(ByVal value As String)
            ist_header.comments_line1 = value
        End Set
    End Property

    Public Property comments_line2() As String
        Get
            Return ist_header.comments_line2
        End Get
        Set(ByVal value As String)
            ist_header.comments_line2 = value
        End Set
    End Property

    Public Property status() As String
        Get
            Return ist_header.status
        End Get
        Set(ByVal value As String)
            ist_header.status = value
        End Set
    End Property

    Public Property request_auth_user() As String
        Get
            Return ist_header.request_auth_user
        End Get
        Set(ByVal value As String)
            ist_header.request_auth_user = value
        End Set
    End Property
    Public Property request_auth_dt() As DateTime
        Get
            Return ist_header.request_auth_dt
        End Get
        Set(ByVal value As DateTime)
            ist_header.request_auth_dt = value
        End Set
    End Property

    Public Property last_saved_dt() As DateTime
        Get
            Return ist_header.last_saved_dt
        End Get
        Set(ByVal value As DateTime)
            ist_header.last_saved_dt = value
        End Set
    End Property

    Public Property last_saved_by() As String
        Get
            Return ist_header.last_saved_by
        End Get
        Set(ByVal value As String)
            ist_header.last_saved_by = value
            on_inv_co.em_no = value
        End Set
    End Property

    Public Property last_update_dt() As DateTime
        Get
            Return ist_header.last_update_dt
        End Get
        Set(ByVal value As DateTime)
            ist_header.last_update_dt = value
        End Set
    End Property

    Public Property last_update_by() As String
        Get
            Return ist_header.last_update_by
        End Get
        Set(ByVal value As String)
            ist_header.last_update_by = value
            on_inv_co.em_no = value
        End Set
    End Property

    Public Property inv_dt() As DateTime
        Get
            Return on_invoice.inv_dt
        End Get
        Set(ByVal value As DateTime)
            on_invoice.inv_dt = value
            on_inv_co.inv_dt = value
        End Set
    End Property

    Public Property cu_no() As String
        Get
            Return on_invoice.inv_cu_no
        End Get
        Set(ByVal value As String)
            on_invoice.inv_cu_no = value
            If on_cohdr Is Nothing = False Then
                on_cohdr.cu_no = value
            End If
            If cohdr Is Nothing = False Then
                cohdr.cu_no = value
            End If
        End Set
    End Property
    Public Property action() As String
        Get
            Return ist_header.action
        End Get
        Set(ByVal value As String)

            ist_header.action = value
            on_invoice.action = value
            on_inv_co.action = value

            If on_cohdr Is Nothing = False Then
                on_cohdr.action = value
            End If
            If cohdr Is Nothing = False Then
                cohdr.action = value
            End If
        End Set
    End Property
    Public ReadOnly Property IST_DTL_List() As DataTable
        Get
            'Return ist_detailsList.IST_DTL_List
            Dim tbl As New DataTable
            With tbl.Columns
                .Add("INV_NO")
                .Add("CO_ODNO")
                .Add("COD_LINE")
                .Add("ITEM")
                .Add("ITEM_DESC")
                .Add("PACKAGING_CODE")
                .Add("ORIG_QTY", GetType(System.Int32))
                .Add("SAVED_COD_QTY", GetType(System.Int32))
                .Add("COD_QTY", GetType(System.Int32))
                .Add("PICKED_QTY", GetType(System.Int32))
                .Add("DELETED_FL")
                .Add("UPDATE_BY")
                .Add("UPDATE_DT")
            End With

            Dim dr As DataRow = Nothing
            Dim coditem As REC_CODTL = Nothing
            Dim picked As Integer = 0
            For Each dtl As REC_LCBO_IST_DTL In ist_detailsList.GetValueList
                picked = 0
                If codtl Is Nothing = False Then
                    coditem = codtl(dtl.cod_line)
                    picked = coditem.picked_qty
                End If
                dr = tbl.NewRow
                dr("INV_NO") = dtl.inv_no
                dr("CO_ODNO") = dtl.co_odno
                dr("COD_LINE") = dtl.cod_line
                dr("ITEM") = dtl.item
                dr("ITEM_DESC") = dtl.item_desc
                dr("PACKAGING_CODE") = dtl.packaging_code
                dr("ORIG_QTY") = dtl.orig_qty
                dr("SAVED_COD_QTY") = dtl.saved_cod_qty
                dr("COD_QTY") = dtl.cod_qty
                dr("DELETED_FL") = dtl.deleted_fl
                dr("UPDATE_BY") = dtl.update_by
                dr("UPDATE_DT") = dtl.update_dt
                dr("PICKED_QTY") = picked
                tbl.Rows.Add(dr)
            Next
            Return tbl
        End Get
    End Property

    Public Property co_status() As String
        Get
            Dim cs As String = ""
            If on_cohdr Is Nothing = False Then
                cs = on_cohdr.co_status
            End If
            If cohdr Is Nothing = False Then
                cs = cohdr.co_status
            End If
            Return cs
        End Get
        Set(ByVal value As String)
            If on_cohdr Is Nothing = False Then
                on_cohdr.co_status = value
            End If
            If cohdr Is Nothing = False Then
                cohdr.co_status = value
            End If
        End Set
    End Property

    Sub New()
        ist_header = New REC_LCBO_IST_HDR
        ist_detailsList = New REC_LCBO_IST_DTL_LIST

        on_invoice = New REC_ON_INVOICE

        on_inv_co = New REC_ON_INV_CO
        on_inv_co.co_inv_type = "E"
        on_inv_codtl = New REC_ON_INV_CODTL_LIST

        on_cohdr = New REC_ON_COHDR
        on_cohdr.order_type = "IST"
        on_codtl = New REC_ON_CODTL_LIST

        Me.order_dt_tm = Now
        Me.inv_dt = Now
        Me.on_cohdr.order_type = "IST"
        Me.co_status = "W"


        'cohdr = New REC_COHDR
        'codtl = New REC_CODTL_LIST
    End Sub

    Sub New(ByVal co_odno As String)
        Try
            Dim sCoInvType As String = "E"

            ist_header = New REC_LCBO_IST_HDR(co_odno)
            ist_detailsList = New REC_LCBO_IST_DTL_LIST(co_odno)

            on_invoice = New REC_ON_INVOICE
            on_invoice.SetInfo(DB.GetOnInvoiceInfo(ist_header.inv_no))

            'If ist_header.status = "INVOICED" Then
            '    sCoInvType = "A"
            'End If

            on_inv_co = New REC_ON_INV_CO(co_odno, sCoInvType)
            on_inv_codtl = New REC_ON_INV_CODTL_LIST(on_inv_co.co_odno, on_inv_co.co_inv_type)

            If ist_header.status = "INVOICED" Then 'COHDR/CODTL
                'If ist_header.status = IST_STATUS.INVOICED Then 'COHDR/CODTL
                cohdr = New REC_COHDR(co_odno)
                codtl = New REC_CODTL_LIST(co_odno)
            ElseIf ist_header.status = "SUBMITTED" Then
                Try
                    'it has been traffic planned
                    cohdr = New REC_COHDR(co_odno)
                    codtl = New REC_CODTL_LIST(co_odno)
                Catch ex As Exception
                    'not traffic planned yet
                    on_cohdr = New REC_ON_COHDR(co_odno)
                    on_codtl = New REC_ON_CODTL_LIST(co_odno)
                End Try
            Else
                on_cohdr = New REC_ON_COHDR(co_odno)
                on_codtl = New REC_ON_CODTL_LIST(co_odno)
            End If

        Catch ex As Exception
            Throw New Exception("ERROR IST->NEW(" & co_odno & "): " & ex.Message.ToString)
        End Try

    End Sub
    Function itemExist(ByVal item As String) As Boolean
        Dim bExist As Boolean = False
        Dim rec As REC_LCBO_IST_DTL = ist_detailsList.GetOrderLine(item)
        If rec Is Nothing = False Then
            If rec.deleted_fl <> "R" Then
                If rec.cod_qty <> 0 Then
                    bExist = True
                End If
            End If
        End If
        Return bExist
    End Function
    Public Function AddItem(ByVal item As String, ByVal qty As Integer, ByVal sUser As String, ByRef oracmd As OracleCommand) As Integer
        'check if item already exist
        Dim iRetval As Integer = 0
        Dim istItem As REC_LCBO_IST_DTL = Me.ist_detailsList.GetOrderLine(item)
        Dim ocItem As REC_ON_CODTL = Nothing
        Dim invItem As REC_ON_INV_CODTL = Nothing

        If istItem Is Nothing Then
            'Dim iUncommitted As Integer = DB.GetItemUncommittedQty(item)
            'If iUncommitted >= qty Then
            Dim drItemInfo As DataRow = DB.GetItemInfo(item).Rows(0)
            istItem = New REC_LCBO_IST_DTL
            With istItem
                .inv_no = inv_no
                .co_odno = co_odno
                .cod_line = ist_detailsList.nextOrderLine
                .item = item
                .item_desc = drItemInfo("im_desc")
                .packaging_code = drItemInfo("packaging_code")
                .orig_qty = 0
                .saved_cod_qty = 0
                .cod_qty = qty
                .deleted_fl = "N"
                .update_by = sUser
                .update_dt = Now
                .action = "A"
            End With
            ist_detailsList.Add(istItem.cod_line, istItem)
            'Add item to on_codtl
            ocItem = New REC_ON_CODTL
            With ocItem
                .co_odno = co_odno
                .cod_line = istItem.cod_line
                .item = item
                .packaging_code = istItem.packaging_code
                .cod_qty = istItem.cod_qty
                .total_allocated_qty = istItem.cod_qty
                .deleted_fl = istItem.deleted_fl
                .unit_price = drItemInfo("UNIT_PRICE")
                .action = "A"
            End With
            on_codtl.Add(ocItem.cod_line, ocItem)

            'Add item to on_inv_codtl
            invItem = New REC_ON_INV_CODTL
            Dim cases As Integer = qty / drItemInfo("CASE_SZ")
            Dim units As Integer = qty Mod drItemInfo("CASE_SZ")
            With invItem
                .inv_no = inv_no
                .co_odno = co_odno
                .co_inv_type = on_inv_co.co_inv_type
                .cod_line = istItem.cod_line
                .Price = drItemInfo("UNIT_PRICE")
                .extd_price = drItemInfo("UNIT_PRICE") * istItem.cod_qty
                .item = istItem.item
                .qty = qty
                .item_category = drItemInfo("ITEM_CATEGORY")
                .orig_price = drItemInfo("UNIT_PRICE")
                .ttl_cases = cases
                .ttl_units = units
                .action = "A"
            End With
            on_inv_codtl.Add(invItem.cod_line, invItem)
            UpdateCasePartTotals()
            Me.last_update_by = sUser
            Me.last_update_dt = Now
            If Me.status.StartsWith("SAVE") Then
                Me.status = IST_STATUS.SAVED_EDIT
            ElseIf Me.status.StartsWith("NEW") Then
                Me.status = IST_STATUS.NEW_EDIT
            End If
            Me.action = "U"
            Me.Update(oracmd)

            'Else
            '    iRetval = iUncommitted - qty
            'End If
        Else
            'item exist return cod_line
            iRetval = istItem.cod_line
        End If


        Return iRetval
    End Function
    Function AddItem(ByVal cod_line As Integer, ByVal item As String, ByVal orig_qty As Integer, ByVal qty As Integer, ByVal sUser As String, ByRef oracmd As OracleCommand) As Integer
        'check if item already exist
        Dim iRetval As Integer = 0
        Dim istItem As REC_LCBO_IST_DTL = Nothing
        Dim ocItem As REC_ON_CODTL = Nothing
        Dim invItem As REC_ON_INV_CODTL = Nothing

        Dim iUncommitted As Integer = DB.GetItemUncommittedQty(item)
        If iUncommitted >= qty Then
            Dim drItemInfo As DataRow = DB.GetItemInfo(item).Rows(0)
            istItem = New REC_LCBO_IST_DTL
            With istItem
                .inv_no = inv_no
                .co_odno = co_odno
                .cod_line = cod_line
                .item = item
                .item_desc = drItemInfo("im_desc")
                .packaging_code = drItemInfo("packaging_code")
                .orig_qty = orig_qty
                .saved_cod_qty = 0
                .cod_qty = qty
                .deleted_fl = "N"
                .update_by = sUser
                .update_dt = Now
                .action = "A"
            End With
            ist_detailsList.Add(istItem.cod_line, istItem)
            'Add item to on_codtl
            ocItem = New REC_ON_CODTL
            With ocItem
                .co_odno = co_odno
                .cod_line = istItem.cod_line
                .item = item
                .packaging_code = istItem.packaging_code
                .cod_qty = istItem.cod_qty
                .total_allocated_qty = istItem.cod_qty
                .deleted_fl = istItem.deleted_fl
                .unit_price = drItemInfo("UNIT_PRICE")
                .action = "A"
            End With
            on_codtl.Add(ocItem.cod_line, ocItem)

            'Add item to on_inv_codtl
            invItem = New REC_ON_INV_CODTL
            Dim cases As Integer = qty / drItemInfo("CASE_SZ")
            Dim units As Integer = qty Mod drItemInfo("CASE_SZ")
            With invItem
                .inv_no = inv_no
                .co_odno = co_odno
                .co_inv_type = on_inv_co.co_inv_type
                .cod_line = istItem.cod_line
                .Price = drItemInfo("UNIT_PRICE")
                .extd_price = drItemInfo("UNIT_PRICE") * istItem.cod_qty
                .item = istItem.item
                .qty = qty
                .item_category = drItemInfo("ITEM_CATEGORY")
                .orig_price = drItemInfo("UNIT_PRICE")
                .ttl_cases = cases
                .ttl_units = units
                .action = "A"
            End With
            on_inv_codtl.Add(invItem.cod_line, invItem)
            UpdateCasePartTotals()
            Me.last_update_by = sUser
            Me.last_update_dt = Now
            If Me.status.StartsWith("SAVE") Then
                Me.status = IST_STATUS.SAVED_EDIT
            ElseIf Me.status.StartsWith("NEW") Then
                Me.status = IST_STATUS.NEW_EDIT
            End If
            Me.action = "U"
            Me.Update(oracmd)

        Else
            iRetval = iUncommitted - qty
        End If

        Return iRetval
    End Function
    Function UpdateItem(ByVal cod_line As Integer, ByVal qty As Integer, ByVal sUser As String, ByRef oracmd As OracleCommand) As Integer
        'check if item already exist
        'cannot updateitem if order is already in Request,submitted, invoiced            
        Dim iRetval As Integer = 0

        If Me.status.StartsWith("NEW") Or
           Me.status.StartsWith("SAVED") Then
            Dim istItem As REC_LCBO_IST_DTL = Me.ist_detailsList.GetOrderLine(cod_line)
            Dim ocItem As REC_ON_CODTL = Nothing
            Dim invItem As REC_ON_INV_CODTL = Nothing

            If istItem Is Nothing = False Then
                If istItem.cod_qty <> qty Then
                    'Dim iUncommitted As Integer = DB.GetItemUncommittedQty(istItem.item) 'OVERCOMMIT
                    'If iUncommitted >= (qty - istItem.cod_qty) Then 'OVERCOMMIT
                    Dim drItemInfo As DataRow = DB.GetItemInfo(istItem.item).Rows(0)
                    With istItem
                        .cod_qty = qty
                        .deleted_fl = IIf(qty = 0, "R", "N")
                        .update_by = sUser
                        .update_dt = Now
                        .action = "U"
                    End With
                    ' ist_detailsList.Add(istItem.cod_line, istItem)
                    'Add item to on_codtl
                    'If ocItem.cod_qty < qty Then
                    ocItem = on_codtl.GetOrderLine(istItem.cod_line)
                    'If ocItem.cod_qty < qty Then 'OVERCOMMIT
                    With ocItem
                        .cod_qty = istItem.cod_qty
                        .total_allocated_qty = istItem.cod_qty
                        .action = "U"
                    End With
                    'End If 'OVERCOMMIT
                    ' on_codtl.Add(ocItem.cod_line, ocItem)
                    'End If

                    'Add item to on_inv_codtl
                    invItem = on_inv_codtl.GetOrderLine(cod_line)
                    Dim cases As Integer = Math.Floor(qty / Convert.ToInt32(drItemInfo("CASE_SZ")))
                    Dim units As Integer = qty Mod Convert.ToInt32(drItemInfo("CASE_SZ"))
                    With invItem
                        .inv_no = inv_no
                        .co_odno = co_odno
                        .cod_line = istItem.cod_line
                        .Price = drItemInfo("UNIT_PRICE")
                        .extd_price = drItemInfo("UNIT_PRICE") * istItem.cod_qty
                        .item = istItem.item
                        .qty = qty
                        .item_category = drItemInfo("ITEM_CATEGORY")
                        .orig_price = drItemInfo("UNIT_PRICE")
                        .ttl_cases = cases
                        .ttl_units = units
                        .action = "U"
                    End With
                    '  on_inv_codtl.Add(invItem.cod_line, invItem)
                    UpdateCasePartTotals()
                    Me.last_update_by = sUser
                    Me.last_update_dt = Now
                    If Me.status.StartsWith("SAVE") Then
                        Me.status = IST_STATUS.SAVED_EDIT
                    ElseIf Me.status.StartsWith("NEW") Then
                        Me.status = IST_STATUS.NEW_EDIT
                    End If
                    Me.action = "U"
                    Me.Update(oracmd)


                    'Else 'OVERCOMMIT
                    '    iRetval = iUncommitted - (qty - istItem.cod_qty) 'OVERCOMMIT
                    'End If 'OVERCOMMIT
                End If
            Else
                'item exist return cod_line
                iRetval = istItem.cod_line
            End If
        Else
            iRetval = 8888
        End If

        Return iRetval
    End Function
    Sub Update(ByRef oracmd As OracleCommand)
        Me.ist_header.Update(oracmd)
        Me.ist_detailsList.Update(oracmd)

        Me.on_invoice.Update(oracmd)
        Me.on_inv_co.Update(oracmd)
        Me.on_inv_codtl.Update(oracmd)
        If Me.on_cohdr Is Nothing = False Then
            Me.on_cohdr.Update(oracmd)
            Me.on_codtl.Update(oracmd)
        End If
        If Me.cohdr Is Nothing = False Then
            'cohdr.Update(oracmd) '941 DOES NOT UPDATE THIS TABLE
            'codtl.Update(oracmd) '941 DOES NOT UPDATE THIS TABLE
        End If
    End Sub

    Sub UpdateCasePartTotals()
        'update on_inv_co full cases and part cases
        Dim full_cases As Integer = 0
        Dim part_cases As Integer = 0
        Dim divisor As Integer = 12
        Dim accumulator As Integer = 0
        Dim dr As DataRow = Nothing
        Dim prodCount As Integer = 0
        Dim unitCount As Integer = 0

        For Each itm As REC_ON_INV_CODTL In on_inv_codtl.GetValueList
            dr = DB.GetItemInfo(itm.item).Rows(0)
            full_cases = full_cases + itm.ttl_cases
            'calculate the part cases
            accumulator = accumulator + (itm.ttl_units * dr("PACK_FACTOR"))
        Next
        part_cases = Math.Floor(accumulator / divisor)
        If (accumulator Mod divisor > 0) Then
            part_cases = part_cases + 1
        End If

        on_inv_co.ttl_full_cases = full_cases
        on_inv_co.ttl_part_cases = part_cases
        on_inv_co.action = "U"

        'Update NUM of Prod and NUM of units
        For Each itm As REC_LCBO_IST_DTL In Me.ist_detailsList.GetValueList
            If itm.deleted_fl <> "R" Then
                If itm.cod_qty > 0 Then
                    prodCount = prodCount + 1
                    unitCount = unitCount + itm.cod_qty
                End If
            End If
        Next
        Me.Num_Of_Prod = prodCount
        Me.Num_Of_Units = unitCount
        action = "U"


    End Sub
    Sub SaveOrder(ByVal sUser As String, ByRef oracmd As OracleCommand)
        'UPDATE THE SAVED_QTY WITH COD_QTY
        Dim ocItem As REC_ON_CODTL = Nothing
        Dim invItem As REC_ON_INV_CODTL = Nothing

        For Each ISTDTL As REC_LCBO_IST_DTL In ist_detailsList.GetValueList
            ocItem = on_codtl.GetOrderLine(ISTDTL.cod_line)
            invItem = on_inv_codtl.GetOrderLine(ISTDTL.cod_line)

            If ISTDTL.saved_cod_qty <> ISTDTL.cod_qty Or
               ISTDTL.deleted_fl <> ocItem.deleted_fl Then

                ISTDTL.saved_cod_qty = ISTDTL.cod_qty
                ISTDTL.last_saved_by = ISTDTL.update_by
                ISTDTL.last_saved_dt = ISTDTL.update_dt
                ISTDTL.action = "U"

                If ISTDTL.cod_qty = 0 Then
                    ISTDTL.deleted_fl = "R"
                Else
                    ISTDTL.deleted_fl = "N"
                End If

                ocItem.cod_qty = ISTDTL.cod_qty
                ocItem.total_allocated_qty = ISTDTL.cod_qty
                ocItem.deleted_fl = ISTDTL.deleted_fl
                ocItem.action = "U"

                invItem.qty = ISTDTL.cod_qty
                invItem.extd_price = Math.Round(invItem.Price * invItem.qty, 2, MidpointRounding.AwayFromZero)
                invItem.action = "U"
            End If
        Next
        Me.UpdateCasePartTotals()

        'Me.last_update_by = sUser
        'Me.last_update_dt = Now
        Me.last_saved_by = sUser
        Me.last_saved_dt = Now
        'Me.co_status = "O" 'DSO
        Me.co_status = "W" '941
        Me.status = IST_STATUS.SAVED
        Me.action = "U"

        Me.Update(oracmd)
    End Sub
    ''' <summary>
    ''' Revert the order back to the last saved
    ''' This only applies to the items in the order.
    ''' </summary>
    ''' <param name="sUser"></param>
    ''' <param name="oracmd"></param>
    ''' <remarks></remarks>
    Sub RevertSaveOrder(ByVal sUser As String, ByRef oracmd As OracleCommand)
        'UPDATE THE SAVED_QTY WITH COD_QTY
        Dim ocItem As REC_ON_CODTL = Nothing
        Dim invItem As REC_ON_INV_CODTL = Nothing

        For Each ISTDTL As REC_LCBO_IST_DTL In ist_detailsList.GetValueList
            ocItem = on_codtl.GetOrderLine(ISTDTL.cod_line)
            invItem = on_inv_codtl.GetOrderLine(ISTDTL.cod_line)

            If ISTDTL.saved_cod_qty <> ISTDTL.cod_qty Or
               ISTDTL.deleted_fl <> ocItem.deleted_fl Then
                ISTDTL.cod_qty = ISTDTL.saved_cod_qty
                ISTDTL.update_by = ISTDTL.last_saved_by
                ISTDTL.update_dt = ISTDTL.last_saved_dt
                ISTDTL.action = "U"

                If ISTDTL.cod_qty = 0 Then
                    ISTDTL.deleted_fl = "R"
                End If

                ocItem.cod_qty = ISTDTL.cod_qty
                ocItem.deleted_fl = ISTDTL.deleted_fl
                ocItem.action = "U"

                invItem.qty = ISTDTL.cod_qty
                invItem.extd_price = Math.Round(invItem.qty * invItem.Price, 2, MidpointRounding.AwayFromZero)
                invItem.action = "U"

            End If
        Next
        Me.UpdateCasePartTotals()
        'Me.last_update_by = sUser
        'Me.last_update_dt = Now
        Me.last_saved_by = sUser
        Me.last_saved_dt = Now

        'Me.co_status = "W" '941
        Me.status = IST_STATUS.SAVED

        Me.action = "U"

        Me.Update(oracmd)
    End Sub
    Sub CancelOrder(ByVal sUser As String, ByRef oraCmd As OracleCommand)
        Dim ocItem As REC_ON_CODTL = Nothing
        Dim invItem As REC_ON_INV_CODTL = Nothing

        For Each ISTDTL As REC_LCBO_IST_DTL In ist_detailsList.GetValueList
            ocItem = on_codtl.GetOrderLine(ISTDTL.cod_line)
            invItem = on_inv_codtl.GetOrderLine(ISTDTL.cod_line)

            ISTDTL.saved_cod_qty = 0
            ISTDTL.cod_qty = 0
            ISTDTL.last_saved_by = ISTDTL.update_by
            ISTDTL.last_saved_dt = ISTDTL.update_dt
            ISTDTL.deleted_fl = "R"
            ISTDTL.action = "U"

            ocItem.cod_qty = ISTDTL.cod_qty
            ocItem.deleted_fl = ISTDTL.deleted_fl
            ocItem.action = "U"

            invItem.qty = ISTDTL.cod_qty
            invItem.extd_price = ISTDTL.cod_qty * invItem.Price
            invItem.action = "U"
        Next
        'Need to recalculate the units
        Me.UpdateCasePartTotals()

        'Me.last_update_by = sUser
        'Me.last_update_dt = Now
        Me.last_saved_by = sUser
        Me.last_saved_dt = Now
        Me.co_status = "D"
        Me.status = IST_STATUS.CANCELLED
        Me.action = "U"
        Me.Update(oraCmd)
    End Sub

    Sub RecallOrder(ByVal sUser As String, ByRef oraCmd As OracleCommand)

        Me.last_saved_by = sUser
        Me.last_saved_dt = Now
        Me.co_status = "W"
        Me.status = IST_STATUS.SAVED
        Me.action = "U"
        Me.Update(oraCmd)

    End Sub

    Sub RequestForAuth(ByVal sUser As String, ByRef oraCmd As OracleCommand)
        'ONLY ALLOW SAVED OR SAVED-EDIT ORDERS BE SAVED
        If Me.status.StartsWith("SAVE") Then

            Me.last_saved_by = sUser
            Me.last_saved_dt = Now
            'Me.co_status = "O" 'DSO
            Me.co_status = "W" '941
            Me.status = IST_STATUS.APPROVAL_REQUESTED

            Me.request_auth_user = Me.last_saved_by
            Me.request_auth_dt = Me.last_saved_dt
            Me.action = "U"
            Me.Update(oraCmd)
        End If
    End Sub

    Sub SubmitOrder(ByVal sUser As String, ByRef oraCmd As OracleCommand)
        Dim ocItem As REC_ON_CODTL = Nothing
        Dim invItem As REC_ON_INV_CODTL = Nothing
        Dim codItem As REC_CODTL = Nothing

        'CAN ONLY SUBMIT IF IT IS IN APPROVAL REQUESTED STATUS
        If Me.status = IST_STATUS.APPROVAL_REQUESTED Then

            Me.cohdr = New REC_COHDR(Me.on_cohdr)
            Me.cohdr.co_status = "O"

            Me.codtl = New REC_CODTL_LIST

            For Each ISTDTL As REC_LCBO_IST_DTL In Me.ist_detailsList.GetValueList
                ocItem = Me.on_codtl.GetOrderLine(ISTDTL.cod_line)
                invItem = Me.on_inv_codtl.GetOrderLine(ISTDTL.cod_line)

                'FOREIGN KEY CONSTRAINT NEED TO DELETE IMMEDIATELY
                'ocItem.action = "D" 'DSO
                'ocItem.Update(oraCmd) 'DSO

                'JUST UPDATE FOR 941 
                ocItem.action = "U" '941
                ocItem.Update(oraCmd) '941

                'THE INSERT IS ONLY FOR DSO???? ASK SHIRLEY AGAIN
                codItem = New REC_CODTL(ocItem) 'DSO
                codItem.action = "A" 'DSO
                Me.codtl.Add(codItem.cod_line, codItem)

            Next

            Me.authorized_by = sUser
            Me.last_update_dt = Now
            Me.last_update_by = sUser
            Me.co_status = "O"
            Me.status = IST_STATUS.SUBMITTED
            Me.action = "U"

            'NEED TO OVERRIDE HERE
            'Me.on_cohdr.action = "D" 'DSO
            Me.on_cohdr.co_status = "O" '941
            Me.on_cohdr.action = "U" '941

            'THE INSERT IS ONLY FOR DSO???? ASK SHIRLEY AGAIN
            Me.cohdr.action = "A"

            Me.Update(oraCmd)
        End If
    End Sub


End Class
