Imports Microsoft.VisualBasic
Imports System.Data
Imports System.Data.OleDb
Imports System.Data.OracleClient

Public Class REC_ON_COHDR

    Private sCo_odno As String
    Public Property co_odno() As String
        Get
            Return sCo_odno
        End Get
        Set(ByVal value As String)
            sCo_odno = value
        End Set
    End Property


    Private sCO_Status As String
    Public Property co_status() As String
        Get
            Return sCO_Status
        End Get
        Set(ByVal value As String)
            sCO_Status = value
        End Set
    End Property


    Private sRoute_Code As String
    Public Property route_code() As String
        Get
            Return sRoute_Code
        End Get
        Set(ByVal value As String)
            sRoute_Code = value
        End Set
    End Property


    Private iRoute_Stop As Integer
    Public Property route_stop() As Integer
        Get
            Return iRoute_Stop
        End Get
        Set(ByVal value As Integer)
            iRoute_Stop = value
        End Set
    End Property

    Private sPrior_Name As String
    Public Property prior_name() As String
        Get
            Return sPrior_Name
        End Get
        Set(ByVal value As String)
            sPrior_Name = value
        End Set
    End Property


    Private sCu_No As String
    Public Property cu_no() As String
        Get
            Return sCu_No
        End Get
        Set(ByVal value As String)
            sCu_No = value
        End Set
    End Property

    Private sShip_To As String
    Public Property ship_to() As String
        Get
            Return sShip_To
        End Get
        Set(ByVal value As String)
            sShip_To = value
        End Set
    End Property


    Private dtmOrder_dt_tm As DateTime
    Public Property order_dt_tm() As DateTime
        Get
            Return dtmOrder_dt_tm
        End Get
        Set(ByVal value As DateTime)
            dtmOrder_dt_tm = value
        End Set
    End Property


    Private dtmOrder_Reqd_dt As DateTime
    Public Property order_reqd_dt() As DateTime
        Get
            Return dtmOrder_Reqd_dt
        End Get
        Set(ByVal value As DateTime)
            dtmOrder_Reqd_dt = value
        End Set
    End Property


    Private dtmShip_Reqd_dt As DateTime
    Public Property ship_reqd_dt() As DateTime
        Get
            Return dtmShip_Reqd_dt
        End Get
        Set(ByVal value As DateTime)
            dtmShip_Reqd_dt = value
        End Set
    End Property

    Private sOrder_Type As String
    Public Property order_type() As String
        Get
            Return sOrder_Type
        End Get
        Set(ByVal value As String)
            sOrder_Type = value
        End Set
    End Property

    Private sCarrier As String
    'carrier char
    'order_del_dt
    'traffic_ref_no


    Private iSite As Integer
    Public Property site() As Integer
        Get
            Return iSite
        End Get
        Set(ByVal value As Integer)
            iSite = value
        End Set
    End Property

    Private sPU_Order_Fl As String
    Public Property pu_order_fl() As String
        Get
            Return sPU_Order_Fl
        End Get
        Set(ByVal value As String)
            sPU_Order_Fl = value
        End Set
    End Property


    Private sDelivery_Fl As String
    Public Property delivery_fl() As String
        Get
            Return sDelivery_Fl
        End Get
        Set(ByVal value As String)
            sDelivery_Fl = value
        End Set
    End Property

    Private sEntry_Fl As String
    Public Property entry_fl() As String
        Get
            Return sEntry_Fl
        End Get
        Set(ByVal value As String)
            sEntry_Fl = value
        End Set
    End Property


    Private sAction As String
    Public Property action() As String
        Get
            Return sAction
        End Get
        Set(ByVal value As String)
            sAction = value
        End Set
    End Property

    Sub New()
        co_odno = ""
        co_status = ""
        route_code = "9999"
        route_stop = 1
        prior_name = ""
        cu_no = ""
        ship_to = ""
        order_dt_tm = New DateTime
        order_reqd_dt = New DateTime
        ship_reqd_dt = New DateTime
        order_type = ""
        site = 3
        pu_order_fl = "N"
        delivery_fl = "Y"
        entry_fl = "N"
        action = "N"
    End Sub
    Sub New(ByVal sOrderNum As String)
        Me.New()
        Dim dr As DataRow = DB.GetONCOHDRInfo(sOrderNum)
        SetInfo(dr)
    End Sub
    Sub New(ByVal dr As DataRow)
        Me.New()
        SetInfo(dr)
    End Sub

    Sub SetInfo(ByVal drHeader As DataRow)
        If IsDBNull(drHeader("co_odno")) = False Then
            co_odno = drHeader("co_odno")
        End If

        If IsDBNull(drHeader("co_status")) = False Then
            co_status = drHeader("co_status")
        End If

        If IsDBNull(drHeader("route_code")) = False Then
            route_code = drHeader("route_code")
        End If

        If IsDBNull(drHeader("route_stop")) = False Then
            route_stop = drHeader("route_stop")
        End If

        If IsDBNull(drHeader("prior_name")) = False Then
            prior_name = drHeader("prior_name")
        End If

        If IsDBNull(drHeader("cu_no")) = False Then
            cu_no = drHeader("cu_no")
        End If

        If IsDBNull(drHeader("ship_to")) = False Then
            ship_to = drHeader("ship_to")
        End If

        If IsDBNull(drHeader("order_dt_tm")) = False Then
            order_dt_tm = DateTime.Parse(drHeader("order_dt_tm"))
        End If

        If IsDBNull(drHeader("order_reqd_dt")) = False Then
            order_reqd_dt = DateTime.Parse(drHeader("order_reqd_dt"))
        End If

        If IsDBNull(drHeader("ship_reqd_dt")) = False Then
            ship_reqd_dt = DateTime.Parse(drHeader("ship_reqd_dt"))
        End If

        If IsDBNull(drHeader("order_type")) = False Then
            order_type = drHeader("order_type")
        End If

        If IsDBNull(drHeader("site")) = False Then
            site = drHeader("site")
        End If

        If IsDBNull(drHeader("pu_order_fl")) = False Then
            pu_order_fl = drHeader("pu_order_fl")
        End If

        If IsDBNull(drHeader("delivery_fl")) = False Then
            delivery_fl = drHeader("delivery_fl")
        End If

        If IsDBNull(drHeader("entry_fl")) = False Then
            entry_fl = drHeader("entry_fl")
        End If

        If IsDBNull(drHeader("action")) = False Then
            action = drHeader("action")
        End If
    End Sub

    Function validate() As Boolean
        Dim bValid As Boolean = True
        bValid = (co_odno.Trim <> "")
        bValid = bValid AndAlso (co_status.Trim <> "")
        bValid = bValid AndAlso (route_code <> "")
        bValid = bValid AndAlso (cu_no.Trim <> "")
        bValid = bValid AndAlso (ship_to.Trim <> "")
        bValid = bValid AndAlso (order_type.Trim <> "")
        bValid = bValid AndAlso order_dt_tm.Year > 2000

        Return bValid
    End Function

    Sub Update(ByVal oracmd As OracleCommand)
        Dim bValid As Boolean = validate()

        Dim sQry As String = ""

        If action = "A" Then
            sQry = "INSERT INTO DBO.ON_COHDR (" & vbNewLine & _
                   "CO_ODNO " & vbNewLine & _
                   ",CO_STATUS " & vbNewLine & _
                   ",ROUTE_CODE " & vbNewLine & _
                   ",ROUTE_STOP " & vbNewLine & _
                   ",PRIOR_NAME " & vbNewLine & _
                   ",CU_NO " & vbNewLine & _
                   ",SHIP_TO " & vbNewLine & _
                   ",ORDER_DT_TM " & vbNewLine & _
                   ",ORDER_REQD_DT " & vbNewLine & _
                   ",SHIP_REQD_DT " & vbNewLine & _
                   ",ORDER_TYPE " & vbNewLine & _
                   ",SITE " & vbNewLine & _
                   ",PU_ORDER_FL " & vbNewLine & _
                   ",DELIVERY_FL " & vbNewLine & _
                   ",ENTRY_FL " & vbNewLine & _
                   ",PAY_METHOD " & vbNewLine & _
                   ") VALUES (" & vbNewLine & _
                   "'" & co_odno & "'" & vbNewLine & _
                   ",'" & co_status & "'" & vbNewLine & _
                   ",'" & route_code & "'" & vbNewLine & _
                   "," & route_stop & vbNewLine & _
                   ",'" & prior_name & "'" & vbNewLine & _
                   ",'" & cu_no & "'" & vbNewLine & _
                   ",'" & ship_to & "'" & vbNewLine & _
                   ",TO_DATE('" & order_dt_tm.ToString("yyyy-MM-dd HH:mm:ss") & "','YYYY-MM-DD HH24:MI:SS') " & vbNewLine & _
                   "," & IIf(order_reqd_dt.Year < 2000, "NULL", "TO_DATE('" & order_reqd_dt.ToString("yyyy-MM-dd HH:mm:ss") & "','YYYY-MM-DD HH24:MI:SS')") & vbNewLine & _
                   "," & IIf(ship_reqd_dt.Year < 2000, "NULL", "TO_DATE('" & ship_reqd_dt.ToString("yyyy-MM-dd HH:mm:ss") & "','YYYY-MM-DD HH24:MI:SS') ") & vbNewLine & _
                   ",'" & order_type & "'" & vbNewLine & _
                   "," & site & vbNewLine & _
                   ",'" & pu_order_fl & "'" & vbNewLine & _
                   ",'" & delivery_fl & "'" & vbNewLine & _
                   ",'" & entry_fl & "'" & vbNewLine & _
                   ",' '" & vbNewLine & _
                   ")"

        ElseIf action = "U" Then
            sQry = "UPDATE DBO.ON_COHDR " & vbNewLine & _
                   "SET CO_STATUS = '" & co_status & "'" & vbNewLine & _
                   ",ROUTE_CODE = '" & route_code & "'" & vbNewLine & _
                   ",ROUTE_STOP = " & route_stop & vbNewLine & _
                   ",PRIOR_NAME = '" & prior_name & "'" & vbNewLine & _
                   ",CU_NO = '" & cu_no & "'" & vbNewLine & _
                   ",SHIP_TO = '" & ship_to & "'" & vbNewLine & _
                   ",ORDER_DT_TM = TO_DATE('" & order_dt_tm.ToString("yyyy-MM-dd HH:mm:ss") & "','YYYY-MM-DD HH24:MI:SS') " & vbNewLine & _
                   ",ORDER_REQD_DT = " & IIf(order_reqd_dt.Year < 2000, "NULL", "TO_DATE('" & order_reqd_dt.ToString("yyyy-MM-dd HH:mm:ss") & "','YYYY-MM-DD HH24:MI:SS')") & vbNewLine & _
                   ",SHIP_REQD_DT = " & IIf(ship_reqd_dt.Year < 2000, "NULL", "TO_DATE('" & ship_reqd_dt.ToString("yyyy-MM-dd HH:mm:ss") & "','YYYY-MM-DD HH24:MI:SS') ") & vbNewLine & _
                   ",ORDER_TYPE = '" & order_type & "'" & vbNewLine & _
                   ",SITE = " & site & vbNewLine & _
                   ",PU_ORDER_FL = '" & pu_order_fl & "'" & vbNewLine & _
                   ",DELIVERY_FL = '" & delivery_fl & "'" & vbNewLine & _
                   ",ENTRY_FL = '" & entry_fl & "'" & vbNewLine & _
                   " WHERE CO_ODNO = '" & co_odno.Trim.PadRight(16) & "'"
        ElseIf action = "D" Then
            sQry = "DELETE DBO.ON_COHDR " & vbNewLine & _
                   " WHERE CO_ODNO = '" & co_odno.Trim.PadRight(16) & "'"
        Else
            Exit Sub
        End If

        Try
            If bValid = False Then
                Throw New Exception("Error validating ON_COHDR record")
            End If
            oracmd.CommandText = sQry
            oracmd.Parameters.Clear()

            If oracmd.ExecuteNonQuery <= 0 Then
                Throw New Exception("Error UPDATING COHDR record query: " & sQry & "!")
            End If

        Catch ex As Exception
            Throw New Exception("COHDR.Update->" & co_odno & ".Error Message:" & ex.Message.ToString)
        End Try
    End Sub



End Class
