﻿Imports System.Web.Script.Serialization

Public Class Utils

    Public Shared Function Environment() As String
        Return ConfigurationManager.AppSettings.Get("ENVIRONMENT")
    End Function

    Public Shared Function IsApprovalRequired() As Boolean
        If Not ConfigurationManager.AppSettings.Get("APPROVAL_REQUIRED") Is Nothing Then
            Return (ConfigurationManager.AppSettings.Get("APPROVAL_REQUIRED").ToUpper() = "Y")
        End If
        Return True
    End Function

    Public Shared Function DataTableToDictionary(table As DataTable) As List(Of Dictionary(Of String, Object))
        Dim list As New List(Of Dictionary(Of String, Object))()

        For Each row As DataRow In table.Rows
            Dim dict As New Dictionary(Of String, Object)()

            For Each col As DataColumn In table.Columns
                dict(col.ColumnName) = row(col)
            Next
            list.Add(dict)
        Next

        Return list

    End Function

    Public Shared Function DataTableToJSON(table As DataTable) As String
        Dim serializer As New JavaScriptSerializer()
        Return serializer.Serialize(DataTableToDictionary(table))
    End Function

End Class

