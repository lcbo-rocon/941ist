﻿Imports System.Exception
Imports System.Runtime.Serialization

<Serializable()> _
Public Class ISTException
    Inherits Exception
    Implements ISerializable

    Private _errorCode As Int32
    Private _dateTime As DateTime

    Public ReadOnly Property ErrorCode() As Int32
        Get
            Return _errorCode
        End Get
    End Property

    Public Sub New(ByVal ISTErrorCode As Int32)
        MyBase.New()
        _errorCode = ISTErrorCode
        _dateTime = Now()
    End Sub

    Public Sub New(ByVal ISTErrorCode As Int32, ByVal message As String)
        MyBase.New(message)
        _errorCode = ISTErrorCode
        _dateTime = Now()
    End Sub

    Public Sub New(ByVal ISTErrorCode As Int32, ByVal message As String, ByVal inner As Exception)
        MyBase.New(message, inner)
        _errorCode = ISTErrorCode
        _dateTime = Now()
    End Sub

    ' This constructor is needed for serialization.
    Protected Sub New(ByVal ISTErrorCode As Int32, ByVal info As SerializationInfo, ByVal context As StreamingContext)
        MyBase.New(info, context)
        _errorCode = ISTErrorCode
        _dateTime = Now()
    End Sub

    Public Overrides Function ToString() As String
        Dim sb As StringBuilder = New StringBuilder()
        sb.Append(_dateTime.ToString("yyyy/MM/dd hh:mm:ss"))
        sb.Append(" - ")
        sb.Append(_errorCode.ToString())
        sb.Append(" - ")
        sb.Append(MyBase.Message)
        sb.AppendLine()
        sb.Append(MyBase.ToString())

        Return sb.ToString()
    End Function

End Class
