Imports Microsoft.VisualBasic
Imports System.Collections
Imports System.Data.OleDb
Imports System.Data
Imports System.Data.OracleClient

Public Class REC_ON_INV_CODTL
    Implements IComparable

    Private iInv_no As Integer
    Public Property inv_no() As Integer
        Get
            Return iInv_no
        End Get
        Set(ByVal value As Integer)
            iInv_no = value
        End Set
    End Property

    Private sCo_odno As String
    Public Property co_odno() As String
        Get
            Return sCo_odno
        End Get
        Set(ByVal value As String)
            sCo_odno = value
        End Set
    End Property

    ''' <summary>
    ''' E = Estimate
    ''' A = Actual
    ''' R = Return
    ''' </summary>
    ''' <remarks></remarks>
    Private sCo_Inv_Type As String
    Public Property co_inv_type() As String
        Get
            Return sCo_Inv_Type
        End Get
        Set(ByVal value As String)
            sCo_Inv_Type = value
        End Set
    End Property

    ''' <summary>
    ''' orderline
    ''' </summary>
    ''' <remarks></remarks>
    Private iCod_Line As Integer
    Public Property cod_line() As Integer
        Get
            Return iCod_Line
        End Get
        Set(ByVal value As Integer)
            iCod_Line = value
        End Set
    End Property

    ''' <summary>
    ''' This is the basic unit price including bottle deposit
    ''' </summary>
    ''' <remarks></remarks>
    Private dPrice As Decimal
    Public Property Price() As Decimal
        Get
            Return dPrice
        End Get
        Set(ByVal value As Decimal)
            dPrice = value
        End Set
    End Property

    ''' <summary>
    ''' Price * Qty (Basic including bottle deposit * order Qty)
    ''' </summary>
    ''' <remarks></remarks>
    Private dExtd_Price As Decimal
    Public Property extd_price() As Decimal
        Get
            Return dExtd_Price
        End Get
        Set(ByVal value As Decimal)
            dExtd_Price = value
        End Set
    End Property

    Private iTtl_Cases As Integer
    Public Property ttl_cases() As Integer
        Get
            Return iTtl_Cases
        End Get
        Set(ByVal value As Integer)
            iTtl_Cases = value
        End Set
    End Property

    Private iTtl_Units As Integer
    Public Property ttl_units() As Integer
        Get
            Return iTtl_Units
        End Get
        Set(ByVal value As Integer)
            iTtl_Units = value
        End Set
    End Property

    ''' <summary>
    ''' sku
    ''' </summary>
    ''' <remarks></remarks>
    Private sItem As String
    Public Property item() As String
        Get
            Return sItem
        End Get
        Set(ByVal value As String)
            sItem = value
        End Set
    End Property

    ''' <summary>
    ''' always ' '
    ''' </summary>
    ''' <remarks></remarks>
    Private sPackaging_Code As String
    Public Property packaging_code() As String
        Get
            Return sPackaging_Code
        End Get
        Set(ByVal value As String)
            sPackaging_Code = value
        End Set
    End Property

    ''' <summary>
    ''' bottle deposit for item per unit
    ''' </summary>
    ''' <remarks></remarks>
    Private dDeposit_Price As Decimal
    Public Property deposit_price() As Decimal
        Get
            Return dDeposit_Price
        End Get
        Set(ByVal value As Decimal)
            dDeposit_Price = value
        End Set
    End Property

    ''' <summary>
    ''' shipped qty
    ''' </summary>
    ''' <remarks></remarks>
    Private iQty As Integer
    Public Property qty() As Integer
        Get
            Return iQty
        End Get
        Set(ByVal value As Integer)
            iQty = value
        End Set
    End Property

    ''' <summary>
    ''' item category
    ''' </summary>
    ''' <remarks></remarks>
    Private sItem_Category As String
    Public Property item_category() As String
        Get
            Return sItem_Category
        End Get
        Set(ByVal value As String)
            sItem_Category = value
        End Set
    End Property

    ''' <summary>
    ''' Original Basic Price including deposit per unit.
    ''' This will be different than Price if there is a price override
    ''' </summary>
    ''' <remarks></remarks>
    Private dOrig_Price As Decimal
    Public Property orig_price() As Decimal
        Get
            Return dOrig_Price
        End Get
        Set(ByVal value As Decimal)
            dOrig_Price = value
        End Set
    End Property


    Private sPrice_Chg_Comment As String
    Public Property price_chg_comment() As String
        Get
            Return sPrice_Chg_Comment
        End Get
        Set(ByVal value As String)
            sPrice_Chg_Comment = value
        End Set
    End Property

    ''' <summary>
    ''' This field should include the final price per unit (all tax including etc...)
    ''' However, ROC/ROCON is buggy and doesn't populate this correctly and Qlogitek doesn't
    ''' touch this column.
    ''' 
    ''' Nice to have if we can populate it. Calculate based on the basic price (from price)
    ''' </summary>
    ''' <remarks></remarks>
    Private dRetail_Price As Decimal
    Public Property retail_price() As Decimal
        Get
            Return dRetail_Price
        End Get
        Set(ByVal value As Decimal)
            dRetail_Price = value
        End Set
    End Property

    ''' <summary>
    ''' This field should include the final price per unit (all tax including etc...)
    ''' However, ROC/ROCON is buggy and doesn't populate this correctly and Qlogitek doesn't
    ''' touch this column.
    ''' 
    ''' Nice to have if we can populate it. Calculate based on the basic price (from orig_price)
    ''' </summary>
    ''' <remarks></remarks>
    Private dOrig_Retail_Price As Decimal
    Public Property orig_retail_price() As Decimal
        Get
            Return dOrig_Retail_Price
        End Get
        Set(ByVal value As Decimal)
            dOrig_Retail_Price = value
        End Set
    End Property


    Private sAction As String
    Public Property action() As String
        Get
            Return sAction
        End Get
        Set(ByVal value As String)
            sAction = value
        End Set
    End Property

    Sub New()
        inv_no = 0
        co_odno = ""
        co_inv_type = ""
        cod_line = 0
        Price = 0D
        extd_price = 0D
        ttl_cases = 0
        ttl_units = 0
        item = ""
        packaging_code = " "
        deposit_price = 0D
        qty = 0
        item_category = ""
        orig_price = 0D
        price_chg_comment = ""
        retail_price = 0D
        orig_retail_price = 0D
        action = "N"
    End Sub
    Sub New(ByVal dr As DataRow)
        Me.New()
        SetLineItemInfo(dr)
    End Sub

    Public Sub SetLineItemInfo(ByVal dr As DataRow)
        If IsDBNull(dr("INV_NO")) = False Then
            inv_no = dr("INV_NO")
        End If

        If IsDBNull(dr("CO_ODNO")) = False Then
            co_odno = dr("CO_ODNO")
        End If

        If IsDBNull(dr("CO_INV_TYPE")) = False Then
            co_inv_type = dr("CO_INV_TYPE")
        End If

        If IsDBNull(dr("COD_LINE")) = False Then
            cod_line = dr("COD_LINE")
        End If

        If IsDBNull(dr("PRICE")) = False Then
            Price = dr("PRICE")
        End If

        If IsDBNull(dr("EXTD_PRICE")) = False Then
            extd_price = dr("EXTD_PRICE")
        End If

        If IsDBNull(dr("TTL_CASES")) = False Then
            ttl_cases = dr("TTL_CASES")
        End If

        If IsDBNull(dr("TTL_UNITS")) = False Then
            ttl_units = dr("TTL_UNITS")
        End If

        If IsDBNull(dr("ITEM")) = False Then
            item = dr("ITEM")
        End If

        If IsDBNull(dr("PACKAGING_CODE")) = False Then
            packaging_code = dr("PACKAGING_CODE")
        End If

        If IsDBNull(dr("DEPOSIT_PRICE")) = False Then
            deposit_price = dr("DEPOSIT_PRICE")
        End If

        If IsDBNull(dr("QTY")) = False Then
            qty = dr("QTY")
        End If

        If IsDBNull(dr("ITEM_CATEGORY")) = False Then
            item_category = dr("ITEM_CATEGORY")
        End If

        If IsDBNull(dr("ORIG_PRICE")) = False Then
            orig_price = dr("ORIG_PRICE")
        End If

        If IsDBNull(dr("PRICE_CHG_COMMENT")) = False Then
            price_chg_comment = dr("PRICE_CHG_COMMENT")
        End If

        If IsDBNull(dr("RETAIL_PRICE")) = False Then
            retail_price = dr("RETAIL_PRICE")
        End If

        If IsDBNull(dr("ORIG_RETAIL_PRICE")) = False Then
            orig_retail_price = dr("ORIG_RETAIL_PRICE")
        End If

        If IsDBNull(dr("ACTION")) = False Then
            action = dr("ACTION")
        End If
    End Sub

    Public Function validate() As Boolean
        Dim bValid As Boolean = True

        bValid = inv_no > 0
        bValid = bValid AndAlso (co_odno.Trim <> "")
        bValid = bValid AndAlso (co_inv_type.Trim <> "")
        bValid = bValid AndAlso (cod_line > 0)
        'bValid = bValid AndAlso (Price > 0)
        bValid = bValid AndAlso (extd_price = (Price * qty))
        bValid = bValid AndAlso (item.Trim <> "")
        bValid = bValid AndAlso (qty >= 0)
        bValid = bValid AndAlso (item_category.Trim <> "")

        Return bValid
    End Function

    Sub Update(ByRef oraCmd As OracleCommand)
        Dim bValid As Boolean = validate()
        Dim sQry As String = ""

        If action = "A" Then
            sQry = "INSERT INTO DBO.ON_INV_CODTL (" & vbNewLine & _
                                 " INV_NO " & vbNewLine & _
                                 ",CO_ODNO " & vbNewLine & _
                                 ",CO_INV_TYPE " & vbNewLine & _
                                 ",COD_LINE " & vbNewLine & _
                                 ",PRICE " & vbNewLine & _
                                 ",EXTD_PRICE " & vbNewLine & _
                                 ",TTL_CASES " & vbNewLine & _
                                 ",TTL_UNITS " & vbNewLine & _
                                 ",ITEM " & vbNewLine & _
                                 ",PACKAGING_CODE " & vbNewLine & _
                                 ",DEPOSIT_PRICE " & vbNewLine & _
                                 ",QTY " & vbNewLine & _
                                 ",ITEM_CATEGORY " & vbNewLine & _
                                 ",ORIG_PRICE " & vbNewLine & _
                                 ",RETAIL_PRICE " & vbNewLine & _
                                 ",ORIG_RETAIL_PRICE " & vbNewLine & _
                                 ") VALUES (" & vbNewLine & _
                                 inv_no & vbNewLine & _
                                 ",'" & co_odno & "'" & vbNewLine & _
                                 ",'" & co_inv_type & "'" & vbNewLine & _
                                 "," & cod_line & vbNewLine & _
                                 "," & Price & vbNewLine & _
                                 "," & extd_price & vbNewLine & _
                                 "," & ttl_cases & vbNewLine & _
                                 "," & ttl_units & vbNewLine & _
                                 ",'" & item & "'" & vbNewLine & _
                                 ",'" & packaging_code & "'" & vbNewLine & _
                                 "," & deposit_price & vbNewLine & _
                                 "," & qty & vbNewLine & _
                                 ",'" & item_category & "'" & vbNewLine & _
                                 "," & orig_price & vbNewLine & _
                                 "," & retail_price & vbNewLine & _
                                 "," & orig_retail_price & vbNewLine & _
                                 ")"
        ElseIf action = "U" Then
            sQry = "UPDATE DBO.ON_INV_CODTL " & vbNewLine & _
                     " SET CO_INV_TYPE = '" & co_inv_type & "'" & vbNewLine & _
                     ",COD_LINE  = " & cod_line & vbNewLine & _
                     ",PRICE = " & Price & vbNewLine & _
                     ",EXTD_PRICE = " & extd_price & vbNewLine & _
                     ",TTL_CASES = " & ttl_cases & vbNewLine & _
                     ",TTL_UNITS = " & ttl_units & vbNewLine & _
                     ",ITEM = '" & item & "'" & vbNewLine & _
                     ",PACKAGING_CODE = '" & packaging_code & "'" & vbNewLine & _
                     ",DEPOSIT_PRICE = " & deposit_price & vbNewLine & _
                     ",QTY = " & qty & vbNewLine & _
                     ",ITEM_CATEGORY ='" & item_category & "'" & vbNewLine & _
                     ",ORIG_PRICE = " & orig_price & vbNewLine & _
                     ",RETAIL_PRICE = " & retail_price & vbNewLine & _
                     ",ORIG_RETAIL_PRICE = " & orig_retail_price & vbNewLine & _
                     " WHERE INV_NO = " & inv_no & vbNewLine & _
                     " AND CO_ODNO = '" & co_odno.TrimEnd.PadRight(16) & "'" & vbNewLine & _
                     " AND COD_LINE = " & cod_line
        Else
            Exit Sub
        End If

        Try
            If bValid = False Then
                Throw New Exception("Error validating ON_INV_CODTL record")
            End If
            oraCmd.CommandText = sQry
            oraCmd.Parameters.Clear()

            If oraCmd.ExecuteNonQuery <= 0 Then
                Throw New Exception("Error Updating ON_INV_CODTL record query: " & sQry & "!")
            End If

        Catch ex As Exception
            Throw New Exception("ON_INV_CODTL.Insert->" & co_odno & "," & cod_line & ".Error Message:" & ex.Message.ToString)
        End Try

    End Sub
    Public Function CompareTo(ByVal obj As Object) As Integer Implements System.IComparable.CompareTo
        Dim b As REC_ON_INV_CODTL = obj
        Return Me.cod_line.CompareTo(b.cod_line)
    End Function
End Class

'INV_NO,NUMBER,10,,N,,
'CO_ODNO,CHAR,16,,N,,
'CO_INV_TYPE,CHAR,1,,N,,
'COD_LINE,NUMBER,10,,N,,
'PRICE,NUMBER,14,2,N,,
'EXTD_PRICE,NUMBER,14,2,N,,
'TTL_CASES,NUMBER,10,,Y,,
'TTL_UNITS,NUMBER,10,,Y,,
'ITEM,CHAR,20,,Y,,
'PACKAGING_CODE,CHAR,1,,Y,,
'DEPOSIT_PRICE,NUMBER,14,2,Y,,
'QTY,NUMBER,10,,Y,,
'ITEM_CATEGORY,CHAR,10,,Y,,
'ORIG_PRICE,NUMBER,14,2,N,,0
'PRICE_CHG_COMMENT,VARCHAR2,32,,Y,,
'RETAIL_PRICE,NUMBER,14,2,Y,,
'ORIG_RETAIL_PRICE,NUMBER,14,2,Y,,