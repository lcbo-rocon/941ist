Imports Microsoft.VisualBasic
Imports System.Data
Imports System.Data.OleDb
Imports System.Data.OracleClient

Public Class REC_ON_CODTL_LIST
    Inherits SortedList
    'co_odno = ""
    '       cod_line = 0
    '       item = ""
    '       packaging_code = " "
    '       cod_qty = 0
    '       use_blocked_stock_fl = "N"
    '       total_allocated_qty = 0
    '       picked_qty = 0
    '       deleted_fl = "N"
    '       unit_price = 0D
    Public ReadOnly Property ON_CODTL_LIST() As DataTable
        Get
            Dim dt As New DataTable
            Dim dr As DataRow = Nothing

            With dt.Columns
                .Add("CO_ODNO")
                .Add("COD_LINE")
                .Add("PACKAGING_CODE")
                .Add("COD_QTY")
                .Add("USE_BLOCK_STOCK_FL")
                .Add("TOTAL_ALLOCATED_QTY")
                .Add("PICKED_QTY")
                .Add("DELETED_FL")
                .Add("UNIT_PRICE")
            End With

            For Each rec As REC_ON_CODTL In Me.GetValueList
                dr = dt.NewRow
                dr("CO_ODNO") = rec.co_odno
                dr("COD_LINE") = rec.cod_line
                dr("PACKAGING_CODE") = rec.packaging_code
                dr("COD_QTY") = rec.cod_qty
                dr("USE_BLOCK_STOCK_FL") = rec.use_blocked_stock_fl
                dr("TOTAL_ALLOCATED_QTY") = rec.total_allocated_qty
                dr("PICKED_QTY") = rec.picked_qty
                dr("DELETED_FL") = rec.deleted_fl
                dr("UNIT_PRICE") = rec.unit_price
            Next
            Return dt
        End Get

    End Property

    Private sCo_Odno As String
    Public Property co_odno() As String
        Get
            Return sCo_Odno
        End Get
        Set(ByVal value As String)
            sCo_Odno = value
            For Each rec As REC_ON_CODTL In Me.GetValueList
                rec.co_odno = value
            Next
        End Set
    End Property

    Sub New()
        MyBase.New()
        co_odno = ""
    End Sub
    Sub New(ByVal sOrderNum As String)
        Me.New()
        co_odno = sOrderNum
        Dim dt As DataTable = DB.GetONCODTL(sOrderNum)
        setInfo(dt)
    End Sub
    Sub New(ByVal dtONCodtl As DataTable)
        Me.New()
        setInfo(dtONCodtl)
        If dtONCodtl.Rows.Count > 0 Then
            co_odno = dtONCodtl.Rows(0).Item("CO_ODNO")
        End If
    End Sub
    Sub setInfo(ByVal dtONCodtl As DataTable)
        Dim rec As REC_ON_CODTL = Nothing
        For Each dr As DataRow In dtONCodtl.Rows
            rec = New REC_ON_CODTL(dr)
            Me.Add(rec.cod_line, rec)
        Next
    End Sub
    Public Overloads Sub Add(ByVal iCod_Line As Integer, ByVal obj As REC_ON_CODTL)
        Try
            MyBase.Add(iCod_Line, obj)
        Catch ex As Exception
            Throw New Exception("REC_ON_CODTL_LIST.ADD(" & iCod_Line & ", REC_ON_CODTL) " & ex.Message.ToString)
        End Try

    End Sub

    Public Function GetOrderLine(ByVal cod_line As Integer) As REC_ON_CODTL
        Return Item(cod_line)
    End Function

    Public Function GetOrderLine(ByVal item As String) As REC_ON_CODTL
        Dim rec As REC_ON_CODTL = Nothing
        For Each itm As REC_ON_CODTL In Me.GetValueList
            If itm.item.Trim = item.Trim And itm.deleted_fl = "N" Then
                rec = itm
            End If
        Next
        Return rec
    End Function

    Public Sub Update(ByVal oracmd As OracleCommand)
        For Each rec As REC_ON_CODTL In Me.GetValueList
            If rec.action = "A" Or rec.action = "U" Then
                rec.Update(oracmd)
            End If
        Next
    End Sub

End Class
