Imports Microsoft.VisualBasic
Imports System.Data
Imports System.Data.OleDb
Imports System.Data.OracleClient

Public Class REC_COHDR
    ''' <summary>
    ''' CO_ODNO,CHAR,16,,N,,
    ''' </summary>
    ''' <remarks></remarks>
    Private sCo_odno As String
    Public Property co_odno() As String
        Get
            Return sCo_odno
        End Get
        Set(ByVal value As String)
            sCo_odno = value
        End Set
    End Property

    ''' <summary>
    ''' CO_STATUS,CHAR,1,,N,,'O'
    ''' </summary>
    ''' <remarks></remarks>
    Private sCo_Status As String
    Public Property co_status() As String
        Get
            Return sCo_Status
        End Get
        Set(ByVal value As String)
            sCo_Status = value
        End Set
    End Property

    ''' <summary>
    ''' ROUTE_CODE,CHAR,4,,N,,
    ''' </summary>
    ''' <remarks></remarks>
    Private sRoute_Code As String
    Public Property route_code() As String
        Get
            Return sRoute_Code
        End Get
        Set(ByVal value As String)
            sRoute_Code = value
        End Set
    End Property

    ''' <summary>
    ''' PRIOR_NAME,CHAR,6,,Y,,  
    ''' Consider using this as a linkage. Put the freight forwarded order number in here
    ''' </summary>
    ''' <remarks></remarks>
    Private sPrior_Name As String
    Public Property prior_name() As String
        Get
            Return sPrior_Name
        End Get
        Set(ByVal value As String)
            sPrior_Name = value
        End Set
    End Property

    ''' <summary>
    ''' CU_NO,CHAR,10,,N,,
    ''' </summary>
    ''' <remarks></remarks>
    Private sCu_No As String
    Public Property cu_no() As String
        Get
            Return sCu_No
        End Get
        Set(ByVal value As String)
            sCu_No = value
        End Set
    End Property

    ''' <summary>
    ''' SHIP_TO,CHAR,10,,Y,,
    ''' always same as customer number at 941.
    ''' </summary>
    ''' <remarks></remarks>
    Private sShip_To As String
    Public Property ship_to() As String
        Get
            Return sShip_To
        End Get
        Set(ByVal value As String)
            sShip_To = value
        End Set
    End Property

    ''' <summary>
    ''' ORDER_DT_TM,DATE,7,,N,,
    ''' date order was placed.
    ''' </summary>
    ''' <remarks></remarks>
    Private dtOrder_Dt_TM As DateTime
    Public Property order_dt_tm() As DateTime
        Get
            Return dtOrder_Dt_TM
        End Get
        Set(ByVal value As DateTime)
            dtOrder_Dt_TM = value
        End Set
    End Property

    ''' <summary>
    ''' ORDER_REQD_DT,DATE,7,,Y,,
    ''' </summary>
    ''' <remarks></remarks>
    Private sOrder_Reqd_Dt As String
    Public Property order_reqd_dt() As String
        Get
            Return sOrder_Reqd_Dt
        End Get
        Set(ByVal value As String)
            sOrder_Reqd_Dt = value
        End Set
    End Property

    ''' <summary>
    ''' ORDER_TYPE,CHAR,3,,Y,,
    ''' </summary>
    ''' <remarks></remarks>
    Private sOrder_Type As String
    Public Property order_type() As String
        Get
            Return sOrder_Type
        End Get
        Set(ByVal value As String)
            sOrder_Type = value
        End Set
    End Property

    ''' <summary>
    ''' TRANSPORT_MODE,CHAR,2,,N,,
    ''' use blank
    ''' </summary>
    ''' <remarks></remarks>
    Private sTransport_Mode As String
    Public Property transport_mode() As String
        Get
            Return sTransport_Mode
        End Get
        Set(ByVal value As String)
            sTransport_Mode = value
        End Set
    End Property

    ''' <summary>
    ''' ORDER_CANCELED_Y_OR_N,CHAR,1,,N,,'N'
    ''' </summary>
    ''' <remarks></remarks>
    Private sOrder_Cancelled_Y_Or_N As String
    Public Property order_canceled_y_or_n() As String
        Get
            Return sOrder_Cancelled_Y_Or_N
        End Get
        Set(ByVal value As String)
            sOrder_Cancelled_Y_Or_N = value
        End Set
    End Property

    'CARRIER,CHAR,15,,Y,,
    'skipping timestamp since it is written by trigger

    'SHIP_REQD_DT,DATE,7,,Y,,
    'PICK_SEQ_NO,NUMBER,10,,Y,,

    ''' <summary>
    ''' STOP_SEQ_NO,VARCHAR2,4,,Y,,
    ''' </summary>
    ''' <remarks></remarks>
    Private sStop_Seq_No As String
    Public Property stop_seq_no() As String
        Get
            Return sStop_Seq_No
        End Get
        Set(ByVal value As String)
            sStop_Seq_No = value
        End Set
    End Property

    'MODE_CODE,VARCHAR2,3,,Y,,
    'PRICE_CODE,CHAR,1,,Y,,
    'DISCOUNT_CODE,VARCHAR2,2,,Y,,
    'MERCHANDISE_CLASS,CHAR,1,,Y,,
    'PRICE_SYMBOL,CHAR,1,,Y,,
    'SHIP_CODE,CHAR,1,,Y,,
    'OWNERSHIP_CODE,VARCHAR2,10,,Y,,
    'SHIPPING_INSTRUCTION,VARCHAR2,30,,Y,,
    'EMPLOYEE_NUMBER,VARCHAR2,11,,Y,,
    'TRACKING_CODE,CHAR,1,,Y,,
    'ACCOUNTING_CODE,VARCHAR2,10,,Y,,
    'CHARGE_FREIGHT,CHAR,1,,Y,,

    ''' <summary>
    '''     COHDR_COMMENT,VARCHAR2,25,,Y,,
    ''' consider using this as a comment for freight forward
    ''' </summary>
    ''' <remarks></remarks>
    Private sCohdr_Comment As String
    Public Property cohdr_comment() As String
        Get
            Return sCohdr_Comment
        End Get
        Set(ByVal value As String)
            sCohdr_Comment = value
        End Set
    End Property

    'TOTAL_ORDER_VALUE,NUMBER,10,,Y,,
    'CONSOLIDATE_ORDERS_FL,CHAR,1,,Y,,

    ''' <summary>
    ''' 
    ''' </summary>
    ''' <remarks></remarks>
    Private sEnd_Of_Order_Fl As String
    Public Property end_of_order_fl() As String
        Get
            Return sEnd_Of_Order_Fl
        End Get
        Set(ByVal value As String)
            sEnd_Of_Order_Fl = value
        End Set
    End Property

    ''' <summary>
    ''' PAY_METHOD,CHAR,16,,Y,,
    ''' captures air miles number
    ''' </summary>
    ''' <remarks></remarks>
    Private sPay_Method As String
    Public Property pay_method() As String
        Get
            Return sPay_Method
        End Get
        Set(ByVal value As String)
            sPay_Method = value
        End Set
    End Property

    'SHIPSET_NO,VARCHAR2,6,,Y,,
    'HOST_ODNO,VARCHAR2,22,,Y,,
    'EDI_STATUS,CHAR,1,,Y,,
    'PRINT_DT,DATE,7,,Y,,
    'SERVICE_LEVEL,CHAR,3,,Y,,

    ''' <summary>
    ''' default is null
    ''' </summary>
    ''' <remarks></remarks>
    Private sOrder_Del_Dt As String
    Public Property order_del_dt() As String
        Get
            Return sOrder_Del_Dt
        End Get
        Set(ByVal value As String)
            sOrder_Del_Dt = value
        End Set
    End Property
    'ISSUED_BY,CHAR,11,,Y,,
    'AUTHORIZED_BY,CHAR,11,,Y,,
    'REQUESTED_BY,CHAR,11,,Y,,
    'PAPER_CODE,CHAR,4,,Y,,
    'ACCT_DIST_CD,CHAR,2,,Y,,

    ''' <summary>
    ''' TOTAL_CASES,NUMBER,10,,Y,,0
    ''' </summary>
    ''' <remarks></remarks>
    Private iTotal_Cases As Integer
    Public Property total_cases() As Integer
        Get
            Return iTotal_Cases
        End Get
        Set(ByVal value As Integer)
            iTotal_Cases = value
        End Set
    End Property

    ''' <summary>
    ''' used in ROCON to capture SOP number
    ''' </summary>
    ''' <remarks></remarks>
    Private sTraffic_Ref_No As String
    Public Property traffic_ref_no() As String
        Get
            Return sTraffic_Ref_No
        End Get
        Set(ByVal value As String)
            sTraffic_Ref_No = value
        End Set
    End Property

    ''' <summary>
    ''' N = ROCON
    ''' W = Webstore
    ''' F = Freight Forwarded?
    ''' </summary>
    ''' <remarks></remarks>
    Private sEntry_Fl As String
    Public Property entry_fl() As String
        Get
            Return sEntry_Fl
        End Get
        Set(ByVal value As String)
            sEntry_Fl = value
        End Set
    End Property

    'CLI_NO,CHAR,10,,Y,,

    ''' <summary>
    ''' Pick up order flag 
    ''' Y = pick up order
    ''' N = delivery order
    ''' </summary>
    ''' <remarks></remarks>
    Private sPu_Order_Fl As String
    Public Property pu_order_fl() As String
        Get
            Return sPu_Order_Fl
        End Get
        Set(ByVal value As String)
            sPu_Order_Fl = value
        End Set
    End Property

    'TTL_MAS_BOL,NUMBER,10,,Y,,
    'DUNS_NO,CHAR,20,,Y,,
    'MAS_BOL_NO,VARCHAR2,20,,Y,,

    ''' <summary>
    ''' DELIVERY_FL,CHAR,1,,Y,,
    ''' looks like it is not used.
    ''' delivery_fl is always 'N'
    ''' </summary>
    ''' <remarks></remarks>
    Private sDelivery_Fl As String
    Public Property delivery_fl() As String
        Get
            Return sDelivery_Fl
        End Get
        Set(ByVal value As String)
            sDelivery_Fl = value
        End Set
    End Property


    Private sAction As String
    Public Property action() As String
        Get
            Return sAction
        End Get
        Set(ByVal value As String)
            sAction = value
        End Set
    End Property


    Sub New()
        co_odno = ""
        co_status = "O"
        route_code = "9999"
        prior_name = ""
        cu_no = ""
        ship_to = ""
        order_dt_tm = New DateTime
        order_reqd_dt = ""
        order_type = ""
        transport_mode = " "
        order_canceled_y_or_n = "N"
        stop_seq_no = "0"
        cohdr_comment = ""
        end_of_order_fl = "N"
        pay_method = ""
        order_del_dt = ""
        total_cases = 0
        traffic_ref_no = ""
        entry_fl = ""
        pu_order_fl = ""
        delivery_fl = "N"
        action = "N"

    End Sub
    Sub New(ByVal dr As DataRow)
        Me.New()
        SetInfo(dr)
    End Sub
    Sub New(ByVal sOrderNum As String)
        Me.New()
        Dim dr As DataRow = DB.GetCOHDRInfo(sOrderNum)
        SetInfo(dr)
    End Sub
    Sub New(ByVal on_cohdr As REC_ON_COHDR)
        Me.New()
        co_odno = on_cohdr.co_odno
        co_status = on_cohdr.co_status
        prior_name = on_cohdr.prior_name
        cu_no = on_cohdr.cu_no
        ship_to = on_cohdr.ship_to
        order_dt_tm = on_cohdr.order_dt_tm
        order_reqd_dt = on_cohdr.order_reqd_dt
        order_type = on_cohdr.order_type
        entry_fl = on_cohdr.entry_fl
        pu_order_fl = on_cohdr.pu_order_fl
        delivery_fl = on_cohdr.delivery_fl
    End Sub
    Sub SetInfo(ByVal dr As DataRow)
        Try
            If IsDBNull(dr("CO_ODNO")) = False Then
                co_odno = dr("CO_ODNO")
            End If

            If IsDBNull(dr("CO_STATUS")) = False Then
                co_status = dr("CO_STATUS")
            End If

            If IsDBNull(dr("ROUTE_CODE")) = False Then
                route_code = dr("ROUTE_CODE")
            End If
            If IsDBNull(dr("PRIOR_NAME")) = False Then
                prior_name = dr("PRIOR_NAME")
            End If

            If IsDBNull(dr("CU_NO")) = False Then
                cu_no = dr("CU_NO")
            End If

            If IsDBNull(dr("SHIP_TO")) = False Then
                ship_to = dr("SHIP_TO")
            End If

            If IsDBNull(dr("ORDER_DT_TM")) = False Then
                order_dt_tm = dr("ORDER_DT_TM")
            End If

            If IsDBNull(dr("order_reqd_dt")) = False Then
                order_reqd_dt = DateTime.Parse(dr("order_reqd_dt"))
            End If

            If IsDBNull(dr("ORDER_TYPE")) = False Then
                order_type = dr("ORDER_TYPE")
            End If

            If IsDBNull(dr("TRANSPORT_MODE")) = False Then
                transport_mode = dr("TRANSPORT_MODE")
            End If

            If IsDBNull(dr("ORDER_CANCELED_Y_OR_N")) = False Then
                order_canceled_y_or_n = dr("ORDER_CANCELED_Y_OR_N")
            End If

            If IsDBNull(dr("STOP_SEQ_NO")) = False Then
                stop_seq_no = dr("STOP_SEQ_NO")
            End If

            If IsDBNull(dr("COHDR_COMMENT")) = False Then
                cohdr_comment = dr("COHDR_COMMENT")
            End If

            If IsDBNull(dr("END_OF_ORDER_FL")) = False Then
                end_of_order_fl = dr("END_OF_ORDER_FL")
            End If

            If IsDBNull(dr("PAY_METHOD")) = False Then
                pay_method = dr("PAY_METHOD")
            End If

            If IsDBNull(dr("ORDER_DEL_DT")) = False Then
                order_del_dt = dr("ORDER_DEL_DT")
            End If

            If IsDBNull(dr("TOTAL_CASES")) = False Then
                total_cases = dr("TOTAL_CASES")
            End If

            If IsDBNull(dr("TRAFFIC_REF_NO")) = False Then
                traffic_ref_no = dr("TRAFFIC_REF_NO")
            End If

            If IsDBNull(dr("ENTRY_FL")) = False Then
                entry_fl = dr("ENTRY_FL")
            End If

            If IsDBNull(dr("PU_ORDER_FL")) = False Then
                pu_order_fl = dr("PU_ORDER_FL")
            End If

            If IsDBNull(dr("DELIVERY_FL")) = False Then
                delivery_fl = "N"
            End If

            If IsDBNull(dr("ACTION")) = False Then
                action = dr("ACTION")
            End If
        Catch ex As Exception
            Throw New Exception("REC_COHDR.SETINFO(" & dr("CO_ODNO") & ")->" & ex.Message.ToString)
        End Try


    End Sub

    Public Function validate() As Boolean
        Dim bValid As Boolean = True
        bValid = (co_odno.Trim <> "")
        bValid = bValid AndAlso (co_status.Trim <> "")
        bValid = bValid AndAlso (route_code <> "")
        bValid = bValid AndAlso (cu_no.Trim <> "")
        bValid = bValid AndAlso (ship_to.Trim <> "")
        bValid = bValid AndAlso order_dt_tm.Year > 2000

        Dim tmpDate As DateTime = Nothing
        If bValid AndAlso DateTime.TryParse(order_reqd_dt, tmpDate) Then
            order_reqd_dt = tmpDate.ToString("yyyy-MM-dd")
        Else
            bValid = False
        End If
        bValid = bValid AndAlso (order_type.Trim <> "")

        If transport_mode = "" Then
            transport_mode = " "
        End If

        bValid = bValid AndAlso (order_canceled_y_or_n.Trim <> "")

        If stop_seq_no = "" Then
            stop_seq_no = "0000"
        End If
        cohdr_comment = cohdr_comment.Trim

        bValid = bValid AndAlso (end_of_order_fl.Trim <> "")

        pay_method = pay_method.Trim
        traffic_ref_no = traffic_ref_no.Trim

        bValid = bValid AndAlso (pu_order_fl.Trim <> "")

        If delivery_fl.Trim = "" Then
            delivery_fl = "N"
        End If

        Return bValid
    End Function


    Sub Update(ByRef oraCmd As OracleCommand)
        Dim bValid As Boolean = validate()

        Dim sQry As String = ""

        If action = "A" Then
            sQry = "INSERT INTO DBO.COHDR (" & vbNewLine & _
                                 "CO_ODNO " & vbNewLine & _
                                 ",CO_STATUS " & vbNewLine & _
                                 ",ROUTE_CODE " & vbNewLine & _
                                 ",PRIOR_NAME " & vbNewLine & _
                                 ",CU_NO " & vbNewLine & _
                                 ",SHIP_TO " & vbNewLine & _
                                 ",ORDER_DT_TM " & vbNewLine & _
                                 ",ORDER_REQD_DT " & vbNewLine & _
                                 ",ORDER_TYPE " & vbNewLine & _
                                 ",TRANSPORT_MODE " & vbNewLine & _
                                 ",ORDER_CANCELED_Y_OR_N " & vbNewLine & _
                                 ",STOP_SEQ_NO " & vbNewLine & _
                                 ",COHDR_COMMENT " & vbNewLine & _
                                 ",END_OF_ORDER_FL " & vbNewLine & _
                                 ",PAY_METHOD " & vbNewLine & _
                                 ",ORDER_DEL_DT " & vbNewLine & _
                                 ",TOTAL_CASES " & vbNewLine & _
                                 ",TRAFFIC_REF_NO " & vbNewLine & _
                                 ",ENTRY_FL " & vbNewLine & _
                                 ",PU_ORDER_FL " & vbNewLine & _
                                 ",DELIVERY_FL " & vbNewLine & _
                                 ") VALUES (" & vbNewLine & _
                                 "'" & co_odno & "'" & vbNewLine & _
                                 ",'" & co_status & "'" & vbNewLine & _
                                 ",'" & route_code & "'" & vbNewLine & _
                                 ",'" & prior_name & "'" & vbNewLine & _
                                 ",'" & cu_no & "'" & vbNewLine & _
                                 ",'" & ship_to & "'" & vbNewLine & _
                                 ",TO_DATE('" & order_dt_tm.ToString("yyyy-MM-dd HH:mm:ss") & "','YYYY-MM-DD HH24:MI:SS')" & vbNewLine & _
                                 ",TO_DATE('" & order_reqd_dt & "','YYYY-MM-DD HH24:MI:SS')" & vbNewLine & _
                                 ",'" & order_type & "'" & vbNewLine & _
                                 ",'" & transport_mode & "'" & vbNewLine & _
                                 ",'" & order_canceled_y_or_n & "'" & vbNewLine & _
                                 ",'" & stop_seq_no & "'" & vbNewLine & _
                                 ",'" & cohdr_comment & "'" & vbNewLine & _
                                 ",'" & end_of_order_fl & "'" & vbNewLine & _
                                 ",'" & pay_method & "'" & vbNewLine & _
                                 "," & IIf(order_del_dt.Trim = "", "NULL", "TO_DATE('" & order_del_dt & "','YYYY-MM-DD HH24:MI:SS')") & vbNewLine & _
                                 "," & total_cases & vbNewLine & _
                                 ",'" & traffic_ref_no & "'" & vbNewLine & _
                                 ",'" & entry_fl & "'" & vbNewLine & _
                                 ",'" & pu_order_fl & "'" & vbNewLine & _
                                 ",'" & delivery_fl & "'" & vbNewLine & _
                                 ")"
        ElseIf action = "U" Then
            sQry = "UPDATE INTO DBO.COHDR " & vbNewLine & _
                     " SET CO_STATUS = '" & co_status & "'" & vbNewLine & _
                     ",ROUTE_CODE = '" & route_code & "'" & vbNewLine & _
                     ",PRIOR_NAME = '" & prior_name & "'" & vbNewLine & _
                     ",CU_NO = '" & cu_no & "'" & vbNewLine & _
                     ",SHIP_TO = '" & ship_to & "'" & vbNewLine & _
                     ",ORDER_DT_TM = TO_DATE('" & order_dt_tm.ToString("yyyy-MM-dd HH:mm:ss") & "','YYYY-MM-DD HH24:MI:SS')" & vbNewLine & _
                     ",ORDER_REQD_DT = TO_DATE('" & order_reqd_dt & "','YYYY-MM-DD HH24:MI:SS')" & vbNewLine & _
                     ",ORDER_TYPE = '" & order_type & "'" & vbNewLine & _
                     ",TRANSPORT_MODE = '" & transport_mode & "'" & vbNewLine & _
                     ",ORDER_CANCELED_Y_OR_N = '" & order_canceled_y_or_n & "'" & vbNewLine & _
                     ",STOP_SEQ_NO = '" & stop_seq_no & "'" & vbNewLine & _
                     ",COHDR_COMMENT = '" & cohdr_comment & "'" & vbNewLine & _
                     ",END_OF_ORDER_FL = '" & end_of_order_fl & "'" & vbNewLine & _
                     ",PAY_METHOD = '" & pay_method & "'" & vbNewLine & _
                     ",ORDER_DEL_DT = " & IIf(order_del_dt.Trim = "", "NULL", "TO_DATE('" & order_del_dt & "','YYYY-MM-DD HH24:MI:SS')") & vbNewLine & _
                     ",TOTAL_CASES = " & total_cases & vbNewLine & _
                     ",TRAFFIC_REF_NO = '" & traffic_ref_no & "'" & vbNewLine & _
                     ",ENTRY_FL = '" & entry_fl & "'" & vbNewLine & _
                     ",PU_ORDER_FL = '" & pu_order_fl & "'" & vbNewLine & _
                     ",DELIVERY_FL = '" & delivery_fl & "'" & vbNewLine & _
                     " WHERE CO_ODNO = '" & co_odno.Trim.PadRight(16) & "'"
        Else
            Exit Sub
        End If

        Try
            If bValid = False Then
                Throw New Exception("Error validating COHDR record")
            End If
            oraCmd.CommandText = sQry
            oraCmd.Parameters.Clear()

            If oraCmd.ExecuteNonQuery <= 0 Then
                Throw New Exception("Error Inserting COHDR record query: " & sQry & "!")
            End If

        Catch ex As Exception
            Throw New Exception("COHDR.Insert->" & co_odno & ".Error Message:" & ex.Message.ToString)
        End Try

    End Sub
End Class

'CO_ODNO,CHAR,16,,N,,
'CO_STATUS,CHAR,1,,N,,'O'
'ROUTE_CODE,CHAR,4,,N,,
'PRIOR_NAME,CHAR,6,,Y,,
'CU_NO,CHAR,10,,N,,
'SHIP_TO,CHAR,10,,Y,,
'ORDER_DT_TM,DATE,7,,N,,
'ORDER_REQD_DT,DATE,7,,Y,,
'ORDER_TYPE,CHAR,3,,Y,,
'TRANSPORT_MODE,CHAR,2,,N,,
'ORDER_CANCELED_Y_OR_N,CHAR,1,,N,,'N'
'CARRIER,CHAR,15,,Y,,
'TIMESTAMP,DATE,7,,Y,,
'SHIP_REQD_DT,DATE,7,,Y,,
'PICK_SEQ_NO,NUMBER,10,,Y,,
'STOP_SEQ_NO,VARCHAR2,4,,Y,,
'MODE_CODE,VARCHAR2,3,,Y,,
'PRICE_CODE,CHAR,1,,Y,,
'DISCOUNT_CODE,VARCHAR2,2,,Y,,
'MERCHANDISE_CLASS,CHAR,1,,Y,,
'PRICE_SYMBOL,CHAR,1,,Y,,
'SHIP_CODE,CHAR,1,,Y,,
'OWNERSHIP_CODE,VARCHAR2,10,,Y,,
'SHIPPING_INSTRUCTION,VARCHAR2,30,,Y,,
'EMPLOYEE_NUMBER,VARCHAR2,11,,Y,,
'TRACKING_CODE,CHAR,1,,Y,,
'ACCOUNTING_CODE,VARCHAR2,10,,Y,,
'CHARGE_FREIGHT,CHAR,1,,Y,,
'COHDR_COMMENT,VARCHAR2,25,,Y,,
'TOTAL_ORDER_VALUE,NUMBER,10,,Y,,
'CONSOLIDATE_ORDERS_FL,CHAR,1,,Y,,
'END_OF_ORDER_FL,CHAR,1,,N,,'N'
'PAY_METHOD,CHAR,16,,Y,,
'SHIPSET_NO,VARCHAR2,6,,Y,,
'HOST_ODNO,VARCHAR2,22,,Y,,
'EDI_STATUS,CHAR,1,,Y,,
'PRINT_DT,DATE,7,,Y,,
'SERVICE_LEVEL,CHAR,3,,Y,,
'ORDER_DEL_DT,DATE,7,,Y,,
'ISSUED_BY,CHAR,11,,Y,,
'AUTHORIZED_BY,CHAR,11,,Y,,
'REQUESTED_BY,CHAR,11,,Y,,
'PAPER_CODE,CHAR,4,,Y,,
'ACCT_DIST_CD,CHAR,2,,Y,,
'TOTAL_CASES,NUMBER,10,,Y,,0
'TRAFFIC_REF_NO,CHAR,16,,Y,,
'ENTRY_FL,CHAR,1,,Y,,
'CLI_NO,CHAR,10,,Y,,
'PU_ORDER_FL,CHAR,1,,Y,,
'TTL_MAS_BOL,NUMBER,10,,Y,,
'DUNS_NO,CHAR,20,,Y,,
'MAS_BOL_NO,VARCHAR2,20,,Y,,
'DELIVERY_FL,CHAR,1,,Y,,