﻿Public Class IST_STORES

    Private Shared _cacheKey As String = "IST_STORES"

    Public Shared Function GetStoreList() As DataTable
        Dim dtStores As DataTable
        Dim drStore As DataRow
        Dim dtCust As DataTable
        Dim dtSTNA As DataTable
        Dim CU_NO_NoS As String
        Dim tempCU_NO As String
        Dim currFilter As String
        Dim tempLCTN_NO As String
        Dim bAddStore As Boolean
        Dim excludeStores As List(Of String)

        dtStores = HttpContext.Current.Application(_cacheKey)

        If dtStores Is Nothing Then
            'cache needs to be populated

            'get the list of excluded stores from appSettings
            excludeStores = New List(Of String)(ConfigurationManager.AppSettings("EXCLUDE_IST_LOC").Replace(" ", "").Split(","c))

            dtStores = New DataTable
            With dtStores.Columns
                .Add("LCTN_NO")
                .Add("LCTN_NAME")
                .Add("LCTN_ADDRESS1")
                .Add("LCTN_ADDRESS2")
                .Add("CITY")
                .Add("POSTAL_CODE")
            End With

            dtCust = DB.GetStoreListFromCustomers()
            dtSTNA = DB.GetStoreListFromSTNA()

            For Each dr As DataRow In dtCust.Rows
                bAddStore = False
                CU_NO_NoS = "000000000"
                tempCU_NO = String.Empty

                'making sure it is not DBNull before operating on it
                If Not IsDBNull(dr("CU_NO")) Then
                    tempCU_NO = dr("CU_NO").ToString()
                    CU_NO_NoS = tempCU_NO.ToUpper().Replace("S", "")
                End If

                ' Attempt with a like condition: currFilter = "LCTN_STAT_CD <> 'C' AND LCTN_NO LIKE '%" & CU_NO_NoS & "'" ' As it was before: "LCTN_STAT_CD <> 'C' AND LCTN_NO = '" & dr("CU_NO") & "'"
                ' Getting rid of the leading zeros in both sides and avoiding no numberic values as in TEMP1 which as found
                currFilter = "LCTN_STAT_CD <> 'C' AND CONVERT(LCTN_NO, System.Int32) = CONVERT('" & IIf(IsNumeric(CU_NO_NoS), CU_NO_NoS, 0) & "', System.Int32)"
                For Each drSTNA As DataRow In dtSTNA.Select(currFilter)

                    'viewing what is resulting from filter
                    tempLCTN_NO = drSTNA("LCTN_NO").ToString()

                    bAddStore = Not excludeStores.Contains(tempLCTN_NO)
                Next

                If bAddStore = True Then
                    drStore = dtStores.NewRow
                    drStore("LCTN_NO") = tempCU_NO
                    drStore("LCTN_NAME") = dr("CU_NAME")
                    drStore("LCTN_ADDRESS1") = dr("ADD1_SH")
                    drStore("LCTN_ADDRESS2") = dr("ADD2_SH")
                    drStore("CITY") = dr("CITY_SH")
                    drStore("POSTAL_CODE") = dr("POST_CODE")
                    dtStores.Rows.Add(drStore)
                End If

            Next

            HttpContext.Current.Application(_cacheKey) = dtStores

        End If

        Return dtStores

    End Function

    Public Shared Function IsValidStore(ByVal store As String) As Boolean
        Dim dt As DataTable

        dt = GetStoreList()
        Return dt.Select("LCTN_NO = '" & store & "'").Count > 0

    End Function

End Class
