Imports Microsoft.VisualBasic
Imports System.Data
Imports System.Data.OleDb
Imports System.Data.OracleClient

Public Class REC_CODTL_LIST
    Inherits SortedList

    Public ReadOnly Property CODTL_List() As DataTable
        Get
            Dim dt As New DataTable
            Dim dr As DataRow = Nothing
            With dt.Columns
                .Add("CO_ODNO")
                .Add("COD_LINE")
                .Add("ITEM")
                .Add("PICKED_QTY")
                .Add("UNIT_PRICE")
                .Add("COD_QTY")
                .Add("TOTAL_ALLOCATED_QTY")
                .Add("PLANNED_QTY")
                .Add("USE_BLOCKED_STOCK_FL")
                .Add("SHIPPED_QTY")
                .Add("DELETED_fL")
                .Add("PICK_PLANNED_Y_OR_N")
                .Add("PICKED_CASES")
                .Add("PICKED_UNITS")
            End With

            For Each rec As REC_CODTL In Me.GetValueList
                dr = dt.NewRow
                dr("CO_ODNO") = rec.co_odno
                dr("COD_LINE") = rec.cod_line
                dr("ITEM") = rec.item
                dr("PICKED_QTY") = rec.picked_qty
                dr("UNIT_PRICE") = rec.Unit_Price
                dr("COD_QTY") = rec.cod_qty
                dr("TOTAL_ALLOCATED_QTY") = rec.total_allocated_qty
                dr("PLANNED_QTY") = rec.planned_qty
                dr("USE_BLOCKED_STOCK_FL") = rec.use_blocked_stock_fl
                dr("SHIPPED_QTY") = rec.shipped_qty
                dr("DELETED_fL") = rec.deleted_fl
                dr("PICK_PLANNED_Y_OR_N") = rec.pick_planned_y_or_n
                dr("PICKED_CASES") = rec.picked_cases
                dr("PICKED_UNITS") = rec.picked_units
                dt.Rows.Add(dr)
            Next

            Return dt
        End Get
    End Property

    Private sCo_odno As String
    Public Property co_odno() As String
        Get
            Return sCo_odno
        End Get
        Set(ByVal value As String)
            sCo_odno = value
            For Each rec As REC_CODTL In Me.GetValueList
                rec.co_odno = value
            Next
        End Set
    End Property
    Sub New()
        MyBase.New()
        co_odno = ""
    End Sub
    Sub New(ByVal sOrderNum As String)
        Me.New()
        co_odno = sOrderNum
        Dim dtCodtl As DataTable = DB.getCODTL(sOrderNum)
        SetInfo(dtCodtl)
    End Sub
    Sub New(ByVal dtCodtl As DataTable)
        Me.New()
        SetInfo(dtCodtl)
    End Sub
    Sub SetInfo(ByVal dtCodtl As DataTable)
        Dim rec As REC_CODTL = Nothing
        For Each dr As DataRow In dtCodtl.Rows
            rec = New REC_CODTL(dr)
            Me.Add(rec.cod_line, rec)
        Next
    End Sub

    Public Overloads Sub Add(ByVal iCod_Line As Integer, ByVal obj As REC_CODTL)
        Try
            MyBase.Add(iCod_Line, obj)
        Catch ex As Exception
            Throw New Exception("REC_LCBO_IST_HDR_LIST.ADD(" & iCod_Line & ", REC_LCBO_IST_DTL) " & ex.Message.ToString)
        End Try

    End Sub

    Public Function GetOrderLine(ByVal cod_line As Integer) As REC_CODTL
        Return Item(cod_line)
    End Function

    Public Function GetOrderLine(ByVal item As String) As REC_CODTL
        Dim rec As REC_CODTL = Nothing
        For Each itm As REC_CODTL In Me.GetValueList
            If itm.item.Trim = item.Trim And itm.deleted_fl = "N" Then
                rec = itm
            End If
        Next
        Return rec
    End Function

    Public Sub Update(ByVal oracmd As OracleCommand)
        For Each rec As REC_CODTL In Me.GetValueList
            If rec.action = "A" Or rec.action = "U" Then
                rec.Update(oracmd)
            End If
        Next
    End Sub

    'co_odno = ""
    '   cod_line = 0
    '   item = ""
    '   picked_qty = 0
    '   Unit_Price = 0
    '   cod_qty = 0
    '   total_allocated_qty = 0
    '   planned_qty = 0
    '   use_blocked_stock_fl = "N"
    '   shipped_qty = 0
    '   deleted_fl = "N"
    '   pick_planned_y_or_n = ""
    '   picked_cases = 0
    '   picked_units = 0
    '   action = "N"


End Class
