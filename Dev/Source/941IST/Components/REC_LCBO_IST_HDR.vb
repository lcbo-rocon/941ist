Imports Microsoft.VisualBasic
Imports System.Data
Imports System.Data.OleDb
Imports System.Data.OracleClient

Public Class REC_LCBO_IST_HDR
    Implements IComparable

    Private iInv_No As Integer
    Public Property inv_no() As Integer
        Get
            Return iInv_No
        End Get
        Set(ByVal value As Integer)
            iInv_No = value
        End Set
    End Property

    Private sCo_OdNo As String
    Public Property co_odno() As String
        Get
            Return sCo_OdNo
        End Get
        Set(ByVal value As String)
            sCo_OdNo = value
        End Set
    End Property

    Private sTo_Store As String
    Public Property to_store() As String
        Get
            Return sTo_Store
        End Get
        Set(ByVal value As String)
            sTo_Store = value
        End Set
    End Property
    Private iOrig_Inv_No As Integer
    Public Property orig_inv_no() As Integer
        Get
            Return iOrig_Inv_No
        End Get
        Set(ByVal value As Integer)
            iOrig_Inv_No = value
        End Set
    End Property

    Private sOrig_Co_OdNo As String
    Public Property orig_co_odno() As String
        Get
            Return sOrig_Co_OdNo
        End Get
        Set(ByVal value As String)
            sOrig_Co_OdNo = value
        End Set
    End Property

    Private dtOrder_DT_TM As DateTime
    Public Property order_dt_tm() As DateTime
        Get
            Return dtOrder_DT_TM
        End Get
        Set(ByVal value As DateTime)
            dtOrder_DT_TM = value
        End Set
    End Property

    Private dtOrder_reqd_dt As DateTime
    Public Property order_reqd_dt() As DateTime
        Get
            Return dtOrder_reqd_dt
        End Get
        Set(ByVal value As DateTime)
            dtOrder_reqd_dt = value
        End Set
    End Property

    Private sIST_Type As String
    Public Property ist_type() As String
        Get
            Return sIST_Type
        End Get
        Set(ByVal value As String)
            sIST_Type = value
        End Set
    End Property

    Private sPU_Order_fl As String
    Public Property pu_order_fl() As String
        Get
            Return sPU_Order_fl
        End Get
        Set(value As String)
            sPU_Order_fl = value
        End Set
    End Property


    Private iNum_OF_Prod As Integer
    Public Property num_of_prod() As Integer
        Get
            Return iNum_OF_Prod
        End Get
        Set(ByVal value As Integer)
            iNum_OF_Prod = value
        End Set
    End Property


    Private iNum_Of_Units As Integer
    Public Property num_of_units() As Integer
        Get
            Return iNum_Of_Units
        End Get
        Set(ByVal value As Integer)
            iNum_Of_Units = value
        End Set
    End Property


    Private sSubmitted_By As String
    Public Property submitted_by() As String
        Get
            Return sSubmitted_By
        End Get
        Set(ByVal value As String)
            sSubmitted_By = value
        End Set
    End Property

    Private sRequestAuthBy As String
    Public Property request_auth_user() As String
        Get
            Return sRequestAuthBy
        End Get
        Set(ByVal value As String)
            sRequestAuthBy = value
        End Set
    End Property

    Private sAuthorized_By As String
    Public Property authorized_by() As String
        Get
            Return sAuthorized_By
        End Get
        Set(ByVal value As String)
            sAuthorized_By = value
        End Set
    End Property

    Private sComments_Line1 As String
    Public Property comments_line1() As String
        Get
            Return sComments_Line1
        End Get
        Set(ByVal value As String)
            sComments_Line1 = value
        End Set
    End Property

    Private sComments_Line2 As String
    Public Property comments_line2() As String
        Get
            Return sComments_Line2
        End Get
        Set(ByVal value As String)
            sComments_Line2 = value
        End Set
    End Property

    Private sStatus As String
    Public Property status() As String
        Get
            Return sStatus
        End Get
        Set(ByVal value As String)
            sStatus = value
        End Set
    End Property

    Private dtRequest_Auth_Dt As DateTime
    Public Property request_auth_dt() As DateTime
        Get
            Return dtRequest_Auth_Dt
        End Get
        Set(ByVal value As DateTime)
            dtRequest_Auth_Dt = value
        End Set
    End Property


    Private dtLast_Saved_Dt As DateTime
    Public Property last_saved_dt() As DateTime
        Get
            Return dtLast_Saved_Dt
        End Get
        Set(ByVal value As DateTime)
            dtLast_Saved_Dt = value
        End Set
    End Property


    Private sLast_Saved_By As String
    Public Property last_saved_by() As String
        Get
            Return sLast_Saved_By
        End Get
        Set(ByVal value As String)
            sLast_Saved_By = value
        End Set
    End Property

    Private dtLast_Update_Dt As DateTime
    Public Property last_update_dt() As DateTime
        Get
            Return dtLast_Update_Dt
        End Get
        Set(ByVal value As DateTime)
            dtLast_Update_Dt = value
        End Set
    End Property

    Private sLast_Update_By As String
    Public Property last_update_by() As String
        Get
            Return sLast_Update_By
        End Get
        Set(ByVal value As String)
            sLast_Update_By = value
        End Set
    End Property


    Private sAction As String
    Public Property action() As String
        Get
            Return sAction
        End Get
        Set(ByVal value As String)
            sAction = value
        End Set
    End Property

    Sub New()
        inv_no = 0
        co_odno = ""
        to_store = ""
        orig_inv_no = 0
        orig_co_odno = ""
        order_dt_tm = New DateTime
        order_reqd_dt = New DateTime
        ist_type = ""
        pu_order_fl = "N"
        num_of_prod = 0
        num_of_units = 0
        submitted_by = ""
        authorized_by = ""
        request_auth_user = ""
        request_auth_dt = New DateTime
        comments_line1 = ""
        comments_line2 = ""
        status = ""
        last_saved_dt = New DateTime
        last_saved_by = ""
        last_update_dt = New DateTime
        last_update_by = ""
        action = "N"
    End Sub

    Sub New(ByVal objIST As REC_LCBO_IST_HDR)
        Me.New()
        inv_no = objIST.inv_no
        co_odno = objIST.co_odno
        to_store = objIST.to_store
        orig_inv_no = objIST.inv_no
        orig_co_odno = objIST.co_odno
        order_dt_tm = objIST.order_dt_tm
        order_reqd_dt = objIST.order_reqd_dt
        ist_type = objIST.ist_type
        pu_order_fl = objIST.pu_order_fl
        num_of_prod = objIST.num_of_prod
        num_of_units = objIST.num_of_units
        submitted_by = objIST.submitted_by
        authorized_by = objIST.authorized_by
        request_auth_user = objIST.request_auth_user
        request_auth_dt = objIST.request_auth_dt
        comments_line1 = objIST.comments_line1
        comments_line2 = objIST.comments_line2
        status = objIST.status
        last_saved_dt = objIST.last_saved_dt
        last_saved_by = objIST.last_saved_by
        last_update_dt = objIST.last_update_dt
        last_update_by = objIST.last_update_by
        action = objIST.action
    End Sub
    Sub New(ByVal sOrderNum As String)
        Me.New()
        Dim dt As DataTable = DB.GetISTHeaderInfo(sOrderNum)
        If dt.Rows.Count = 1 Then
            SetInfo(dt.Rows(0))
        End If
    End Sub
    Sub New(ByVal drIST As DataRow)
        Me.New()
        SetInfo(drIST)
    End Sub
    Sub SetInfo(ByVal drIST As DataRow)
        If IsDBNull(drIST("inv_no")) = False Then
            inv_no = drIST("inv_no")
        End If

        If IsDBNull(drIST("co_odno")) = False Then
            co_odno = drIST("co_odno")
        End If

        If IsDBNull(drIST("orig_co_odno")) = False Then
            orig_co_odno = drIST("orig_co_odno")
        End If

        If IsDBNull(drIST("to_store")) = False Then
            to_store = drIST("to_store")
        End If

        If IsDBNull(drIST("order_dt_tm")) = False Then
            order_dt_tm = drIST("order_dt_tm")
        End If

        If IsDBNull(drIST("order_reqd_dt")) = False Then
            order_reqd_dt = drIST("order_reqd_dt")
        End If

        If IsDBNull(drIST("ist_type")) = False Then
            ist_type = drIST("ist_type")
        End If

        If IsDBNull(drIST("pu_order_fl")) = False Then
            pu_order_fl = drIST("pu_order_fl")
        End If

        If IsDBNull(drIST("num_of_prod")) = False Then
            num_of_prod = drIST("num_of_prod")
        End If

        If IsDBNull(drIST(num_of_units)) = False Then
            num_of_units = drIST("num_of_units")
        End If

        If IsDBNull(drIST("submitted_by")) = False Then
            submitted_by = drIST("submitted_by")
        End If

        If IsDBNull(drIST("authorized_by")) = False Then
            authorized_by = drIST("authorized_by")
        End If

        If IsDBNull(drIST("comments_line1")) = False Then
            comments_line1 = drIST("comments_line1")
        End If

        If IsDBNull(drIST("comments_line2")) = False Then
            comments_line2 = drIST("comments_line2")
        End If

        If IsDBNull(drIST("status")) = False Then
            status = drIST("status")
        End If

        If IsDBNull(drIST("last_saved_dt")) = False Then
            last_saved_dt = drIST("last_saved_dt")
        End If

        If IsDBNull(drIST("last_saved_by")) = False Then
            last_saved_by = drIST("last_saved_by")
        End If

        If IsDBNull(drIST("last_update_dt")) = False Then
            last_update_dt = drIST("last_update_dt")
        End If

        If IsDBNull(drIST("last_update_by")) = False Then
            last_update_by = drIST("last_update_by")
        End If

        If IsDBNull(drIST("request_auth_user")) = False Then
            request_auth_user = drIST("request_auth_user")
        End If

        If IsDBNull(drIST("request_auth_dt")) = False Then
            request_auth_dt = drIST("request_auth_dt")
        End If

        If IsDBNull(drIST("action")) = False Then
            action = drIST("action")
        End If
    End Sub

    Sub validate()
        Dim reason As String = ""
        Dim iInvalid As Integer = 0
        If inv_no = 0 Then
            iInvalid = 1
            reason = "inv_no not set!"
        End If

        If iInvalid = 0 AndAlso co_odno.Trim = "" Then
            iInvalid = 2
            reason = "co_odno not set!"
        End If

        If iInvalid = 0 AndAlso ist_type = "" Then
            iInvalid = 3
            reason = "ist type not set!"
        End If

        If iInvalid = 0 AndAlso to_store.Trim = "" Then
            iInvalid = 4
            reason = "to store not set!"
        End If

        If iInvalid = 0 AndAlso IST_STORES.IsValidStore(to_store) = False Then
            iInvalid = 5
            reason = "invalid location for ist type: " & ist_type & "."
        End If

        If iInvalid = 0 AndAlso status <> IST_ORDER.IST_STATUS.CANCELLED AndAlso LCBO_CALENDAR.IsValidDate(order_reqd_dt) = False Then
            iInvalid = 6
            reason = "Invalid required date."
        End If

        If iInvalid = 0 And String.IsNullOrEmpty(pu_order_fl) Then
            iInvalid = 7
            reason = "Invalid Pickup Order Flag."
        End If

        If iInvalid <> 0 Then
            Throw New ISTException((10000 + iInvalid), "Failed validation: " & reason)
        End If


    End Sub

    Sub Update(ByVal oracmd As OracleCommand)
        Dim sQry As String = ""
        Dim commentCount As Int32 = 0

        If last_saved_dt.Year < 2000 Then
            last_saved_dt = Now
        End If

        If last_update_dt.Year < 2000 Then
            last_update_dt = Now
        End If

        Try
            validate()
            If action = "A" Then
                sQry = "INSERT INTO DBO.LCBO_IST_HDR ( " & vbNewLine &
                       " INV_NO " & vbNewLine &
                       ",CO_ODNO " & vbNewLine &
                       ",TO_STORE " & vbNewLine &
                       ",ORIG_INV_NO " & vbNewLine &
                       ",ORIG_CO_ODNO " & vbNewLine &
                       ",ORDER_DT_TM " & vbNewLine &
                       ",ORDER_REQD_DT " & vbNewLine &
                       ",IST_TYPE " & vbNewLine &
                       ",PU_ORDER_FL " & vbNewLine &
                       ",NUM_OF_PROD " & vbNewLine &
                       ",NUM_OF_UNITS " & vbNewLine &
                       ",SUBMITTED_BY " & vbNewLine &
                       ",COMMENTS_LINE1 " & vbNewLine &
                       ",COMMENTS_LINE2 " & vbNewLine &
                       ",STATUS " & vbNewLine &
                       ",LAST_SAVED_DT " & vbNewLine &
                       ",LAST_SAVED_BY " & vbNewLine &
                       ",LAST_UPDATE_DT " & vbNewLine &
                       ",LAST_UPDATE_BY " & vbNewLine &
                       ") VALUES (" & vbNewLine &
                       inv_no & vbNewLine &
                       ",'" & co_odno & "'" & vbNewLine &
                       ",'" & to_store & "'" & vbNewLine &
                       "," & orig_inv_no & vbNewLine &
                       ",'" & orig_co_odno & "'" & vbNewLine &
                       ",SYSDATE " & vbNewLine &
                       ", TO_DATE('" & order_reqd_dt.ToString("yyyy-MM-dd HH:mm:ss") & "','YYYY-MM-DD HH24:MI:SS')" & vbNewLine &
                       ",'" & ist_type & "'" & vbNewLine &
                       ",'" & pu_order_fl & "'" & vbNewLine &
                       "," & num_of_prod & vbNewLine &
                       "," & num_of_units & vbNewLine &
                       ",'" & submitted_by & "'" & vbNewLine &
                       ",'" & comments_line1.Replace("'", "''") & "'" & vbNewLine &
                        ",'" & comments_line2.Replace("'", "''") & "'" & vbNewLine &
                       ",'" & status & "'" & vbNewLine &
                       ",SYSDATE " & vbNewLine &
                       ",'" & last_saved_by & "'" & vbNewLine &
                       ",SYSDATE " & vbNewLine &
                       ",'" & last_update_by & "')"

            ElseIf action = "U" Then
                sQry = "UPDATE DBO.LCBO_IST_HDR " & vbNewLine &
                       " SET TO_STORE = '" & to_store & "'" & vbNewLine &
                       ", ORDER_REQD_DT = TO_DATE('" & order_reqd_dt.ToString("yyyy-MM-dd HH:mm:ss") & "','YYYY-MM-DD HH24:MI:SS')" & vbNewLine &
                       " ,IST_TYPE = '" & ist_type & "'" & vbNewLine &
                       " ,PU_ORDER_FL = '" + pu_order_fl & "'" & vbNewLine &
                       " ,NUM_OF_PROD = " & num_of_prod & vbNewLine &
                       " ,NUM_OF_UNITS = " & num_of_units & vbNewLine &
                       " ,COMMENTS_LINE1 = '" & comments_line1.Replace("'", "''") & "'" & vbNewLine &
                       " ,COMMENTS_LINE2 = '" & comments_line2.Replace("'", "''") & "'" & vbNewLine &
                       " ,STATUS = '" & status & "'" & vbNewLine &
                       " ,LAST_SAVED_BY = '" & last_saved_by & "'" & vbNewLine &
                       " ,LAST_SAVED_DT = TO_DATE('" & last_saved_dt.ToString("yyyy-MM-dd HH:mm:ss") & "','YYYY-MM-DD HH24:MI:SS')" & vbNewLine &
                       " ,LAST_UPDATE_BY = '" & last_update_by & "'" & vbNewLine &
                       " ,LAST_UPDATE_DT = SYSDATE " & vbNewLine &
                       " ,AUTHORIZED_BY = '" & authorized_by & "'" & vbNewLine &
                       " WHERE INV_NO = " & inv_no & vbNewLine &
                       " AND CO_ODNO = '" & co_odno.Trim.PadRight(16) & "'"
            Else
                Exit Sub
            End If

            oracmd.CommandText = sQry
            oracmd.Parameters.Clear()

            If oracmd.ExecuteNonQuery <= 0 Then
                Throw New Exception("IST ORDER: " & inv_no & "," & co_odno & " -> DID NOT UPDATE SUCCESSFULLY!")
            End If

            If Me.action = "U" AndAlso Me.status = IST_ORDER.IST_STATUS.SUBMITTED Then

                'It's possible that records already exist if the user had recalled the order,
                'so let's delete any existing records first
                sQry = "DELETE FROM DBO.ON_ODCMT "
                sQry += "WHERE CO_ODNO = '" & co_odno.Trim() & "' "

                oracmd.CommandText = sQry
                oracmd.Parameters.Clear()

                oracmd.ExecuteNonQuery()

                If String.IsNullOrEmpty(comments_line1) = False Then
                    sQry = "INSERT INTO DBO.ON_ODCMT ("
                    sQry += "CO_ODNO, "
                    sQry += "OC_SEQN, "
                    sQry += "OC_LINE, "
                    sQry += "ODCMT_COMMENT, "
                    sQry += "OC_TYPE"
                    sQry += ")"
                    sQry += "VALUES ("
                    sQry += "'" & co_odno.Trim() & "', "
                    sQry += "0, "
                    sQry += "0, "
                    sQry += "'" & comments_line1.Replace("'", "''") & "', "
                    sQry += "'L'"
                    sQry += ")"

                    oracmd.CommandText = sQry
                    oracmd.Parameters.Clear()

                    If oracmd.ExecuteNonQuery <= 0 Then
                        Throw New Exception("IST ORDER: " & inv_no & "," & co_odno & " -> Error adding comments!")
                    End If
                End If
                If String.IsNullOrEmpty(comments_line2) = False Then
                    sQry = "INSERT INTO DBO.ON_ODCMT ("
                    sQry += "CO_ODNO, "
                    sQry += "OC_SEQN, "
                    sQry += "OC_LINE, "
                    sQry += "ODCMT_COMMENT, "
                    sQry += "OC_TYPE"
                    sQry += ")"
                    sQry += "VALUES ("
                    sQry += "'" & co_odno.Trim() & "', "
                    sQry += "1, "
                    sQry += "0, "
                    sQry += "'" & comments_line2.Replace("'", "''") & "', "
                    sQry += "'L'"
                    sQry += ")"

                    oracmd.CommandText = sQry
                    oracmd.Parameters.Clear()

                    If oracmd.ExecuteNonQuery <= 0 Then
                        Throw New Exception("IST ORDER: " & inv_no & "," & co_odno & " -> Error adding comments!")
                    End If
                End If
            End If

            If dtRequest_Auth_Dt.Year > 2000 Then
                sQry = "UPDATE DBO.LCBO_IST_HDR " & vbNewLine &
                     " SET REQUEST_AUTH_DT = TO_DATE('" & request_auth_dt.ToString("yyyy-MM-dd HH:mm:ss") & "','YYYY-MM-DD HH24:MI:SS') " & vbNewLine &
                      " ,LAST_SAVED_BY = '" & last_saved_by & "'" & vbNewLine &
                       " ,LAST_SAVED_DT = TO_DATE('" & last_saved_dt.ToString("yyyy-MM-dd HH:mm:ss") & "','YYYY-MM-DD HH24:MI:SS')" & vbNewLine &
                       " ,LAST_UPDATE_BY = '" & last_update_by.Trim & "'" & vbNewLine &
                       " ,LAST_UPDATE_DT = SYSDATE " & vbNewLine &
                       " ,AUTHORIZED_BY = '" & authorized_by.Trim & "'" & vbNewLine &
                       " ,REQUEST_AUTH_USER = '" & request_auth_user.Trim & "'" & vbNewLine &
                     " WHERE INV_NO = " & inv_no & vbNewLine &
                     " AND CO_ODNO = '" & co_odno.Trim.PadRight(16) & "'"

                oracmd.CommandText = sQry
                oracmd.Parameters.Clear()

                If oracmd.ExecuteNonQuery <= 0 Then
                    Throw New Exception("IST ORDER: " & inv_no & "," & co_odno & " -> DID NOT UPDATE SUCCESSFULLY!")
                End If

            End If
        Catch ex As ISTException
            Throw ex
        Catch ex As Exception
            Throw New Exception("REC_LCBO_IST_HDR.UPDATE(" & inv_no & "," & co_odno & ")" & vbNewLine &
                                "QRY: " & sQry & vbNewLine &
                                "ERR: " & ex.Message.ToString)
        End Try
    End Sub

    Public Function CompareTo(ByVal obj As Object) As Integer Implements System.IComparable.CompareTo
        Dim b As REC_LCBO_IST_HDR = obj
        Return Me.co_odno.CompareTo(b.co_odno)
    End Function
End Class
