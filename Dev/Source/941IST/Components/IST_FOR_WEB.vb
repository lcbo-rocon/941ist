﻿Imports System.Web.Script.Serialization
Imports System.Collections.Generic

<Serializable()> _
Public Class IST_FOR_WEB

    Public CO_ODNO As String
    Public TO_STORE As String
    Public ORDER_DT_TM As String
    Public ORDER_REQD_DT As String
    Public STATUS As String
    Public COMMENTS_LINE_1 As String
    Public COMMENTS_LINE_2 As String
    Public LAST_UPDATE_BY As String
    Public PU_ORDER_FL As String
    Public LINES As List(Of ITEM)

    Public Sub New()
        LINES = New List(Of ITEM)
    End Sub

    Public Sub New(ByVal IstOrder As IST_ORDER)
        Dim row As IST_FOR_WEB.ITEM
        Dim lineId As Int32 = 0
        LINES = New List(Of ITEM)

        CO_ODNO = IstOrder.co_odno
        TO_STORE = IstOrder.to_store
        ORDER_DT_TM = IstOrder.order_dt_tm.ToString("MM/dd/yyyy")
        ORDER_REQD_DT = IstOrder.order_reqd_dt.ToString("MM/dd/yyyy")
        STATUS = IstOrder.status
        COMMENTS_LINE_1 = IstOrder.comments_line1
        COMMENTS_LINE_2 = IstOrder.comments_line2
        LAST_UPDATE_BY = IstOrder.last_update_by
        PU_ORDER_FL = IstOrder.pu_order_fl
        For Each itm As DataRow In IstOrder.IST_DTL_List.Rows
            row = New IST_FOR_WEB.ITEM
            row.id = lineId
            row.COD_LINE = itm("cod_line")
            row.ITEM = itm("item")
            row.ITEM_DESC = itm("item_desc")
            row.COD_QTY = itm("cod_qty")
            row.ORIG_QTY = itm("orig_qty")
            row.DELETED_FL = itm("deleted_fl")
            LINES.Add(row)
            lineId += 1
        Next
    End Sub

    <Serializable()> _
    Public Class ITEM
        Public id As Int32
        Public COD_LINE As Int32
        Public ITEM As String
        Public ITEM_DESC As String
        Public COD_QTY As Int32
        Public ORIG_QTY As Int32
        Public DELETED_FL As String
    End Class

End Class
