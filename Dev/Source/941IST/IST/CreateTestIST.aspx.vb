﻿Imports System.Data
Imports System.Web.Services
Imports System.Web.Script.Serialization

Public Class CreateTestIST
    Inherits ISTDefaultPage

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If UserInfo.RSGUserSecurity.IsAuthorized("USER", True) = False Then
            PageInfo("Mode") = "V"
        End If

        Call initText()
        Call initControls()

    End Sub

    Private Sub initText()

        If PageInfo("Mode") <> "V" Then
            Page.Title += " - Add/Update"
        End If

    End Sub

    Private Sub initControls()
        Dim dt As DataTable
        Dim json As StringBuilder = New StringBuilder()
        Dim jss As New JavaScriptSerializer()
        Dim istFromWeb As New IST_FROM_WEB()

        dt = IST_STORES.GetStoreList().Copy()

        dt.Columns("LCTN_NO").ColumnName = "value"
        dt.Columns("LCTN_NAME").ColumnName = "label"
        json.Append("var StoreList = ")
        json.Append(Utils.DataTableToJSON(dt))
        json.Append(";")

        json.Append("var IstTemplate = ")
        json.Append(jss.Serialize(istFromWeb))
        json.Append(";")

        ScriptManager.RegisterStartupScript(Me, GetType(Page), "clientData", json.ToString(), True)

    End Sub

    <WebMethod()> _
    Public Shared Function CreateIST(ByVal store As String, ByVal reqDate As String, ByVal numItems As String, ByVal submittedBy As String) As String
        Dim js As JavaScriptSerializer = New JavaScriptSerializer()
        Dim IstFromWeb As IST_FROM_WEB = New IST_FROM_WEB()
        Dim IstOrder As IST_ORDER
        Dim IstForWeb As IST_FOR_WEB = Nothing
        Dim response As RSGBase.RSGResponse = New RSGBase.RSGResponse()
        Dim line As IST_FROM_WEB.LINE
        Dim maxLines As Int32 = Convert.ToInt32(numItems)
        Dim curLine As Int32 = 0

        Dim dt As DataTable

        Try

            IstFromWeb.co_odno = ""
            IstFromWeb.to_store = store
            IstFromWeb.order_reqd_dt = reqDate
            IstFromWeb.submittedBy = submittedBy

            'add items to order
            dt = DB.GetDataTable("941", "select t1.item from DBO.IMMAS T1 INNER JOIN DBO.ON_CUST_CATALOG T2 ON T1.ITEM = T2.ITEM AND T1.PACKAGING_CODE = T2.PACKAGING_CODE where min_stock_lev > 0 and active_fl = 'Y' group by t1.item order by to_number(t1.item)")
            For Each i As DataRow In dt.Rows
                If DB.GetItemUncommittedQty(Trim(i("ITEM"))) > 10 Then
                    line = New IST_FROM_WEB.LINE()
                    curLine += 1
                    line.COD_LINE = curLine
                    line.COD_QTY = 1
                    line.ITEM = Trim(i("ITEM"))
                    line.ORIG_QTY = 0
                    IstFromWeb.items.Add(line)
                    If curLine = maxLines Then
                        Exit For
                    End If
                End If
            Next



            If IstFromWeb.co_odno.Length = 0 AndAlso IstFromWeb.items.Count > 0 Then
                IstOrder = ISTManager.CreateIST(IstFromWeb)
                IstForWeb = New IST_FOR_WEB()
                If Not IstOrder Is Nothing Then
                    For Each line In IstFromWeb.items
                        Try
                            ISTManager.AddItem(IstOrder.co_odno, line.ITEM, line.COD_QTY, IstFromWeb.submittedBy, "N")
                        Catch ex As Exception
                            'Throw New ISTException(1001, "Error adding item to the IST.")
                        End Try
                    Next
                    IstForWeb = New IST_FOR_WEB(IstOrder)
                    IstForWeb.LINES = New List(Of IST_FOR_WEB.ITEM) 'remove list of items to reduce network/client payload
                End If
            Else
                Throw New ISTException(1000, "Error creating IST, order # was specified.")
            End If

            response.IsSuccessful = True
            response.ResponseTip = Enums.RSGResponseTip.Success
            response.ReturnedObject = IstForWeb
        Catch ex As ISTException
            response.IsSuccessful = False
            response.ResponseTip = Enums.RSGResponseTip.Exception
            response.ResponseCode = ex.ErrorCode
            response.Details = ex.Message
        Catch ex As Exception
            response.IsSuccessful = False
            response.ResponseTip = Enums.RSGResponseTip.Exception
            response.Details = ex.Message
        End Try

        Return response.ToJSON()

    End Function

End Class