﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="CreateTestIST.aspx.vb" Inherits="_941IST.CreateTestIST" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link href="../css/Standard.css" rel="stylesheet" />
    <link href="../css/smoothness/jquery-ui-1.10.3.custom.css" rel="stylesheet" />
    <link href="../css/SlickGrid/slick.grid.css" rel="stylesheet" />
    <link href="../js/AutoCompleteDDL/AutoCompleteDDL.css" rel="stylesheet" />
    <link href="../js/Wait/lcboMessage.css" rel="stylesheet" />

    <script src="../js/jquery-1.9.1.min.js"></script>
    <script src="../js/jquery-ui-1.10.3.custom.min.js"></script>
    <script src="../js/Wait/lcboWait.js"></script>

    <script type="text/javascript">
        var msie = (function () {

            var undef,
                v = 3,
                div = document.createElement('div'),
                all = div.getElementsByTagName('i');

            while (
                div.innerHTML = '<!--[if gt IE ' + (++v) + ']><i></i><![endif]-->',
                all[0]
            );

            return v > 4 ? v : undef;

        }());

        //create JSON.stringify function
        var JSON = JSON || {};
        // implement JSON.stringify serialization
        JSON.stringify = JSON.stringify || function (obj) {

            var t = typeof (obj);
            if (t != "object" || obj === null) {

                // simple data type
                if (t == "string") obj = '"' + obj + '"';
                return String(obj);

            }
            else {

                // recurse array or object
                var n, v, json = [], arr = (obj && obj.constructor == Array);

                for (n in obj) {
                    v = obj[n]; t = typeof (v);

                    if (t == "string") v = '"' + v + '"';
                    else if (t == "object" && v !== null) v = JSON.stringify(v);

                    json.push((arr ? "" : '"' + n + '":') + String(v));
                }

                return (arr ? "[" : "{") + String(json) + (arr ? "]" : "}");
            }
        };

        $.ajaxSetup({
            type: "POST",
            contentType: "application/json; charset=utf-8",
            data: "{}",
            dataFilter: function (data) {
                var msg;

                if (typeof (JSON) !== 'undefined' && typeof (JSON.parse) === 'function') {
                    msg = JSON.parse(data);
                }
                else {
                    msg = eval('(' + data + ')');
                }
                if (msg.hasOwnProperty('d')) {
                    return msg.d;
                }
                else {
                    return msg;
                }
            },
            beforeSend: function () {
                lcboWaitMsg.show();
            }
        });

        $(function () {
            $("#reqDate").datepicker();
            $("#createOrder").click(CreateIST);

            $("#store").autocomplete({
                minLength: 1,
                source: function (request, response) {
                    var matcher = new RegExp($.ui.autocomplete.escapeRegex(request.term), "i");
                    response($.grep(StoreList, function (value) {
                        value = value.value || value.desc || value.icon;
                        return matcher.test(value);
                    }));
                },
                select: function (event, ui) {
                    $("#store").val(ui.item.value);
                    return false;
                }
            })
                    .data("ui-autocomplete")._renderItem = function (ul, item) {
                        return $("<li>")
                          .append("<a>" + item.value + " - " + item.label + "</a>")
                          .appendTo(ul);
                    };
        });

        function GetISTToSave() {
            var Ist = $.extend({}, true, IstTemplate),
                msgs = [],
                storeExists = false;

            Ist.co_odno = "";
            Ist.to_store = $("#store").val().toUpperCase();
            Ist.order_reqd_dt = $("#reqDate").val();
            Ist.comments1 = "c1";
            Ist.comments2 = "c2";
            Ist.status = "NEW";
            Ist.submittedBy = UserInfo.UserID;

            for (var x = 0, l = StoreList.length; x < l; x++) {
                if (StoreList[x].value === Ist.to_store) {
                    storeExists = true;
                    break;
                }
            }
            if (!storeExists) {
                msgs.push("Please enter a valid Store #.");
            }

            if (Ist.order_reqd_dt.length === 0) {
                msgs.push("Please enter a required by date.");
            }

            if (msgs.length > 0) {
                alert(msgs.join("\n"));
                return null;
            }

            return Ist;
        }

        function CreateIST() {
            var Ist = GetISTToSave(),
                returnObj = null;

            if (!Ist) {
                return;
            }

            $.ajax({
                type: "POST",
                async: false,
                url: "CreateTestIST.aspx/CreateIST",
                data: "{'store': '" + Ist.to_store + "', 'reqDate': '" + Ist.order_reqd_dt + "', 'numItems': '" + $("#numItems").val() + "', 'submittedBy': '" + UserInfo.UserID + "'}",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (result) { returnObj = CreateISTComplete(result, true); },
                error: function (result) { returnObj = CreateISTComplete(result, false); }
            });
            lcboWaitMsg.hide();
            if (!returnObj) {
                return false;
            }
            else {
                reloadList = true;
                alert("IST " + returnObj.CO_ODNO + " was created.");
                return true;
            }
        }

        function CreateISTComplete(result, success) {
            var msg = "There was a problem creating the IST.";

            if (success) {
                if (result.IsSuccessful) {
                    return $.extend({}, true, result.ReturnedObject);
                }
                else {
                    msg = "There was a problem creating the IST.\n\nERROR: " + result.ResponseCode + " - " + result.Details;
                }
            }
            alert(msg);
            return null;
        }

    </script>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <div>Store: <input type="text" id="store" value="" /></div>
        <div>Req Date: <input type="text" id="reqDate" value="" /></div>
        <div># Items: <input type="text" id="numItems" value="" /></div>
        <div><input type="button" id="createOrder" value="Create Order" /></div>
    </div>
    </form>
</body>
</html>
