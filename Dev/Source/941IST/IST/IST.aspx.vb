﻿Imports System.Data
Imports System.Web.Services
Imports System.Web.Script.Serialization

Public Class IST
    Inherits ISTDefaultPage 'System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If UserInfo.RSGUserSecurity.IsAuthorized("USER", True) = False Then
            PageInfo("Mode") = "V"
        End If

        Call initText()
        Call initControls()

    End Sub

    Private Sub initText()

        If PageInfo("Mode") <> "V" Then
            Page.Title += " - Add/Update"
        End If

    End Sub

    Private Sub initControls()
        Dim dt As DataTable
        Dim json As StringBuilder = New StringBuilder()
        Dim jss As New JavaScriptSerializer()
        Dim istFromWeb As New IST_FROM_WEB()

        dt = IST_STORES.GetStoreList().Copy()

        dt.Columns("LCTN_NO").ColumnName = "value"
        dt.Columns("LCTN_NAME").ColumnName = "label"
        json.Append("var StoreList = ")
        json.Append(Utils.DataTableToJSON(dt))
        json.Append(";")

        json.Append("var IstTemplate = ")
        json.Append(jss.Serialize(istFromWeb))
        json.Append(";")

        json.Append("var IsApprovalRequired = ")
        json.Append(Utils.IsApprovalRequired.ToString().ToLower())
        json.Append(";")

        ScriptManager.RegisterStartupScript(Me, GetType(Page), "clientData", json.ToString(), True)

    End Sub

    <WebMethod()> _
    Public Shared Function GetIST(ByVal co_odno As String) As String
        Dim ist As IST_ORDER
        Dim istFW As IST_FOR_WEB
        Dim response As RSGBase.RSGResponse = New RSGBase.RSGResponse()

        Try
            ist = ISTManager.GetIST(co_odno)
            If Not ist Is Nothing Then
                istFW = New IST_FOR_WEB(ist)

                response.IsSuccessful = True
                response.ResponseTip = Enums.RSGResponseTip.Success
                response.ReturnedObject = istFW
            End If
        Catch ex As ISTException
            response.IsSuccessful = False
            response.ResponseTip = Enums.RSGResponseTip.Exception
            response.ResponseCode = ex.ErrorCode
            response.Details = ex.Message
        Catch ex As Exception
            response.IsSuccessful = False
            response.ResponseTip = Enums.RSGResponseTip.Exception
        End Try

        Return response.ToJSON()

    End Function

    <WebMethod()> _
    Public Shared Function LookupItem(ByVal item As String) As String
        Dim dt As DataTable
        Dim uncommittedQty As Int32
        Dim response As RSGBase.RSGResponse = New RSGBase.RSGResponse()

        dt = DB.GetItemInfo(item)
        dt.Columns.Add(New DataColumn("UNCOMMITTED_QTY", Type.GetType("System.Int32")))

        uncommittedQty = DB.GetItemUncommittedQty(item)
        dt.Rows(0).Item("UNCOMMITTED_QTY") = uncommittedQty

        response.IsSuccessful = True
        response.ReturnedObject = Utils.DataTableToDictionary(dt)

        Return response.ToJSON()

    End Function

    <WebMethod()> _
    Public Shared Function CreateIST(ByVal ist As String) As String
        Dim js As JavaScriptSerializer = New JavaScriptSerializer()
        Dim IstFromWeb As IST_FROM_WEB = New IST_FROM_WEB()
        Dim IstOrder As IST_ORDER
        Dim IstForWeb As IST_FOR_WEB = Nothing
        Dim response As RSGBase.RSGResponse = New RSGBase.RSGResponse()

        Try
            IstFromWeb = js.Deserialize(Of IST_FROM_WEB)(ist)
            IstFromWeb.comments1 = IstFromWeb.comments1.Replace("%27", "'").Replace("%22", """")
            IstFromWeb.comments2 = IstFromWeb.comments2.Replace("%27", "'").Replace("%22", """")

            If IstFromWeb.co_odno.Length = 0 Then
                IstOrder = ISTManager.CreateIST(IstFromWeb)
                IstForWeb = New IST_FOR_WEB()
                If Not IstOrder Is Nothing Then
                    IstForWeb = New IST_FOR_WEB(IstOrder)
                    IstForWeb.LINES = New List(Of IST_FOR_WEB.ITEM) 'remove list of items to reduce network/client payload
                End If
            Else
                Throw New ISTException(1000, "Error creating IST, order # was specified.")
            End If

            response.IsSuccessful = True
            response.ResponseTip = Enums.RSGResponseTip.Success
            response.ReturnedObject = IstForWeb
        Catch ex As ISTException
            response.IsSuccessful = False
            response.ResponseTip = Enums.RSGResponseTip.Exception
            response.ResponseCode = ex.ErrorCode
            response.Details = ex.Message
        Catch ex As Exception
            response.IsSuccessful = False
            response.ResponseTip = Enums.RSGResponseTip.Exception
            response.Details = ex.Message
        End Try

        Return response.ToJSON()

    End Function

    <WebMethod()> _
    Public Shared Function UpdateIST(ByVal ist As String) As String
        Dim js As JavaScriptSerializer = New JavaScriptSerializer()
        Dim IstFromWeb As IST_FROM_WEB = New IST_FROM_WEB()
        Dim IstOrder As IST_ORDER
        Dim IstForWeb As IST_FOR_WEB = Nothing
        Dim response As RSGBase.RSGResponse = New RSGBase.RSGResponse()

        Try
            IstFromWeb = js.Deserialize(Of IST_FROM_WEB)(ist)
            IstFromWeb.comments1 = IstFromWeb.comments1.Replace("%27", "'").Replace("%22", """")
            IstFromWeb.comments2 = IstFromWeb.comments2.Replace("%27", "'").Replace("%22", """")

            If IstFromWeb.co_odno.Length = 0 Then
                Throw New ISTException(1000, "Error updating IST, order # was not specified.")
            Else
                If ISTManager.UpdateIST(IstFromWeb) = False Then
                    Throw New ISTException(1000, "There was a problem updating the IST.")
                End If
            End If

            IstOrder = ISTManager.GetIST(IstFromWeb.co_odno)
            IstForWeb = New IST_FOR_WEB()
            If Not IstOrder Is Nothing Then
                IstForWeb = New IST_FOR_WEB(IstOrder)
                IstForWeb.LINES = New List(Of IST_FOR_WEB.ITEM) 'remove list of items to reduce network/client payload
            End If

            response.IsSuccessful = True
            response.ResponseTip = Enums.RSGResponseTip.Success
            response.ReturnedObject = IstForWeb
        Catch ex As ISTException
            response.IsSuccessful = False
            response.ResponseTip = Enums.RSGResponseTip.Exception
            response.ResponseCode = ex.ErrorCode
            response.Details = ex.Message
        Catch ex As Exception
            response.IsSuccessful = False
            response.ResponseTip = Enums.RSGResponseTip.Exception
            response.ReturnedObject = ex
        End Try

        Return response.ToJSON()
    End Function

    <WebMethod()> _
    Public Shared Function AddItem(ByVal co_odno As String, _
                                   ByVal item As String, _
                                   ByVal qty As Int32, _
                                   ByVal submittedBy As String, _
                                   ByVal insufficientQtyOverride As String) As String
        Dim IstOrder As IST_ORDER
        Dim IstForWeb As IST_FOR_WEB = Nothing
        Dim response As RSGBase.RSGResponse = New RSGBase.RSGResponse()

        Try
            If ISTManager.AddItem(co_odno, item, qty, submittedBy, insufficientQtyOverride) = False Then
                Throw New ISTException("There was a problem adding the item, please try again.")
            End If

            IstOrder = ISTManager.GetIST(co_odno)
            IstForWeb = New IST_FOR_WEB()
            If Not IstOrder Is Nothing Then
                IstForWeb = New IST_FOR_WEB(IstOrder)
                IstForWeb.LINES = New List(Of IST_FOR_WEB.ITEM) 'remove list of items to reduce network/client payload
            End If

            response.IsSuccessful = True
            response.ResponseTip = Enums.RSGResponseTip.Success
            response.ReturnedObject = IstForWeb
        Catch ex As ISTException
            response.IsSuccessful = False
            response.ResponseTip = Enums.RSGResponseTip.Exception
            response.ResponseCode = ex.ErrorCode
            response.Details = ex.Message
        Catch ex As Exception
            response.IsSuccessful = False
            response.ResponseTip = Enums.RSGResponseTip.Exception
            response.ResponseCode = 1
        End Try

        Return response.ToJSON()

    End Function

    <WebMethod()> _
    Public Shared Function UpdateItem(ByVal co_odno As String, _
                                      ByVal cod_line As String, _
                                      ByVal item As String, _
                                      ByVal qty As Int32, _
                                      ByVal origQty As Int32, _
                                      ByVal submittedBy As String, _
                                      ByVal insufficientQtyOverride As String) As String
        Dim IstOrder As IST_ORDER
        Dim IstForWeb As IST_FOR_WEB = Nothing
        Dim response As RSGBase.RSGResponse = New RSGBase.RSGResponse()

        Try
            If ISTManager.UpdateItem(co_odno, cod_line, item, qty, origQty, submittedBy, insufficientQtyOverride) = False Then
                Throw New ISTException("There was a problem updating the item, please try again.")
            End If

            IstOrder = ISTManager.GetIST(co_odno)
            IstForWeb = New IST_FOR_WEB()
            If Not IstOrder Is Nothing Then
                IstForWeb = New IST_FOR_WEB(IstOrder)
                IstForWeb.LINES = New List(Of IST_FOR_WEB.ITEM) 'remove list of items to reduce network/client payload
            End If

            response.IsSuccessful = True
            response.ResponseTip = Enums.RSGResponseTip.Success
            response.ReturnedObject = IstForWeb
        Catch ex As ISTException
            response.IsSuccessful = False
            response.ResponseTip = Enums.RSGResponseTip.Exception
            response.ResponseCode = ex.ErrorCode
            response.Details = ex.Message
        Catch ex As Exception
            response.IsSuccessful = False
            response.ResponseTip = Enums.RSGResponseTip.Exception
            response.ResponseCode = 1
        End Try

        Return response.ToJSON()

    End Function

    <WebMethod()> _
    Public Shared Function ResetIST(ByVal ist As String) As String
        Dim js As JavaScriptSerializer = New JavaScriptSerializer()
        Dim IstFromWeb As IST_FROM_WEB = New IST_FROM_WEB()
        Dim IstOrder As IST_ORDER
        Dim IstForWeb As IST_FOR_WEB = Nothing
        Dim response As RSGBase.RSGResponse = New RSGBase.RSGResponse()

        Try
            IstFromWeb = js.Deserialize(Of IST_FROM_WEB)(ist)

            If IstFromWeb.co_odno.Length = 0 Then
                Throw New ISTException(1000, "Error resetting IST, order # was not specified.")
            Else
                If ISTManager.ResetIST(IstFromWeb) = False Then
                    Throw New ISTException(1000, "There was a problem resetting the IST.")
                End If
            End If

            IstOrder = ISTManager.GetIST(IstFromWeb.co_odno)
            IstForWeb = New IST_FOR_WEB()
            If Not IstOrder Is Nothing Then
                IstForWeb = New IST_FOR_WEB(IstOrder)
                IstForWeb.LINES = New List(Of IST_FOR_WEB.ITEM) 'remove list of items to reduce network/client payload
            End If

            response.IsSuccessful = True
            response.ResponseTip = Enums.RSGResponseTip.Success
            response.ReturnedObject = IstForWeb
        Catch ex As ISTException
            response.IsSuccessful = False
            response.ResponseTip = Enums.RSGResponseTip.Exception
            response.ResponseCode = ex.ErrorCode
            response.Details = ex.Message
        Catch ex As Exception
            response.IsSuccessful = False
            response.ResponseTip = Enums.RSGResponseTip.Exception
            response.ReturnedObject = ex
        End Try

        Return response.ToJSON()
    End Function

    <WebMethod()> _
    Public Shared Function ISTRequestApproval(ByVal co_odno As String, ByVal submittedBy As String) As String
        Dim IstOrder As IST_ORDER
        Dim IstForWeb As IST_FOR_WEB = Nothing
        Dim response As RSGBase.RSGResponse = New RSGBase.RSGResponse()

        Try
            If ISTManager.ISTRequestApproval(co_odno, submittedBy) = False Then
                Throw New ISTException(1000, "There was a problem sending the IST for approval.")
            End If

            IstOrder = ISTManager.GetIST(co_odno)
            IstForWeb = New IST_FOR_WEB()
            If Not IstOrder Is Nothing Then
                IstForWeb = New IST_FOR_WEB(IstOrder)
                'IstForWeb.LINES = New List(Of IST_FOR_WEB.ITEM) 'remove list of items to reduce network/client payload
            End If

            response.IsSuccessful = True
            response.ResponseTip = Enums.RSGResponseTip.Success
            response.ReturnedObject = IstForWeb
        Catch ex As ISTException
            response.IsSuccessful = False
            response.ResponseTip = Enums.RSGResponseTip.Exception
            response.ResponseCode = ex.ErrorCode
            response.Details = ex.Message
        Catch ex As Exception
            response.IsSuccessful = False
            response.ResponseTip = Enums.RSGResponseTip.Exception
            response.Details = ex.Message
        End Try

        Return response.ToJSON()

    End Function

End Class