﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="ISTReport.aspx.vb" Inherits="_941IST.ISTReport" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>941 IST Report</title>
    <link href="../css/Standard.css" rel="stylesheet" />
    <link href="../css/smoothness/jquery-ui-1.10.3.custom.css" rel="stylesheet" />
    <link href="../css/SlickGrid/slick.grid.css" rel="stylesheet" />
    <link href="../js/AutoCompleteDDL/AutoCompleteDDL.css" rel="stylesheet" />
    <link href="../js/Wait/lcboMessage.css" rel="stylesheet" />

    <style type="text/css">

        .content
        {
            width: 1000px;
        }
         .tallHeader .slick-header,.tallHeader  .slick-header-columns,.tallHeader  .slick-header-column.ui-state-default
        {
        	height: 43px;
        }
        .tallHeaderCssClass {text-align: center; white-space: normal;}
        .gridColC {text-align: center;}
        .gridColR {text-align: right;}
        .gridCount
        {
            text-align: right;
            font-weight: bold;
        }
        .view
        {
            cursor: pointer; 
            text-decoration: underline;
            color: blue;
        }
        .lbl
        {
            display: inline-block;
            width: 120px;
            text-align: right;
            padding-right: 5px;
            font-weight: bold;
        }
        legend
        {
            font-weight: bold;
        }
        .excel-icon
        {
            display: inline-block;
            cursor: pointer;
            padding-left: 20px;
            background-image: url(../images/document-excel-csv_16x16.png);
            background-repeat: no-repeat;
            background-position: top left;
            font-weight: bold;
        }
        input[type='button']
        {
            width: 100px;
        }
    </style>

    <script src="../js/jquery-1.9.1.min.js"></script>
    <script src="../js/jquery-ui-1.10.3.custom.min.js"></script>
    <script src="../js/jquery.event.drag-2.2.js"></script>
    <script src="../js/jquery.event.drop-2.2.js"></script>
    <script src="../js/SlickGrid/slick.core.js"></script>
    <script src="../js/SlickGrid/slick.grid.js"></script>
    <script src="../js/SlickGrid/slick.dataview.js"></script>
    <script src="../js/SlickGrid/slick.dataview.js"></script>
    <script src="../js/SlickGrid/slick.groupitemmetadataprovider.js"></script>
    <script src="../js/Wait/lcboWait.js"></script>

    <script type="text/javascript">
        var data = [],
            DV = null,
            grid = null,
            gridOptions = null,
            columns = null,
            sortdir = "",
            sortcol = "",
            filters = {
                co_odno: null,
                to_store: null,
                dateFrom: null,
                dateTo: null,
                authBy: null,
                subBy: null,
                status: null,
                hideCancelled: true
            },
            ctlRowCnt = null,
            sortdir = null,
            sortcol = null,
            numItems = 0,
            months = {
                "01": "January",
                "02": "February",
                "03": "March",
                "04": "April",
                "05": "May",
                "06": "June",
                "07": "July",
                "08": "August",
                "09": "September",
                "10": "October",
                "11": "November",
                "12": "December"
            },
            T1Statuses = {
                "C": "Complete",
                "A": "ASN Arrived",
                "V": "Voided"
            };
        
        var comparers = {
            comparer: function (a, b) {
                var x = a[sortcol], y = b[sortcol];
                return (x == y ? 0 : (x > y ? 1 : -1));
            },

            comparerNumeric: function (a, b) {
                var x = parseFloat(a[sortcol]), y = parseFloat(b[sortcol]);
                var retVal;
                if (isNaN(x)) {
                    x = null;
                }
                if (isNaN(y)) {
                    y = null;
                }
                if (!x && !y) {
                    retVal = 0;
                }
                else if (!x && y > 0) {
                    retVal = -1;
                }
                else if (!y && x < 0) {
                    retVal = 1;
                }
                else {
                    retVal = (x == y ? 0 : (x > y ? 1 : -1));
                }

                return retVal;
            }
        };

        var msie = (function () {

            var undef,
                v = 3,
                div = document.createElement('div'),
                all = div.getElementsByTagName('i');

            while (
                div.innerHTML = '<!--[if gt IE ' + (++v) + ']><i></i><![endif]-->',
                all[0]
            );

            return v > 4 ? v : undef;

        }());

        //create JSON.stringify function
        var JSON = JSON || {};
        // implement JSON.stringify serialization
        JSON.stringify = JSON.stringify || function (obj) {

            var t = typeof (obj);
            if (t != "object" || obj === null) {

                // simple data type
                if (t == "string") obj = '"' + obj + '"';
                return String(obj);

            }
            else {

                // recurse array or object
                var n, v, json = [], arr = (obj && obj.constructor == Array);

                for (n in obj) {
                    v = obj[n]; t = typeof (v);

                    if (t == "string") v = '"' + v + '"';
                    else if (t == "object" && v !== null) v = JSON.stringify(v);

                    json.push((arr ? "" : '"' + n + '":') + String(v));
                }

                return (arr ? "[" : "{") + String(json) + (arr ? "]" : "}");
            }
        };

        $.ajaxSetup({
            type: "POST",
            contentType: "application/json; charset=utf-8",
            data: "{}",
            dataFilter: function (data) {
                var msg;

                if (typeof (JSON) !== 'undefined' && typeof (JSON.parse) === 'function') {
                    msg = JSON.parse(data);
                }
                else {
                    msg = eval('(' + data + ')');
                }
                if (msg.hasOwnProperty('d')) {
                    return msg.d;
                }
                else {
                    return msg;
                }
            },
            beforeSend: function () {
                lcboWaitMsg.show();
            }
        });

        $(function () {
            var groupItemMetadataProvider;

            $("#btnClose").click(closeWin);
            $("#btnRefresh").click(getISTReport);
            ctlRowCnt = document.getElementById("ISTCount");
            $("#ddlStatus").change(txtFilter);
            $("#ddlGroupBy").change(groupBy);
            $("#chkExcludeCancelled").click(checkboxFilter);
            $("#export").click(ExportToExcel);
            $(".txtFilter").keyup(function (e) {

                // clear on Esc
                if (e.which == 27) {
                    this.value = "";
                }

                txtFilter(e);
            });
            $("#txtDateFrom,#txtDateTo")
                .datepicker({
                    onSelect: dateFilter,
                    showOn: "button",
                    buttonImage: "../images/calendar.gif",
                    buttonImageOnly: true
                })
                .keydown(function (e) {
                    var that = this;
                    //alert('down');
                    if (e.which == 27 || e.which == 46) {
                        this.value = "";
                        setTimeout(function () { dateFilter("", that); }, 0);
                    }
                    else if (e.which == 9) {
                        return true;
                    }
                    e.stopPropagation();
                    e.preventDefault();
                    return false;
                });

            gridOptions = {
                enableCellNavigation: true,
                editable: false,
                rowHeight: 18
            };

            columns = [
                { id: "dummy", name: "", field: "dummy", width: 1, cssClass: "", headerCssClass: "tallHeaderCssClass", sortable: true, sortNumeric: true },
                { id: "CO_ODNO", name: "Order #", field: "CO_ODNO", width: 70, minWidth: 50, cssClass: "view gridColC", headerCssClass: "tallHeaderCssClass", sortable: true, sortNumeric: true },
                { id: "TO_STORE", name: "Store #", field: "TO_STORE", width: 55, cssClass: "gridColC", headerCssClass: "tallHeaderCssClass", sortable: true },
                { id: "ORDER_DT_TM", name: "Order Date", field: "ORDER_DT_TM", cssClass: "gridColC", width: 85, headerCssClass: "tallHeaderCssClass", sortable: true, sortNumeric: true, formatter: DateFormatter },
                /*{ id: "IST_TYPE", name: "IST Type", field: "IST_TYPE", width: 95, cssClass: "gridColC", headerCssClass: "tallHeaderCssClass", sortable: true },*/
                { id: "PU_ORDER_FL", name: "Pickup", field: "PU_ORDER_FL", width: 60, cssClass: "gridColC", headerCssClass: "tallHeaderCssClass", sortable: true },
                { id: "NUM_OF_PROD", name: "# Products", field: "NUM_OF_PROD", width: 70, cssClass: "gridColC", headerCssClass: "tallHeaderCssClass", sortable: true, sortNumeric: true, groupTotalsFormatter: sumTotalsFormatter },
                { id: "NUM_OF_UNITS", name: "# Units", field: "NUM_OF_UNITS", width: 70, cssClass: "gridColC", headerCssClass: "tallHeaderCssClass", sortable: true, sortNumeric: true, groupTotalsFormatter: sumTotalsFormatter },
                { id: "STATUS", name: "Status", field: "STATUS", width: 100, headerCssClass: "tallHeaderCssClass", sortable: true },
                { id: "TRAFFIC_PLANNED", name: "Traffic Planned", field: "TRAFFIC_PLANNED", width: 70, headerCssClass: "tallHeaderCssClass", cssClass: "gridColC", sortable: true },
                { id: "PICK_PLANNED", name: "Pick Planned", field: "PICK_PLANNED", width: 70, headerCssClass: "tallHeaderCssClass", cssClass: "gridColC", sortable: true },
                { id: "EORD", name: "EORD", field: "EORD", width: 63, headerCssClass: "tallHeaderCssClass", cssClass: "gridColC", sortable: true },
                { id: "INVOICED", name: "Invoiced", field: "INVOICED", width: 70, headerCssClass: "tallHeaderCssClass", cssClass: "gridColC", sortable: true },
                { id: "ASN_SENT", name: "ASN Sent", field: "ASN_SENT", width: 90, headerCssClass: "tallHeaderCssClass", cssClass: "gridColC", sortable: true, formatter: DateFormatter },
                { id: "T1_STATUS", name: "T1 Status", field: "T1_STATUS", width: 80, headerCssClass: "tallHeaderCssClass", cssClass: "gridColC", sortable: true, formatter: T1StatusFormatter }
            ];

            groupItemMetadataProvider = new Slick.Data.GroupItemMetadataProvider();

            DV = new Slick.Data.DataView({
                groupItemMetadataProvider: groupItemMetadataProvider
            });
            DV.setFilterArgs(filters);
            DV.setFilter(myFilter);

            grid = new Slick.Grid("#grid", DV, columns, gridOptions);

            // register the group item metadata provider to add expand/collapse group handlers
            grid.registerPlugin(groupItemMetadataProvider);

            // wire up model events to drive the grid
            DV.onRowCountChanged.subscribe(function (e, args) {
                grid.updateRowCount();
                grid.render();
                ctlRowCnt.innerText = setCount();
            });

            DV.onRowsChanged.subscribe(function (e, args) {
                grid.invalidateRows(args.rows);
                grid.render();
            });

            grid.onSort.subscribe(function (e, args) {
                sortdir = args.sortAsc ? 1 : -1;
                sortcol = args.sortCol.field;
                if (args.sortCol.sortNumeric) {
                    DV.sort(comparers.comparerNumeric, args.sortAsc);
                }
                else {
                    //DV.sort(comparers.comparer, args.sortAsc);
                    DV.fastSort(sortcol, (sortdir === 1));
                }

            });

            grid.onClick.subscribe(function (e, args) {
                el = e.srcElement ? e.srcElement : e.target;

                var data = this.getDataItem(args.row);
                var colId = this.getColumns()[args.cell].id;

                if (data && colId === "CO_ODNO") {
                    openIST(data.CO_ODNO);
                }
            });

            getISTReport();
        });

        function getISTReport() {
            lcboWaitMsg.show();

            DV.beginUpdate();
            DV.setItems([]);
            DV.endUpdate();

            $.ajax({
                type: "POST",
                url: "ISTReport.aspx/GetISTReport",
                data: "{}",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (result) { getISTReportComplete(result, true); },
                error: function (result) { getISTReportComplete(result, false); }
            });
        }

        function closeWin() {
            window.close();
        }

        function openIST(co_odno) {
            var sFeatures = "dialogWidth: 790px; dialogHeight: 650px; status: no; resizable: no; scroll: no;",
                result = null,
                json = null;

            result = window.showModalDialog("IST.aspx?mode=V", { "co_odno": co_odno }, sFeatures);
        }

        function getISTReportComplete(result, success) {
            //var orders = {};

            lcboWaitMsg.hide();

            if (success && result.IsSuccessful) {
                DV.beginUpdate();
                DV.setItems($.extend([], result.ReturnedObject));
                DV.endUpdate();
                
                /*
                for (var x = 0, l = result.ReturnedObject.length; x < l; x++) {
                    if (result.ReturnedObject[x].SAVED_OLD == 1) {
                        orders[result.ReturnedObject[x].id] = {};
                        orders[result.ReturnedObject[x].id].CO_ODNO = "oldSave";
                    }
                    else if (result.ReturnedObject[x].REQ_AUTH_OLD == 1) {
                        orders[result.ReturnedObject[x].id] = {};
                        orders[result.ReturnedObject[x].id].CO_ODNO = "oldAuth";
                    }
                }
                grid.setCellCssStyles("highlight", orders);
                grid.render();
                */
            }
        }

        function DateFormatter(row, cell, value, columnDef, dataContext) {
            if (!value) { return; }
            return value.substring(4, 6) + "/" + value.substring(6, 8) + "/" + value.substring(0, 4);
        }

        function T1StatusFormatter(row, cell, value, columnDef, dataContext) {
            if (!value) { return; }
            return (T1Statuses[value] !== "undefined" ? T1Statuses[value] : value);
        }

        function dateFilter(txtDate, ctrl) {
            var val = txtDate.replace(/ /g, ""),
                id = ctrl.id;
            val = (val.length > 0 ? parseInt((val.substring(6, 10) + val.substring(0, 2) + val.substring(3, 5)), 10) : null);
            if (id.indexOf("From") !== -1) {
                filters.dateFrom = val;
            }
            else {
                filters.dateTo = val;
            }
            DV.refresh();
        }

        function checkboxFilter(e) {
            filters.hideCancelled = $("#" + e.target.id).prop("checked");
            DV.refresh();
        }

        function txtFilter(e) {
            var val = $(e.target).val();
            switch (e.target.id) {
                case "txtOrder":
                    filters.co_odno = (val === "" || val === "-1" ? null : val);
                    break;
                case "txtStore":
                    filters.to_store = (val === "" || val === "-1" ? null : val.toUpperCase());
                    break;
                case "txtSubBy":
                    filters.subBy = (val === "" || val === "-1" ? null : val.toUpperCase());
                    break;
                case "txtAuthBy":
                    filters.authBy = (val === "" || val === "-1" ? null : val.toUpperCase());
                    break;
                case "ddlStatus":
                    filters.status = (val === "" ? null : val);
                    break;
                default:
                    break;
            };
            DV.refresh();
        }

        function myFilter(item, args) {
            if (args.co_odno && item["CO_ODNO"].indexOf(args.co_odno) === -1) {
                return false;
            }
            if (args.to_store && item["TO_STORE"].indexOf(args.to_store) === -1) {
                return false;
            }
            if (args.dateFrom && parseInt(item["ORDER_DT_TM"], 10) < args.dateFrom) {
                return false;
            }
            if (args.dateTo && parseInt(item["ORDER_DT_TM"], 10) > args.dateTo) {
                return false;
            }
            if (args.subBy && (!item["SUBMITTED_BY"] || item["SUBMITTED_BY"].indexOf(args.subBy) === -1)) {
                return false;
            }
            if (args.authBy && (!item["AUTHORIZED_BY"] || item["AUTHORIZED_BY"].indexOf(args.authBy) === -1)) {
                return false;
            }
            if (args.status && item["STATUS"] !== args.status) {
                return false;
            }
            if (item["STATUS"] === "CANCELLED") {
                return !args.hideCancelled;
            }

            return true;
        }

        function groupBy(e) {
            numItems = 0;
            switch ($(arguments[0].target).val()) {
                case "M":
                    groupByMonth();
                    break;
                case "S":
                    groupByStore();
                    break;
                default:
                    DV.setGrouping([]);
                    numItems = DV.getLength();
                    break;
            }
            setCount();
        }

        function groupByStore() {
            sortcol = "TO_STORE";
            //grid.invalidateAllRows();
            DV.sort(comparers.comparer, true);
            //grid.render();
            DV.setGrouping({
                getter: "TO_STORE",
                formatter: function (g) {
                    numItems += g.count;
                    return "Store:  " + g.value + " - <span style='color:green'>" + g.count + " IST(s)</span>";
                },
                aggregators: [
                  new Slick.Data.Aggregators.Sum("NUM_OF_PROD"),
                  new Slick.Data.Aggregators.Sum("NUM_OF_UNITS")
                ],
                aggregateCollapsed: false
            });
        }

        function groupByMonth() {
            sortcol = "ORDER_DT_TM";
            grid.invalidateAllRows();
            DV.sort(comparers.comparerNumeric, true);
            grid.render();
            DV.setGrouping([
                {
                    getter: "ORDER_YEAR",
                    formatter: function (g) {
                        numItems += g.count;
                        return g.value + " - <span style='color:green'>" + g.count + " IST(s)</span>";
                    },
                    aggregators: [
                      new Slick.Data.Aggregators.Sum("NUM_OF_PROD"),
                      new Slick.Data.Aggregators.Sum("NUM_OF_UNITS")
                    ],
                    aggregateCollapsed: false
                },
                {
                    getter: "ORDER_MONTH",
                    formatter: function (g) {
                        return months[g.value] + " - <span style='color:green'>" + g.count + " IST(s)</span>";
                    },
                    aggregators: [
                      new Slick.Data.Aggregators.Sum("NUM_OF_PROD"),
                      new Slick.Data.Aggregators.Sum("NUM_OF_UNITS")
                    ],
                    aggregateCollapsed: false
                }
            ]);
        }

        function sumTotalsFormatter(totals, columnDef) {
            var val = totals.sum && totals.sum[columnDef.field];
            if (val != null) {
                return (Math.round(parseFloat(val) * 100) / 100);
            }
            return "";
        }

        function setCount() {
            /*
            var md,
                l = DV.getLength(),
                mdCount = 0;

            for (var x = 0; x < l; x++) {
                md = DV.getItemMetadata(x); 
                if (md && md.cssClasses.indexOf("group") > -1) {
                    mdCount += 1;
                }
            }
            return (l - mdCount);
            */
            if ($("#ddlGroupBy").val().length > 0) {
                return numItems;
            }
            return DV.getLength();
        }

        function ExportToExcel() {
            var line = '',
                doc = [],
                data = [],//DV.getItems(),
                cols = {};

            for (var x = 0, l = DV.getLength() ; x < l; x++) {
                data.push(DV.getItem(x));
            }
            //debugger;
            //generate header
            for (var x = 0, l = columns.length; x < l; x++) {
                var value = columns[x].name + "";
                line += '"' + value.replace(/"/g, '""') + '",';
                cols[columns[x].field] = {};
                cols[columns[x].field].formatter = (typeof columns[x].formatter !== "undefined" ? columns[x].formatter : null);
            }
            line = line.slice(0, -1);
            doc.push(line);

            //generate rows
            for (var i = 0, l = data.length; i < l; i++) {
                var line = '';

                for (var fld in cols) {
                    var value = (cols[fld].formatter ? cols[fld].formatter(null, null, data[i][fld]) : data[i][fld]);
                    line += '"' + (value && typeof value !== "undefined" ? value.toString().replace(/"/g, '""') : "") + '",';
                }

                line = line.slice(0, -1);
                doc.push(line);
            }
            //window.open("data:text/csv;charset=utf-8," + escape(doc.join("\r\n")));

            post_to_url("IstReport.aspx?Export=y", { 'data': doc.join("\r\n").replace(/<br\/>/gi, '') }, "post");
        }

        function post_to_url(path, params, method) {
            method = method || "post"; // Set method to post by default if not specified.
            //debugger;
            // The rest of this code assumes you are not using a library.
            // It can be made less wordy if you use one.
            var form = document.createElement("form");
            form.setAttribute("method", method);
            form.setAttribute("action", path);

            for (var key in params) {
                if (params.hasOwnProperty(key)) {
                    var hiddenField = document.createElement("input");
                    hiddenField.setAttribute("type", "hidden");
                    hiddenField.setAttribute("name", key);
                    hiddenField.setAttribute("value", params[key]);

                    form.appendChild(hiddenField);
                }
            }

            document.body.appendChild(form);
            form.submit();
        }

    </script>

</head>
<body>
    <form id="form1" runat="server">
    <div id="hdr"><span>941 IST REPORT</span></div>
    <div class="main">
        <div class="content">
            <fieldset>
                <legend>Search Criteria</legend>
                <div><span class="lbl">Order #:</span><input type="text" id="txtOrder" class="txtFilter" /></div>
                <div><span class="lbl">Store #:</span><input type="text" id="txtStore" class="txtFilter" /></div>
                <div><span class="lbl">Order Date From:</span><input type="text" id="txtDateFrom" /><span class="lbl" style="padding-left: 15px;">Order Date To: </span><input type="text" id="txtDateTo" /></div>
                <div style="display: none;"><span class="lbl">Submitted By:</span><input type="text" id="txtSubBy" class="txtFilter" /></div>
                <div style="display: none;"><span class="lbl">Authorized By:</span><input type="text" id="txtAuthBy" class="txtFilter" /></div>
                <div><span class="lbl">Status:</span><asp:DropDownList ID="ddlStatus" runat="server"></asp:DropDownList><asp:CheckBox ID="chkExcludeCancelled" runat="server" Text="Exclude Cancelled Orders" Checked="true" /></div>
                <div><span class="lbl">Group By:</span><asp:DropDownList ID="ddlGroupBy" runat="server"><asp:ListItem Selected="True" Text="" Value=""></asp:ListItem><asp:ListItem Text="Month" Value="M"></asp:ListItem><asp:ListItem Text="Store" Value="S"></asp:ListItem></asp:DropDownList></div>
            </fieldset>
            <div style="margin-top: 10px;">
                <div>
                    <div id="export" style="width: 49%; float: left;"><span class="excel-icon">Export to Excel</span></div>
                    <div class="gridCount" style="display: none; width:49%; float: right;"><span># of ISTs:&nbsp;</span><span id="ISTCount">0</span></div>
                    <div style="clear: both;"></div>
                </div>
                <div id="grid" class="grids tallHeader" style="height: 400px; width: 100%;"></div>
            </div>
            <div style="margin-top: 20px; text-align: center;">
                <input type="button" id="btnRefresh" value="Refresh Data" />
                <input type="button" id="btnClose" value="Close" />
            </div>
        </div>
    </div>
    </form>
</body>
</html>
