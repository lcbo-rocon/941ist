﻿Imports System.Data
Imports System.Web.Services

Public Class ISTReport
    Inherits ISTDefaultPage

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load


        'if (!string.IsNullOrEmpty(Request.QueryString["Export"]))
        '    {
        '        ExportToExcel(Request.Form["data"], "StoreK52");
        '        Response.End();
        '    }

        If String.IsNullOrEmpty(Request.QueryString("Export")) = False Then
            ExportToExcel(Request.Form("data"), "IST Report")
        End If

        Call initText()
        Call initControls()

    End Sub

    Private Sub initText()

    End Sub

    Private Sub initControls()
        Dim json As StringBuilder = New StringBuilder()

        ddlStatus.Items.Add(New ListItem("ALL", ""))
        ddlStatus.Items.Add(New ListItem("APPROVAL REQUESTED", "APPROVAL REQUESTED"))
        ddlStatus.Items.Add(New ListItem("CANCELLED", "CANCELLED"))
        ddlStatus.Items.Add(New ListItem("INVOICED", "INVOICED"))
        ddlStatus.Items.Add(New ListItem("NEW-EDIT", "NEW-EDIT"))
        ddlStatus.Items.Add(New ListItem("SAVED", "SAVED"))
        ddlStatus.Items.Add(New ListItem("SAVED-EDIT", "SAVED-EDIT"))
        ddlStatus.Items.Add(New ListItem("SUBMITTED", "SUBMITTED"))
    End Sub

    <WebMethod()> _
    Public Shared Function GetISTReport() As String
        Dim dt As DataTable
        Dim id As Int32
        Dim response As RSGBase.RSGResponse = New RSGBase.RSGResponse()

        Try
            dt = ISTManager.GetISTReport()

            'add in id column to allow grid to display
            'done here instead of the client for expediency purposes 
            dt.Columns.Add(New DataColumn("id", GetType(Int32)))
            For Each row As DataRow In dt.Rows
                row("id") = id
                id += 1
            Next

            response.IsSuccessful = True
            response.ReturnedObject = Utils.DataTableToDictionary(dt)
            Return response.ToJSON()
        Catch ex As ISTException
            response.IsSuccessful = False
            response.ResponseTip = Enums.RSGResponseTip.Exception
            response.ResponseCode = ex.ErrorCode
            response.Details = ex.Message
        Catch ex As Exception
            response.IsSuccessful = False
            response.ResponseTip = Enums.RSGResponseTip.Exception
            response.Details = ex.Message
        End Try

        Return response.ToJSON()

    End Function

    Private Sub ExportToExcel(data As String, fileName As String)

        Response.ClearHeaders()
        Response.Charset = [String].Empty
        Response.ContentType = "application/vnd.ms-excel"

        Dim contentDisposition As String = [String].Concat("inline; filename=Export_", fileName, ".csv")
        Response.AppendHeader("content-disposition", contentDisposition)
        Response.Write(data.Replace("\r\n", Environment.NewLine))
        Response.Flush()
        Response.Close()
        Response.[End]()

    End Sub

End Class