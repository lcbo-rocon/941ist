﻿Imports System.Data
Imports System.Web.Services


Public Class ISTList
    Inherits ISTDefaultPage 'System.Web.UI.Page

    Dim IsUser As Boolean = False
    Dim IsApprover As Boolean = False

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        IsUser = UserInfo.RSGUserSecurity.IsAuthorized("USER", False)
        IsApprover = UserInfo.RSGUserSecurity.IsAuthorized("APPROVER", False)

        If IsUser = False And IsApprover = False Then
            'force redirect to no access page if required
            UserInfo.RSGUserSecurity.IsAuthorized("dummy", True)
        End If

        Call initText()
        Call initControls()

        'ISTManager.ISTRequestApproval("5943430", "ITWXH")

    End Sub

    Private Sub initText()

    End Sub

    Private Sub initControls()
        Dim json As StringBuilder = New StringBuilder()

        ddlStatus.Items.Add(New ListItem("ALL", ""))
        If Utils.IsApprovalRequired Then
            ddlStatus.Items.Add(New ListItem("APPROVAL REQUESTED", "APPROVAL REQUESTED"))
        End If
        ddlStatus.Items.Add(New ListItem("CANCELLED", "CANCELLED"))
        ddlStatus.Items.Add(New ListItem("INVOICED", "INVOICED"))
        ddlStatus.Items.Add(New ListItem("NEW-EDIT", "NEW-EDIT"))
        ddlStatus.Items.Add(New ListItem("SAVED", "SAVED"))
        ddlStatus.Items.Add(New ListItem("SAVED-EDIT", "SAVED-EDIT"))
        ddlStatus.Items.Add(New ListItem("SUBMITTED", "SUBMITTED"))

        json.Append("var IsUser = ")
        json.Append(IsUser.ToString().ToLower())
        json.Append(";")
        json.Append("var IsApprover = ")
        json.Append(IsApprover.ToString().ToLower())
        json.Append(";")
        json.Append("var IsApprovalRequired = ")
        json.Append(Utils.IsApprovalRequired.ToString().ToLower())
        json.Append(";")

        json.Append("var savedThreshold = ")
        json.Append(ConfigurationManager.AppSettings.Get("SAVED_THRESHOLD_DAYS"))
        json.Append(";")
        json.Append("var reqAuthThreshold = ")
        json.Append(ConfigurationManager.AppSettings.Get("AUTH_THRESHOLD_DAYS"))
        json.Append(";")

        ScriptManager.RegisterStartupScript(Me, GetType(Page), "clientData", json.ToString(), True)

    End Sub

    <WebMethod()> _
    Public Shared Function GetISTList(ByVal co_odno As String) As String
        Dim dt As DataTable
        Dim id As Int32
        Dim response As RSGBase.RSGResponse = New RSGBase.RSGResponse()

        Try
            dt = ISTManager.GetISTList(co_odno)

            If String.IsNullOrEmpty(co_odno) Then
                'add in id column to allow grid to display
                'done here instead of the client for expediency purposes 
                dt.Columns.Add(New DataColumn("id", GetType(Int32)))
                For Each row As DataRow In dt.Rows
                    row("id") = id
                    id += 1
                Next
            End If

            response.IsSuccessful = True
            response.ReturnedObject = Utils.DataTableToDictionary(dt)
            Return response.ToJSON()
        Catch ex As ISTException
            response.IsSuccessful = False
            response.ResponseTip = Enums.RSGResponseTip.Exception
            response.ResponseCode = ex.ErrorCode
            response.Details = ex.Message
        Catch ex As Exception
            response.IsSuccessful = False
            response.ResponseTip = Enums.RSGResponseTip.Exception
            response.Details = ex.Message
        End Try

        Return response.ToJSON()

    End Function

    <WebMethod()> _
    Public Shared Function CancelIST(ByVal co_odno As String, ByVal submittedBy As String) As String
        Dim response As RSGBase.RSGResponse = New RSGBase.RSGResponse()

        Try
            If ISTManager.CancelIST(co_odno, submittedBy) = False Then
                Throw New ISTException(1000, "There was a problem cancelling the IST.")
            End If

            response.IsSuccessful = True
            response.ResponseTip = Enums.RSGResponseTip.Success
        Catch ex As ISTException
            response.IsSuccessful = False
            response.ResponseTip = Enums.RSGResponseTip.Exception
            response.ResponseCode = ex.ErrorCode
            response.Details = ex.Message
        Catch ex As Exception
            response.IsSuccessful = False
            response.ResponseTip = Enums.RSGResponseTip.Exception
            response.Details = ex.Message
        End Try

        Return response.ToJSON()

    End Function

    <WebMethod()> _
    Public Shared Function ISTRequestApproval(ByVal co_odno As String, ByVal submittedBy As String) As String
        Dim response As RSGBase.RSGResponse = New RSGBase.RSGResponse()

        Try
            If ISTManager.ISTRequestApproval(co_odno, submittedBy) = False Then
                Throw New ISTException(1000, "There was a problem sending the IST for approval.")
            End If

            response.IsSuccessful = True
            response.ResponseTip = Enums.RSGResponseTip.Success

        Catch ex As ISTException
            response.IsSuccessful = False
            response.ResponseTip = Enums.RSGResponseTip.Exception
            response.ResponseCode = ex.ErrorCode
            response.Details = ex.Message
        Catch ex As Exception
            response.IsSuccessful = False
            response.ResponseTip = Enums.RSGResponseTip.Exception
            response.Details = ex.Message
        End Try

        Return response.ToJSON()

    End Function

    <WebMethod()> _
    Public Shared Function ISTApprove(ByVal co_odno As String, ByVal submittedBy As String) As String
        Dim response As RSGBase.RSGResponse = New RSGBase.RSGResponse()

        Try
            If ISTManager.ISTApprove(co_odno, submittedBy, False) = False Then
                Throw New ISTException(1000, "There was a problem approving the IST.")
            End If

            response.IsSuccessful = True
            response.ResponseTip = Enums.RSGResponseTip.Success

        Catch ex As ISTException
            response.IsSuccessful = False
            response.ResponseTip = Enums.RSGResponseTip.Exception
            response.ResponseCode = ex.ErrorCode
            response.Details = ex.Message
        Catch ex As Exception
            response.IsSuccessful = False
            response.ResponseTip = Enums.RSGResponseTip.Exception
            response.Details = ex.Message
        End Try

        Return response.ToJSON()

    End Function

    <WebMethod()> _
    Public Shared Function RecallIST(ByVal co_odno As String, ByVal submittedBy As String) As String
        Dim response As RSGBase.RSGResponse = New RSGBase.RSGResponse()

        Try
            If ISTManager.RecallIST(co_odno, submittedBy) = False Then
                Throw New ISTException(1000, "There was a problem recalling the IST.")
            End If

            response.IsSuccessful = True
            response.ResponseTip = Enums.RSGResponseTip.Success
        Catch ex As ISTException
            response.IsSuccessful = False
            response.ResponseTip = Enums.RSGResponseTip.Exception
            response.ResponseCode = ex.ErrorCode
            response.Details = ex.Message
        Catch ex As Exception
            response.IsSuccessful = False
            response.ResponseTip = Enums.RSGResponseTip.Exception
            response.Details = ex.Message
        End Try

        Return response.ToJSON()

    End Function

End Class