﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="IST.aspx.vb" Inherits="_941IST.IST" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>IST</title>
    <link href="../css/Standard.css" rel="stylesheet" />
    <link href="../css/smoothness/jquery-ui-1.10.3.custom.css" rel="stylesheet" />
    <link href="../css/SlickGrid/slick.grid.css" rel="stylesheet" />
    <link href="../js/AutoCompleteDDL/AutoCompleteDDL.css" rel="stylesheet" />
    <link href="../js/Wait/lcboMessage.css" rel="stylesheet" />

    <style type="text/css">

        .content
        {
            width: 715px;
        }
        .ui-autocomplete { height: 200px; overflow-y: scroll; overflow-x: hidden;}
        .lbl
        {
            display: inline-block;
            font-weight: bold;
            text-align: right;
            width: 100px;
            margin-right: 5px;
        }
        .gridColC {text-align: center;}
        .gridColR {text-align: right;}
        .delete
        {
            cursor: pointer; 
            display: inline-block; 
            width: 16px; 
            background-image:url('../images/editdelete_16x16.gif'); 
            background-repeat:no-repeat;
            background-position:center;
        }
        input.editor-text {
          width: 100%;
          height: 100%;
          border: 0;
          margin: 0;
          background: transparent;
          outline: 0;
          padding: 0;
        }
        .edited-cell
        {
            background: url('../images/dirtyCell.gif') no-repeat left top;
        }
        .text-error
        {
            background-color: #ff9292;
        }
        .btn
        {
            width: 90px;
        }
        .rotb
        {
            background-color: #efefef;
        }
        .req
        {
            margin-left: 5px;
            color: red;
        }
    </style>

    <script src="../js/jquery-1.9.1.min.js"></script>
    <script src="../js/jquery-ui-1.10.3.custom.min.js"></script>
    <script src="../js/jquery.event.drag-2.2.js"></script>
    <script src="../js/jquery.event.drop-2.2.js"></script>
    <script src="../js/SlickGrid/slick.core.js"></script>
    <script src="../js/SlickGrid/slick.grid.js"></script>
    <script src="../js/SlickGrid/slick.dataview.js"></script>
    <script src="../js/Wait/lcboWait.js"></script>

    <script type="text/javascript">
        var data = [],
            DV = null,
            grid = null,
            gridOptions = null,
            columns = null,
            sortdir = "",
            sortcol = "",
            ctlRowCnt = null,
            ctlItem = null,
            ctlItemDesc = null,
            ctlQty = null,
            reloadList = false,
            maxLineFromDB = 0;

        var msie = (function () {

            var undef,
                v = 3,
                div = document.createElement('div'),
                all = div.getElementsByTagName('i');

            while (
                div.innerHTML = '<!--[if gt IE ' + (++v) + ']><i></i><![endif]-->',
                all[0]
            );

            return v > 4 ? v : undef;

        }());

        //create JSON.stringify function
        var JSON = JSON || {};
        // implement JSON.stringify serialization
        JSON.stringify = JSON.stringify || function (obj) {

            var t = typeof (obj);
            if (t != "object" || obj === null) {

                // simple data type
                if (t == "string") obj = '"' + obj + '"';
                return String(obj);

            }
            else {

                // recurse array or object
                var n, v, json = [], arr = (obj && obj.constructor == Array);

                for (n in obj) {
                    v = obj[n]; t = typeof (v);

                    if (t == "string") v = '"' + v + '"';
                    else if (t == "object" && v !== null) v = JSON.stringify(v);

                    json.push((arr ? "" : '"' + n + '":') + String(v));
                }

                return (arr ? "[" : "{") + String(json) + (arr ? "]" : "}");
            }
        };

        $.ajaxSetup({
            type: "POST",
            contentType: "application/json; charset=utf-8",
            data: "{}",
            dataFilter: function (data) {
                var msg;

                if (typeof (JSON) !== 'undefined' && typeof (JSON.parse) === 'function') {
                    msg = JSON.parse(data);
                }
                else {
                    msg = eval('(' + data + ')');
                }
                if (msg.hasOwnProperty('d')) {
                    return msg.d;
                }
                else {
                    return msg;
                }
            },
            beforeSend: function () {
                lcboWaitMsg.show();
            }
        });

        $(function () {
            if (PageInfo.Mode === "A") {
                $("#hdr span").text($("#hdr span").text() + " Creation");
            } else if (PageInfo.Mode === "E") {
                $("#hdr span").text($("#hdr span").text() + " Update");
            };
            ctlRowCnt = document.getElementById("numItems");
            ctlItem = $("#txtItem");
            ctlItemDesc = $("#itemDesc");
            ctlAvailQty = $("#lblAvailQty");
            ctlQty = $("#txtQty");
            ctlQtyTotal = $("#lblQtyTotal");

            $("#btnClose").click(CloseWin).focus();
            $("#btnSave").click(UpdateIST);
            $("#btnResetCancel").click(ResetIST);
            $("#btnRequestApproval").click(RequestApproal);
           
            if (PageInfo.Mode !== "V") {
                $("input[type='text']").data("val", "");
                //add or edit mode
                ctlItem
                    .change(LookupItem)
                    .keypress(function(event) {
                        if (event.which == 13) {
                            LookupItem();
                        }
                    });
                ctlQty.keypress(function (event) {
                    if (event.which == 13) {
                        AddItem();
                    }
                });
                $("#btnAddItem").click(function () { AddItem(); });
                //$("#btnSave").show().click(SavePO);
                $("#addItems").show();
                $(".dates").prop("readonly", true).datepicker({
                    showOn: "button",
                    buttonImage: "../images/calendar.gif",
                    buttonImageOnly: true
                });
                $("#txtStore")
                    .keypress(function (evt, ui) {
                        if (evt.which === 27) {
                            $("#txtStore").val("");
                            $("#lblStoreName").text("");
                        }
                    })
                    .autocomplete({
                        minLength: 1,
                        source: function (request, response) {
                            var matcher = new RegExp($.ui.autocomplete.escapeRegex(request.term), "i");
                            response($.grep(StoreList, function (value) {
                                value = value.value || value.desc || value.icon;
                                return matcher.test(value);
                            }));
                        },
                        select: function (event, ui) {
                            $("#txtStore").val(ui.item.value);
                            $("#lblStoreName").text(ui.item.label);
                            return false;
                        },
                        change: function (event, ui) {
                            if ($("#txtStore").val().length === 0) {
                                $("#lblStoreName").text("");
                            }
                        }
                    })
                    .data("ui-autocomplete")._renderItem = function (ul, item) {
                        return $("<li>")
                          .append("<a>" + item.value + " - " + item.label + "</a>")
                          .appendTo(ul);
                    };
            }
            else {
                $("#btnSave").hide();
                $("#btnResetCancel").hide();
                $("#btnRequestApproval").hide();
                $("input[type='text']").prop("readonly", true);
                $(".rotb").removeClass("rotb");
                $("#chkPu_order_fl").prop("disabled", true);
                $(".req").hide();
                $(".content").width($(".content").width() - 65);
            }

            columns = [
                { id: "ACTION", name: "", field: "ACTION", width: 30, sortable: false, cssClass: "gridColC", focusable: false, formatter: ActionFormatter },
                { id: "COD_LINE", name: "Line", field: "COD_LINE", width: 58, minWidth: 50, sortable: false, focusable: false, headerCssClass: "gridColC", cssClass: "gridColC" },
                { id: "ITEM", name: "Item", field: "ITEM", width: 85, sortable: false, focusable: false, headerCssClass: "gridColC", cssClass: "gridColC" },
                { id: "ITEM_DESC", name: "Description", field: "ITEM_DESC", width: 415, sortable: false, focusable: false },
                { id: "COD_QTY", name: "Qty", field: "COD_QTY", width: 60, sortable: false, headerCssClass: "gridColC", cssClass: "gridColC", editor: IntegerEditor }
            ];

            if (PageInfo.Mode === "V") {
                columns[3].width += columns[0].width;
                columns.splice(0, 1);
            }

            gridOptions = {
                enableCellNavigation: true,
                editable: (PageInfo.Mode !== "V"),
                asyncEditorLoading: true,
                asyncEditorLoadDelay: 0,
                rowHeight: 18
            };

            DV = new Slick.Data.DataView();

            grid = new Slick.Grid("#ISTLines", DV, columns, gridOptions);

            // wire up model events to drive the grid
            DV.onRowCountChanged.subscribe(function (e, args) {
                grid.updateRowCount();
                grid.render();
                ctlRowCnt.innerText = DV.getLength();
            });

            DV.onRowsChanged.subscribe(function (e, args) {
                grid.invalidateRows(args.rows);
                grid.render();
            });

            grid.onCellChange.subscribe(function(e, args) {
                CalcTotalQty();
            });

            grid.onSort.subscribe(function (e, args) {
                sortdir = args.sortAsc ? 1 : -1;
                sortcol = args.sortCol.field;

                if (msie && msie <= 8) {
                    // using temporary Object.prototype.toString override
                    // more limited and does lexicographic sort only by default, but can be much faster

                    var percentCompleteValueFn = function () {
                        var val = this["percentComplete"];
                        if (val < 10) {
                            return "00" + val;
                        } else if (val < 100) {
                            return "0" + val;
                        } else {
                            return val;
                        }
                    };

                    // use numeric sort of % and lexicographic for everything else
                    DV.fastSort((sortcol == "percentComplete") ? percentCompleteValueFn : sortcol, args.sortAsc);
                }
                else {
                    // using native sort with comparer
                    // preferred method but can be very slow in IE with huge datasets
                    DV.sort(comparer, args.sortAsc);
                }
            });

            grid.onClick.subscribe(function (e, args) {
                el = e.srcElement ? e.srcElement : e.target;

                var data = this.getDataItem(args.row);
                var colId = this.getColumns()[args.cell].id;
                var fn = el.getAttribute("function");
                var del = null;

                if (colId === "ACTION" && fn && fn === "D") {
                    DeleteItem($.extend({}, data));
                }
            });

            grid.onValidationError.subscribe(function (e, args) {
                alert(args.validationResults.msg);
            });

            if (window.dialogArguments.co_odno.length > 0) {
                GetISTFromDB(window.dialogArguments.co_odno);
            }


        });

        function ActionFormatter(row, cell, value, columnDef, dataContext) {
            return (PageInfo.Mode !== "V" && parseInt(dataContext.COD_QTY,10) !== 0 ? "<span class='delete' function='D'>&nbsp;</span>" : "");
        }

        function ValidationError() {
            debugger;
        }

        function LookupItem(e) {
            var item = ctlItem.val().replace(/ /g, "");

            if (item.length === 0 || isNaN(item)) {
                ctlItem.addClass("text-error");
                alert("Please enter a number.");
                return;
            }

            lcboWaitMsg.show();
            ctlItem.removeClass("text-error");

            $.ajax({
                type: "POST",
                url: "IST.aspx/LookupItem",
                data: "{'item':'" + item + "'}",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (result) { LookupItemComplete(result, true); },
                error: function (result) { LookupItemComplete(result, false); }
            });
        }

        function LookupItemComplete(result, success) {
            lcboWaitMsg.hide();

            if (success && result.IsSuccessful) {
                ctlItemDesc.text(result.ReturnedObject[0].IM_DESC);
                ctlAvailQty.text(result.ReturnedObject[0].UNCOMMITTED_QTY);
                ctlQty.focus();
                return;
            }
            alert("Item not found.");
            ctlItem.val("");
            ctlItemDesc.text("");
        }

        function AddItemToGrid() {
            var item = {
                    "id": -1,
                    "CO_ODNO": 0,
                    "COD_LINE": null,
                    "ITEM": ctlItem.val().replace(/ /g, ""),
                    "ITEM_DESC": ctlItemDesc.text(),
                    "COD_QTY": ctlQty.val().replace(/ /g, ""),
                    "ORIG_QTY": 0,
                    "dirty": true
                },
                msg = new Array(),
                items = DV.getItems(),
                maxCodLine = 0;

            //get new COD_LINE val
            //for (var x = 0, l = items.length; x < l; x++) {
            //    if (parseInt(items[x].COD_LINE, 10) > maxCodLine) {
            //        maxCodLine = parseInt(items[x].COD_LINE, 10);
            //    }
            //}
            //if (maxCodLine < maxLineFromDB) {
            //    maxCodLine = maxLineFromDB;
            //}

            item.COD_LINE = (maxLineFromDB += 1);//maxCodLine + 1;
            item.id = item.COD_LINE;
            item.ITEM = parseFloat(item.ITEM);
            item.COD_QTY = parseInt(item.COD_QTY, 10);

            DV.beginUpdate();
            DV.addItem(item);
            DV.endUpdate();

            ctlItem
                .val("")
                .focus();
            ctlItemDesc.text("");
            ctlQty.val("");
            ctlAvailQty.text("0");
            CalcTotalQty();
        }

        function CalcTotalQty() {
            var items,
                total = 0;

            items = DV.getItems();
            for (var x = 0, l = items.length; x < l; x++) {
                total += parseInt(items[x].COD_QTY, 10);
            }
            ctlQtyTotal.text(total);
        }

        function IsFormDirty() {
            var Ist = {
                    to_store : $("#txtStore").val(),
                    order_reqd_dt : $("#txtRequiredDate").val(),
                    comments1 : $.trim($("#txtComment1").val()),
                    comments2: $.trim($("#txtComment2").val()),
                    pu_order_fl: ($("#chkPu_order_fl").prop("checked") === true ? "Y" : "N")
            };

            return Ist.to_store !== $("#txtStore").data("val") ||
                Ist.order_reqd_dt !== $("#txtRequiredDate").data("val") ||
                Ist.comments1 !== $("#txtComment1").data("val") ||
                Ist.comments2 !== $("#txtComment2").data("val") ||
                Ist.pu_order_fl !== $("#chkPu_order_fl").data("val");
        }

        function CloseWin() {
            var msg = "You have unsaved changes. Click [OK] to lose changes, or click [CANCEL] to remain on the screen.",
                err = false;

            if (PageInfo.Mode !== "V" && (IsFormDirty())) {
                if (!confirm("You have unsaved changes. Click [OK] to lose your changes, or click [Cancel] to continue editing.")) {
                    return;
                }
            }
            window.returnValue = JSON.stringify({'reloadList': reloadList, 'co_odno': $("#txtISTNumber").val()});
            window.close();
        }

        function GetISTFromDB(co_odno) {

            DV.beginUpdate();
            DV.setItems([]);
            DV.endUpdate();

            lcboWaitMsg.show();

            $.ajax({
                type: "POST",
                url: "IST.aspx/GetIST",
                data: "{'co_odno':'" + co_odno + "'}",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (result) { GetISTFromDBComplete(result, true); },
                error: function (result) { GetISTFromDBComplete(result, false); }
            });
        }

        function GetISTFromDBComplete(result, success) {
            var ist;
            lcboWaitMsg.hide();

            if (success) {
                if (result.IsSuccessful) {
                    ist = result.ReturnedObject;
                    SetFormFields(ist);
                    //debugger;
                    //remove 'deleted' lines
                    //so the user doesn't see them
                    if (ist.LINES.length > 0) {
                        maxLineFromDB = parseInt(ist.LINES[ist.LINES.length - 1].COD_LINE, 10);
                    }
                    for (var i = ist.LINES.length - 1; i >= 0; i--) {
                        if (ist.LINES[i].DELETED_FL === "R") {
                            ist.LINES.splice(i, 1);
                        }
                    }

                    DV.beginUpdate();
                    DV.setItems($.extend([], ist.LINES));
                    DV.endUpdate();

                    CalcTotalQty();
                }
                else {
                    alert("Error: " + result.ResponseCode + (result.Details ? " - " + result.Details : ""));
                }
                return;
            }
            alert("There was a problem retrieving the IST order.");
        }

        function SetFormFields(ist) {
            $("#txtISTNumber").val($.trim(ist.CO_ODNO)).data("val", $.trim(ist.CO_ODNO));
            $("#txtStore").val($.trim(ist.TO_STORE)).data("val", $.trim(ist.TO_STORE));
            $("#txtOrderDate").val(ist.ORDER_DT_TM.substring(0, 10)).data("val", ist.ORDER_DT_TM.substring(0, 10));
            $("#txtRequiredDate").val(ist.ORDER_REQD_DT.substring(0, 10)).data("val", ist.ORDER_REQD_DT.substring(0, 10));
            $("#txtStatus").val(ist.STATUS).data("val", ist.STATUS);
            $("#chkPu_order_fl").prop("checked", (ist.PU_ORDER_FL === "Y")).data("val", ist.PU_ORDER_FL);
            $("#txtComment1").val($.trim(ist.COMMENTS_LINE_1)).data("val", $.trim(ist.COMMENTS_LINE_1));
            $("#txtComment2").val($.trim(ist.COMMENTS_LINE_2)).data("val", $.trim(ist.COMMENTS_LINE_2));
        }

        function GetISTToSave(doValidation) {
            var Ist = $.extend({}, true, IstTemplate),
                items = DV.getItems(),
                msgs = [],
                storeExists = false;

            Ist.co_odno = $.trim($("#txtISTNumber").val());
            Ist.to_store = $("#txtStore").val().toUpperCase();
            Ist.order_reqd_dt = $("#txtRequiredDate").val();
            Ist.comments1 = $.trim($("#txtComment1").val());
            Ist.comments2 = $.trim($("#txtComment2").val());
            Ist.status = $.trim($("#txtStatus").val());
            Ist.pu_order_fl = ($("#chkPu_order_fl").prop("checked") === true ? "Y" : "N");
            Ist.submittedBy = UserInfo.UserID;

            for (var x = 0, l = StoreList.length; x < l; x++) {
                if (StoreList[x].value === Ist.to_store) {
                    storeExists = true;
                    break;
                }
            }
            if (!storeExists) {
                msgs.push("Please enter a valid Store #.");
            }

            if (Ist.order_reqd_dt.length === 0) {
                msgs.push("Please enter a required by date.");
            }

            for (var x = 0, l = items.length; x < l; x++) {
                Ist.items.push({ "COD_LINE": items[x].COD_LINE, "ITEM": items[x].ITEM, "COD_QTY": items[x].COD_QTY, "ORIG_QTY": items[x].ORIG_QTY });
                if (items[x].ITEM.length == 0 || isNaN(items[x].COD_QTY)) {
                    msgs.push("Line " + items[x].COD_LINE + " has an invalid quantity.");
                }
            }

            if (doValidation && msgs.length > 0) {
                alert(msgs.join("\n"));
                return null;
            }

            return Ist;
        }

        function CreateIST() {
            var Ist = GetISTToSave(),
                returnObj = null;

            if (!Ist) {
                return;
            }

            Ist.comments1 = Ist.comments1.replace(/\'/g, "%27").replace(/\"/g, "%22").replace(/\\/g, "\\\\");
            Ist.comments2 = Ist.comments2.replace(/\'/g, "%27").replace(/\"/g, "%22").replace(/\\/g, "\\\\");

            $.ajax({
                type: "POST",
                async: false,
                url: "IST.aspx/CreateIST",
                data: "{'ist':'" + JSON.stringify(Ist) + "'}",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (result) { returnObj = CreateISTComplete(result, true); },
                error: function (result) { returnObj = CreateISTComplete(result, false); }
            });
            lcboWaitMsg.hide();
            if (!returnObj) {
                return false;
            }
            else {
                reloadList = true;
                SetFormFields(returnObj);
                return true;
            }
        }

        function CreateISTComplete(result, success) {
            var msg = "There was a problem creating the IST.";

            if (success) {
                if (result.IsSuccessful) {
                    return $.extend({}, true, result.ReturnedObject);
                }
                else {
                    msg = "There was a problem creating the IST.\n\nERROR: " + result.ResponseCode + " - " + result.Details;
                }
            }
            alert(msg);
            return null;
        }

        function UpdateIST() {
            var Ist = GetISTToSave();
            
            if (!Ist) {
                return;
            }

            if (Ist.co_odno.length === 0) {
                CreateIST();
            }
            else {
                lcboWaitMsg.show();

                Ist.comments1 = Ist.comments1.replace(/\'/g, "%27").replace(/\"/g, "%22").replace(/\\/g, "\\\\");
                Ist.comments2 = Ist.comments2.replace(/\'/g, "%27").replace(/\"/g, "%22").replace(/\\/g, "\\\\");

                $.ajax({
                    type: "POST",
                    url: "IST.aspx/UpdateIST",
                    data: "{'ist': '" + JSON.stringify(Ist) + "'}",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (result) { UpdateISTComplete(result, true); },
                    error: function (result) { UpdateISTComplete(result, false); }
                });
            }
        }

        function UpdateISTComplete(result, success) {

            lcboWaitMsg.hide();

            if (success) {
                if (result.IsSuccessful) {
                    reloadList = true;
                    SetFormFields(result.ReturnedObject);
                    alert("The IST was saved.");
                }
                else {
                    alert("There was a problem updating the IST.\n\nError: " + result.ResponseCode + " - " + result.Details);
                }
                return;
            }
            alert("There was a problem updating the IST.");
        }

        function ResetIST() {
            var Ist = GetISTToSave();

            if (!Ist) {
                return;
            }

            lcboWaitMsg.show();
            $.ajax({
                type: "POST",
                url: "IST.aspx/ResetIST",
                data: "{'ist':'" + JSON.stringify(Ist) + "'}",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (result) { ResetISTComplete(result, true); },
                error: function (result) { ResetISTComplete(result, false); }
            });
        }

        function ResetISTComplete(result, success) {
            lcboWaitMsg.hide();

            if (success) {
                if (result.IsSuccessful) {
                    reloadList = true;
                    alert("The IST has been reset.");
                    GetISTFromDB($("#txtISTNumber").val());
                }
                else {
                    alert("There was a problem resetting the IST.\n\nERROR: " + result.ResponseCode + " - " + result.Details);
                }
                return;
            }
            alert("There was a problem resetting the IST.");
        }

        function AddItem(insufficientQtyOverride) {
            var item = {
                    "co_odno": $("#txtISTNumber").val(),
                    "item": ctlItem.val().replace(/ /g, ""),
                    "qty": ctlQty.val().replace(/ /g, ""),
                    "submittedBy": UserInfo.UserID,
                    "insufficientQtyOverride": ((typeof insufficientQtyOverride == "undefined" || insufficientQtyOverride.length == 0) ? "N" : insufficientQtyOverride)
                },
                msg = new Array();

            if (item.item.length === 0 || isNaN(item.item) || parseFloat(item.item) == 0) {
                msg.push("Please enter a valid item #.");
            }
            if (item.qty.length === 0 || isNaN(item.qty) || parseInt(item.qty, 10) == 0) {
                msg.push("Please enter a valid quantity.");
            }

            if (msg.length > 0) {
                alert(msg.join("\n"));
                return;
            }
            else {
                item.item = parseFloat(item.item);
                item.qty = parseInt(item.qty, 10);
            }

            if (item.co_odno.length == 0 && !CreateIST()) {
                return;
            }
            else {
                item.co_odno = $("#txtISTNumber").val();
            }

            lcboWaitMsg.show();
            $.ajax({
                type: "POST",
                url: "IST.aspx/AddItem",
                data: JSON.stringify(item),
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (result) { AddItemComplete(result, true); },
                error: function (result) { AddItemComplete(result, false); }
            });
        }

        function AddItemComplete(result, success) {
            lcboWaitMsg.hide();

            if (success) {
                if (result.IsSuccessful) {
                    reloadList = true;
                    SetFormFields(result.ReturnedObject);
                    AddItemToGrid();
                }
                else if (result.ResponseCode === 1004) {
                    if (window.confirm("Item " + ctlItem.val().replace(/ /g, "") + " has insufficient stock. Click OK to over-commit.")) {
                        AddItem("Y");
                    }
                }
                else {
                    alert("Error: " + result.ResponseCode + " - " + result.Details);
                }
                return;
            }

            alert("There was a problem adding the item, please try again.");
        }

        function UpdateItem(row, newVal, isDelete, insufficientQtyOverride) {
            var item = {
                    "co_odno": $("#txtISTNumber").val(),
                    "cod_line": row.COD_LINE,
                    "item": row.ITEM,
                    "qty": newVal,
                    "origQty": row.ORIG_QTY,
                    "submittedBy": UserInfo.UserID,
                    "insufficientQtyOverride": ((typeof insufficientQtyOverride == "undefined" || insufficientQtyOverride.length == 0) ? "N" : insufficientQtyOverride)
                },
                msg = new Array(),
                retObj = null;

            if (item.item.length === 0 || isNaN(item.item) || parseFloat(item.item) == 0) {
                msg.push("Please enter a valid item #.");
            }
            if (item.qty.length === 0 || isNaN(item.qty) || (!isDelete && parseInt(item.qty, 10) == 0)) {
                msg.push("Please enter a valid quantity.");
            }

            if (msg.length > 0) {
                alert(msg.join("\n"));
                return;
            }
            else {
                item.item = parseFloat(item.item);
                item.qty = parseInt(item.qty, 10);
                item.origQty = parseInt(item.origQty, 10);
            }

            lcboWaitMsg.show();
            $.ajax({
                type: "POST",
                async: false,
                url: "IST.aspx/UpdateItem",
                data: JSON.stringify(item),
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (result) { retObj = UpdateItemComplete(result, true, $.extend({}, row), newVal); },
                error: function (result) { retObj = UpdateItemComplete(result, false); }
            });
            return retObj;
        }

        function UpdateItemComplete(result, success, row, newVal, isDelete) {
            var retObj = {
                valid: false,
                msg: null
            };
            lcboWaitMsg.hide();

            if (success) {
                if (result.IsSuccessful) {
                    reloadList = true;
                    SetFormFields(result.ReturnedObject);
                    retObj.valid = true;
                }
                else if (result.ResponseCode === 1004) {
                    if (window.confirm("Item " + row.ITEM.replace(/ /gi, "") + " has insufficient stock. Click OK to over-commit.")) {
                        return UpdateItem(row, newVal, false, "Y");
                    }
                }
                else {
                    retObj.msg = "Error: " + result.ResponseCode + " - " + result.Details;
                }
            }
            else {
                retObj.msg = "There was a problem updating the item, please try again.";
            }

            return retObj;
        }

        function IntegerEditor(args) {
            var $input;
            var defaultValue;
            var scope = this;

            this.init = function () {
                $input = $("<INPUT type=text class='editor-text' />");

                $input.bind("keydown.nav", function (e) {
                    if (e.keyCode === $.ui.keyCode.LEFT || e.keyCode === $.ui.keyCode.RIGHT) {
                        e.stopImmediatePropagation();
                    }
                });

                $input.appendTo(args.container);
                $input.focus().select();
            };

            this.destroy = function () {
                $input.remove();
            };

            this.focus = function () {
                $input.focus();
            };

            this.loadValue = function (item) {
                defaultValue = item[args.column.field];
                $input.val(defaultValue);
                $input[0].defaultValue = defaultValue;
                $input.select();
            };

            this.serializeValue = function () {
                return parseInt($input.val(), 10) || 0;
            };

            this.applyValue = function (item, state) {
                item[args.column.field] = state;
                if (this.isValueChanged) {
                    item.dirty = true;
                }
            };

            this.isValueChanged = function () {
                return (!($input.val() == "" && defaultValue == null)) && ($input.val() != defaultValue);
            };

            this.validate = function () {
                if (isNaN($input.val())) {
                    return {
                        valid: false,
                        msg: "Please enter a valid integer"
                    };
                }
                var updItem = UpdateItem(args.item, $input.val());

                if (typeof updItem == "undefined") {
                    return {
                        valid: false,
                        msg: "There was a problem updating the quantity."
                    };
                }
                return updItem;
            };

            this.init();
        }

        function DeleteItem(item) {
            if (confirm("Are you sure you want to remove this item?")) {
                del = UpdateItem(item, 0, true);
                if (del.valid) {
                    var item = DV.getItemById(item.id);
                    item.COD_QTY = 0;
                    DV.beginUpdate();
                    DV.updateItem(item.id, item);
                    DV.endUpdate();
                    CalcTotalQty();
                    DV.deleteItem(item.id);
                }
            }
        }

        function RequestApproal() {
            var Ist = GetISTToSave(),
                msg = "Are you sure you want make the request to approve order: " + Ist.co_odno + "? Once sent for approval, the order will no longer be editable.";

            if (Ist.co_odno.length === 0 || $.trim(Ist.status) !== "SAVED" || IsFormDirty()) {
                alert("You cannot submit this order, please save the order before submitting.");
                return;
            }

            if (!IsApprovalRequired) {
                msg = "Are you sure you want to approve order: " + Ist.co_odno + "? Once approved, order will be sent to ROC.";
            }

            if(!confirm(msg)) {
                return;
            }

            lcboWaitMsg.show();
            $.ajax({
                type: "POST",
                url: "IST.aspx/ISTRequestApproval",
                data: "{'co_odno': '" + Ist.co_odno + "', 'submittedBy': '" + UserInfo.UserID + "'}",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (result) { RequestApproalComplete(result, true); },
                error: function (result) { RequestApproalComplete(result, false); }
            });
        }

        function RequestApproalComplete(result, success) {
            var msg = "The IST has been submitted for approval.";

            lcboWaitMsg.hide();

            if (!IsApprovalRequired) {
                msg = "The IST has been sent to ROC.";
            }

            if (success) {
                if (result.IsSuccessful) {
                    reloadList = true;
                    SetFormFields(result.ReturnedObject);
                    alert(msg);
                    CloseWin();
                }
                else {
                    alert("There was a problem submitting the IST.\n\nERROR: " + result.ResponseCode + " - " + result.Details);
                }
                return;
            }
            alert("There was a problem submitting the IST.");
        }

    </script>
</head>
<body>
    <form id="form1" runat="server">
    <div id="hdr"><span>941 IST</span></div>
    <div class="main">
        <div class="content">
            <div><span class="lbl">IST #:</span><input type="text" id="txtISTNumber" readonly="readonly" class="rotb" style="width: 70px;" /><span class="lbl" style="margin-left: 25px;">Order Date:</span><input type="text" id="txtOrderDate" readonly="readonly" class="rotb" style="width: 70px;" /></div>
            <div><span class="lbl">Store #:</span><input type="text" id="txtStore" style="width: 70px;" maxlength="5" /><span class="req">*</span>&nbsp;<span id="lblStoreName"></span></div>
            <div><span class="lbl">Required Date:</span><input type="text" id="txtRequiredDate" class="dates" readonly="readonly" style="width: 70px;" /><span class="req">*</span></div>
            <div><span class="lbl">Status:</span><input type="text" id="txtStatus" readonly="readonly" class="rotb" /></div>
            <div><span class="lbl">Pickup:</span><input type="checkbox" id="chkPu_order_fl" /></div>
            <div><span class="lbl">Comments:</span><input type="text" id="txtComment1" style="width:260px;" maxlength="30" /></div>
            <div><span class="lbl"></span><input type="text" id="txtComment2" style="width:260px;" maxlength="30" /></div>
            <div>
                <div style="text-align: right; width: 648px; font-weight:bold;"># of items:&nbsp;<span id="numItems">0</span></div>
                <div id="ISTLines" class="grids" style="height: 330px; width: 665px;"></div>
                <div style="text-align:right; width: 655px;" background-color: green;">
                    <span class="lbl">Total Quantity:</span>
                    <span class="lbl" id="lblQtyTotal" style="width: 60px; text-align: center; border-left: 1px solid black; border-right: 1px solid black; border-bottom: 1px solid black; padding: 3px 0px 3px 0px;">0</span>
                </div>
                <div id="addItems" style="border: 1px solid black; margin-top: 5px; display: none;">
                    <span class="lbl" style="width: 78px;">Add Item:</span>
                    <input type="text" id="txtItem" style="width: 75px;" />
                    <span id="itemDesc" style="display: inline-block; width: 251px;"></span>
                    <span class="lbl" style="width: 145px; text-align: right; padding-right: 7px;">Available Qty:&nbsp;<span id="lblAvailQty">0</span></span>
                    <input type="text" id="txtQty" style="width: 55px; margin-right: 12px;" />
                    <input type="button" id="btnAddItem" value="Add" />
                </div>
            </div>
            <div style="text-align: center; margin-top: 25px;">
                <input type="button" id="btnSave" value="Save" class="btn" style="margin-right: 10px;" />
                <input type="button" id="btnResetCancel" value="Reset" class="btn" style="margin-right: 10px; display: none;" />
                <input type="button" id="btnRequestApproval" value="Submit" class="btn" style="margin-right: 10px;" />
                <input type="button" id="btnClose" value="Close" class="btn" />
            </div>
        </div>
    </div>
    </form>
</body>
</html>
