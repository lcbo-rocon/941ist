﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="ISTList.aspx.vb" Inherits="_941IST.ISTList" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>941 IST Administration</title>

    <link href="../css/Standard.css" rel="stylesheet" />
    <link href="../css/smoothness/jquery-ui-1.10.3.custom.css" rel="stylesheet" />
    <link href="../css/SlickGrid/slick.grid.css" rel="stylesheet" />
    <link href="../js/AutoCompleteDDL/AutoCompleteDDL.css" rel="stylesheet" />
    <link href="../js/Wait/lcboMessage.css" rel="stylesheet" />

    <style type="text/css">

        .content
        {
            width: 872px;
        }
        legend
        {
            font-weight: bold;
        }
        .gridCount
        {
            text-align: right;
            font-weight: bold;
        }
        .gridBtn
        {
            display: inline-block; 
            width: 16px; 
            background-repeat:no-repeat;
            background-position:center;
        }
        .gridBtnEdit
        {
            cursor: pointer; 
            background-image:url('../images/edit_16x16.gif'); 
        }
        .gridBtnDelete
        {
            cursor: pointer; 
            background-image:url('../images/editdelete_16x16.gif'); 
        }
        .gridBtnApprove
        {
            cursor: pointer; 
            background-image:url('../images/check_16x16.png'); 
        }
        .gridBtnRecall
        {
            cursor: pointer; 
            background-image:url('../images/recallOrder_16x16.gif'); 
        }
        .view
        {
            cursor: pointer; 
            text-decoration: underline;
            color: blue;
        }
        .lbl
        {
            display: inline-block;
            width: 135px;
            text-align: right;
            padding-right: 5px;
            font-weight: bold;
        }
        .tallHeader .slick-header,.tallHeader  .slick-header-columns,.tallHeader  .slick-header-column.ui-state-default
        {
        	height: 43px;
        }
        .tallHeaderCssClass {text-align: center; white-space: normal;}
        .gridColC {text-align: center;}
        .gridColR {text-align: right;}
        input[type='button']
        {
            width: 120px;
        }
        .oldSave
        {
            color: red!important;
        }
        .oldAuth
        {
            color: red!important;
        }
    </style>

    <script src="../js/jquery-1.9.1.min.js"></script>
    <script src="../js/jquery-ui-1.10.3.custom.min.js"></script>
    <script src="../js/jquery.event.drag-2.2.js"></script>
    <script src="../js/jquery.event.drop-2.2.js"></script>
    <script src="../js/SlickGrid/slick.core.js"></script>
    <script src="../js/SlickGrid/slick.grid.js"></script>
    <script src="../js/SlickGrid/slick.dataview.js"></script>
    <script src="../js/Wait/lcboWait.js"></script>

    <script type="text/javascript">

        var data = [],
            DV = null,
            grid = null,
            gridOptions = null,
            columns = null,
            sortdir = "",
            sortcol = "",
            filters = {
                co_odno: null,
                to_store: null,
                dateFrom: null,
                dateTo: null,
                authBy: null,
                subBy: null,
                status: null,
                hideCancelled: true
            },
            ctlRowCnt = null,
            allowEdit = {
                "SAVED": "",
                "NEW": "",
                "SAVED-EDIT": "",
                "NEW-EDIT": ""
            },
            allowDelete = {
                "NEW": "",
                "SAVED": ""
            },
            allowRecall = {
                "SUBMITTED": ""
            },
            sortdir = null,
            sortcol = null,
            reportWin;

        var msie = (function () {

            var undef,
                v = 3,
                div = document.createElement('div'),
                all = div.getElementsByTagName('i');

            while (
                div.innerHTML = '<!--[if gt IE ' + (++v) + ']><i></i><![endif]-->',
                all[0]
            );

            return v > 4 ? v : undef;

        }());

        //create JSON.stringify function
        var JSON = JSON || {};
        // implement JSON.stringify serialization
        JSON.stringify = JSON.stringify || function (obj) {

            var t = typeof (obj);
            if (t != "object" || obj === null) {

                // simple data type
                if (t == "string") obj = '"' + obj + '"';
                return String(obj);

            }
            else {

                // recurse array or object
                var n, v, json = [], arr = (obj && obj.constructor == Array);

                for (n in obj) {
                    v = obj[n]; t = typeof (v);

                    if (t == "string") v = '"' + v + '"';
                    else if (t == "object" && v !== null) v = JSON.stringify(v);

                    json.push((arr ? "" : '"' + n + '":') + String(v));
                }

                return (arr ? "[" : "{") + String(json) + (arr ? "]" : "}");
            }
        };

        var comparers = {
            comparer: function (a, b) {
                var x = a[sortcol], y = b[sortcol];
                return (x == y ? 0 : (x > y ? 1 : -1));
            },

            comparerNumeric: function (a, b) {
                var x = parseFloat(a[sortcol]), y = parseFloat(b[sortcol]);
                var retVal;
                if (isNaN(x)) {
                    x = null;
                }
                if (isNaN(y)) {
                    y = null;
                }
                if (!x && !y) {
                    retVal = 0;
                }
                else if (!x && y > 0) {
                    retVal = -1;
                }
                else if (!y && x < 0) {
                    retVal = 1;
                }
                else {
                    retVal = (x == y ? 0 : (x > y ? 1 : -1));
                }

                return retVal;
            }
        };

        $.ajaxSetup({
            type: "POST",
            contentType: "application/json; charset=utf-8",
            data: "{}",
            dataFilter: function (data) {
                var msg;

                if (typeof (JSON) !== 'undefined' && typeof (JSON.parse) === 'function') {
                    msg = JSON.parse(data);
                }
                else {
                    msg = eval('(' + data + ')');
                }
                if (msg.hasOwnProperty('d')) {
                    return msg.d;
                }
                else {
                    return msg;
                }
            },
            beforeSend: function () {
                lcboWaitMsg.show();
            }
        });

        $(function () {

            $("#btnAddNew").click(function () {
                openIST("", "A", null);
            });
            $("#btnReport").click(openReport);
            $("#btnClose").click(closeWin);
            ctlRowCnt = document.getElementById("ISTCount");
            $("#ddlStatus").change(txtFilter);
            $("#chkExcludeCancelled").click(checkboxFilter);
            $(".txtFilter").keyup(function (e) {

                // clear on Esc
                if (e.which == 27) {
                    this.value = "";
                }

                txtFilter(e);
            });
            $("#txtDateFrom,#txtDateTo")
                .datepicker({
                    onSelect: dateFilter,
                    showOn: "button",
                    buttonImage: "../images/calendar.gif",
                    buttonImageOnly: true
                })
                .keydown(function (e) {
                    var that = this;
                    //alert('down');
                    if (e.which == 27 || e.which == 46) {
                        this.value = "";
                        setTimeout(function () { dateFilter("", that); }, 0);
                    }
                    else if (e.which == 9) {
                        return true;
                    }
                    e.stopPropagation();
                    e.preventDefault();
                    return false;
                });

            gridOptions = {
                enableCellNavigation: true,
                editable: false,
                rowHeight: 18
            };

            columns = [
                { id: "ACTION", name: "Action", field: "ACTION", width: 75, sortable: false, cssClass: "gridColC", headerCssClass: "tallHeaderCssClass", formatter: ActionFormatter },
                { id: "CO_ODNO", name: "Order #", field: "CO_ODNO", width: 80, minWidth: 50, cssClass: "view gridColC", headerCssClass: "tallHeaderCssClass", sortable: true, sortNumeric: true, formatter: OrderFormatter },
                { id: "TO_STORE", name: "Store #", field: "TO_STORE", width: 60, cssClass: "gridColC", headerCssClass: "tallHeaderCssClass", sortable: true },
                { id: "ORDER_DT_TM", name: "Order Date", field: "ORDER_DT_TM", width: 95, cssClass: "gridColC", headerCssClass: "tallHeaderCssClass", sortable: true, sortNumeric: true, formatter: DateFormatter },
                /*{ id: "IST_TYPE", name: "IST Type", field: "IST_TYPE", width: 95, cssClass: "gridColC", headerCssClass: "tallHeaderCssClass", sortable: true },
                */
                { id: "NUM_OF_PROD", name: "# Products", field: "NUM_OF_PROD", width: 75, cssClass: "gridColC", headerCssClass: "tallHeaderCssClass", sortable: true, sortNumeric: true },
                { id: "NUM_OF_UNITS", name: "# Units", field: "NUM_OF_UNITS", width: 75, cssClass: "gridColC", headerCssClass: "tallHeaderCssClass", sortable: true, sortNumeric: true },
                { id: "SUBMITTED_BY", name: "Submitted By", field: "SUBMITTED_BY", width: 90, cssClass: "gridColC", headerCssClass: "tallHeaderCssClass", sortable: true },
                { id: "LAST_UPDATE_BY", name: "Last Update By", field: "LAST_UPDATE_BY", width: 100, cssClass: "gridColC", headerCssClass: "tallHeaderCssClass", sortable: true },
                /*
                { id: "REQUEST_AUTH_USER", name: "Request Auth By", field: "REQUEST_AUTH_USER", width: 70, headerCssClass: "tallHeaderCssClass", sortable: true },
                { id: "AUTHORIZED_BY", name: "Authorized By", field: "AUTHORIZED_BY", width: 70, headerCssClass: "tallHeaderCssClass", sortable: true },
                */
                { id: "STATUS", name: "Status", field: "STATUS", width: 145, headerCssClass: "tallHeaderCssClass", sortable: true },
                { id: "PU_ORDER_FL", name: "Pickup", field: "PU_ORDER_FL", width: 60, headerCssClass: "tallHeaderCssClass", cssClass: "gridColC", sortable: true }
            ];

            DV = new Slick.Data.DataView();
            DV.setFilterArgs(filters);
            DV.setFilter(myFilter);

            grid = new Slick.Grid("#ISTList", DV, columns, gridOptions);

            // wire up model events to drive the grid
            DV.onRowCountChanged.subscribe(function (e, args) {
                grid.updateRowCount();
                grid.render();
                ctlRowCnt.innerText = DV.getLength();
            });

            DV.onRowsChanged.subscribe(function (e, args) {
                grid.invalidateRows(args.rows);
                grid.render();
            });

            grid.onSort.subscribe(function (e, args) {
                sortdir = args.sortAsc ? 1 : -1;
                sortcol = args.sortCol.field;

                if (args.sortCol.sortNumeric) {
                    DV.sort(comparers.comparerNumeric, args.sortAsc);
                }
                else {
                    DV.sort(comparers.comparer, args.sortAsc);
                }

            });

            grid.onClick.subscribe(function (e, args) {
                el = e.srcElement ? e.srcElement : e.target;

                var data = this.getDataItem(args.row);
                var colId = this.getColumns()[args.cell].id;
                var mode = ((IsUser || IsApprover) && colId === "ACTION" ? "E" : colId === "CO_ODNO" ? "V" : "");
                var fn = (mode == "E" ? el.getAttribute("fn") : "");

                if (data && mode) {
                    if (fn == "C") {
                        CancelIST(data.CO_ODNO);
                    }
                    else if (fn == "A") {
                        ISTApprove(data.CO_ODNO);
                    }
                    else if (fn == "R") {
                        RecallIST(data.CO_ODNO);
                    }
                    else if (fn == "E" || mode == "V") {
                        openIST(data.CO_ODNO, mode, data.id);
                    }
                }
            });

            //DV.getItemMetadata = metadata;

            getISTList();

            //ctlTest = document.getElementById("test");
        });

        /*
        function metadata(row) {
            var itm = this.getItem(row);
            //ctlTest.innerHTML += itm.SAVED_OLD.toString() + " " + itm.REQ_AUTH_OLD.toString() + "<br/>";
            if (itm.SAVED_OLD || itm.REQ_AUTH_OLD) {
                return {
                    "cssClasses": "oldISTAlert"
                };
            }
            else if (row % 2 == 0) {
                return {
                    "cssClasses": "altRow"
                };
            }
        }
        */

        function ActionFormatter(row, cell, value, columnDef, dataContext) {
            var btns = [];

            btns.push((IsUser && typeof allowEdit[dataContext.STATUS] !== "undefined" ? "<span class='gridBtn gridBtnEdit' fn='E' title='Edit'>&nbsp;</span>" : "<span class='gridBtn'>&nbsp;</span>"));
            
            if (typeof allowRecall[dataContext.STATUS] !== "undefined" && dataContext.RECALLABLE === "Y") {
                btns.push("<span class='gridBtn gridBtnRecall' fn='R' title='Recall Order from ROCON'>&nbsp;</span>");
            }
            else {
                btns.push(IsApprover && dataContext.STATUS === "APPROVAL REQUESTED" ? "<span class='gridBtn gridBtnApprove' fn='A' title='Approve'>&nbsp;</span>" : "<span class='gridBtn'>&nbsp;</span>");
            }
            btns.push("<span class='gridBtn'>&nbsp;</span>");
            btns.push((IsUser && typeof allowDelete[dataContext.STATUS] !== "undefined" || (dataContext.STATUS.indexOf("EDIT") > 0 && dataContext.LAST_UPDATE_BY == UserInfo.UserID) ? "<span class='gridBtn gridBtnDelete' fn='C' title='Cancel'>&nbsp;</span>" : "<span class='gridBtn'>&nbsp;</span>"));
            return btns.join("");
        }

        function OrderFormatter(row, cell, value, columnDef, dataContext) {
            if (dataContext.SAVED_OLD == 1) {
                return "<div class='oldSave' title='Warning: order is in SAVED status for over " + savedThreshold + " days.'>" + value + "</div>";
            }
            else if (dataContext.REQ_AUTH_OLD == 1) {
                return "<div class='oldAuth' title='Warning: order is in APPROVAL REQUESTED status for over " + reqAuthThreshold + " days.'>" + value + "</div>";
            }
            return value;
        }

        function DateFormatter(row, cell, value, columnDef, dataContext) {
            return value.substring(4, 6) + "/" + value.substring(6, 8) + "/" + value.substring(0, 4);
        }

        function dateFilter(txtDate, ctrl) {
            var val = txtDate.replace(/ /g, ""),
                id = ctrl.id;
            val = (val.length > 0 ? parseInt((val.substring(6, 10) + val.substring(0, 2) + val.substring(3, 5)), 10) : null);
            if (id.indexOf("From") !== -1) {
                filters.dateFrom = val;
            }
            else {
                filters.dateTo = val;
            }
            DV.refresh();
        }

        function checkboxFilter(e) {
            filters.hideCancelled = $("#" + e.target.id).prop("checked");
            DV.refresh();
        }

        function txtFilter(e) {
            var val = $(e.target).val();
            switch (e.target.id) {
                case "txtOrder":
                    filters.co_odno = (val === "" || val === "-1" ? null : val);
                    break;
                case "txtStore":
                    filters.to_store = (val === "" || val === "-1" ? null : val.toUpperCase());
                    break;
                case "txtSubBy":
                    filters.subBy = (val === "" || val === "-1" ? null : val.toUpperCase());
                    break;
                case "txtAuthBy":
                    filters.authBy = (val === "" || val === "-1" ? null : val.toUpperCase());
                    break;
                case "ddlStatus":
                    filters.status = (val === "" ? null : val);
                    break;
                default:
                    break;
            };
            DV.refresh();
        }

        function myFilter(item, args) {
            if (args.co_odno && item["CO_ODNO"].indexOf(args.co_odno) === -1) {
                return false;
            }
            if (args.to_store && item["TO_STORE"].indexOf(args.to_store) === -1) {
                return false;
            }
            if (args.dateFrom && parseInt(item["ORDER_DT_TM"], 10) < args.dateFrom) {
                return false;
            }
            if (args.dateTo && parseInt(item["ORDER_DT_TM"], 10) > args.dateTo) {
                return false;
            }
            if (args.subBy && item["SUBMITTED_BY"].indexOf(args.subBy) === -1) {
                return false;
            }
            if (args.authBy && (!item["AUTHORIZED_BY"] || item["AUTHORIZED_BY"].indexOf(args.authBy) === -1)) {
                return false;
            }
            if (args.status && item["STATUS"] !== args.status) {
                return false;
            }
            if (item["STATUS"] === "CANCELLED") {
                return !args.hideCancelled;
            }

            return true;
        }

        function getISTList() {
            lcboWaitMsg.show();

            DV.beginUpdate();
            DV.setItems([]);
            DV.endUpdate();

            $.ajax({
                type: "POST",
                url: "ISTList.aspx/GetISTList",
                data: "{'co_odno' : ''}",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (result) { getISTListComplete(result, true); },
                error: function (result) { getISTListComplete(result, false); }
            });
        }

        function getISTListComplete(result, success) {
            //var orders = {};

            lcboWaitMsg.hide();
            
            if (success && result.IsSuccessful) {
                DV.beginUpdate();
                DV.setItems($.extend([], result.ReturnedObject));
                DV.endUpdate();
                /*
                for (var x = 0, l = result.ReturnedObject.length; x < l; x++) {
                    if (result.ReturnedObject[x].SAVED_OLD == 1) {
                        orders[result.ReturnedObject[x].id] = {};
                        orders[result.ReturnedObject[x].id].CO_ODNO = "oldSave";
                    }
                    else if (result.ReturnedObject[x].REQ_AUTH_OLD == 1) {
                        orders[result.ReturnedObject[x].id] = {};
                        orders[result.ReturnedObject[x].id].CO_ODNO = "oldAuth";
                    }
                }
                grid.setCellCssStyles("highlight", orders);
                grid.render();
                */
            }
        }

        function getIST(co_odno, id) {
            lcboWaitMsg.show();

            $.ajax({
                type: "POST",
                url: "ISTList.aspx/GetISTList",
                data: "{'co_odno' : '" + co_odno + "'}",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (result) { getISTComplete(result, true, id); },
                error: function (result) { getISTComplete(result, false); }
            });
        }

        function getISTComplete(result, success, id) {
            var line = null,
                newLine = null;

            lcboWaitMsg.hide();

            if (success) {
                if (result.IsSuccessful) {
                    if (result.ReturnedObject.length == 1) {
                        line = DV.getItemById(id);
                        newLine = result.ReturnedObject[0];
                        newLine.id = line.id;
                        DV.beginUpdate();
                        DV.updateItem(line.id, newLine);
                        DV.endUpdate();
                        return;
                    }
                }
            }
            //force reload of entire list
            getISTList();
        }

        function closeWin() {
            window.close();
        }

        function openReport() {
            if (typeof (reportWin) != "object" || reportWin.closed) {
                reportWin = window.open("ISTReport.aspx", "ISTReport", "width=1020px,height=670px,location=no,menubar=no,resizable=no,scrollbars=no,status=no,titlebar=no,toolbar=no");
            }

            reportWin.focus();
        }

        function openIST(co_odno, mode, id) {
            var sFeatures = "dialogWidth: 790px; dialogHeight: 650px; status: no; resizable: no; scroll: no;",
                result = null,
                json = null;

            result = window.showModalDialog("IST.aspx?mode=" + mode, { "co_odno": co_odno }, sFeatures);
            if (typeof result !== "undefined") {
                if (mode === "A") {
                    //user added a new IST, just reload entire list
                    getISTList();
                }
                else {
                    //user updated an item, just retrieve that item's info
                    json = $.parseJSON(result);
                    if (json.reloadList) {
                        getIST(json.co_odno, id);
                    }
                }
            }
        }

        function CancelIST(co_odno, id) {
            if (!confirm("Are you sure you want to permanently cancel IST " + $.trim(co_odno) + "?")) {
                return;
            }

            lcboWaitMsg.show();

            $.ajax({
                type: "POST",
                url: "ISTList.aspx/CancelIST",
                data: "{'co_odno':'" + co_odno + "', 'submittedBy' : '" + UserInfo.UserID + "'}",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (result) { CancelISTComplete(result, true); },
                error: function (result) { CancelISTComplete(result, false); }
            });
        }

        function CancelISTComplete(result, success) {
            lcboWaitMsg.hide();

            if (success) {
                if (result.IsSuccessful) {
                    alert("The IST has been cancelled.");
                    getISTList();
                }
                else {
                    alert("There was a problem cancelling the IST.\n\nERROR: " + result.ResponseCode + " - " + result.Details);
                }
                return;
            }
            alert("There was a problem cancelling the IST.");
        }

        function ISTApprove(co_odno, id) {
            if (!confirm("Are you sure you want to approve order: " + $.trim(co_odno) + "? Once approved, order will be sent to ROC.")) {
                return;
            }

            lcboWaitMsg.show();

            $.ajax({
                type: "POST",
                url: "ISTList.aspx/ISTApprove",
                data: "{'co_odno':'" + co_odno + "', 'submittedBy' : '" + UserInfo.UserID + "'}",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (result) { ISTApproveComplete(result, true); },
                error: function (result) { ISTApproveComplete(result, false); }
            });
        }

        function ISTApproveComplete(result, success) {
            lcboWaitMsg.hide();

            if (success) {
                if (result.IsSuccessful) {
                    alert("The IST has been approved.");
                    getISTList();
                }
                else {
                    alert("There was a problem approving the IST.\n\nERROR: " + result.ResponseCode + " - " + result.Details);
                }
                return;
            }
            alert("There was a problem approving the IST.");
        }

        function RecallIST(co_odno, id) {
            if (!confirm("Are you sure you want to recall IST " + $.trim(co_odno) + " for further editing?")) {
                return;
            }

            lcboWaitMsg.show();

            $.ajax({
                type: "POST",
                url: "ISTList.aspx/RecallIST",
                data: "{'co_odno':'" + co_odno + "', 'submittedBy' : '" + UserInfo.UserID + "'}",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (result) { RecallISTComplete(result, true); },
                error: function (result) { RecallISTComplete(result, false); }
            });
        }

        function RecallISTComplete(result, success) {
            lcboWaitMsg.hide();

            if (success) {
                if (result.IsSuccessful) {
                    alert("The IST has been recalled.");
                    getISTList();
                }
                else {
                    alert("There was a problem recalling the IST.\n\nERROR: " + result.ResponseCode + " - " + result.Details);
                    if (result.ResponseCode == 2001) {
                        getISTList();
                    }
                }
                return;
            }
            alert("There was a problem recalling the IST.");
        }

    </script>

</head>
<body>
    <form id="form1" runat="server">
    <div id="hdr"><span>941 IST Administration</span></div>
    <div class="main">
        <div class="content">
            <fieldset>
                <legend>Search</legend>
                <div style="float: left;">
                    <div><span class="lbl">Order #:</span><input type="text" id="txtOrder" class="txtFilter" style="width: 70px;" maxlength="7" /></div>
                    <div><span class="lbl">Store #:</span><input type="text" id="txtStore" class="txtFilter" style="width: 70px;" maxlength="7" /></div>
                    <div><span class="lbl">Submitted By:</span><input type="text" id="txtSubBy" class="txtFilter" style="width: 70px;" maxlength="7" /></div>
                    <div style="display: none;"><span class="lbl">Authorized By:</span><input type="text" id="txtAuthBy" class="txtFilter" /></div>
                </div>
                <div style="float: left; margin-left:30px;">
                    <div><span class="lbl">Order Date From:</span><input type="text" id="txtDateFrom" style="width: 80px;" /></div>
                    <div><span class="lbl">Order Date To: </span><input type="text" id="txtDateTo" style="width: 80px;" /></div>
                    <div><span class="lbl">Status:</span><asp:DropDownList ID="ddlStatus" runat="server"></asp:DropDownList><asp:CheckBox ID="chkExcludeCancelled" runat="server" Text="Exclude Cancelled Orders" Checked="true" /></div>
                </div>
            </fieldset>
            
            <div style="margin-top: 10px;">
                <div class="gridCount"><span># of ISTs:&nbsp;</span><span id="ISTCount">0</span></div>
                <div id="ISTList" class="grids tallHeader1" style="height: 350px; width: 100%;"></div>
            </div>
            <div style="margin-top: 20px; text-align: center;">
                <input type="button" id="btnReport" style="margin-right: 10px;" value="View Report" />
                <input type="button" id="btnAddNew" style="margin-right: 10px;" value="Create IST" />
                <input type="button" id="btnClose" value="Close" />
            </div>
        </div>
    </div>
    </form>
</body>
</html>
